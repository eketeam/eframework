﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="eFrameWork.Application.Default" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%= appTitle %></title>
    <META HTTP-EQUIV="imagetoolbar" CONTENT="NO">
</head>
<style>
html,body{margin:0px;padding:0px;width:100%;height:100%;}
    .appbox
    {
        border:0px solid #cccccc;
    }
    .appbox a
    {
        text-decoration:none;
        display:inline-block;
        border:1px solid #cccccc;
        width:120px;
        height:100px;
        white-space:normal;
        margin-left:10px;
        margin-top:10px;
    }
    .appbox span
    { 
        display: inline-block;height: 100%;vertical-align: middle;
    }
    .appbox p
    {
        vertical-align: middle;display: inline-block;*display: inline;text-align: center;
        width:100px;
        line-height:22px;
        color:#0099FF;
        margin-left:10px; background-color:#ffffff;
    }
	
.eapp {border:0px solid #000; width:670px; margin:auto;padding-top:15px;}
.eapp dl
{
border:1px solid #ccc;width:200px;float:left;dis3play:inline-block;
border-radius: 6px;-webkit-border-radius: 6px;-moz-border-radius: 6px;-o-border-radius: 6px;
-webkit-box-shadow: 0px 1px 3px rgb(190, 190, 190);-moz-box-shadow: 0px 1px 3px rgb(190, 190, 190);-o-box-shadow: 0px 1px 3px rgb(190, 190, 190);box-shadow: 0px 1px 3px rgb(190, 190, 190);
margin:0px 0px 15px 15px;
}
.eapp a{display:inline-block;color:#666; text-decoration:none;font-size:13px;}
.eapp dl dt{border:0px solid #ff0000;margin:6px;height:120px;overflow:hidden;}
.eapp dl dt img{border:0px;display:inline-block;width:188px;height:120px;}
.eapp dl dd{margin:6px;border:0px solid #00cc00;height:20px;line-height:20px;overflow:hidden; text-align:center;white-space: nowrap;-o-text-overflow:ellipsis;text-overflow:ellipsis;}

</style>
<body>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle">
<div class="eapp">
<asp:Repeater id="Rep" runat="server">
<itemtemplate>
<dl>
<dt><a href="<%#Eval("href").ToString() %>" title="<%#Eval("MC")%>"><img src="<%#Eval("Pic").ToString().Length > 10 ? Eval("Pic").ToString() : "../images/none.gif" %>"></a></dt>
<dd><a href="<%#Eval("href").ToString() %>" title="<%#Eval("MC")%>"><%#Eval("MC")%></a></dd>
</dl>
</itemtemplate>
</asp:Repeater>
<div style="clear:both;font-size:0px;height:0px;"></div>
</div>
</td>
  </tr>
</table>


<asp:Repeater id="Rep_Bak" runat="server">
<headertemplate><div class="appbox"></headertemplate>
<itemtemplate>
 <a href="<%#appmenu.getTopModelUrl(Eval("ApplicationID").ToString()) %>"><span></span><p><%#Eval("MC")%></p></a>
</itemtemplate>
<footertemplate></div></footertemplate>
</asp:Repeater>
</body>
</html>