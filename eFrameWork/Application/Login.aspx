﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="eFrameWork.Application.Login" %><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%= eConfig.ApplicationTitle("") %></title>
    <META HTTP-EQUIV="imagetoolbar" CONTENT="NO">
</head>
<link href="../Plugins/eLogin/standard/style.css" rel="stylesheet" type="text/css" /> 
<script src="../Scripts/jquery.js"></script>
<script src="../Scripts/eketeam.js?ver=<%=Common.Version %>"></script>
<script src="../Plugins/layui226/layui.all.js"></script>
<script>
    function getrnd(type)
    {
        if (type == 0 && document.getElementById("rndpic").src.indexOf("none.gif") == -1) {
            return;
        }
	document.getElementById("rndpic").src = "../Plugins/RndPic.aspx?type=gif&bgcolor=ffffff&color=1B6CD1&t=" + now();
};
function WxWorkLogin()
{
	var fromURL="<%=HttpUtility.UrlEncode(wxworkFromURL)%>";
    var url = "<%= (eBase.WXWorkAccount.getValue("Proxy").Length== 0 ? eBase.getAbsolutePath() : eBase.WXWorkAccount.getValue("Proxy") ) + "Plugins/Tencent/WxWork/WxWorkScanLogin.aspx?mode=getimage"%>";
	if(fromURL.length>0){url+="&fromURL=" + fromURL;}
    layer.open({
      type: 2,
      title: "企业微信扫一扫登录",
      maxmin: false,
      shadeClose: true, //点击遮罩关闭层 
      area : ["330px" , "450px"],	
	 //  content: ['WxWorkCode.aspx','no'], 
        //content: "WxWorkCode.aspx",
      content: [url, 'no'],
	  success: function(layero, index){},
	  cancel: function(index, layero){},
	  end : function(index){}
    });	
};
function WeChatLogin()
{
    var fromURL = "<%=HttpUtility.UrlEncode(wechatFromURL)%>";
    var url = "<%=(eBase.WeChatAccount.getValue("Proxy").Length== 0 ? eBase.getAbsolutePath() : eBase.WeChatAccount.getValue("Proxy") ) + "Plugins/Tencent/WeChat/WeChatScanLogin.aspx?mode=getimage"%>";
    if (fromURL.length > 0) { url += "&fromURL=" + fromURL; }
    layer.open({
        type: 2,
        title: "微信扫一扫登录",
        maxmin: false,
        shadeClose: true, //点击遮罩关闭层 
        area: ["330px", "450px"],
        //content: ['WeChatCode.aspx', 'no'],
        //content: "WeChatCode.aspx",
        content: [url, 'no'],
        success: function (layero, index) { },
        cancel: function (index, layero) { },
        end: function (index) { }
    });
};
function DingTalkLogin()
{
	var fromURL = "<%=HttpUtility.UrlEncode(dingtalkFromURL)%>";
	var url = "<%=(eBase.DingTalkAccount.getValue("Proxy").Length== 0 ? eBase.getAbsolutePath() : eBase.DingTalkAccount.getValue("Proxy") ) + "Plugins/aliyun/DingTalk/DingTalkScanLogin.aspx?mode=getimage"%>";
    if (fromURL.length > 0) { url += "&fromURL=" + fromURL; }
	 layer.open({
        type: 2,
        title: "钉钉扫一扫登录",
        maxmin: false,
        shadeClose: true, //点击遮罩关闭层 
        area: ["330px", "450px"],
        content: [url, 'no'],
        success: function (layero, index) { },
        cancel: function (index, layero) { },
        end: function (index) { }
    });
};
</script>
<body>
<form id="form1" name="form1" method="post" action="">
<input name="siteid" type="hidden" id="siteid" value="" />
<div class="logincontainer">
	<div class="loginbox">
		<div class="left" >
			<h1><%= eConfig.ApplicationTitle("") %></h1>
			<p><%= eConfig.ApplicationProfiles("") %></p>
			<img src="loginbg.png" style="left:-10px;bottom:-100px;width:600px;displ3ay:none;">

		</div>
		<div class="right">
			<div class="body">
				<center style="margin-top:60px;">用户登录</center>
				<div class="rowitem"><input type="text" name="user"  id="user" class="user" notnull="true" fieldname="登陆帐号" autocomplete="off" placeholder="登陆帐号"></div>
				<div class="rowitem"><input type="password" name="pass"  id="pass" class="pass" notnull="true" fieldname="登陆密码" autocomplete="off" placeholder="登陆密码"></div>
				 <%if (eConfig.openRndCode()){ %>
                <div class="rowitem"><input type="text" name="code"  id="code" class="code" notnull="true" fieldname="验证码" style="width:160px;" maxlength="4" autocomplete="off" onFocus="getrnd(0);" placeholder="验证码"><img id="rndpic" style="cursor:pointer;" onClick="getrnd();" height="35" class="code" align="absMiddle" src="../images/none.gif"></div>
				<%} %>
				<div class="rowitem" style="display:none;">
                    <span style="display:none;">
                    <input type="checkbox" id="keepstate" name="keepstate" value="true"><label for="keepstate">保持登录</label>
                    </span>
                    <span style="float:right;">
                    <a href="javascript:;" style="display:none;">忘记密码?</a><a href="Register.aspx">注册帐号</a>
                    </span>
                    <div class="clear"></div>
				</div>
				<!--
				<a class="btn" onclick="if(form1.onsubmit()!=false){form1.submit();}">登录</a>
				-->
				
				<input type="submit" name="Submit" class="btn" value="登录" />
				<div class="horizontal">
				<span>其他登录方式</span>
				</div>
				<div class="tool">
				<a href="javascript:;" class="wechat<%=wechat?"":" gray"%>" onClick="<%=wechat?"WeChatLogin();":"layer.msg('未正确绑定公众号!');"%>"></a> 
				<!--<a href="javascript:;" class="qq gray" onclick="layer.msg('暂不支持!');"></a> -->
				<a href="javascript:;" class="wxwork<%=wxwork?"":" gray"%>" onClick="<%=wxwork?"WxWorkLogin();":"layer.msg('未正确绑定企业微信!');"%>"></a> 
				<a href="javascript:;" class="dingtalk<%=dingtalk?"":" gray"%>" onClick="<%=dingtalk?"DingTalkLogin();":"layer.msg('未正确绑定钉钉应用!');"%>"></a> 
				</div>
			</div>
		</div>
	</div>
</div>
  </form>
</body>
</html>