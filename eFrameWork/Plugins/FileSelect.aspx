﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileSelect.aspx.cs" Inherits="eFrameWork.Plugins.FileSelect" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>文件选择</title>
</head>
<link href="../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
<link href="../Plugins/Theme/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
<script src="../Scripts/jquery.js"></script>
<script src="../Scripts/eketeam.js?ver=<%=Common.Version %>"></script>
<style>
    html, body {
    margin:0px;padding:0px;font-size:13px;
    }
body {overflow:auto;}
</style>
<body>
<asp:Literal id="litBody" runat="server" />
</body>
</html>