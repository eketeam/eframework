﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DepartmentSelect.aspx.cs" Inherits="eFrameWork.Plugins.DepartmentSelect" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>用户选择</title>
</head>
<link href="../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
<link href="../Plugins/Theme/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
<script src="../Scripts/jquery.js"></script>
<script src="../Scripts/eketeam.js?ver=<%=Common.Version %>"></script>
<style>
    html, body {
    margin:0px;padding:0px;font-size:13px;
    }
body {overflow:auto;}
</style>
<script>
var objid = "<%=obj%>";
$(document).ready(function () {
	var tree = new eTree("etree");	
});
function setDepartment(id, name)
{
    var box = $(parent.UserSelectParent).find("#" + objid + "_box");
    var ovalue = $(parent.UserSelectParent).find("[postname='" + objid + "']").val();
    if (ovalue == id)//删除
    {
        $(parent.UserSelectParent).find("[postname='" + objid + "']").val("");
        $(parent.UserSelectParent).find("[postname='" + objid + "_text']").val("");
        box.find(".useritem").remove();
    }
    else {
        $(parent.UserSelectParent).find("[postname='" + objid + "']").val(id);
        $(parent.UserSelectParent).find("[postname='" + objid + "_text']").val(name);
        if (box.find(".useritem").length == 1) {
            box.find(".useritem p").html(name);
        }
        else {
            var html = '<a class="useritem" href="javascript:;">';
            html += '<p>' + name + '</p><img src="../images/none.gif" onclick="multipleuser_delete(this);"></a>';
            box.find(".useritem_open").before(html);
        }
        parent.layer.close(parent.arrLayerIndex.pop());
    }
};
   
</script>
<body>
<div style="margin:10px;">
<asp:Literal id="litBody" runat="server" />
</div>
</body>
</html>