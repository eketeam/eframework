﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.Plugins
{
    public partial class UserSelect : System.Web.UI.Page
    {        
        public string obj = eParameters.QueryString("obj");
        protected void Page_Load(object sender, EventArgs e)
        {
            litBody.Text = getTree("");
        }
        private string getTree(string ParentID)
        {
            string siteid = eBase.getSiteID();// eConfig.SiteID();
            StringBuilder sb = new StringBuilder();



            string sql = "select OrganizationalID,ParentID,MC,Code from Organizationals where DelTag=0";
            sql += (ParentID.Length == 0 ? " and ParentID IS NULL" : " and ParentID='" + ParentID + "'");
            sql += " and " + (siteid.Length > 0 ? "(SiteID=0 or SiteID='" + siteid + "')" : "SiteID=0");
            sql += " Order by PX,addTime";
            DataTable tb = eBase.DataBase.getDataTable(sql);
            //eBase.PrintDataTable(tb);

            if (ParentID.Length == 0)
            {
                sb.Append("<ul id=\"etree\" class=\"etree\" PID=\"NULL\">\r\n");
            }
            else
            {
                sb.Append("<ul PID=\"" + ParentID + "\">\r\n");
            }
            foreach (DataRow dr in tb.Rows)
            {
                #region 循环部门
                string ct = eBase.DataBase.getValue("select count(*) from  Organizationals where SiteID='" + siteid + "' and DelTag=0 and ParentID='" + dr["OrganizationalID"].ToString() + "'");
                sql = "select count(1) from a_eke_sysUsers where code like '" + dr["code"].ToString() + "%' and len(xm)>0 and Active=1 and deltag=0";
                string userct = eBase.UserInfoDB.getValue(sql);
                if (ct == "0" && userct == "0") continue;


                sb.Append("<li oncont3extmenu=\"return false;\" dataid=\"" + dr["OrganizationalID"].ToString() + "\" ");
                sb.Append(" class=\"\">");
                sb.Append("<div oncont3extmenu=\"return false;\">");
                sb.Append("<a href=\"javascript:;\" onclick=\"setUsers(this)\" oncontextmenu=\"return false;\">");
                sb.Append( dr["MC"].ToString() );
                sb.Append( "</a>"); // (" + ct + ")
                sb.Append("</div>");
                if (userct != "0")
                { 
                    //部门
                    sql = "select UserID,XM from a_eke_sysUsers where code='" + dr["code"].ToString() + "' and len(xm)>0 and Active=1 and deltag=0";
                    DataTable dt = eBase.UserInfoDB.getDataTable(sql);
                    if (dt.Rows.Count > 0)
                    {
                        sb.Append("<ul PID=\"" + dr["OrganizationalID"].ToString() + "\">\r\n");
                        foreach (DataRow _dr in dt.Rows)
                        {
                            sb.Append("<li userid=\"" + _dr["UserID"].ToString() + "\" name=\"" + _dr["xm"].ToString() + "\" oncont3extmenu=\"return false;\" dataid=\"\"");
                            sb.Append(" class=\"user" + (_dr["xm"].ToString().IndexOf("DD") > -1 ? " userselected" : "") + "\" onclick=\"setUser('" + _dr["UserID"].ToString() + "','" + _dr["XM"].ToString() + "');\">");
                            sb.Append("<div oncont3extmenu=\"return false;\">");
                            //sb.Append("<a href=\"javascript:;\" oncontextmenu=\"return false;\">");
                            sb.Append( _dr["xm"].ToString());
                            //sb.Append("</a>"); // (" + ct + ")
                            sb.Append("</div>");
                            sb.Append("</li>\r\n");
                        }
                        sb.Append("</ul>\r\n");
                    }
                }
                if (ct != "0") sb.Append(getTree(dr["OrganizationalID"].ToString()));
                sb.Append("</li>\r\n");
                #endregion
            }
            if (ParentID.Length == 0)
            {
              
                sql = "select UserID,XM from a_eke_sysUsers where code is null and len(xm)>0 and Active=1 and deltag=0";
                DataTable dt = eBase.UserInfoDB.getDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    sb.Append("<li oncont3extmenu=\"return false;\" dataid=\"0\" ");
                    sb.Append(" class=\"\">");
                    sb.Append("<div oncont3extmenu=\"return false;\">");
                    sb.Append("<a href=\"javascript:;\" onclick=\"setUsers(this)\" oncontextmenu=\"return false;\">");
                    //sb.Append("未设置部门");
                    sb.Append("部门不详");
                    sb.Append("</a>"); // (" + ct + ")
                    sb.Append("</div>");
                    if (dt.Rows.Count > 0)
                    {
                        sb.Append("<ul PID=\"0\">\r\n");
                        foreach (DataRow _dr in dt.Rows)
                        {
                            sb.Append("<li userid=\"" + _dr["UserID"].ToString() + "\" name=\"" + _dr["xm"].ToString() + "\" oncont3extmenu=\"return false;\" dataid=\"\"");
                            sb.Append(" class=\"user" + (_dr["xm"].ToString().IndexOf("DD") > -1 ? " userselected" : "") + "\" onclick=\"setUser('" + _dr["UserID"].ToString() + "','" + _dr["XM"].ToString() + "');\">");
                            sb.Append("<div oncont3extmenu=\"return false;\">");
                            //sb.Append("<a href=\"javascript:;\" oncontextmenu=\"return false;\">");
                            sb.Append(_dr["xm"].ToString());
                            //sb.Append("</a>"); // (" + ct + ")
                            sb.Append("</div>");
                            sb.Append("</li>\r\n");
                        }
                        sb.Append("</ul>\r\n");
                    }
                    sb.Append("</li>\r\n");
                }
            }
            sb.Append("</ul>\r\n");
            return sb.ToString();
        }
    }
}