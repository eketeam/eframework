﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="scanQRCode.aspx.cs" Inherits="Plugins_Tencent_WxWork_scanQRCode" %>
<!DOCTYPE html>
<html>
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" /> 
    <title>条码扫描</title>
     <script src="<%=eBase.getAbsolutePath() %>Scripts/jquery.js"></script>
     <script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
</head>
<script>
    $(document).ready(function () {
        wx.config({
            debug: false,
            beta: true,
            appId: '<%=shareData.appid%>',
            timestamp: <%=shareData.timestamp%>,
             nonceStr: '<%=shareData.noncestr%>',
             signature: '<%=shareData.signature%>',
            jsApiList: ['scanQRCode'],
            complete:function(ret){
                //alert("wx.config.complete:" + JSON.stringify(ret))
            }
        });
        wx.error(function (res) {
            //alert("出错了：" + res.errMsg);//这个地方的好处就是wx.config配置错误，会弹出窗口哪里错误，然后根据微信文档查询即可。
            alert(JSON.stringify(res));
        });

    wx.ready(function () 
    {
        if(typeof(parent.<%=callback%>)=="function")
        {
            wx.scanQRCode({needResult:1,desc:"scanQRCode desc",
                success:function(res){
                    var obj="<%=obj%>";
                    if(obj.length>0)
                    {
                        $(parent.document).find("#" + obj).val(res.resultStr);
                    }
                    //alert(res.resultStr);//alert(JSON.stringify(res));
                    parent.<%=callback%>(res.resultStr);
                },
                error: function(res)
                {
                    if (res.errMsg.indexOf('function_not_exist') > 0) 
                    {
                        alert("企业微信版本过低!");
                    }
                    else
                    {
                        alert("wx.scanQRCode.error:" + JSON.stringify(res));
                    }
                }
            });
        }
    });
    wx.error(function(res){
        //config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
        //var result = res.resultStr// eslint-disable-line no-unused-vars
        alert("wx.error:" + JSON.stringify(res));
    });
    });
</script>
<body>

</body>
</html>
