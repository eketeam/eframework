﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;
using EKETEAM.Tencent.WxWork;

public partial class Plugins_Tencent_WxWork_getShareData : System.Web.UI.Page
{
    public string UserArea = "Application";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["area"] != null) UserArea = Request.QueryString["area"].ToString();
        eUser user = new eUser(UserArea);
        JsonData json = new JsonData();
        json["success"] = "1";
        json["errcode"] = "1";
        json["message"] = "请求成功!";
        if (user.Logined)
        {
            ShareData shareData = new ShareData();
            shareData.title = "标题";
            shareData.desc = "描述";
            shareData.link = Request.UrlReferrer != null ? Request.UrlReferrer.AbsoluteUri : Request.Url.AbsoluteUri;
            //shareData.imgurl = "ht tp://test.ynzfwh.com/WeChat/images/touch-icon.png";
            WxWorkHelper.getShareData(shareData);
            JsonData data = JsonMapper.ToObject("[]");
            JsonData item = new JsonData();

            item["appid"] = shareData.appid;
            item["timestamp"] = shareData.timestamp;
            item["noncestr"] = shareData.noncestr;
            item["signature"] = shareData.signature;
            data.Add(item);
            json["data"] = data;
        }
        else
        {
            JsonData data = JsonMapper.ToObject("[]");
            json["data"] = data;
        }      
        //eResult.WriteJson(json);
        eBase.WriteJson(json.ToJson());

    }
}