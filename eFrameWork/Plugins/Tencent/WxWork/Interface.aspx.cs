﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using EKETEAM.FrameWork;
using EKETEAM.Tencent.WxWork;
using LitJson;

public partial class Plugins_Tencent_WxWork_Interface : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //WXWork/Message
        //eBase.AppendLog(postjson + "::" + (Request.UrlReferrer==null ? "null" : Request.UrlReferrer.AbsoluteUri ));
        //eBase.AppendLog(postjson + "::" + (Request.UrlReferrer == null ? "null" : Request.UrlReferrer.Host));
        if (Request.UrlReferrer == null) return;
        string host = Request.UrlReferrer.Host;
        if (eBase.WXWorkAccount.getValue("ReverseProxy").ToLower().IndexOf(host) == -1) return;

        //StreamReader sr = new StreamReader(HttpContext.Current.Request.InputStream, Encoding.UTF8);
        //String postjson = sr.ReadToEnd();
        //sr.Close();
        //sr.Dispose();
       

        string postjson = eBase.getPostJsonString();
        //eBase.AppendLog(postjson);

        string result = "error";
        if (postjson.StartsWith("{") && postjson.EndsWith("}"))
        {
            JsonData jd = JsonMapper.ToObject(postjson);
            string method = jd.getValue("method");
            //eBase.AppendLog(method);
            switch (method)
            { 
                case "sendText":
                    result = WxWorkHelper.sendText(jd["qyuserid"].ToString(), jd["content"].ToString()).ToString();
                    break;
                case "sendArticles":
                    result = WxWorkHelper.sendArticles(jd["qyuserid"].ToString(), jd["title"].ToString(), jd["description"].ToString(), jd["url"].ToString(), jd["picurl"].ToString()).ToString();
                    break;
                case "sendTextCard":
                    result = WxWorkHelper.sendTextCard(jd["qyuserid"].ToString(), jd["title"].ToString(), jd["description"].ToString(), jd["url"].ToString(), jd["btntxt"].ToString()).ToString();
                    break;
            }            
        }
        Response.Write(result);
        Response.End();
    }
}