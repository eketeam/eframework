﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BindWxWorkCode.aspx.cs" Inherits="eFrameWork.Plugins.BindWxWorkCode" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>企业微信扫码绑定</title>
      <script src="http://rescdn.qqmail.com/node/ww/wwopenmng/js/sso/wwLogin-1.0.0.js"></script>
     <script src="<%=eBase.getAbsolutePath() %>Scripts/jquery.js"></script>
</head>
 <style>
.impowerBox .qrcode {width: 200px;}
.impowerBox .title {display: none;}
.impowerBox .info {width: 200px;}
.status_icon {display: none  !important}
.impowerBox .status {text-align: center;}

</style>
<body>
     <div id="login_container"></div>
</body>
</html>
<script>

    $(function () {
        //alert(window.WwLogin);
        window.WwLogin({
        "id": "login_container",
        "appid": "<%=eBase.WXWorkAccount.getValue("CorpID")%>",
        "agentid": "<%=eBase.WXWorkAccount.getValue("AgentId")%>",
        "redirect_uri": "<%=Request.Url.PathAndFile().UrlEncode()%>",
        "state": "",
        "href": "data:text/css;base64,LmltcG93ZXJCb3ggLnRpdGxle2Rpc3BsYXk6bm9uZTt9",
    });
});
</script>