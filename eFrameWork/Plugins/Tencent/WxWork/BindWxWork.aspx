﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BindWxWork.aspx.cs" Inherits="eFrameWork.Plugins.BindWxWork" %>
<script>
    var WxWork_Index = 0;
    function BindWxWork() {
        //var url = "eBase.getAbsolutePath() Customs/BindWxWorkCode.aspx?mode=getimage";
        //var url = "< %=eRequest.PathAndQuery().Path() + "BindWxWorkCode.aspx?mode=getimage" %>";
        var url = "<%=(eBase.WXWorkAccount.getValue("Proxy").Length== 0 ? eRequest.PathAndQuery().Path() +  "BindWxWorkCode.aspx?mode=getimage" :  eBase.WXWorkAccount.getValue("Proxy") +  "Plugins/Tencent/WxWork/WxWorkScanLogin.aspx?mode=getimage&fromURL="  + HttpUtility.UrlEncode(Request.Url.Scheme + "://" + Request.Url.Host + (Request.Url.Port==80 ||Request.Url.Port==443 ? "" : Request.Url.Port.ToString() ) + Request.CurrentExecutionFilePath)) %>";
    WxWork_Index = layer.open({
        type: 2,
        title: "企业微信扫一扫绑定",
        maxmin: false,
        shadeClose: true, //点击遮罩关闭层 
        area: ["330px", "450px"],
        //content: ['WxWorkCode.aspx','no'], 
        //content: "WxWorkCode.aspx",
        content: [url, 'no'],
        success: function (layero, index) { },
        cancel: function (index, layero) { },
        end: function (index) { }
    });   
    };
    function CloseWxWork() {
        layer.close(WxWork_Index);
        $('#bindWxWork').html('已绑定!');
    };
</script>
<div id="bindWxWork">
<asp:Literal id="litBody" runat="server" />
</div>