﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BindWeChatCode.aspx.cs" Inherits="eFrameWork.Plugins.BindWeChatCode" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>微信扫码绑定</title>
     <script src="http://res.wx.qq.com/connect/zh_CN/htmledition/js/wxLogin.js"></script>
     <script src="<%=eBase.getAbsolutePath() %>Scripts/jquery.js"></script>
</head>
 <style>
.impowerBox .qrcode {width: 200px;}
.impowerBox .title {display: none;}
.impowerBox .info {width: 200px;}
.status_icon {display: none  !important}
.impowerBox .status {text-align: center;}

</style>
<body>
     <div id="login_container"></div>
</body>
</html>
<script>
$(function () {
        //开放平台申请 网站应用才可以扫码登录，公众号不支持。
        var obj = new WxLogin({
        "self_redirect": true,
        "id": "login_container",//div的id
        "appid": "<%=eBase.WeChatAccount.getValue("OpenAppID") %>",
        "scope": "snsapi_login",
        "redirect_uri": "<%=Request.Url.PathAndFile().UrlEncode()%>",//回调地址
        "state": "", //参数，可带可不带
        "style": "",//样式  提供"black"、"white"可选，默认为黑色文字描述
        "href": "data:text/css;base64,LmxvZ2luUGFuZWwgLnRpdGxle2Rpc3BsYXk6bm9uZTt9" //自定义样式链接，第三方可根据实际需求覆盖默认样式。 
    });
});
</script>