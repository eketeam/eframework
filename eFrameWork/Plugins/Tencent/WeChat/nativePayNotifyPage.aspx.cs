﻿
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using EKETEAM.Tencent.WeChat;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.Plugins
{
    public partial class nativePayNotifyPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NativeNotify nativeNatify = new NativeNotify(this);
            #region 返回数据
            try
            {

                WxPayData notifyData = nativeNatify.GetNotifyData();

                string transaction_id = notifyData.GetValue("transaction_id").ToString();
                string out_trade_no = notifyData.GetValue("out_trade_no").ToString();
                string attach = notifyData.GetValue("attach").ToString();
                string[] attachs = attach.Split(",".ToCharArray());

                //eBase.WriteLog(nativeNatify.WxPayDataString);
                //eBase.WriteLog(transaction_id);


                string tmp = eBase.DataBase.getValue("select count(*) from eWeb_PayLog where transaction_id='" + transaction_id + "'");
                if (tmp == "0")
                {
                    eTable etab = new eTable("eWeb_PayLog");
                    if (attachs.Length > 0) etab.Fields.Add("UserID", attachs[0]);
                    if (attachs.Length > 1) etab.Fields.Add("OrderID", attachs[1]);
                    //if (attachs.Length > 2) etab.Fields.Add("ProductID", attachs[2]);
                    etab.Fields.Add("transaction_id", transaction_id);
                    etab.Fields.Add("out_trade_no", out_trade_no);
                    etab.Fields.Add("trade_type", notifyData.GetValue("trade_type").ToString());


                    double money = Convert.ToDouble(notifyData.GetValue("total_fee").ToString()) / 100;
                    etab.Fields.Add("money", money.ToString());
                    etab.Fields.Add("Result", nativeNatify.WxPayDataString);


                    //<xml><appid><![CDATA[wx85b0bf2351900f84]]></appid> <attach><![CDATA[f9ca0e08-0ce6-433e-8c0a-b08b56c86e4b,37215b67-d6e8-4095-a43d-d3c6244620ab,c26a00d2-a09f-47d7-962f-6ab891fa4aa0]]></attach> <bank_type><![CDATA[OTHERS]]></bank_type> <cash_fee><![CDATA[1]]></cash_fee> <fee_type><![CDATA[CNY]]></fee_type> <is_subscribe><![CDATA[Y]]></is_subscribe> <mch_id><![CDATA[1513756691]]></mch_id> <nonce_str><![CDATA[3852978918]]></nonce_str> <openid><![CDATA[ozfMB1AkkcEMEXVav680Y_lOhp80]]></openid> <out_trade_no><![CDATA[151375669120200315112500808]]></out_trade_no> <result_code><![CDATA[SUCCESS]]></result_code> <return_code><![CDATA[SUCCESS]]></return_code> <sign><![CDATA[5B226F1FCBF5A279D15A5BCB4D34777BBFB7EF1709EB63A606FE2BDD204ADD04]]></sign> <time_end><![CDATA[20200315112524]]></time_end> <total_fee>1</total_fee> <trade_type><![CDATA[NATIVE]]></trade_type> <transaction_id><![CDATA[4200000477202003152825871887]]></transaction_id> </xml>
                    etab.Add();
                }



                //此时还没提交订单
                //if (attachs.Length > 1)  eOleDB.Execute("update eWeb_Orders set transaction_id='" + transaction_id + "' where OrderID='" + attachs[1] + "'");
            }
            catch
            {
            }
            #endregion

            nativeNatify.ProcessNotify();
        }
    }
}