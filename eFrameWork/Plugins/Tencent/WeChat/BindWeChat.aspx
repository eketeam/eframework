﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BindWeChat.aspx.cs" Inherits="eFrameWork.Plugins.BindWeChat" %>
<script>
    var WeChat_Index = 0;
    function BindWeChat(){
        //var url = "eBase.getAbsolutePath Plugins/Tencent/WeChat/BindWeChatCode.aspx?mode=getimage";
        //var url = "< %=eRequest.PathAndQuery().Path() + "BindWeChatCode.aspx?mode=getimage" %>";
        var url = "<%=(eBase.WeChatAccount.getValue("Proxy").Length== 0 ? eRequest.PathAndQuery().Path() + "BindWeChatCode.aspx?mode=getimage" :  eBase.WeChatAccount.getValue("Proxy") + "Plugins/Tencent/WeChat/WeChatScanLogin.aspx?mode=getimage&fromURL=" + HttpUtility.UrlEncode(Request.Url.Scheme + "://" + Request.Url.Host + (Request.Url.Port==80 ||Request.Url.Port==443 ? "" : Request.Url.Port.ToString() ) + Request.CurrentExecutionFilePath) ) %>";
    WeChat_Index=layer.open({
        type: 2,
        title: "微信扫一扫绑定",
        maxmin: false,
        shadeClose: true, //点击遮罩关闭层 
        area: ["330px", "450px"],
        //content: ['WeChatCode.aspx', 'no'],
        //content: "WeChatCode.aspx",
        content: [url, 'no'],
        success: function (layero, index) { },
        cancel: function (index, layero) { },
        end: function (index) { }
    });
   
    };
    function CloseWeChat () {
        layer.close(WeChat_Index);
        $('#bindWeChat').html('已绑定!');
    };
</script>
<div id="bindWeChat">    
<asp:Literal id="litBody" runat="server" />
</div>