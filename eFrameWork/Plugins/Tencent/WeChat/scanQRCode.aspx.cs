﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Tencent.WeChat;
using EKETEAM.FrameWork;


namespace eFrameWork.WeChat
{
    public partial class scanQRCode : System.Web.UI.Page
    {
        public ShareData shareData = new ShareData();
        protected void Page_Load(object sender, EventArgs e)
        {
            shareData.title = "标题一";
            shareData.desc = "描述二";
            shareData.link = Request.Url.AbsoluteUri;
            shareData.imgurl = "http://test.ynzfwh.com/WeChat/images/touch-icon.png";
            // WeChatHelper.getShareDataProxy(shareData);//代理不成功
            WeChatHelper.getShareData(shareData);

        }

    }
}