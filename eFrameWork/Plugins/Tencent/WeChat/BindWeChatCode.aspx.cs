﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using EKETEAM.Tencent.WeChat;
using LitJson;

namespace eFrameWork.Plugins
{
    public partial class BindWeChatCode : System.Web.UI.Page
    {
        public string UserArea = "Application";
        eUser user;
        string redirectUrl = "";
        //开放平台申请 网站应用才可以扫码登录，公众号不支持。
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["mode"] != null) return;
            user = new eUser(UserArea);
            redirectUrl = HttpUtility.UrlEncode(Request.Url.Path() + Request.Url.File());
            string code = Request["code"];
            if (string.IsNullOrEmpty(code))
            {
                OpenAccess();
            }
            JsonData json = WeChatHelper.getOpenAccessToken(code);
            string accesstoken = json.GetValue("access_token");
            string openid = json.GetValue("openid");
            string unionid = json.GetValue("unionid");
            if (openid.Length == 0 && unionid.Length == 0)
            {
                Response.Write("授权失败!");
                Response.End();
            }

            JsonData jd = WeChatHelper.getUserInfo(accesstoken, openid);

            eTable etb = new eTable("a_eke_sysUsers", user);
            etb.DataBase = eBase.UserInfoDB;
            etb.Fields.Add("openid", openid);
            if (unionid.Length > 0) etb.Fields.Add("unionid", unionid);
            etb.Fields.Add("nickname", jd.GetValue("nickname"));
            etb.Fields.Add("sex", jd.GetValue("sex"));

            string headimgurl = jd.GetValue("headimgurl").Replace("\\", "");
            string face = WeChatHelper.downHeadImage(user.ID, headimgurl);

            etb.Fields.Add("headimgurl", headimgurl);
            etb.Fields.Add("face", face);
            etb.Fields.Add("country", jd.GetValue("country"));
            etb.Fields.Add("province", jd.GetValue("province"));
            etb.Fields.Add("city", jd.GetValue("city"));
            etb.Where.Add("UserID='" + user.ID + "'");
            etb.Update();
            //Response.Write("<script>top.CloseWeChat();</script>");//AppFrame无效
            Response.Write("<script>if(top.frames.length==1){top.CloseWeChat();}else{parent.parent.CloseWeChat();}</script>");
            Response.End();
        }
        private void getCode()
        {
 
        }
        private void OpenAccess()
        {
            //开放平台申请 网站应用才可以扫码登录，公众号不支持。
            if (!user.Logined)
            {
                //认证第一步：重定向跳转至认证网址
                string url = "https://open.weixin.qq.com/connect/qrconnect?appid=" + eBase.WeChatAccount.getValue("OpenAppID") + "&redirect_uri=" + redirectUrl + "&response_type=code&scope=snsapi_login&state=STATE#wechat_redirect";
                Response.Redirect(url);
            }
            else
            {
                //跳转到前端页面.aspx
                Response.Redirect(Request.Url.ToString());
            }
        }
    }
}