﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="scanQRCode.aspx.cs" Inherits="eFrameWork.WeChat.scanQRCode" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>分享</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <script src="../../../Scripts/jquery.js?ver=1.0.61"></script>

    <script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	
	 <script sr44c="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
	  <script s66rc="http://res.wx.qq.com/open/js/jweixin-1.3.2.js"></script>
	 
</head>
<script>
$(document).ready(function () {
       var jsApiListArr = ["checkJsApi","scanQRCode", "onMenuShareTimeline", "onMenuShareAppMessage"];
	   //var jsApiListArr = ["checkJsApi", "onMenuShareTimeline", "onMenuShareAppMessage"];
       wx.config({ debug: false, appId: "<%=shareData.appid %>", timestamp: "<%=shareData.timestamp %>", nonceStr: "<%=shareData.noncestr %>", signature: "<%=shareData.signature%>", jsApiList: jsApiListArr });
       var shareData = {
            title: "<%=shareData.title %>",
            desc: "<%=shareData.desc %>",
            link: "<%=shareData.link %>",
            imgUrl: "<%=shareData.imgurl %>",
            success: function (res) {
                //alert('已分享');
            },
            cancel: function (res) {
            },
            fail: function (res) {
                alert("AA:" + JSON.stringify(res));
            }
        };

        wx.ready(function () {
			wx.onMenuShareTimeline(shareData); //分享到朋友圈
            wx.onMenuShareAppMessage(shareData);//发送给朋友
			//wx.hideOptionMenu();
			/*
			wx.checkJsApi({
				jsApiList: ['onMenuShareTimeline','scanQRCode'],
			  	success: function(res) {
			 	//alert("BB" + JSON.stringify(res));
			  	}
			});
			*/
			/*
			wx.scanQRCode({needResult: 1,
			scanType: ["qrCode","barCode"],
			success: function (res){
			alert(1);
			//var result = res.resultStr;
			//alert(res.resultStr);
			alert(JSON.stringify(res));
			},
			error: function(res)
			{
				alert(JSON.stringify(res));
			}
			});
			*/

			//wx.scanQRCode({needResult:1,desc:"scanQRCode desc",success:function(e){alert(JSON.stringify(e))}});//自己处理
            
        });
		wx.error(function (res) {
			alert(JSON.stringify(res));
        	//alert("error: " + res.errMsg);  
		});  
});
function scanQRCodex()
{
//wx.scanQRCode({needResult:1,desc:"scanQRCode desc",success:function(e){alert(JSON.stringify(e))}});//自己处理
            
 //wx.scanQRCode({needResult:1,scanType: ["qrCode","barCode"],success:function(e){alert(JSON.stringify(e))}});
 
//alert(wx.scanQRCode);

      wx.scanQRCode({needResult: 1,
			scanType: ["qrCode","barCode"],
			success: function (res){
			//alert(res.resultStr);
			//alert(JSON.stringify(res));
			var result = res.resultStr;
			document.title=result;
			
			},
			error: function(res)
			{
				alert(JSON.stringify(res));
			}

			});
	
};
</script>
<body>

请使用微信右上角功能菜单进行测试
 <button onclick="scanQRCodex();" style="display:block;line-height:45px;width:100%;height:45px;">扫一扫</button>

</body>
</html>