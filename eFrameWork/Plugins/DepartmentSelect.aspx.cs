﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.Plugins
{
    public partial class DepartmentSelect : System.Web.UI.Page
    {        
        public string obj = eParameters.QueryString("obj");
        protected void Page_Load(object sender, EventArgs e)
        {
            litBody.Text = getTree("");
        }
        private string getTree(string ParentID)
        {
            string siteid = eBase.getSiteID();// eConfig.SiteID();
            StringBuilder sb = new StringBuilder();



            string sql = "select OrganizationalID,ParentID,MC,Code from Organizationals where DelTag=0";
            sql += (ParentID.Length == 0 ? " and ParentID IS NULL" : " and ParentID='" + ParentID + "'");
            sql += " and " + (siteid.Length > 0 ? "(SiteID=0 or SiteID='" + siteid + "')" : "SiteID=0");
            sql += " Order by PX,addTime";
            DataTable tb = eBase.DataBase.getDataTable(sql);
            //eBase.PrintDataTable(tb);

            if (ParentID.Length == 0)
            {
                sb.Append("<ul id=\"etree\" class=\"etree\" PID=\"NULL\">\r\n");
            }
            else
            {
                sb.Append("<ul PID=\"" + ParentID + "\">\r\n");
            }
            foreach (DataRow dr in tb.Rows)
            {
                string ct = eBase.DataBase.getValue("select count(*) from  Organizationals where SiteID='" + siteid + "' and DelTag=0 and ParentID='" + dr["OrganizationalID"].ToString() + "'");
               
                //if (ct == "0") continue;
                sb.Append("<li oncont3extmenu=\"return false;\" dataid=\"" + dr["OrganizationalID"].ToString() + "\" ");
                sb.Append(" class=\"\">");
                sb.Append("<div oncont3extmenu=\"return false;\">");
                sb.Append("<a href=\"javascript:;\" onclick=\"setDepartment('" + dr["OrganizationalID"].ToString() + "','" + dr["MC"].ToString() + "')\" oncontextmenu=\"return false;\">");
                sb.Append( dr["MC"].ToString() );
                sb.Append( "</a>"); // (" + ct + ")
                sb.Append("</div>");

                sb.Append(getTree(dr["OrganizationalID"].ToString()));

                sb.Append("</li>\r\n");
            }
            sb.Append("</ul>\r\n");
            return sb.ToString();
        }
    }
}