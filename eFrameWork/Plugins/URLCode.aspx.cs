﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using ThoughtWorks;
using ThoughtWorks.QRCode;
using ThoughtWorks.QRCode.Codec;
using ThoughtWorks.QRCode.Codec.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.Plugins
{
    public partial class URLCode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //初始化二维码生成工具
            //ht tp://192.168.1.8/ekeframev11/plugins/urlcode.aspx?url=http%3A%2F%2F192.168.1.8%2Fekeframev9%2F&scale=30
            if (Request.QueryString["url"] != null)
            {
                string str = Request.QueryString["url"].ToString().UrlDecode();
                int scale = 5;
                if (Request.QueryString["scale"] != null) scale = Convert.ToInt32(Request.QueryString["scale"]);
                QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
                qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
                qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
                qrCodeEncoder.QRCodeVersion = 0;
                qrCodeEncoder.QRCodeScale = scale;

                //将字符串生成二维码图片
                Bitmap image = qrCodeEncoder.Encode(str, Encoding.Default);

                //保存为PNG到内存流  
                MemoryStream ms = new MemoryStream();
                image.Save(ms, ImageFormat.Png);

                //输出二维码图片
                Response.ContentType = "image/png";
                Response.BinaryWrite(ms.GetBuffer());
                Response.End();
            }

        }
    }
}