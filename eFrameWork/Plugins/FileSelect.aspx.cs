﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.Plugins
{
    public partial class FileSelect : System.Web.UI.Page
    {
        private string basePath = "";//当前目录
        public string path = eParameters.QueryString("path");
        string aspxFile = eBase.getAspxFileName();
        public string obj = eParameters.QueryString("obj");


        protected void Page_Load(object sender, EventArgs e)
        {
            basePath =  eRunTime.fileManagePath;
            if (!Directory.Exists(basePath)) Directory.CreateDirectory(basePath);
            if (path.Contains("..")) path = "";//禁止越权上级目录
            if (path.Length > 0) path = path.toUrlFormat();
            if (path.EndsWith("/")) path = path.Substring(0, path.Length - 1);
            if (!Directory.Exists(basePath + path.toLocalFormat())) path = "";
            if (path.Length > 0) basePath += path.toLocalFormat() + "\\"; 

            


            StringBuilder sb = new StringBuilder();
            sb.Append("<div class=\"filemanage_local\">");
            if (path.Length > 0)
            {
                sb.Append("<a href=\"" + aspxFile + "?obj=" + obj  + "\">根目录</a>");
                string[] arr = path.Split("/".ToCharArray());
                string tmp = "";
                for (int i = 0; i < arr.Length; i++)
                {
                    if (i > 0) tmp += "/";
                    tmp += arr[i];
                    if (i == arr.Length - 1)
                    {
                        sb.Append(" / " + arr[i]);
                    }
                    else
                    {
                        sb.Append(" / <a href=\"" + aspxFile + "?obj=" + obj + "&path=" + tmp +  "\">" + arr[i] + "</a>");
                    }

                }
            }
            else
            {
                sb.Append("根目录");
            }
            sb.Append("</div>");

            sb.Append("<div class=\"filemanage_files\">");
            eDirectoryInfo einfo = new eDirectoryInfo(basePath);
            foreach (DirectoryInfo folder in einfo.GetDirectories())
            {
                sb.Append("<a href=\"" + aspxFile + "?obj=" + obj + "&path=" + (path.Length > 0 ? path + "/" : path) + folder.Name.ToString() +  "\" title=\"" + folder.Name + "\">\r\n");
                sb.Append("<dl>\r\n");
                sb.Append("<dt class=\"folder\"></dt>\r\n");
                sb.Append("<dd>" + folder.Name + "</dd>\r\n");
                sb.Append("</dl>\r\n");
                sb.Append("</a>\r\n");
            }
            foreach (FileInfo file in einfo.GetFiles())
            {
                getFileHTML(sb, file.Name);     
            }
            sb.Append("</div>");
            litBody.Text = sb.ToString();
        }
        private void getFileHTML(StringBuilder sb, string name)
        {
            string _path = basePath + name;
            FileInfo info = new FileInfo(_path);
            string file = _path.toVirtualUrl();
            eFileInfo fi = new eFileInfo(name);
            sb.Append("<a href=\"javascript:;\" onclick=\"" + (obj.Length > 0 ? "parent.$('#" + obj + "').val('" + eRunTime.baseUrl + file + "')" : "parent.addPath('" + file + "')") + ";parent.layer.close(parent.arrLayerIndex.pop());\"  title=\"" + name + "&#10;创建时间：" + string.Format("{0:yyyy-MM-dd HH:mm:ss}", info.CreationTime) + "&#10;修改时间：" + string.Format("{0:yyyy-MM-dd HH:mm:ss}", info.LastWriteTime) + "&#10;大小：" + info.Size() + "\">\r\n");
            sb.Append("<dl>\r\n");
            if (".jpg.jpeg.gif.bmp.png.tif.".IndexOf(fi.fileExtension + ".") > -1)
            {
                sb.Append("<dt><img src=\"" + eRunTime.absRoot + file + "\" /></dt>\r\n");
            }
            else
            {
                sb.Append("<dt class=\"" + eRunTime.getFileClass(fi.fileExtension) + "\"></dt>\r\n");
            }
            sb.Append("<dd>" + name + "</dd>\r\n");
            sb.Append("</dl>\r\n");
            sb.Append("</a>\r\n");
        }
    }
}