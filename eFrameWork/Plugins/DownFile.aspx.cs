﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using System.IO;

public partial class DownFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region 下载文件参数及安全性检测
        string fileName = eParameters.QueryString("name");
        fileName = HttpUtility.UrlDecode(fileName);
        string path = eParameters.QueryString("path");
        if (path.IndexOf("..") > -1) path = "";//禁止越权上级目录
        if (path.Length > 0) path = path.toUrlFormat();
        if (path.Length < 10)
        {
            Response.Write("文件不存在!");
            Response.End();
        }
        if (path.ToLower().IndexOf("upload/") == -1)
        {
            Response.Write("禁止其他目录文件下载!");
            Response.End();
        }
        string ext = path.fileExtension();
        if (eConfig.PreventExtensions.Contains(ext))
        {
            Response.Write("文件类型不允许下载!");
            Response.End();
        }
        string filePath = Server.MapPath("~/" + path);      
        if (!File.Exists(filePath))
        {
            Response.Write("文件不存在或已被删除!");
            Response.End();
        }
        #endregion
        FileInfo fileInfo = new FileInfo(filePath);
        Response.Clear();
        Response.ClearContent();
        Response.ClearHeaders();
        Response.AddHeader("Content-Disposition", "attachment;filename=" + Server.UrlPathEncode(fileName));
        Response.AddHeader("Content-Length", fileInfo.Length.ToString());
        Response.AddHeader("Content-Transfer-Encoding", "binary");
        Response.ContentType = "application/octet-stream";
        Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
        Response.WriteFile(fileInfo.FullName);
        Response.Flush();
        Response.End();

    }
}
