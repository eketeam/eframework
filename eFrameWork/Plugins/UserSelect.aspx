﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserSelect.aspx.cs" Inherits="eFrameWork.Plugins.UserSelect" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>用户选择</title>
</head>
<link href="../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
<link href="../Plugins/Theme/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
<script src="../Scripts/jquery.js"></script>
<script src="../Scripts/eketeam.js?ver=<%=Common.Version %>"></script>
<style>
    html, body {
    margin:0px;padding:0px;font-size:13px;
    }
body {overflow:auto;}
</style>
<script>
var objid = "<%=obj%>";
$(document).ready(function () {
	var tree = new eTree("etree");	
});
function setUser(id, name)
{

    if ($(parent.UserSelectParent).find("#" + objid + "_box").attr("etype") == "singleuser")
	{
        setSingle(id, name);
	}
	else
	{
		setMultiple(id,name);
	}	
};
function setUsers(obj)
{
	if ($(parent.UserSelectParent).find("#" + objid + "_box").attr("etype") == "singleuser") { return; }
	var box = $(parent.UserSelectParent).find("#" + objid + "_box");
	var ovalue=$(parent.UserSelectParent).find("[postname='" + objid + "']").val();
	
	$(obj).parent().next().find("li[userid]").each(function(i,elem){
		if(ovalue.indexOf($(this).attr("userid"))==-1)
		{
			var ntext=$(parent.UserSelectParent).find("[postname='" + objid + "_text']").val();
			var nvalue=ovalue;
			if(nvalue.length>0){nvalue+=",";ntext+=",";}
			nvalue+=$(this).attr("userid");
			ntext+=$(this).attr("name");
			ovalue=nvalue;
			$(parent.UserSelectParent).find("[postname='" + objid + "']").val(nvalue);
			$(parent.UserSelectParent).find("[postname='" + objid + "_text']").val(ntext);
			
			var html='<a class="useritem" href="javascript:;">';			
			html += '<p>' + $(this).attr("name") + '</p><img src="../images/none.gif" onclick="multipleuser_delete(this);"></a>';
			box.find(".useritem_open").before(html);
		}
	});
};
function setUsersv1(obj)
{	
	//alert($(obj).next().get(0).tagName);
    if ($(parent.UserSelectParent).find("#" + objid + "_box").attr("etype") == "singleuser") { return; }
	//alert($(obj).parent().next().get(0).tagName);	
    var box = $(parent.UserSelectParent).find("#" + objid + "_box");
	$(obj).parent().next().find("li[userid]").each(function(i,elem){
		if(box.find("input[value='" + $(this).attr("userid") + "']").length == 0)
		{
			//alert($(this).attr("userid"));
			var html='<a class="useritem" href="javascript:;">';
			html += '<input etype="' + box.attr("etype") + '" type="hidden" name="' + objid + '" postname="' + objid + '" value="' + $(this).attr("userid") + '">';
			html += '<input etype="' + box.attr("etype") + '" type="hidden" name="' + objid + '_text" postname="' + objid + '_text" value="' + $(this).attr("name") + '">';
			html += '<p>' + $(this).attr("name") + '</p><img src="../images/none.gif" onclick="multipleuser_delete(this);"></a>';
			box.find(".useritem_open").before(html);
		}
	});
};
function setSingle(id, name)
{
	var box = $(parent.UserSelectParent).find("#" + objid + "_box");
	var ovalue=$(parent.UserSelectParent).find("[postname='" + objid + "']").val();
	if(ovalue==id)//删除
	{
		$(parent.UserSelectParent).find("[postname='" + objid + "']").val("");
		$(parent.UserSelectParent).find("[postname='" + objid + "_text']").val("");
		box.find(".useritem").remove();
	}
	else
	{
		$(parent.UserSelectParent).find("[postname='" + objid + "']").val(id);
		$(parent.UserSelectParent).find("[postname='" + objid + "_text']").val(name);
		if(box.find(".useritem").length==1)
		{
			box.find(".useritem p").html(name);
		}
		else
		{
			var html='<a class="useritem" href="javascript:;">';
			html += '<p>' + name + '</p><img src="../images/none.gif" onclick="multipleuser_delete(this);"></a>';
		    //box.find(".useritem_open").before(html);
			if (box.find(".useritem_open").length > 0) {
			    box.find(".useritem_open").before(html);
			}
			else {
			    box.append(html);
			}
			//box.append(html);
		}
		parent.layer.close(parent.arrLayerIndex.pop());
	}
};

function setSinglev1(id, name)
{
    if ($(parent.UserSelectParent).find("#" + objid).length == 1)
	{
        //$(parent.UserSelectParent).find("#" + objid).val(id);
        //$(parent.UserSelectParent).find("#" + objid + "_text").val(name);
        
        $(parent.UserSelectParent).find("[postname='" + objid + "_text']").val(name);
    	parent.layer.close(parent.arrLayerIndex.pop());
	}
	else
	{
        var box = $(parent.UserSelectParent).find("#" + objid + "_box");
		if(box.find("input[value='" + id + "']").length == 1)//删除
		{
			box.find("input[value='" + id + "']").parent().remove();
		}
		else //添加或替换
		{
			box.find(".useritem").remove();
			var html='<a class="useritem" href="javascript:;">';
			html += '<input etype="' + box.attr("etype") + '" type="hidden" name="' + objid + '" postname="' + objid + '" value="' + id + '">';
			html += '<input etype="' + box.attr("etype") + '" type="hidden" name="' + objid + '_text" postname="' + objid + '_text" value="' + name + '">';
			html += '<p>' + name + '</p><img src="../images/none.gif" onclick="multipleuser_delete(this);"></a>';
			box.find(".useritem_open").before(html);
			parent.layer.close(parent.arrLayerIndex.pop());
		}
	}
};
function setMultiple(id,name)
{
	var box = $(parent.UserSelectParent).find("#" + objid + "_box");
	var ovalue = $(parent.UserSelectParent).find("[postname='" + objid + "']").val();
	//alert(ovalue + "::" + id);
	if(ovalue.indexOf(id)>-1)
	{
		var arr=ovalue.split(",");
		var idx=-1;
		var nvalue="";
		var ntext="";
		for(var i=0;i<arr.length;i++)
		{
			if(arr[i]==id)
			{
				idx=i;
				continue;
			}
			if(nvalue.length>0){nvalue+=",";ntext+=",";}
			nvalue+=arr[i];
			ntext+=box.find(".useritem:eq(" + i + ") p").html();
		}
		$(parent.UserSelectParent).find("[postname='" + objid + "']").val(nvalue);
		$(parent.UserSelectParent).find("[postname='" + objid + "_text']").val(ntext);
		box.find(".useritem:eq(" + idx + ")").remove();	
	}
	else
	{
		var ntext=$(parent.UserSelectParent).find("[postname='" + objid + "_text']").val();
		var nvalue=ovalue;
		if(nvalue.length>0){nvalue+=",";ntext+=",";}
		nvalue+=id;
		ntext+=name;
		$(parent.UserSelectParent).find("[postname='" + objid + "']").val(nvalue);
		$(parent.UserSelectParent).find("[postname='" + objid + "_text']").val(ntext);
		
		var html='<a class="useritem" href="javascript:;">';
		html += '<p>' + name + '</p><img src="../images/none.gif" onclick="multipleuser_delete(this);"></a>';
	    //box.find(".useritem_open").before(html);
		if (box.find(".useritem_open").length > 0) {
		    box.find(".useritem_open").before(html);
		}
		else {
		    box.append(html);
		}
		//box.append(html);
	}
};
function setMultiplev1(id,name)
{
    //var objs= parent.fillparent.tagName == "TR" ? $(parent.fillparent).find("[postname='" + objvalue[0] + "']") : $(parent.fillparent).find("[name='" + objvalue[0] + "']");

    var box = $(parent.UserSelectParent).find("#" + objid + "_box");
	if(box.find("input[value='" + id + "']").length == 0)
	{
		var html='<a class="useritem" href="javascript:;">';
		html += '<input etype="' + box.attr("etype") + '" type="hidden" name="' + objid + '" postname="' + objid + '" value="' + id + '">';
		html += '<input etype="' + box.attr("etype") + '" type="hidden" name="' + objid + '_text" postname="' + objid + '_text" value="' + name + '">';
		html += '<p>' + name + '</p><img src="../images/none.gif" onclick="multipleuser_delete(this);"></a>';
		box.find(".useritem_open").before(html);
	}
	else
	{
		box.find("input[value='" + id + "']").parent().remove();
	}
};
</script>
<body>
<div style="margin:10px;">
<asp:Literal id="litBody" runat="server" />
</div>
</body>
</html>