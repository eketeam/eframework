﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BindDingTalk.aspx.cs" Inherits="eFrameWork.Plugins.BindDingTalk" %>
<script>
    var DingTalk_Index = 0;
    function BindDingTalk() {
        var url1 = "<%=(eBase.WeChatAccount.getValue("Proxy").Length== 0 ? eRequest.PathAndQuery().Path() + "BindWeChatCode.aspx?mode=getimage" :  eBase.WeChatAccount.getValue("Proxy") + "Plugins/Tencent/WeChat/WeChatScanLogin.aspx?mode=getimage&fromURL=" + HttpUtility.UrlEncode(Request.Url.Scheme + "://" + Request.Url.Host + (Request.Url.Port==80 ||Request.Url.Port==443 ? "" : Request.Url.Port.ToString() ) + Request.CurrentExecutionFilePath) ) %>";
        var url = "<%=(eBase.DingTalkAccount.getValue("Proxy").Length== 0 ? eBase.getAbsolutePath() : eBase.DingTalkAccount.getValue("Proxy") ) + "Plugins/aliyun/DingTalk/DingTalkScanLogin.aspx?mode=getimage&fromURL=" + HttpUtility.UrlEncode(Request.Url.Scheme + "://" + Request.Url.Host + (Request.Url.Port==80 ||Request.Url.Port==443 ? "" : Request.Url.Port.ToString() ) + Request.CurrentExecutionFilePath)%>";
        DingTalk_Index = layer.open({
        type: 2,
        title: "钉钉扫一扫绑定",
        maxmin: false,
        shadeClose: true, //点击遮罩关闭层 
        area: ["330px", "450px"],
        content: [url, 'no'],
        success: function (layero, index) { },
        cancel: function (index, layero) { },
        end: function (index) { }
    });
   
    };
    function CloseDingTalk() {
        layer.close(DingTalk_Index);
        $('#bindDingTalk').html('已绑定!');
    };
</script>
<div id="bindDingTalk">    
<asp:Literal id="litBody" runat="server" />
</div>