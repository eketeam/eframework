﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;
using EKETEAM.aliyun;

namespace eFrameWork.Plugins
{
    public partial class BindDingTalk : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            if (Request.QueryString["data"] != null)
            {
                string data = Request.QueryString["data"].ToString();
                string base64 = Base64.Decode(data);
                if (base64.Length > 5 && base64.StartsWith("\"") && base64.EndsWith("\"")) base64 = base64.Substring(1, base64.Length - 2);
                base64 = base64.Replace("\\\\/", "\\/");
                base64 = base64.Replace("\\\"", "\"");
                if (base64.StartsWith("{"))
                {
                    JsonData json = JsonMapper.ToObject(base64);
                    string dduserid = json["result"].getValue("userid");
                    string unionid = json["result"].getValue("unionid");

                    eTable etb = new eTable("a_eke_sysUsers", user);
                    etb.DataBase = eBase.UserInfoDB;
                    if (dduserid.Length > 0) etb.Fields.Add("dduserid", dduserid);
                    if (unionid.Length > 0) etb.Fields.Add("ddunionid", unionid);



                    string headimgurl = json.getValue("avatar").Replace("\\", "");
                    string face =  DingTalkHelper.downHeadImage(user.ID, headimgurl);
                    etb.Fields.Add("ddavatar", headimgurl);
                    etb.Fields.Add("face", face);


                    etb.Where.Add("UserID='" + user.ID + "'");
                    etb.Update();
                    //Response.Write("<script>top.CloseDingTalk();</script>");//AppFrame无效
                    Response.Write("<script>if(top.frames.length==1){top.CloseDingTalk();}else{parent.parent.CloseDingTalk();}</script>");
                }
                eBase.End();
            }
            if (eBase.DingTalkAccount.getValue("Proxy").Length == 0 && (eBase.DingTalkAccount.getValue("AgentId").Length == 0 || eBase.DingTalkAccount.getValue("AppKey").Length == 0))
            {
                litBody.Text = "系统没有绑定钉钉帐号!";
                return;
            }

            string _openid = eBase.UserInfoDB.getValue("select ddunionid from a_eke_sysUsers where UserID='" + user.ID + "'");
            if (_openid.Length == 0) _openid = eBase.UserInfoDB.getValue("select dduserid from a_eke_sysUsers where UserID='" + user.ID + "'");
            if (_openid.Length > 0)
            {
                litBody.Text = "已绑定!";
            }
            else
            {
                litBody.Text = "<a class=\"button btnprimary\" href=\"javascript:;\" onclick=\"BindDingTalk();\"><span style=\"letter-spacing:1px;\"><i class=\"\">授权绑定</i></span></a>";
            }
        }
    }
}