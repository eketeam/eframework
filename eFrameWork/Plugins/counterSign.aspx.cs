﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;

public partial class Plugins_counterSign : System.Web.UI.Page
{
    public string UserArea = "Application";
    public string AppItem = eParameters.Request("AppItem");
    public string itemid = eParameters.QueryString("itemid");
    public string pid = eParameters.QueryString("pid");
    public string id = eParameters.QueryString("id");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Form.Count == 0) return;
        if (Request.QueryString["UserArea"] != null) UserArea = Request.QueryString["UserArea"].ToString();

        eUser user = new eUser(eBase.getUserArea(UserArea));
        if (!user.Logined) eResult.Error("登录后操作!");
        user.Check();
        eModel model = new eModel();

        DataRow[] rows = eBase.a_eke_sysModelItems.Select("ModelItemID='" + itemid + "'");

        if (rows.Length > 0)
        {
            string code = rows[0]["code"].ToString();
            string signfile = savefile();
            eTable etb = new eTable("a_eke_sysCheckupRecords", user);
            etb.Fields.Add("SignField", code);
            etb.Fields.Add("ModelID", model.ModelID);
            etb.Fields.Add("Checkuptype", model.ModelInfo["code"].ToString());
            etb.Fields.Add("CheckupContentId", id);
            etb.Fields.Add("SignFile", signfile);
            etb.Add();

            string sql = "SELECT SignFile as url FROM a_eke_sysCheckupRecords";
            sql += " where CheckupContentId='" + id + "' and SignField='" + code + "' and delTag=0";
            sql += " order by addTime";

            DataTable dt = eBase.DataBase.Select(sql);
            foreach (DataRow dr in dt.Rows)
            {
                dr["url"] = eBase.getAbsoluteUrl() + dr["url"].ToString();
            }
            JsonData jd = new JsonData();
            jd["success"] = 1;
            jd["errcode"] = "0";
            jd["message"] = "签名成功!";
            jd["data"] = dt.toJSONData();
            eResult.WriteJson(jd);

            //eResult.Success("签名成功!");
            //eResult.Message(new { success = 1, errcode = "0", signfile = signfile, message = "签名成功!" });
            //eResult.Success(model.ModelInfo["code"].ToString() + "::" + id);
            //eResult.Success(signfile);
        }
    }
    /// <summary>
    /// 保存文件
    /// </summary>
    /// <returns>文件相对路径</returns>
    private string savefile()
    {
        string virtualDir = eConfig.UploadPath();
        string basePath = HttpContext.Current.Server.MapPath("~/") + virtualDir.Replace("/", "\\");
        string filename = eBase.GetFileName() + ".png";
        if (!Directory.Exists(basePath)) Directory.CreateDirectory(basePath);
        while (File.Exists(basePath + filename))
        {
            filename = eBase.GetFileName() + ".png";
        }
        string imageData = eParameters.Form("imageData");
        using (FileStream fs = new FileStream(basePath + filename, FileMode.Create))
        {
            using (BinaryWriter bw = new BinaryWriter(fs))
            {
                byte[] data = Convert.FromBase64String(imageData);
                bw.Write(data);
                bw.Close();
            }
        }
        return virtualDir + filename;
    }
}