﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FillSelectData.aspx.cs" Inherits="eFrameWork.Plugins.FillSelectData" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
<%if(IsMobile){ %>
    <link href="../Plugins/Theme/default/mobile/base.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
    <link href="../Plugins/eControls/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- eFrameWork控件样式-->
<%}else{ %>
    <link href="../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />  
  	<link href="../Plugins/Theme/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />  
<%} %>
</head>
<script>var ModelID = "<%=ModelID%>";</script>
<script src="../Scripts/Init.js?ver=<%=Common.Version %>"></script>
<script>
    var SingleFields = "<%=model.eListControl.SingleFields%>";
	function _init()
    {
	    var fieldstr = SingleFields || "";
	    var fieldarr = [];
	    if (fieldstr.length > 0) { fieldarr = fieldstr.split(","); }
	    var trs = $("#eDataTable tbody tr");
	    //alert(trs.length);
	    for (var x = 0; x < trs.length; x++) { //trs.length;
            var tr = trs[x];
            var values = $(tr).attr("jsondata");
            var json = decode64(values);
            var data = JSON.parse(json);            
            //var _trs = $(parent.document).find(".excelTable tbody tr:visible");
            var _trs;
            if (parent.form_area)
            {              
                _trs = parent.form_area.find(".excelTable tbody tr:visible");
            }
            else
            {
                _trs = $(parent.document).find(".excelTable tbody tr:visible");
            }
            if (_trs.length == 0) { continue; }					
            for (var j = 0; j < _trs.length; j++)//
			 {
                var _tr = _trs[j];
				var allcount = 0;
				var hascount = 0;
				$.each(data, function(idx, item)
				{
				    var name = item["id"];
				    if (fieldarr.length > 0 && fieldarr.indexOf(name) == -1) { return true; }
				    var value =item["value"];
				    value = value.replace(/'/gi, "\\'");
				    if ($(_tr).find("[postname='" + name + "']").length == 0) { return true; }

				    //if (x == 0) alert(idx+"::"+ JSON.stringify(item) + "::" + value + "::");
				    //alert($(_tr).find("[postname='" + name + "']")[0].type);				   
				    allcount++;
				    var type = $(_tr).find("[postname='" + name + "']").attr("type").toLowerCase();
				    if (type == "radio")
				    {
				        if($(_tr).find("[postname='" + name + "'][value='" + value + "']:checked").length > 0) { hascount++; }
				    }
				    else if (type == "checkbox")
				    {
				        var tvalue = "," + value + ",";
				        var checkboxs = $(_tr).find("[postname='" + name + "']:checked");
				        checkboxs.each(function () {
				            tvalue = tvalue.replace("," + $(this).val() + ",", ",");
				        });
				        tvalue = tvalue.replace(/,/g, "");
				        if(tvalue.length==0){ hascount++; }
				        //alert("checkbox:" + value + "::" + $(_tr).find("[postname='" + name + "']:checked").val());
				    }
				    else
				    {
				        if ($(_tr).find("[postname='" + name + "'][value='" + value + "']").length > 0) { hascount++; }
				    }
				});
				if (allcount == hascount && allcount > 0) {$(tr).find("input[name='fillselitem']").prop("checked", true);}
			}
        }
    };
    if (window.addEventListener) { window.addEventListener("load", _init, false); } else { window.attachEvent("onload", _init); }
    $(document).ready(function () {});
</script>
<%if(IsMobile){ %>
<script src="Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>
<%} %>
<style>
body {overflow:auto;margin-top:10px;}
</style>
<asp:Literal ID="LitJavascript" runat="server" />
<asp:Literal ID="LitStyle" runat="server" />
<body>
    <%= (model.ModelInfo["StartHTML"].ToString().Length < 3 ? "" : "<div class=\"starthtml\">" + model.ModelInfo["StartHTML"].ToString() + "</div>")   %>
    <asp:Literal ID="LitBody" runat="server" />
</body>
</html>