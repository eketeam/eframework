﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;
using EKETEAM.FrameWork;


namespace eFrameWork.Plugins
{
    public partial class RndPic : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string type = eParameters.QueryString("type").ToLower();
            if (type.Length == 0) type = "png";

            string checkCode = eBase.getRnd(4);// CreateRandomCode(4);

            if (type == "gif")
            {
                CreateGif(checkCode);
            }
            else
            {
                CreateImage(checkCode);
            }
        }
        private void CreateGif(string checkCode)
        {
            
            Validate GifValidate = new Validate();

            #region 对验证码进行设置(不进行设置时,将以默认值生成)
            //验证码位数，不小于4位
            GifValidate.ValidateCodeCount = 4;
            //验证码字体型号(默认13)
            GifValidate.ValidateCodeSize = 16;
            //验证码图片高度,高度越大,字符的上下偏移量就越明显
            GifValidate.ImageHeight = 25;
            //验证码字符及线条颜色(需要参考颜色类)

            //GifValidate.DrawColor = Color.FromArgb(255, 0, 0);// System.Drawing.Color.BlueViolet;

            Color fcolor = Color.FromArgb(153, 0, 0);
            if (Request.QueryString["color"] != null) fcolor = (Color)ColorTranslator.FromHtml("#" + Request.QueryString["color"].ToString().ToUpper());
            GifValidate.DrawColor = fcolor;
            Color bcolor = Color.FromArgb(255, 255, 255);
            if (Request.QueryString["bgcolor"] != null)
            {
                bcolor= (Color)ColorTranslator.FromHtml("#" + Request.QueryString["bgcolor"].ToString().ToUpper());
                GifValidate.backColor = bcolor; 
            }  
            
            //验证码字体(需要填写服务器安装的字体)
            GifValidate.ValidateCodeFont = "Arial";
            //验证码字符是否消除锯齿
            GifValidate.FontTextRenderingHint = true;
            //定义验证码中所有的字符(","分离),似乎暂时不支持中文
            GifValidate.AllChar = "1,2,3,4,5,6,7,8,9,0,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,W,X,Y,Z";

            #endregion

            //输出图像(Session名称)
            GifValidate.OutPutValidate("Plugins_RndCode");


        }
        

        private string CreateRandomCode(int codeCount)
        {
            //string allChar = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,W,X,Y,Z"; 
            string allChar = "0,1,2,3,4,5,6,7,8,9";
            string[] allCharArray = allChar.Split(',');
            string randomCode = "";
            int temp = -1;

            Random rand = new Random();
            for (int i = 0; i < codeCount; i++)
            {
                if (temp != -1)
                {
                    rand = new Random(i * temp * ((int)DateTime.Now.Ticks));
                }
                int t = rand.Next(10);
                if (temp == t)
                {
                    return CreateRandomCode(codeCount);
                }
                temp = t;
                randomCode += allCharArray[t];
            }
            return randomCode;
        }
        private void CreateImage(string checkCode)
        {
            Session["Plugins_RndCode"] = checkCode;
            Bitmap bt = new Bitmap(45, 18, PixelFormat.Format24bppRgb);
            bt = new Bitmap(150, 60, PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(bt);
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;//平滑

            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;


            Color bcolor = Color.FromArgb(255, 255, 255);
            if (Request.QueryString["bgcolor"] != null) bcolor = (Color)ColorTranslator.FromHtml("#" + Request.QueryString["bgcolor"].ToString().ToUpper());            
            //g.Clear(Color.FromArgb(255, 255, 255));//背景颜色
            g.Clear(bcolor);


            Font fn1 = new Font("Arial", 12, FontStyle.Regular, GraphicsUnit.Pixel);
            fn1 = new Font("黑体", 56, FontStyle.Regular, GraphicsUnit.Pixel);
            Color fcolor = Color.FromArgb(153, 0, 0);
            if (Request.QueryString["color"] != null) fcolor = (Color)ColorTranslator.FromHtml("#" + Request.QueryString["color"].ToString().ToUpper());

            System.Drawing.Brush b = new System.Drawing.SolidBrush(fcolor);
            //g.DrawString(checkCode, fn1, b, new PointF(0, 0));
            g.DrawString(checkCode, fn1, b, new PointF(8, 3));




            ImageCodecInfo myImageCodecInfo;
            Encoder myEncoder;
            EncoderParameter myEncoderParameter;
            EncoderParameters myEncoderParameters;
            myImageCodecInfo = ImageCodecInfo.GetImageEncoders()[0];
            myEncoder = Encoder.Quality;
            myEncoderParameters = new EncoderParameters(1);
            myEncoderParameter = new EncoderParameter(myEncoder, 100L);
            myEncoderParameters.Param[0] = myEncoderParameter;

            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            bt.Save(ms, myImageCodecInfo, myEncoderParameters);
            Response.ClearContent();
            Response.ContentType = "image/Jpeg";
            Response.BinaryWrite(ms.ToArray());

            myEncoderParameter.Dispose();
            myEncoderParameters.Dispose();
            b.Dispose();
            g.Dispose();
            bt.Dispose();
            Response.End();
        }
        private void CreateImage1(string checkCode)
        {
            int iwidth = (int)(checkCode.Length * 11.5);
            System.Drawing.Bitmap image = new System.Drawing.Bitmap(iwidth, 20);
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(image);
            //System.Drawing.Font f = new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Regular); 
            System.Drawing.Font f = new System.Drawing.Font("宋体", 12, System.Drawing.FontStyle.Regular);
            System.Drawing.Brush b = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            //g.FillRectangle(new System.Drawing.SolidBrush(Color.Blue),0,0,image.Width, image.Height); 
            //g.Clear(System.Drawing.Color.Blue);
            //g.Clear(System.Drawing.Color.FromName("0000FF"));
            g.Clear(System.Drawing.Color.FromArgb(255, 255, 255));
            g.DrawString(checkCode, f, b, 3, 3);

            System.Drawing.Pen blackPen = new System.Drawing.Pen(System.Drawing.Color.White, 0);
            Random rand = new Random();
            for (int i = 0; i < 5; i++)
            {
                int y = rand.Next(image.Height);
                //g.DrawLine(blackPen, 0, y, image.Width, y); 
            }

            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            Response.ClearContent();
            Response.ContentType = "image/Jpeg";
            Response.BinaryWrite(ms.ToArray());

            g.Dispose();
            image.Dispose();
        } 
    }
}