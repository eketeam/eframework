﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Caching;
using System.Net;
using System.IO;
using System.Text;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using EKETEAM.Tencent.WeChat;
using LitJson;

namespace eFrameWork.WeChat
{
    public partial class Register : System.Web.UI.Page//WXWork
    {
        public string UserArea = "Application";
        public string ModelID = "dd4db3c1-8e5a-407f-848c-f7e96490fd61"; //注册模块ModelID
        eModel model;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["WeChat_UserInfo"] == null)
            {
                Response.Write("微信授权超时,请关闭重新进入!");
                Response.End();
            }

            eUser user = new eUser("Web", "00000000-0000-0000-0000-000000000000");
            model = new eModel(ModelID, user);
            model.Power["add"] = "true"; //给添加权限
            string act = eParameters.Request("act");
            if (act.Length == 0)
            {
                model.Action = "add";
                //litBody.Text = model.getAddHTML();
                litBody.Text = model.getActionHTML();
            }
            else
            {
                //model.eForm.AutoRedirect = false;
                model.eForm.onChange += eform_onChange;
                model.eForm.AutoRedirect = false;
                model.Save();
            }
        }
        protected void eform_onChange(object sender, eFormTableEventArgs e)
        {
            switch (e.eventType)
            {
                case eFormTableEventType.Inserting:
                    //litBody.Text += "添加前事件已调用<br>";
                    model.eForm.Fields.Add("SiteID", eBase.WeChatAccount.getValue("SiteID"));
                    model.eForm.Fields.Add("RoleID", "201310aa-89d7-4209-b3c1-b507e1dc6267");
                    break;
                case eFormTableEventType.Inserted:
                    //litBody.Text += "添加成功,ID为：" + e.ID + "<br>5秒后返回<br><script>setTimeout(function(){document.location='" + "LoadModel.aspx?id=1" + "';},5000);</script>";

                    JsonData jd = JsonMapper.ToObject(Session["WeChat_UserInfo"].ToString());
                    string fromURL = jd.getValue("fromURL");

                    string headimgurl = jd.getValue("headimgurl");
                    string face = WeChatHelper.downHeadImage(e.ID, headimgurl);
                    eTable etb = new eTable("a_eke_sysUsers");
                    etb.DataBase = eBase.UserInfoDB;

                    etb.Fields.Add("openid", jd.GetValue("openid"));
                    etb.Fields.Add("nickname", jd.GetValue("nickname"));
                    etb.Fields.Add("sex", jd.GetValue("sex"));
                    etb.Fields.Add("headimgurl", headimgurl);
                    etb.Fields.Add("country", jd.GetValue("country"));
                    etb.Fields.Add("province", jd.GetValue("province"));
                    etb.Fields.Add("city", jd.GetValue("city"));
                    if (jd.GetValue("unionid").Length > 0) etb.Fields.Add("unionid", jd.GetValue("unionid"));      

                    etb.Where.Add("UserID='" + e.ID + "'");
                    etb.Update();


                    DataTable tb = eBase.UserInfoDB.getDataTable("select top 1 * from a_eke_sysUsers where UserID='" + e.ID + "'");                   
                    if (tb.Rows.Count > 0)
                    {
                        eUser user = new eUser(UserArea);
                        eFHelper.saveUserInfo(user, tb.Rows[0]); //保存用户登录信息
                        eFHelper.UserLoginLog(user);
                    }
                    Session.Remove("WeChat_UserInfo");
                    //Response.Redirect("default.aspx", true);
                    Response.Redirect(HttpUtility.UrlDecode(fromURL), true);
                    break;
            }
        }
    }
}