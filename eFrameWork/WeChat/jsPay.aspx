﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="jsPay.aspx.cs" Inherits="eFrameWork.WeChat.jsPay" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>jsAPI支付</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <script src="../Scripts/jquery.js?ver=1.0.61"></script>
    <script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
</head>
<script>


    var payinfo;
    function jsApiCall() {
        WeixinJSBridge.invoke("getBrandWCPayRequest",
            {
                "appId": payinfo.appId,
                "nonceStr": payinfo.nonceStr,
                "package": payinfo.package,
                "paySign": payinfo.paySign,
                "signType": payinfo.signType,
                "timeStamp": payinfo.timeStamp
            },
        function (res) {
            if (res.err_msg == "get_brand_wcpay_request:ok")
            {
                //支付成功!
            }
        });


    };
    function pay() {
        var url = "?act=getpayinfo";
        $.ajax({
            type: "post",
            url: url,
            data: { je: "0.01" },
            dataType: "json",
            success: function (data) {
                payinfo = data;
                //alert(JSON.stringify(data));
                //return;
                if (typeof WeixinJSBridge == "undefined") {
                    if (document.addEventListener) {
                        document.addEventListener('WeixinJSBridgeReady',jsApiCall, false);
                    }
                    else if (document.attachEvent) {
                        document.attachEvent('WeixinJSBridgeReady', jsApiCall);
                        document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
                    }
                }
                else {
                    jsApiCall();
                }
            }
        });
    };
</script>
 <style>
     div {
     font-size:13px;line-height:25px;
     }
 </style>
<body>
    <div>当前位置：<a href="../default.aspx">首页</a> -> jsAPI支付</div>
    <input id="Button1" type="button" onclick="pay();" value="支付1分" />
</body>
</html>