﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using EKETEAM.Tencent.WeChat;
using LitJson;

namespace eFrameWork.WeChat
{
    public partial class nativePay : System.Web.UI.Page
    {
        public string UserArea = "Application";    
        string act = eParameters.QueryString("act");
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);

            #region 生成订单
            if (act == "getorder")
            {
                int fee = int.Parse("1");

                string _orderid = Guid.NewGuid().ToString();
                string id = Guid.NewGuid().ToString();
                string url = "";

                JsonData json = new JsonData();
                json.Add("success", "1");
                json.Add("orderid", _orderid);
                json.Add("fee", fee.ToString());
                string attach = user.ID + "," + _orderid + "," + id;
                NativePay nativePay = new NativePay();
                url = nativePay.GetPayUrl(id, fee, attach);
                url = "NativePayQRCode.aspx?data=" + HttpUtility.UrlEncode(url);


              
                json.Add("url", url);
                Response.Write(json.ToJson());
                Response.End();
            }
            #endregion
            #region 检查付款状态
            if (act == "checkorder")
            {
                string orderid = eParameters.QueryString("orderid");
                string tmp = eBase.DataBase.getValue("select count(*) from eWeb_PayLog where UserID='" + user.ID + "' and OrderID='" + orderid + "'");// and ProductID='" + id + "'
                if (tmp.Length == 0) tmp = "0";
                JsonData json = new JsonData();
                json.Add("success", "1");
                json.Add("count", tmp);
                Response.Write(json.ToJson());
                Response.End();
            }
            #endregion
        }
    }
}