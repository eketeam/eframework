﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using EKETEAM.FrameWork;
using EKETEAM.Tencent.WeChat;
using LitJson;

public partial class WeChat_shareProxy : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null) return;
        string host = Request.UrlReferrer.Host.ToLower();
        if (eBase.WeChatAccount.getValue("ReverseProxy").ToLower().IndexOf(host) == -1) return; 

        string postjson = eBase.getPostJsonString();
        //eBase.AppendLog(postjson);
        if (Request.ContentType.ToLower() == "application/json")
        {
            if (postjson.StartsWith("{"))
            {
                JsonData jd = JsonMapper.ToObject(postjson);
                ShareData shareData = new ShareData();
                shareData.title = jd["title"].ToString();
                shareData.desc = jd["desc"].ToString();
                shareData.link = jd["link"].ToString();
                shareData.imgurl = jd["imgurl"].ToString();
                WeChatHelper.getShareData(shareData);
                JsonData json = new JsonData();
                json["appid"] = shareData.appid;
                json["timestamp"] = shareData.timestamp;
                json["noncestr"] = shareData.noncestr;
                json["signature"] = shareData.signature;
                eBase.WriteJson(json.ToJson());
                //Response.Write("finish");
            }
        }

        //Response.Write("OK");
        //Response.End();
    }
}