﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="nativePay.aspx.cs" Inherits="eFrameWork.WeChat.nativePay" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script src="../Scripts/jquery.js"></script>
    <script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script src="../Plugins/layui226/layui.all.js"></script>
</head>
<script>
    function now() { date = new Date(); var t = date.getFullYear().toString(); t += (date.getMonth() + 1).toString(); t += (date.getDate()).toString(); t += (date.getHours()).toString(); t += (date.getMinutes()).toString(); t += (date.getSeconds()).toString(); t += (date.getMilliseconds()).toString(); return t; };



    var chkTime = null;
    var json;
    var layerIndex = 0;
    function payFor() {
        var url = "?act=getorder&t=" + now();
        $.ajax({
            type: 'get',
            url: url,
            dataType: "json",
            success: function (data) {

                    json = data;

                    showRQCode(json.url);                   

            }
        });


    };
    function checkOrder() {

        var url = "?act=checkorder&orderid=" + json.orderid + "&t=" + now();
        $.ajax({
            type: 'get',
            url: url,
            dataType: "json",
            success: function (data) {
                    if (data.count != "0") {

                        clearTimeout(chkTime);
                        layer.close(layerIndex);
                        alert("支付成功!");
                    }
            }
        });

    };
    function showRQCode(codeurl) {
        layer.open({
            type: 1 //此处以iframe举例
            , title: "微信支付"
            //,skin: 'layui-layer-molv' //加上边框 layui-layer-rim  layui-layer-lan layui-layer-molv layer-ext-moon
            , shadeClose: false //点击遮罩关闭层
            , area: ["280px", "250px"]
            , shade: 0.2 //透明度
            , maxmin: false
            , resize: true
            , btnAlign: 'l' //lcr
            , moveType: 0 //拖拽模式，0或者1
            , anim: 0 //0-6的动画形式，-1不开启
            , content: '<center style="margin:15px;"><img src="' + codeurl + '"></center>'
            , btn: ['关闭'] //只是为了演示
            , yes: function (index, layero) {
                clearTimeout(chkTime);
                layer.close(index);
            }
            , cancel: function (index, layero) {
                clearTimeout(chkTime);
            }
            , success: function (layero, index) {
                layerIndex = index;
                chkTime = window.setInterval(checkOrder, 2000);
            }
        });
    };

</script>

<body>
      <div>当前位置：<a href="../../default.aspx">首页</a> -> native支付</div>
    <input id="Button1" type="button" onclick="payFor();" value="支付1分" />
</body>
</html>