/**/
//搜索框
function switchSearch()
{
	var sbox = $("#eSearchBox");
	if (sbox.is(":hidden"))
	{
		//sbox.show();
		sbox.slideDown();
    }
	else
	{
		//sbox.hide();
        sbox.slideUp();
    }
};﻿


 $(document).ready(function () {
       

        //$("#treebody").height($("#treebody").height() + "px");
        $("#treeparent").height($(".treeaside").height() + "px");

        $("#sidebody").height($("#sidebody").height() + "px");
        $("#sideparent").height(($(".ui-aside").height() - 59 - ($(".toolmenu") ? $(".toolmenu").height() : 0)) + "px");
        $("#pagebodyb").width($("#pagebodyb").width() + "px");
        $("#pagebodya").width($("#epage").width() + "px");
        var cur = $("#pagebodyb a.cur");
        if (cur.index() > 4) {
            $("#pagebodya").scrollLeft($("#pagebodyb a").eq(cur.index() - 4).get(0).offsetLeft);
        }

        $("#eSearchBox").css("top", "-10000px");
        $("#eSearchBox").css("display", "block");
        $(".eform .text").each(function (index, val) {
            var td = getParent(this, "td");			
            var ele = $(this);
			
			if(ele.parent().attr("etype")=="fileselect")	
			{
				ele.css("width", ($(td).width() - 56) + "px");
				return true;
			}
			if(ele.parent().parent().attr("etype")=="selinput")	
			{
				ele.css("width",  ($(td).width() - 25) + "px");
				return true;
			}
			if(ele.parent().attr("etype")=="searchselect")	
			{
				ele.css("width", "40px");
				return true;
			}
			if(ele.parent().attr("etype")=="dateregion"){
				ele.css("width", "100px");
				return true;
			}
			if(ele.parent().attr("etype")=="date"){
				ele.css("width", $(td).width() + "px");
				return true;
			}
            if (this.getAttribute("eautowidth") == null)
            {
                ele.css("width", $(td).width() + "px");
            }
        });
        $(".eform i:not([class])").hide();
		
        $(".eform select").each(function (index, val) {
            var td = getParent(this, "td");
            var ele = $(this);
			if(ele.parent().attr("etype")=="searchselect")	
			{
				ele.css("width", (  $(td).width() -70 ) + "px");
				return true;
			}
           
            ele.css("width", ($(td).width()) + "px");
        });
        $(".eform textarea").each(function (index, val) {
            var td = getParent(this, "td");
            var ele = $(this);
			//移动端编辑器要去掉
				if(ele.parent().attr("etype")=="textarea")
				{
					//alert(ele.parent().attr("etype"));
					ele.css("min-width", ($(td).width() - 16) + "px");
            		ele.css("width", $(td).width() + "px");
				}
				else
				{
					//alert(55);
				}
            
        });

        $(".eform iframe").each(function (index, val) {
            var td = getParent(this, "td");
            var ele = $(this);
            ele.attr("width", ($(td).width()) + "px");
        });
        $("#eSearchBox").css("top", "0px");
        $("#eSearchBox").css("display", "none");
     
	 
	//选项卡
	
	 function setCurrentSlide(ele, index) {
            $(".swiper_title .swiper-slide").removeClass("selected");
            ele.addClass("selected");
        };
       
        var swiper_title = new Swiper('.swiper_title', {
            slidesPerView: 4,
            paginationClickable: true,
            spaceBetween: 0,
            freeMode: true,
            loop: false,
            onTab: function (swiper) { var n = swiper_title.clickedIndex; }
        });
        if (swiper_title.slides) {
            swiper_title.slides.each(function (index, val) {
                var ele = $(this);
                ele.on("click", function () {
                    setCurrentSlide(ele, index);
                    swiper_body.slideTo(index, 500, false);
                });
            });
        }
        var swiper_body = new Swiper('.swiper_body', {
            direction: 'horizontal',
            loop: false,
            autoHeight: false,
            onSlideChangeEnd: function (swiper) {
                var n = swiper.activeIndex;
                setCurrentSlide($(".swiper_title .swiper-slide").eq(n), n);
                swiper_title.slideTo(n, 500, false);
            }
        });
	 
        var _count = 0;
	 //顶部菜单和目录树
	  var asideLift = $(".ui-aside").asideUi({
            hasmask: true,
            size: "5rem",
            position: "left",
            sidertime: 300
        });
     //.unbind("touchstart")
	  $(".eHeader .menu").on("click", function () {
	     // _count++;
	      //$(".eHeader .center").html(_count);
            asideLift.toggle();
        });

        var asidetree = $(".treeaside").asideUi({
            hasmask: true,
            size: "5rem",
            position: "left",
            sidertime: 300
        });
     //.unbind("touchstart")
     $(".treemenu").on("click", function () {
            asidetree.toggle();
        });
	 
	 
    });