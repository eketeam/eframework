﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LitJson;
using EKETEAM.Tencent.WeChat;
using EKETEAM.FrameWork;

namespace eFrameWork.WeChat
{
    public partial class jsPay : System.Web.UI.Page
    {
        public string UserArea = "Application";    
        public eUser user;        
        private static string _wxJsApiParam = "";
        public static string wxJsApiParam
        {
            get { return _wxJsApiParam; }
            set { _wxJsApiParam = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            user = new eUser(UserArea);
            
            string act = eParameters.QueryString("act");

            if (act == "getpayinfo")
            {
                double je = Convert.ToDouble(eParameters.Form("je"));
                je = je * 100;
                string openid = user["openid"].ToString();
                string total_fee = je.ToString();

                JsApiPay jsApiPay = new JsApiPay(this);
                
                jsApiPay.openid = openid;
                jsApiPay.total_fee = int.Parse(total_fee);

                try
                {
                    string _orderid = Guid.NewGuid().ToString();
                    if (eParameters.QueryString("orderid").Length > 0) _orderid = eParameters.QueryString("orderid");
                    string attach = user.ID + "," + _orderid + "," + "id";
                    WxPayData unifiedOrderResult = jsApiPay.GetUnifiedOrderResult(attach);
                    wxJsApiParam = jsApiPay.GetJsApiParameters(); 

                    JsonData json = JsonMapper.ToObject(wxJsApiParam);
                    json["orderid"] = _orderid;
                    Response.Write(json.ToJson().ToString());

                }
                catch( Exception ex)
                {
                    JsonData json = new JsonData();
                    json["msg"] = ex.Message;
                    Response.Write(json.ToJson().ToString());
                }
                Response.End();
            }

        }
    }
}