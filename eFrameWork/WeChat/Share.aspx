﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Share.aspx.cs" Inherits="eFrameWork.WeChat.Share" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>分享</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <script src="../Scripts/jquery.js?ver=1.0.61"></script>

    <script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
</head>
<script>
    $(document).ready(function () {
       var jsApiListArr = ["checkJsApi", "onMenuShareTimeline", "onMenuShareAppMessage"];
       wx.config({ debug: false, appId: "<%=shareData.appid %>", timestamp: "<%=shareData.timestamp %>", nonceStr: "<%=shareData.noncestr %>", signature: "<%=shareData.signature%>", jsApiList: jsApiListArr });
       var shareData = {
            title: "<%=shareData.title %>",
            desc: "<%=shareData.desc %>",
            link: "<%=shareData.link %>",
            imgUrl: "<%=shareData.imgurl %>",
            success: function (res) {
                //alert('已分享');
            },
            cancel: function (res) {
            },
            fail: function (res) {
                alert(JSON.stringify(res));
            }
        };
        wx.ready(function () {
            wx.onMenuShareTimeline(shareData); //分享到朋友圈
            wx.onMenuShareAppMessage(shareData);//发送给朋友
        });
   });
</script>
<body>

请使用微信右上角功能菜单进行测试
</body>
</html>