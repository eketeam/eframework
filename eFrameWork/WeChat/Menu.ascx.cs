﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;

namespace eFrameWork.WeChat
{
    public partial class Menu : System.Web.UI.UserControl
    {
        private eUser user;
        public string AppItem = eParameters.Request("AppItem");
        ApplicationMenu appmenu;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Application");
            user.Check();
            appmenu = new ApplicationMenu(user, "2");
            LitMenu.Text = appmenu.getMenus("");
        }
    }
}