﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using EKETEAM.Tencent.WeChat;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.WeChat
{
    public partial class jsPayNotifyPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ResultNotify resultNotify = new ResultNotify(this);

            #region 返回数据
            try
            {

               
                WxPayData notifyData = resultNotify.GetNotifyData();

                eBase.AppendLog(resultNotify.WxPayDataString);

                string transaction_id = notifyData.GetValue("transaction_id").ToString();
                string out_trade_no = notifyData.GetValue("out_trade_no").ToString();
                string attach = notifyData.GetValue("attach").ToString();
                string[] attachs = attach.Split(",".ToCharArray());
                eBase.AppendLog(transaction_id);

                string tmp = eBase.DataBase.getValue("select count(*) from eWeb_PayLog where transaction_id='" + transaction_id + "' and DATEDIFF(dd,addTime,getdate())<2");

                if (tmp == "0")
                {
                    eTable etab = new eTable("eWeb_PayLog");
                    if (attachs.Length > 0) etab.Fields.Add("UserID", attachs[0]);
                    if (attachs.Length > 1) etab.Fields.Add("OrderID", attachs[1]);
                    //if (attachs.Length > 2) etab.Fields.Add("ProductID", attachs[2]);
                    etab.Fields.Add("transaction_id", transaction_id);
                    etab.Fields.Add("out_trade_no", out_trade_no);
                    etab.Fields.Add("trade_type", notifyData.GetValue("trade_type").ToString());
                    double money = Convert.ToDouble(notifyData.GetValue("total_fee").ToString()) / 100;
                    etab.Fields.Add("money", money.ToString());
                    etab.Fields.Add("Result", resultNotify.WxPayDataString);
                    //<xml><appid><![CDATA[wx85b0bf2351900f84]]></appid> <attach><![CDATA[295b61c2-a2e8-4a52-a4cf-3c26184303a9,5b126d36-92bf-438a-bd76-b5352ab747c8,96a2fb3d-ccb0-45d8-a917-4170b3e5fd01]]></attach> <bank_type><![CDATA[CFT]]></bank_type> <cash_fee><![CDATA[36]]></cash_fee> <fee_type><![CDATA[CNY]]></fee_type> <is_subscribe><![CDATA[N]]></is_subscribe> <mch_id><![CDATA[1513756691]]></mch_id> <nonce_str><![CDATA[462381480]]></nonce_str> <openid><![CDATA[ozfMB1HeQYndqes6ewfNzvWzVW7o]]></openid> <out_trade_no><![CDATA[151375669120190422120043526]]></out_trade_no> <result_code><![CDATA[SUCCESS]]></result_code> <return_code><![CDATA[SUCCESS]]></return_code> <sign><![CDATA[3607D4514C7A35F05BDD6D4A885C7D8965C2810D12918F49D9D560B2E51B87AE]]></sign> <time_end><![CDATA[20190422120051]]></time_end> <total_fee>36</total_fee> <trade_type><![CDATA[JSAPI]]></trade_type> <transaction_id><![CDATA[4200000310201904224869829051]]></transaction_id> </xml>
                    //<xml><appid><![CDATA[wx85b0bf2351900f84]]></appid><attach><![CDATA[28062bb3-f0b1-41ac-aaeb-5e695ee41acd,9620a6df-28b4-4153-b488-4aa684eaf370,id]]></attach><bank_type><![CDATA[OTHERS]]></bank_type><cash_fee><![CDATA[1]]></cash_fee><fee_type><![CDATA[CNY]]></fee_type><is_subscribe><![CDATA[Y]]></is_subscribe><mch_id><![CDATA[1513756691]]></mch_id><nonce_str><![CDATA[115729563]]></nonce_str><openid><![CDATA[ozfMB1AkkcEMEXVav680Y_lOhp80]]></openid><out_trade_no><![CDATA[151375669120200315003235445]]></out_trade_no><result_code><![CDATA[SUCCESS]]></result_code><return_code><![CDATA[SUCCESS]]></return_code><sign><![CDATA[0DBD964190CC14FB0F9845C112295391602595963E2DBF8EC7239BBE210588B8]]></sign><time_end><![CDATA[20200315003247]]></time_end><total_fee>1</total_fee><trade_type><![CDATA[JSAPI]]></trade_type><transaction_id><![CDATA[4200000490202003155471463883]]></transaction_id></xml>";
                    etab.Add();

                }

                //此时还没提交订单
                //if (attachs.Length > 1)  eOleDB.Execute("update eWeb_Orders set transaction_id='" + transaction_id + "' where OrderID='" + attachs[1] + "'");
            }
            catch
            {
            }
            #endregion

            resultNotify.ProcessNotify();
        }
    }
}