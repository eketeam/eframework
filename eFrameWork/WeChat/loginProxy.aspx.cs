﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using EKETEAM.Tencent.WeChat;
using System.Web.Caching;
using System.Net;
using System.IO;
using System.Text;
using LitJson;

public partial class WeChat_loginProxy : System.Web.UI.Page
{
    public string redirect_uri = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string fromURL = eParameters.QueryString("fromURL");//登录来源页面URL
        string _fromURL = HttpUtility.UrlDecode(fromURL);
        if (!_fromURL.ToLower().StartsWith("http"))
        {
            JsonData jd = new JsonData();
            jd.Add("success", "1");
            jd.Add("errcode", "1");
            jd.Add("message", "路径不正确!");
            string _base64 = Base64.Encode(jd.ToJson());
            fromURL = HttpUtility.UrlDecode(fromURL);
            fromURL = fromURL.addQuery("data", _base64);
            Response.Redirect(fromURL, true);
        }
        string[] arr = _fromURL.Split("/".ToCharArray());
        string host = arr[2].ToLower();
        if (eBase.WeChatAccount.getValue("ReverseProxy").ToLower().IndexOf(host) == -1)
        {
            JsonData jd = new JsonData();
            jd.Add("success", "1");
            jd.Add("errcode", "1");
            jd.Add("message", "没有绑定域名!");
            string _base64 = Base64.Encode(jd.ToJson());
            fromURL = HttpUtility.UrlDecode(fromURL);
            fromURL = fromURL.addQuery("data", _base64);
            Response.Redirect(fromURL, true);
        }
        if (eBase.WeChatAccount.getValue("AppID").Length == 0 || eBase.WeChatAccount.getValue("AppSecret").Length == 0)
        {
            JsonData jd = new JsonData();
            jd.Add("success", "1");
            jd.Add("errcode", "1");
            jd.Add("message", "没有绑定域名、公众号或绑定信息不完整!");
            string _base64 = Base64.Encode(jd.ToJson());
            fromURL = HttpUtility.UrlDecode(fromURL);
            fromURL = fromURL.addQuery("data", _base64);
            Response.Redirect(fromURL, true);
        }
        eFileInfo fi = new eFileInfo(Request.Url.PathAndQuery);
        string folderPath = fi.Path;
        //redirect_uri = Request.Url.Scheme + "://" + Request.Url.Host;
        //redirect_uri += (Request.Url.Port == 80 || Request.Url.Port == 443 ? "" : ":" + Request.Url.Port.ToString()) + folderPath + "loginProxy.aspx";
        redirect_uri = Request.Url.Scheme + "://" + eRequest.fromHost;
        redirect_uri += (eRequest.fromPort == 80 || eRequest.fromPort == 443 ? "" : ":" + eRequest.fromPort.ToString()) + folderPath + "loginProxy.aspx";


        if (fromURL.Length > 0) redirect_uri += "?fromURL=" + HttpUtility.UrlEncode(HttpUtility.UrlEncode(fromURL)); // HttpUtility.UrlDecode(fromURL);
        redirect_uri = HttpUtility.UrlEncode(redirect_uri);


        string code = Request["code"];
        if (string.IsNullOrEmpty(code))
        {
            OpenAccess();
        }
        JsonData json = WeChatHelper.getAccessToken(code);
        string access_token = json.GetValue("access_token");
        string openid = json.GetValue("openid");
        string unionid = json.GetValue("unionid");
        if (openid.Length == 0 && unionid.Length == 0)
        {
            JsonData jd = new JsonData();
            jd.Add("success", "1");
            jd.Add("errcode", "1");
            jd.Add("message", "登录失败,请与企业微信管理员联系!");
            string _base64 = Base64.Encode(jd.ToJson());
            fromURL = HttpUtility.UrlDecode(fromURL);
            fromURL = fromURL.addQuery("data", _base64);
            Response.Redirect(fromURL, true);
        }

        json = WeChatHelper.getUserInfo(access_token, openid);

        string base64 = Base64.Encode(json.ToJson());
        fromURL = HttpUtility.UrlDecode(fromURL);
        fromURL = fromURL.addQuery("data", base64);
        Response.Redirect(fromURL, true);
    }
    private void OpenAccess()
    {
        string url = string.Format("https://open.weixin.qq.com/connect/oauth2/authorize?appid={0}&redirect_uri={1}&response_type=code&scope=snsapi_userinfo&m=oauth2#wechat_redirect", eBase.WeChatAccount.getValue("AppID"), redirect_uri);
        Response.Redirect(url);
    }
}