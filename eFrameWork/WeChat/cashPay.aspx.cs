﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.Tencent.WeChat;



namespace eFrameWork.WeChat
{
    public partial class cashPay : System.Web.UI.Page
    {
        public eUser user;


        string act = eParameters.QueryString("act");
        private static string _wxJsApiParam = "";
        public static string wxJsApiParam
        {
            get { return _wxJsApiParam; }
            set { _wxJsApiParam = value; }
        }
      
        
      

        public string UserArea = "Application";    
        protected void Page_Load(Object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            user.Check();



            #region 提现
            if (Request.Form["je"] != null)
            {
                string cashResult = Enterprise.Pay("1111", Convert.ToString(Convert.ToDouble(Request.Form["je"]) * 100), user["openid"].ToString(), user["nick"].ToString(), "提现");
               // eBase.AppendLog(cashResult);
                WxPayData data = new WxPayData();
                data.FromXml(cashResult);

                if (data.GetValue("return_code").ToString().ToUpper() == "SUCCESS")
                {


                    if (data.GetValue("result_code").ToString().ToUpper() == "SUCCESS")
                    {
                        eResult.Success("提现成功!");    
                    }
                    else
                    {
                        string msg = data.GetValue("err_code_des").ToString();
                        if (msg == "余额不足") msg = "平台帐户余额不足,请与我们联系。";

                        eResult.Success(msg);  


                    }

                }


            }
            #endregion




        }
    }
}