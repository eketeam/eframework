﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;

namespace eFrameWork.WeChat
{
    public partial class Model : System.Web.UI.Page // System.Web.UI.Page   // EKETEAM.UserControl.ePage
    {
        public string UserArea = "Application";
        public eModel model;
        public eUser user;
        DataRow appRow;
        public bool Ajax = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ajax"] != null) Ajax = Convert.ToBoolean(Request.QueryString["ajax"]);
            user = new eUser(UserArea);
            user.Check();
          
            model = new eModel();
            model.clientMode = "mobile";
            //eFHelper.modelHandle(model, LitBody);//进入模块统一处理过程
            LitBody.Text = model.autoHandle();
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null) lit.Text = eConfig.ApplicationTitle(user["SiteID"].ToString()); // model.ModelInfo["mc"].ToString() + " - " +

            lit = (Literal)Master.FindControl("LitJavascript");
            if (lit != null && model.Javasctipt.Length > 0) lit.Text = model.Javasctipt;

            lit = (Literal)Master.FindControl("LitStyle");
            if (lit != null && model.cssText.Length > 0) lit.Text = model.cssText; 
        }
    }
}