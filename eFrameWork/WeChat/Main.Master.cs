﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;


namespace eFrameWork.WeChat
{
    public partial class Main : System.Web.UI.MasterPage  // System.Web.UI.MasterPage   // EKETEAM.UserControl.eMasterPage
    {
        public string AppItem = eParameters.Request("AppItem");
        public eUser user;
        public bool Ajax = false;
        public string appTitle = "";
        public eModel model;
        protected void Page_Init(object sender, EventArgs e)
        {
            user = new eUser("Application");
            user.Check();//检测用户是否登录,未登录则跳转到登录页
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ajax"] != null) Ajax = Convert.ToBoolean(Request.QueryString["ajax"]);         
            appTitle = eConfig.ApplicationTitle(user["SiteID"].ToString());
            model = new eModel();
        }
    }
}