//生成HTML
function createhtml(id)
{
	var url="CreateHTML.aspx?ColumnID=" + id;
	layer.open({
      type: 2,
	  skin: 'layui-layer-rim', //加上边框
      title: "生成静态HTML",
      maxmin: false,
      shadeClose: true, //点击遮罩关闭层
      area : ['200px' , '200px'],
      content: url,
	  success: function(layero, index)
	  {
		arrLayerIndex.push(index);
  	  }
    });
};
function setColumnType(obj)
{
	value= obj.checked ? "1" : "0";
	var url="?ModelID=" + ModelID + "&mid=" + obj.value + "&act=setcolumntype&value=" + value;
	$.ajax({
			type: "get",
			url: url,			
			dataType: "html",
			success: function(html)
			{
				
			}
	});
};
function openCheck(obj)
{
	value= obj.checked ? "1" : "0";
	var url="?ModelID=" + ModelID + "&mid=" + obj.value + "&act=setcheck&value=" + value;
	$.ajax({
			type: "get",
			url: url,			
			dataType: "html",
			success: function(html)
			{
				
			}
	});
};
function getValue(obj)
{
    if (obj.type.toLowerCase() == "checkbox")
	{
		return (obj.checked ? "1" : "0");
	}
	else if(obj.type.toLowerCase() == "radio")
	{
		return obj.value;
	}
	else 
	{
		return obj.value.encode();
	}
};
//复制模型
function copyModel(ID)
{
	
	//formType:0 文本,1 密码 2：多行文本
	layer.prompt({title: '输入数据表名', formType: 3,value:'eWeb_'},function(value, index){
		var url="?act=copy&modelid=" + ModelID + "&id=" + ID + "&code=" + value;
		$.ajax({
			type: "get",
			url: url,			
			dataType: "json",
			success: function(data)
			{		
				layer.msg(data.message);
				
				if(data.success=="1")
				{					
					layer.close(index);
					setTimeout(function(){document.location.reload();},500);
				}
				

			}
		});
	  	
	});
};
//设置列
function setModelItem(obj,ModelItemID,Item)
{
	if (obj.getAttribute("oldvalue") == obj.value && obj.type.toLowerCase()!="checkbox" && obj.type.toLowerCase()!="radio" ) { return; }
	var value = getValue(obj);
	if (value == "error") { return; }
	var _reload=parseBool(Attribute(obj,"reload"));
	showloading();
	var url = "?act=setmodelitem&modelid=" + ModelID + "&id=" + ID + "&modelitemid=" + ModelItemID + "&item=" + Item + "&t=" + now();
	$.ajax({
            type: "post", 
			async: true,
			data:{value:value},
            url: url,
            dataType: "html",
            success: function (data) {
				obj.setAttribute("oldvalue", obj.value);
				hideloading();
				if(!_reload){return;}
                loadData($(obj).parents("div[dataurl]"));
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
				//hideloading();
            }
    });
};
//删除列
function delModelItem(ModelItemID)
{
	if (!confirm('确认要删除吗？')) { return; }
	showloading();
	var url = "?act=delmodelitem&modelid=" + ModelID + "&id=" + ID + "&modelitemid=" + ModelItemID + "&t=" + now();
	$.ajax({
            type: "post", 
			async: true,
            url: url,
            dataType: "html",
            success: function (data) {
				hideloading();
				init();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
    });
};
var layeridx=0;
function show_addItem()
{
	var url="ModelItems.aspx?modelid=" + ModelID + "&id=" + ID + "&act=add&ajax=true";
	layer.open({
      type: 2,
	  //skin: 'layui-layer-rim', //加上边框
      title: "添加字段",
      maxmin: false,
      shadeClose: true, //点击遮罩关闭层
      area : ["500px" , "250px"],
	  //scrollbar: false, 
	  //move: false,		
	  content: [url,'no'], 
	  //content: url,
	  success: function(layero, index)
	  {
		  layeridx=index;
		//arrLayerIndex.push(index);
  	  }
    });	
};
//根据数据模型加载列
function getColumns(id)
{
	var url=document.location.href;
	url=url.addquerystring("act","getcolumns");
	url=url.addquerystring("mid",id);
	$.ajax({
			type: "get",
			url: url,
			dataType: "json",
			success: function(data)
			{		
				//for(var i=0;i < data.data.length; i++)
				if(data.success=="1")		
				{
					ColumnsJson=data.data;
					$("#fields").html('');
					$.each(data.data, function()
					 {
						 var html='<a href="javascript:;" class="htmltag" onclick="addHtmlTag(\'f4\',\'{' + (this["text"] == '内容连接' ? 'url' : 'data') + ':'+ this["value"] +'('+ this["text"] +')}\');">' + this["text"] + '</a>';
						 $("#fields").append(html);
					 });						
				}
			}
	});
};
function addHtmlTag(id,txt)
{
	var obj=document.getElementById(id);	
	if (obj.selectionStart !== undefined)
	{
		var startPos = obj.selectionStart;
		var endPos = obj.selectionEnd;
        obj.value = obj.value.substring(0, startPos) + txt + obj.value.substring(endPos, obj.value.length);
		obj.focus();
        obj.selectionStart = startPos + txt.length;
        obj.selectionEnd = startPos + txt.length;
	}
	else
	{
		  if(document.selection) 
		  { 		  
		  	 obj.focus();
             var sel = document.selection.createRange();
             sel.text =  txt;
		  }
		  else
		  {
		  	obj.value=[obj.value,txt].join("");
		  }
	}	
};

function save_column()
{
	var url= document.location.href;
	$.ajax({
			type: "post",
			url: url,			
			data: $("#frmaddoredit").serialize(),
			dataType: "json",
			success: function(data)
			{

				if(data.success=="1")
				{
					parent.closelayer();
					parent.alert(data.message);
					parent.init();
				}
			}
	});
	
};
function closelayer()
{
	layer.close(layeridx);
};
function showloading()
{
	$("#divloading").show();
};
function hideloading()
{
	$("#divloading").hide();
};
//选项卡切换
function selecttab(a) 
{
        $(a).parent().find("a").attr("class", "");		
        $(a).attr("class", "cur");		
        var index = $(a).index();
		
		
        var dd = $(a).parent().next();
        dd.find("div").hide();
        //var div=dd.find("div:eq(" + index + ")"); //DIV下DIV也算在内
        var div = dd.children("div:eq(" + index + ")");
				
        var loaded = div.attr("loaded");
        

		
        if (loaded == undefined) {
            loadData(div);
        }
        div.show();
};
function eDataTable_event(div)
{
	//基本设置
	var tb1=getobj("eDataTable_Basic");
	if(tb1)
	{
		tb1=new eDataTable("eDataTable_Basic",1);
		tb1.moveRow=function(index,nindex)
		{
			
			$("#eDataTable_Basic tbody tr td:first-child").each(function(index1,obj){
				$(obj).html(1+index1);
			}); 
			var ids="";
			//$("#eDataTable_Basic tbody tr td:first-child input:checked").each(function(index1,obj){	
				//if(index1>0){ids+=",";}
				//ids+=$(obj).parent().parent().attr("erowid");
			//}); 
			$("#eDataTable_Basic tbody tr td:first-child").each(function(index1,obj){	
				if(index1>0){ids+=",";}
				ids+=$(obj).parent().attr("erowid");
			}); 
			
			
			if(ids.length==0){return;}


			showloading();
			var _url = "?act=setmodelitem&modelid=" + ModelID + "&id="+ ID +"&item=setorders&t=" + now();			
			$.ajax({
				type: "POST", async: true,
				data:{ids:ids},
				url: _url,
				dataType: "json",
				success: function (data) 
				{
					hideloading();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					hideloading();
				}
   			});
		};
	}
	var tb2=getobj("eDataTable_Senior");
	if(tb2)
	{
		tb2=new eDataTable("eDataTable_Senior",1);
	}
};
//加载数据
function loadData(div)
{
	showloading();
	$.ajax({
		   type: "GET", async: true,
           url: div.attr("dataurl") + "&t=" + now(),
           dataType: "html",
           success: function (data) {
			    div.html(data);
				eDataTable_event(div);
				hideloading();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //alert("error");
				hideloading();
                div.show();
            }
        });
};
function init()
{
	if($(".eTab dd").length > 0)
	{
		loadData($(".eTab dd").find("div").eq(0));
	}
};
$(document).ready(function () {
	init();
});