﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;

public partial class AppOld_anonymousLogin : System.Web.UI.Page
{
    public string UserArea = "Application";
    protected void Page_Load(object sender, EventArgs e)
    {
        string fromURL = "Login.aspx";
        string sql = "Select * From a_eke_sysUsers Where UserID='6231f6b6-a25d-4c7f-ac8a-910e5fce722333'";//设置好权限的匿名用户ID
        DataTable tb = eBase.UserInfoDB.Select(sql);
        if (tb.Rows.Count == 0)
        {
            Response.Write("<script>alert('登录信息不正确！');document.location='" + fromURL + "';</script>");
            Response.End();
        }
        else
        {
            eUser user = new eUser(UserArea);
            eFHelper.saveUserInfo(user, tb.Rows[0]); //保存用户登录信息
            eFHelper.UserLoginLog(user); //用户登录日志
            Response.Redirect("Default.aspx", true);
        }
    }
}