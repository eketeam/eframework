﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.AppOld
{
    public partial class Menu : System.Web.UI.UserControl
    {
        private eUser user;
        public string AppItem = eParameters.Request("AppItem");
        ApplicationMenu appmenu;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Application");
            appmenu = new ApplicationMenu(user,"1");
            litMenu.Text = appmenu.getMenus("", 1, "_blank");
        }
    }
}