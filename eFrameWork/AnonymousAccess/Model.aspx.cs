﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;

public partial class AnonymousAccess_Model : System.Web.UI.Page
{
    public string ModelID = eParameters.Request("modelid");
    public eModel model;
    protected void Page_Load(object sender, EventArgs e)
    {
        eUser user = new eUser("", "00000000-0000-0000-0000-000000000000");
        user["id"] = "00000000-0000-0000-0000-000000000000";
        user["siteid"] = "1";
        user["Name"] = "匿名用户";        
        user.Expires = DateTime.Now.AddDays(1);
        model = new eModel(ModelID, user);
        if (eBase.IsMobile()) model.clientMode = "mobile";
        if (!eBase.parseBool(model.ModelInfo["AnonymousAccess"]))
        {
            LitBody.Text = "<div style=\"margin:20px 0px 0px 20px;font-size:16px;color:#ff0000;text-align:center;\">无权查看!</div>";
            return;
        }
        if (model.Power.Contains("list")) model.Power["list"] = true;
        if (model.Power.Contains("view")) model.Power["view"] = true;
        LitBody.Text = model.autoHandle();
        if (model.Javasctipt.Length > 0) LitJavascript.Text = model.Javasctipt;
        if ( model.cssText.Length > 0) LitStyle.Text = model.cssText;
    }
}