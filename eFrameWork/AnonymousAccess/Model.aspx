﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Model.aspx.cs" Inherits="AnonymousAccess_Model" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%=(eBase.parseBool(model.ModelInfo["AnonymousAccess"]) ? model.ModelInfo["mc"].ToString() : "无权查看!") %></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" />
    <meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no"/>
    <link href="../Plugins/layui226/css/layui.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
      <%if(eBase.IsMobile()){ %>
    <link href="../Plugins/eControls/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
    <script src="../Plugins/Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>
    <%}else{ %>
      <link href="../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />  
    <%} %>
    <link href="../Plugins/Theme/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
	<link href="../Plugins/Theme/manage/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
	<script src="../Scripts/Init.js?ver=<%=Common.Version %>"></script>
</head>
<asp:Literal ID="LitJavascript" runat="server" />
<asp:Literal ID="LitStyle" runat="server" />
<body>
<%if(eBase.parseBool(model.ModelInfo["AnonymousAccess"])) {%>
<%= model.StartHTML  %>
<%= model.Tip.Length==0? "" : "<div style=\"margin:8px 8px 0px 8px;\">" + model.Tip + "</div>"  %>
<%}%>
    <asp:Literal ID="LitBody" runat="server" />
</body>
</html>