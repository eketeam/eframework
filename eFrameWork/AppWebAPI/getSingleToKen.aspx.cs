﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;

public partial class AppWebAPI_getSingleToKen : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string UserArea = eParameters.Form("area");
        if (UserArea.Length == 0) eResult.Message(new { success = 1, errcode = "-1", message = "请求失败!" });
        eUser user = new eUser(UserArea);
        if (!user.Logined) eResult.Message(new { success = 1, errcode = "-1", message = "用户登录失效!" });
        string data = user.getSingleToKen();      
        eResult.Message(new { success = 1, errcode = "0", message = "请求成功!",data=data });
    }
}