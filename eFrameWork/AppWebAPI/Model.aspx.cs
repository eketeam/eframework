﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using LitJson;

namespace eFrameWork.AppWebAPI
{
    public partial class Model : System.Web.UI.Page
    {
        public string AppItem = eParameters.Request("AppItem");
        public string ModelID = eParameters.Request("modelid");     
        protected void Page_Load(object sender, EventArgs e)
        {
            eUser user;
            eModel model;           
            if (Request.Headers["auth"] == null) eResult.Message(new { success = 1, errcode = "1", message = "未携带eToKen!" });
            string auth = Request.Headers["auth"].ToString();
            if (auth.Length==0) eResult.Message(new { success = 1, errcode = "1", message = "未携带eToKen!" });
   


            DataRow[] appRows = eBase.a_eke_sysApplicationItems.Select("ApplicationItemID='" + AppItem + "'");
            if (appRows.Length == 0) eResult.Message(new { success = 1, errcode = "1", message = "参数错误!" }); 
            if(ModelID.Length==0) ModelID = appRows[0]["ModelID"].ToString();

            if (auth == "anonymous")
            {
                user = new eUser("", "00000000-0000-0000-0000-000000000000");
                user.Expires = DateTime.Now.AddDays(1);
                model = new eModel(AppItem, ModelID, user);
                model.Power["add"] = model.AnonymousAdd;
                model.Power["list"] = model.AnonymousList;
                model.Power["view"] = model.AnonymousView;
                model.Power["del"] = model.AnonymousDelete;
                model.Power["edit"] = model.AnonymousEdit;             
            }
            else
            {
                eToken token = new eToken(auth);
                user = new eUser(token);
                model = new eModel(AppItem, ModelID, user);
            }

            model.Ajax = true;
            model.clientMode = "webapi";
            if (Request.ContentType.ToLower().IndexOf("/json") > -1)
            {
                model.WebAPISave();
            }
            else
            {
                switch (model.Action)
                {
                    case "":
                        model.WebAPIList();
                        break;
                    case "edit":
                        model.WebAPIEdit();
                        break;
                    case "apiedit":
                        model.WebAPIEdit();
                        break;
                    case "view":
                        model.WebAPIView();
                        break;
                    case "apiview":
                        model.WebAPIView();
                        break;
                    case "save":
                        model.WebAPISave();
                        break;
                    case "apisave":
                        model.WebAPISave();
                        break;
                    case "del":
                        model.WebAPIDelete();
                        break;
                    case "apidel":
                        model.WebAPIDelete();
                        break;
                    case "apicontrols":
                        model.WebAPIControls();
                        break;
                }
                eBase.End();
            }
        }
    }
}