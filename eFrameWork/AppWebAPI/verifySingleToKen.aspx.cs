﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;

public partial class AppWebAPI_verifySingleToKen : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string data = eParameters.Form("data");
        if (data.Length < 20) eResult.Message(new { success = 1, errcode = "-1", message = "请求失败!" });
        bool result = eBase.verifySignleToKen(data);
        eResult.Message(new { success = 1, errcode = "0", message = "请求成功!", result = result });
    }
}