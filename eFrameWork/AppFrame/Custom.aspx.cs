﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.AppFrame
{
    public partial class Custom : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public eModel model;
        public bool Ajax = false;
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ajax"] != null) Ajax = Convert.ToBoolean(Request.QueryString["ajax"]);
            eUser user = new eUser(UserArea);
            user.Check();
            ApplicationMenu appmenu = new ApplicationMenu(user,"1");
            eModelInfo customModel = new eModelInfo(user);
            model = customModel.Model;
            model.clientMode = "frame";

            string path = model.ModelInfo["AspxFile"].ToString();
            if (path.IndexOf("?") > -1 || path.ToLower().StartsWith("http"))
            {
                Response.Redirect(path, true);
            }
            else
            {
                eBase.ExecutePage(path, LitBody);
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null) lit.Text = eConfig.ApplicationName(user["SiteID"].ToString()); // model.ModelInfo["mc"].ToString() + " - " +

            lit = (Literal)Master.FindControl("LitJavascript");
            if (lit != null && model.Javasctipt.Length > 0)  lit.Text = model.Javasctipt;


            lit = (Literal)Master.FindControl("LitStyle");
            if (lit != null && model.cssText.Length > 0) lit.Text = model.cssText;
        }
    }
}