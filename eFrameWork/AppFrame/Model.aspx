﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Model.aspx.cs" Inherits="eFrameWork.AppFrame.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%if (!Ajax){ %>
<div class="nav">您当前位置：<a href="<%=eBase.getApplicationHomeURL() %>">首页</a><%= model.ModelLink %>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded){ %><a href="http://frame.eketeam.com\" class="help" target="_blank" title="eFrameWork开发框架">&nbsp;</a><%}%>
<%=model.ActionBottons %></div>
<%= model.StartHTML   %>
<%= model.Tip  %>
<div style="margin-left:1px;">
<%}%>
<asp:Literal ID="LitBody" runat="server" />
<%if (!Ajax && eRegisterInfo.Base == 0 && eRegisterInfo.Loaded && 1==2)
  { %></div>
 <div class="copyright" style="position: absolute;bottom: 0px;text-align: center;height: 35px;line-height: 35px;width: 100%;">技术支持：<a href="http://frame.eketeam.com/" target="_blank">eFrameWork</a></div>
<%}%>
</asp:Content>
