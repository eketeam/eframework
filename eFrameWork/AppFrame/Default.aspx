﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="eFrameWork.AppFrame.Default" %>
<%@ Register Src="Menu.ascx" TagName="Menu" TagPrefix="uc1" %><!DOCTYPE html>
<html>
<head>
<title><%=appTitle%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=9;IE=8;IE=7;" />
	<meta http-equiv="X-UA-Compatible" content="edge" /><!--标准模式-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
	<meta name="renderer" content="webkit"><!--极速模式-->
	<meta name="renderer" content="webkit|ie-comp|ie-stand"> <!--极速模式，兼容模式，IE模式打开-->  
    <link href="../Plugins/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../Plugins/layui226/css/layui.css" rel="stylesheet" type="text/css" />     
    <link href="../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />     
    <link href="../Plugins/Theme/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
    <link href="style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
</head>
<script src="../Scripts/Init.js?ver=<%=Common.Version %>" src2="../Scripts/Init.js?ver=<%=Common.Version %>"></script>
 <%=Common.LoadJS %>
<script src="../Scripts/jquery.nicescroll.min.js?ver=<%=Common.Version %>"></script>
<script src="../Plugins/Theme/default/layout.js?ver=<%=Common.Version %>"></script>
<script>
    var layout_setFixed_file = "default.aspx";
    function bindFrameTag(filter)
    {
        $(filter).each(function (i, el) {
            if ($(this).attr("href").indexOf("javascript") > -1) { return true; }
            if ($(this).attr("href").toLowerCase().indexOf("loginout.aspx") > -1) { return true; }
            $(this).attr("_href", $(this).attr("href"));
            $(this).attr("href", "javascript:;");
            el.onclick = function () {
                $(".emenu div.cur").removeClass("cur");
                $(el).parents("div:first").addClass("cur");
                var _url = $(el).attr("_href");
                var _text = $(el).text();
                eFrameTab_add(_url, _text);
            };
        });
    }
    $(document).ready(function () {
        bindFrameTag(".emenu a,.eLayout_top a");
    });
    function getAppMenu(obj,appid)
    {
        $(".applists a.cur").removeClass("cur");
        $(obj).addClass("cur");
        var url = "default.aspx?act=getappmenu&appid=" + appid;
        $.ajax({
            type: 'get',
            url: url,
            dataType: "html",
            success: function (data) {
                $(".menu_body").html(data);
                bindFrameTag(".emenu a");
            }
        });
    };
</script>
<body>
<div class="eLayout_top<%=(TopFixed? " eLayout_top_fixed" : " eLayout_top_float") %>">
    <a href="javascript:;" onclick="switchTopFix();" class="topfixbtn<%=(TopFixed? " topfixbtn_fixed" : " topfixbtn_float") %>"></a>
    <div class="appname"><%= sysNameorLogo %></div>
    <div class="applists"><%=appNames %></div>

    <div class="userinfo">
         <%if (row_acc != null && user["userpid"].ToString().Length>0)
            { %>
        <a class="switch_user" href="<%=row_acc["href"].ToString() %>">切换帐号</a>
         <%}%>
        <%if(1==2){%>	
        <span class="username"><%= user["Name"] %></span>
        <a href="ModifyPass.aspx?AppItem=<%=AppItem%>" class="chgpass">修改密码</a>
        <a href="LoginOut.aspx?AppItem=<%=AppItem%>" onclick="return confirm('确认要退出登录吗？');" class="loginout">退出登录</a>
        <%}%>
        <span class="popmenu" href="javascript:;">
            <img src="<%= user["face"]  %>" onerror="this.src='../images/head.png';"><%= user["name"]%>
		<ul>
              <%if (row_info != null)
            { %>
		<li><a href="<%=row_info["href"].ToString() %>" class="userinfo"><span><%=row_info["ModelName"].ToString() %></span></a></li>
             <%}%>
             <%if (row_pass != null)
            { %>
        <li><a href="<%=row_pass["href"].ToString() %>" class="modifypass"><span><%=row_pass["ModelName"].ToString() %></span></a></li>
             <%}%>
		<li><a href="LoginOut.aspx?AppItem=<%=AppItem%>" class="loginout" onClick="javascript:return confirm('确认退出登录吗？');"><span>退出登录</span></a></li>
		</ul>
		</span>
    </div> 
</div>
<div class="eLayout_body<%=(TopFixed? " eLayout_body_fixed" : " eLayout_body_float") %>">
<div class="eLayout_menu <%=(LeftFixed? " eLayout_menu_fixed" : " eLayout_menu_float") %>">
<a hef="javascript:;" onclick="switchLeftFix();" class="leftfixbtn<%=(LeftFixed? " leftfixbtn_fixed" : " leftfixbtn_float") %>"></a>
<div class="menu_body"><uc1:Menu ID="Menu1" runat="server" /></div>
</div>
<div class="eLayout_content<%=(LeftFixed? " eLayout_content_fixed" : " eLayout_content_float") %>">
<dl class="eFrameTab">
<dt>
<%=toptab%>
</dt>
<dd>
<%=topframe %>
</dd>
</dl>
</div>
</div>
<script>
    function init() {
        var keyevent = new eKeyEvent();
        keyevent.onkeydown = function () {
            if (this.keyCode == 116) {
                if (keyevent.ctrlKey)
                {
                    keyevent.clearEvent = false;
                    return true;
                }
                keyevent.clearEvent = true;
                var _frame = $(".eFrameTab dd iframe:visible");
                if (_frame.length > 0)
                {
                    _frame[0].contentDocument.location.reload();
                }
            }
        };
    }
    if (window.addEventListener) { window.addEventListener("load", init, false); } else { window.attachEvent("onload", init); }
</script>
</body>
</html>