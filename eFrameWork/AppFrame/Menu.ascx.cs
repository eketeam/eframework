﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.AppFrame
{

    public partial class Menu : System.Web.UI.UserControl
    {
        public string AppItem = eParameters.Request("AppItem");
        public string aspfile = eBase.getAspxFileName().ToLower();
        private eUser user;
        ApplicationMenu appmenu;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Application");
            appmenu = new ApplicationMenu(user,"1");
            litMenu.Text = appmenu.getMenus("");
        }
    }
}