﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;

namespace eFrameWork.AppFrame
{
    public partial class Main : System.Web.UI.MasterPage
    {
        public bool Ajax = false;
        public string AppItem = eParameters.Request("AppItem");
        public string appTitle = "";
        public eUser user;
        protected void Page_Init(object sender, EventArgs e)
        {
            user = new eUser("Application");
            user.Check();//检测用户是否登录,未登录则跳转到登录页
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ajax"] != null) Ajax = Convert.ToBoolean(Request.QueryString["ajax"]);
            appTitle = eConfig.ApplicationTitle(user["SiteID"].ToString());                  
        }
    }
}