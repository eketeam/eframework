﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using System.Text.RegularExpressions;
using LitJson;
using System.Reflection;



namespace eFrameWork
{
    public partial class _Default : System.Web.UI.Page
    {       
        protected void Page_Load(object sender, EventArgs e)
        {

            string ct = eBase.DataBase.getValue("select count(*) from a_eke_sysConfigs");
            if (ct.Length>0)
            {
                LitDBState.Text = "数据库配置：<font color=\"#00cc00\">正确</font><hr />";
            }
            else 
            {
                LitDBState.Text = "数据库配置：<font color=\"#cc0000\">有误</font><hr />";
            }
        }
    }
}