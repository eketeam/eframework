﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Caching;
using System.Net;
using System.IO;
using System.Text;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using EKETEAM.Tencent.WxWork;
using LitJson;

namespace eFrameWork.WXWork
{
    public partial class Register : System.Web.UI.Page//WXWork
    {
        public string UserArea = "Application";
        public string ModelID = "dd4db3c1-8e5a-407f-848c-f7e96490fd61"; //注册模块ModelID
        eModel model;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["WXWork_UserInfo"] == null)
            {
                Response.Write("企业微信信息超时,请关闭重新进入!");
                Response.End();
            }

            eUser user = new eUser("Web", "00000000-0000-0000-0000-000000000000");
            model = new eModel(ModelID, user);
            model.Power["add"] = "true"; //给添加权限
            string act = eParameters.Request("act");
            if (act.Length == 0)
            {
                model.Action = "add";
                litBody.Text = model.getActionHTML();
            }
            else
            {
                //model.eForm.AutoRedirect = false;
                model.eForm.onChange += eform_onChange;
                model.eForm.AutoRedirect = false;
                model.Save();
            }
        }
        protected void eform_onChange(object sender, eFormTableEventArgs e)
        {
            switch (e.eventType)
            {
                case eFormTableEventType.Inserting:
                    //litBody.Text += "添加前事件已调用<br>";
                    model.eForm.Fields.Add("SiteID", eBase.WXWorkAccount.getValue("SiteID"));
                    model.eForm.Fields.Add("RoleID", "201310aa-89d7-4209-b3c1-b507e1dc6267");
                    break;
                case eFormTableEventType.Inserted:
                    //litBody.Text += "添加成功,ID为：" + e.ID + "<br>5秒后返回<br><script>setTimeout(function(){document.location='" + "LoadModel.aspx?id=1" + "';},5000);</script>";

                    JsonData jd = JsonMapper.ToObject(Session["WXWork_UserInfo"].ToString());
                    string fromURL =  jd.getValue("fromURL");
                    string qyuserid =  jd.getValue("userid");
                    string avatar =  jd.getValue("avatar");
                    string face = WxWorkHelper.downHeadImage(e.ID, qyuserid, avatar);
                    eTable etb = new eTable("a_eke_sysUsers");
                    etb.DataBase = eBase.UserInfoDB;
                    etb.Fields.Add("qyuserid", qyuserid);
                    etb.Fields.Add("xm", jd.getValue("name"));
                    etb.Fields.Add("name", jd.getValue("name"));
                    etb.Fields.Add("face", face);
                    etb.Fields.Add("gender", jd.getValue("gender"));
                    etb.Fields.Add("avatar", avatar);
                    etb.Where.Add("UserID='" + e.ID + "'");
                    etb.Update();

                    DataTable tb = eBase.UserInfoDB.getDataTable("select top 1 * from a_eke_sysUsers where UserID='" + e.ID + "'");                   
                    if (tb.Rows.Count > 0)
                    {
                        eUser user = new eUser(UserArea);
                        eFHelper.saveUserInfo(user, tb.Rows[0]); //保存用户登录信息
                        eFHelper.UserLoginLog(user);
                    }
                    Session.Remove("WXWork_UserInfo");
                    //Response.Redirect("default.aspx", true);
                    Response.Redirect(HttpUtility.UrlDecode(fromURL), true);
                    break;
            }
        }
    }
}