﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="eFrameWork.WXWork.Default" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" />
    <title><%= appTitle %></title>
    <META HTTP-EQUIV="imagetoolbar" CONTENT="NO">
    <link href="../Plugins/Theme/default/mobile/base.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
    <link href="../Plugins/eControls/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- eFrameWork控件样式-->
    <link href="../Plugins/Theme/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!--移动端样式-->



    <script src="../Scripts/jquery.js?ver=<%=Common.Version %>"></script>
    <script src="../Scripts/eketeam.js?ver=<%=Common.Version %>"></script>
    <script src="../Plugins/Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>

    <script src="js/fastc444lick.js?ver=<%=Common.Version %>"></script>
</head>
<style>
html,body{margin:0px;padding:0px;width:100%;height:100%;}
	

</style>
<body>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle">

<asp:Repeater id="Rep" runat="server">
<HeaderTemplate><div class="apps"></HeaderTemplate>
<Itemtemplate>
<a href="<%#Eval("href").ToString() %>" title="<%#Eval("MC")%>">
<dl>
<dt><img src="<%#Eval("Pic").ToString().Length > 10 ? Eval("Pic").ToString() : "../images/none.gif" %>"  style="object-fit:fill;"/></dt>
<dd><p class="mc"><%# Eval("MC")%></p></dd>
</dl>
</a>
</Itemtemplate>
<FooterTemplate>
    <div style="clear:both;font-size:0px;height:0px;"></div>
</div></FooterTemplate>
</asp:Repeater>


<asp:Repeater id="Rep_bak" runat="server">
<HeaderTemplate><div class="eapp"></HeaderTemplate>
<Itemtemplate>
<dl>
<dt><a href="<%#appmenu.getTopModelUrl(Eval("ApplicationID").ToString()) %>" title="<%#Eval("MC")%>"><img src="<%#Eval("Pic").ToString().Length > 10 ? Eval("Pic").ToString() : "../images/none.gif" %>"></a></dt>
<dd><a href="<%#appmenu.getTopModelUrl(Eval("ApplicationID").ToString()) %>" title="<%#Eval("MC")%>"><%#Eval("MC")%></a></dd>
</dl>
</Itemtemplate>
<FooterTemplate>
    <div style="clear:both;font-size:0px;height:0px;"></div>
</div></FooterTemplate>
</asp:Repeater>
</td>
  </tr>
</table>
</body>
</html>