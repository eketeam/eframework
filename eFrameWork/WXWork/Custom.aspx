﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Custom.aspx.cs" Inherits="eFrameWork.WXWork.Custom" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%if (!Ajax && eBase.parseBool(model.ModelInfo["mAutoLayout"]))
  { %>
<div class="eHeader">
<div class="left"><%if(model.Action.Length>0) {%><a href="javascript:;" onclick="history.back();" class="return"></a><%}else{%><a href="javascript:;" class="menu" _onclick="_showmenu();"></a><%}%></div>
<div class="center"><%=model.ModelInfo["MC"].ToString()%></div>
<div class="right">
<%if(model.Action=="" && model.Power["Add"]){%><a href="<%=addUrl %>" class="add"></a><%}%>
<%
if (model.Action == "view" && model.ModelInfo["viewActionBotton"].ToString().ToLower().Replace("true", "1") == "1")
{%>
 <% if (model.Power["Edit"]){%>
<a href="<%=eParameters.ReplaceAction("edit")%>" class="edit"></a>
 <%}%>
 <% if (model.Power["Del"]){%>
<a href="<%=eParameters.ReplaceAction("del")%>" onclick="javascript:return confirm('确认要删除吗？');" class="close"></a>
<%}%>
<%}%>
</div>
</div>
<%} %>
<asp:Literal ID="LitBody" runat="server" />
</asp:Content>
