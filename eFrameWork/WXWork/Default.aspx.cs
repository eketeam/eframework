﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;

namespace eFrameWork.WXWork
{
    public partial class Default : System.Web.UI.Page // System.Web.UI.Page   // EKETEAM.UserControl.ePage
    {
        public string UserArea = "Application";
        private eUser user;
        public ApplicationMenu appmenu;
        public string appTitle = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            user.Check();
            appTitle = eConfig.ApplicationTitle(user["SiteID"].ToString());
            appmenu = new ApplicationMenu(user, "2");

            if (appmenu.Applications.Rows.Count == 0)
            {
                Response.Write("没有权限!");
                Response.End();
            }
            if (appmenu.Applications.Rows.Count == 1) //直接跳转应用
            {
                Response.Redirect(appmenu.Applications.Rows[0]["href"].ToString(), true);
            }
            else //用户选择应用
            {
                //Rep.ItemDataBound += new RepeaterItemEventHandler(Rep_ItemDataBound);
                Rep.DataSource = appmenu.Applications;
                Rep.DataBind();
            }
        }
        protected void Rep_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Control ctrl = e.Item.Controls[0];
                Literal lit = (Literal)ctrl.FindControl("LitTags");
                if (lit != null)
                {
                    //lit.Text = DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString();
                }
            }
        }
    }
}