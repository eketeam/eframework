﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ToolMenu.ascx.cs" Inherits="eFrameWork.WXWork.ToolMenu" %>
<%if(topRows.Length > 0){ %>
<div class="toolmenu_mask" ontouchmove="return false;">&nbsp;</div>
<div style="height:50px;"></div>
 <%} %>
<asp:Literal ID="LitMenu" runat="server" />
<%if (topRows.Length > 0 && rsSub.Length>0)
  { %>
<script>
    $(document).ready(function () {
        $('.toolmenu').children().each(function (j, node) {
            if ($(node).find("ul").length > 0) {
                $(this).click(function () {
                    var sul = $(this).find("ul:eq(0)");
                    if ($(this).attr("class") != "on") {
                        $('.toolmenu .on ul').animate({ bottom: -$('.toolmenu .on ul').height() - 6 }, 200);
                        $('.toolmenu .on').removeClass("on");

                        $(this).addClass("on");
                        $('.toolmenu_mask').show();
                        sul.animate({ bottom: 50 }, 200);
                    }
                    else {
                        $(this).removeClass("on");
                        $('.toolmenu_mask').hide();
                        sul.animate({ bottom: -sul.height() - 6 }, 200);
                    }
                });
            }
            else {
                /*
                $(this).unbind("click").on("click", function () {
                        $('.toolmenu .on ul').animate({bottom:-$('.toolmenu .on ul').height() - 6},200);
                        $('.toolmenu .on').removeClass("on");
                        $('.toolmenu_mask').hide();
                    });
                    */
            }

        });
        $('.toolmenu_mask').click(function () {
            $('.toolmenu .on ul').animate({ bottom: -$('.toolmenu .on ul').height() - 6 }, 200);
            $('.toolmenu .on').removeClass("on");
            $(this).hide();
        });
    });
</script>
 <%} %>