﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using LitJson;

namespace eFrameWork.WXWork
{
    public partial class LoginOut : System.Web.UI.Page
    {
        public string UserArea = "Application";  

        protected void Page_Load(object sender, EventArgs e)
        {
            eUser user = new eUser(UserArea);
            string app = "";
            string iphoneapp = "";
            //用户退出日志
            if (user.Logined)
            {
                app = user["app"].ToString();
                iphoneapp = user["iphoneapp"].ToString();
                eFHelper.UserLoginOutLog(user);
            }

            user.Remove();
            if (iphoneapp == "1" || app=="true")
            {
                Response.Write("<script>localStorage.removeItem('cookie');localStorage.removeItem('href');document.location='Login.aspx';</script>");
            }
            else
            {
                Response.Write("退出成功!");
                //Response.Redirect("Login.aspx" + (app.Length > 0 ? "?app=" + app : ""), true);
            }           
        }
    }
}