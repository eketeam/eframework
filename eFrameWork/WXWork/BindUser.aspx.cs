﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using EKETEAM.Tencent.WxWork;
using System.Web.Caching;
using System.Net;
using System.IO;
using System.Text;
using LitJson;

namespace eFrameWork.WXWork
{
    public partial class BindUser : System.Web.UI.Page
    {
        public string UserArea = "Application";
        eUser user;
        public string redirect_uri = "";
        protected void Page_Load(object sender, EventArgs e)
        {            
            string fromURL = "BindUser.aspx";
            if (Session["WXWork_UserInfo"] == null)
            {
                Response.Write("企业微信信息超时,请关闭重新进入!");
                Response.End();
            }
            eFHelper.checkErrorCount(); //检查登录错误次数是否超过设定值

            if (Request.Form["yhm"] != null)
            {
                eFHelper.checkRndCode(Request.Form["yzm"].ToString(), fromURL); //验证验证码是否正确
                string sql = "Select top 1 * From a_eke_sysUsers Where delTag=0 and Active=1 and YHM='" + Request.Form["yhm"].ToString() + "'";
                DataTable tb = eBase.UserInfoDB.getDataTable(sql);
                if (tb.Rows.Count == 0)
                {
                    eFHelper.setErrorSession(); //增加登录错误次数
                    fromURL = eBase.getAspxFileName();
                    Response.Write("<script>alert('登录信息不正确！');document.location='" + fromURL + "';</script>");
                    Response.End();
                }
                else
                {
                    string encpass = Request.Form["mm"].ToString();//得到RSA加密的密码
                    string pass = eRSA.getPass(encpass);//进行RSA解密
                    if (eBase.getPassWord(pass) == tb.Rows[0]["mm"].ToString())
                    {
                        JsonData jd = JsonMapper.ToObject(Session["WXWork_UserInfo"].ToString());
                        fromURL = jd.getValue("fromURL");

                        string userid = tb.Rows[0]["UserID"].ToString();
                        string qyuserid = jd.getValue("userid");
                        string avatar =  jd.getValue("avatar");
                        string face = WxWorkHelper.downHeadImage(userid, qyuserid, avatar);

                        eTable etb = new eTable("a_eke_sysUsers");
                        etb.DataBase = eBase.UserInfoDB;
                        etb.Fields.Add("qyuserid", qyuserid);
                        etb.Fields.Add("xm",  jd.getValue("name"));
                        etb.Fields.Add("name", jd.getValue("name"));
                        etb.Fields.Add("face", face);
                        etb.Fields.Add("gender", jd.getValue("gender"));
                        etb.Fields.Add("avatar", avatar); 
                        etb.Where.Add("UserID='" + userid + "'");
                        etb.Update();


                        eUser user = new eUser(UserArea);
                        eFHelper.saveUserInfo(user, tb.Rows[0]); //保存用户登录信息
                        eFHelper.UserLoginLog(user); //用户登录日志
                        Session.Remove("WXWork_UserInfo");
                        if (eParameters.QueryString("fromURL").Length > 0)
                        {
                            //Response.Redirect(HttpUtility.UrlDecode(eParameters.QueryString("fromURL")), true);
                        }
                        else
                        {
                            //Response.Redirect("Default.aspx", true);
                        }

                        Response.Redirect(HttpUtility.UrlDecode(fromURL), true);
                    }
                    else
                    {
                        eFHelper.setErrorSession(); //增加登录错误次数
                        fromURL = eBase.getAspxFileName();
                        Response.Write("<script>alert('登录信息不正确！');document.location='" + fromURL + "';</script>");
                        Response.End();
                    }
                }
            }          
        }

    }
}