﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="jsAPI.aspx.cs" Inherits="eFrameWork.WXWork.jsAPI" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<!DOCTYPE html>
<html>
<head>
    <title>无标题文档</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" /> 
	
     <script src="../Scripts/jquery.js"></script>
	 <script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
</head>

<script> 
    var signUrl = "<%=shareData.link%>";
    var ticket = "";

    $(function(){


        wx.config({
            debug: false,
            beta: true,
            appId: '<%=shareData.appid%>',
timestamp: <%=shareData.timestamp%>,
    nonceStr: '<%=shareData.noncestr%>',
    signature: '<%=shareData.signature%>',
    jsApiList: [
    'checkJsApi',
    'onMenuShareAppMessage',
    'onMenuShareWechat',
    'onMenuShareTimeline',
    'shareAppMessage',
    'shareWechatMessage',
    'startRecord',
    'stopRecord',
    'onVoiceRecordEnd',
    'playVoice',
    'pauseVoice',
    'stopVoice',
    'uploadVoice',
    'downloadVoice',
    'chooseImage',
    'previewImage',
    'uploadImage',
    'downloadImage',
    'getNetworkType',
    'openLocation',
    'getLocation',
    'hideOptionMenu',
    'showOptionMenu',
    'hideMenuItems',
    'showMenuItems',
    'hideAllNonBaseMenuItem',
    'showAllNonBaseMenuItem',
    'closeWindow',
    'scanQRCode',
    'previewFile',
    'openEnterpriseChat',
    'selectEnterpriseContact',
    'onHistoryBack',
    'openDefaultBrowser',
    ]
});
    wx.ready(function () {
        var shareData = {
            title: "eFrameWork对企业微信JSAPI的支持!",
            desc: "desc",
            link: window.location.href.replace(/#\S*/, ""),
            imgUrl: "http://test.ynzfwh.com/WeChat/images/touch-icon.png",
            success: function (res) {
                //alert('已分享');
            },
            cancel: function (res) {
            },
            fail: function (res) {
                //alert(JSON.stringify(res));
            }
        };
        wx.onMenuShareTimeline(shareData); //分享到朋友圈
        wx.onMenuShareAppMessage(shareData);//发送给朋友
        //wx.scanQRCode();//微信息处理
        //wx.scanQRCode({needResult:1,desc:"scanQRCode desc",success:function(e){alert(JSON.stringify(e))}});//自己处理
        wx.hideOptionMenu();
    });



});

    function scanQRCode()
    {
        wx.scanQRCode({needResult:1,desc:"scanQRCode desc",success:function(e){alert(JSON.stringify(e))}});//自己处理
    };
</script>

<body>

   <button onclick="scanQRCode();" style="display:block;line-height:45px;width:100%;height:45px;">扫一扫</button>

</body>
</html>
