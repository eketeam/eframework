﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using EKETEAM.Tencent.WxWork;
using LitJson;

namespace eFrameWork.WXWork
{
    public partial class ScanLogin : System.Web.UI.Page
    {
        public string UserArea = "Application";
        eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            string fromURL = eParameters.QueryString("state");//登录来源页面URL
            if (fromURL.Length == 0) fromURL = "Default.aspx";
            string code = Request["code"];
            if (string.IsNullOrEmpty(code)) return;

            JsonData json = WxWorkHelper.getUserInfo(code);

            string qyuserid = json.getValue("UserId"); //非企业成员 返回OpenId ，这里为空
            string userticket = json.getValue("user_ticket");
            if (qyuserid.Length == 0)
            {
                Response.Write("登录失败,请与企业微信管理员联系!");
                Response.End();
            }
            DataTable tb = eBase.DataBase.getDataTable("select top 1 * from a_eke_sysUsers where qyUserID='" + qyuserid + "' and delTag=0 order by LEN(ISNULL(qyUserID,'')) desc, addTime");
            if (tb.Rows.Count > 0)
            {
                if (tb.Rows[0]["Active"].ToString() == "False")
                {
                    Response.Write("登录失败,用户信息已停用!");
                    Response.End();
                }
                if (tb.Rows[0]["delTag"].ToString() == "True")
                {
                    Response.Write("登录失败,用户信息已删除!");
                    Response.End();
                }
                user = new eUser(UserArea);
                eFHelper.saveUserInfo(user, tb.Rows[0]); //保存用户登录信息
                eFHelper.UserLoginLog(user); //用户登录日志
                Response.Redirect(HttpUtility.UrlDecode(fromURL), true);

            }

            //Response.Write(code);
            //Response.End();
        }
        
    }
}