﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="eFrameWork.WXWork.Register" %>
<!DOCTYPE html>
<html>
<head>
     <title>帐号注册</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" />
    <meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no"/>
   

    <link href="../Plugins/Theme/default/mobile/base.css?ver=1.0.61" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
    <link href="../Plugins/eControls/default/mobile/style.css?ver=1.0.61" rel="stylesheet" type="text/css" /><!-- eFrameWork控件样式-->
    <link href="../Plugins/Theme/default/mobile/style.css?ver=1.0.61" rel="stylesheet" type="text/css" /><!--移动端样式-->

    <script src="../Scripts/Init.js?ver=1.0.61"></script>
    <script src="../Plugins/Theme/default/mobile/layout.js?ver=1.0.61"></script>
    <script src="js/common.js?ver=1.0.61"></script>
</head>
<body>
   <asp:Literal id="litBody" runat="server" />
</body>
</html>
