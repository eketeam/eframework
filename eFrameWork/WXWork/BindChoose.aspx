﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BindChoose.aspx.cs" Inherits="eFrameWork.WXWork.BindChoose" %>
<!DOCTYPE html>
<html>
<head>
    <title>绑定或创建帐号</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" />
    <meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no"/>
   

    <link href="../Plugins/Theme/default/mobile/base.css?ver=1.0.61" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
    <link href="../Plugins/eControls/default/mobile/style.css?ver=1.0.61" rel="stylesheet" type="text/css" /><!-- eFrameWork控件样式-->
    <link href="../Plugins/Theme/default/mobile/style.css?ver=1.0.61" rel="stylesheet" type="text/css" /><!--移动端样式-->

    <script src="../Scripts/Init.js?ver=1.0.61"></script>
    <script src="../Plugins/Theme/default/mobile/layout.js?ver=1.0.61"></script>
</head>
    <style>
        body {
        margin:20px;
        }
        a {
        display:block;
        line-height:30px;
        margin-top:30px;
        font-size:18px;
        }
    </style>
<body>

    <a href="BindUser.aspx">1.绑定已有帐号</a>
    <a href="Register.aspx">2.注册新帐号</a>
</body>
</html>