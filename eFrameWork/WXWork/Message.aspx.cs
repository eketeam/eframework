﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using EKETEAM.FrameWork;
using LitJson;
using EKETEAM.Tencent.WxWork;



public partial class WXWork_Message : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.UrlReferrer == null) return;
        string host=Request.UrlReferrer.Host.ToLower();
        if (eBase.WXWorkAccount.getValue("ReverseProxy").ToLower().IndexOf(host) == -1) return; 


        string postjson = eBase.getPostJsonString();
        //eBase.AppendLog(postjson);
        if (Request.UrlReferrer != null)
        {
            //eBase.AppendLog(Request.UrlReferrer.Host);
        }
        if (Request.ContentType.ToLower() == "application/json")
        {
            if (postjson.StartsWith("{"))
            {
                JsonData jd = JsonMapper.ToObject(postjson);
                string msg = jd["message"].ToString();
                string userid = jd["userid"].ToString();
                WxWorkHelper.sendText(userid, msg);
                Response.Write("finish");
            }
        }
        Response.End();
    }
}