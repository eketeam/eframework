﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;


namespace eFrameWork.Manage
{
    public partial class ModelItems_Basic : System.Web.UI.Page
    {
        public string modelid = eParameters.QueryString("modelid");
        public string getJsonText(string jsonstr,string name)
        {
            StringBuilder sb = new StringBuilder();
            if (jsonstr.Length > 0)
            {
                /*
                eJson json = new eJson(jsonstr);
                foreach (eJson m in json.GetCollection())
                {
                    sb.Append("<span style=\"display:inline-block;margin-right:6px;border:1px solid #ccc;padding:3px 12px 3px 12px;\">" + HttpUtility.HtmlDecode(m.GetValue(name)) + "</span>");
                }
                */
                JsonData json = jsonstr.ToJsonData();
                foreach (JsonData m in json)
                {
                    sb.Append("<span style=\"display:inline-block;margin-right:6px;border:1px solid #ccc;padding:3px 12px 3px 12px;\">" + HttpUtility.HtmlDecode(m.getValue(name)) + "</span>");
                }
            }
            return sb.ToString();
        }
        private DataTable _allmodels;
        public DataTable allModels
        {
            get
            {
                if (_allmodels == null)
                {
                    _allmodels = eBase.DataBase.getDataTable("select * from a_eke_sysModels where delTag=0");
                }
                return _allmodels;
            }
        }
        private DataTable _customcolumns;
        public DataTable CustomColumns
        {
            get
            {
                if (_customcolumns == null)
                {
                    _customcolumns = eBase.DataBase.getDataTable("select * from a_eke_sysModelItems where modelid='" + modelid + "' and custom=1 and hasui=1 and showadd=1 and delTag=0");// and bindmodelid is null and len(isnull(programefile,' '))=1

                }
                return _customcolumns;
            }
        }
        private JsonData _propertys;
        public JsonData Propertys
        {
            get
            {
                if (_propertys == null)
                {
                    _propertys = JsonMapper.ToObject("{}");
                    if (ModelInfo.Table.Columns.Contains("Propertys") && ModelInfo["Propertys"].ToString().Length > 2 && ModelInfo["Propertys"].ToString().StartsWith("{"))
                    {
                        _propertys = JsonMapper.ToObject(ModelInfo["Propertys"].ToString());
                    }
                }
                return _propertys;
            }
        }
        private DataRow _modelinfo;
        public DataRow ModelInfo
        {
            get
            {
                if (_modelinfo == null)
                {
                    DataTable dt = eBase.DataBase.getDataTable("select * from a_eke_sysModels where ModelID='" + modelid + "'");
                    if (dt.Rows.Count > 0) _modelinfo = dt.Rows[0];
                }
                return _modelinfo;
            }
        }
        private eUser user;
        private int modelpx = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            DataRow dr = allModels.Select("modelid='" + modelid + "'")[0];

            Response.Write("<a href=\"http://frame.eketeam.com\" style=\"float:right;\" target=\"_blank\" title=\"eFrameWork开发框架\"><img src=\"images/help.gif\"></a>");
            
            if (eConfig.showHelp())
            {
                Response.Write("<div class=\"tips\" style=\"margin-bottom:6px;\">");
                Response.Write("<b>基本设置</b><br>管理列在表单的添加、编辑、查看的基本参数。");
                Response.Write("</div> ");
            }
            if (!eBase.parseBool(dr["subModel"]) && (dr["Type"].ToString() == "1" || dr["Type"].ToString() == "4" || dr["Type"].ToString() == "5" || dr["Type"].ToString() == "10" || dr["Type"].ToString() == "11"))
            {
                Response.Write("<label><input reload=\"true\" type=\"checkbox\" onclick=\"setModel(this,'AnonymousAccess');\"" + (eBase.parseBool(ModelInfo["AnonymousAccess"]) ? " checked" : "") + " />允许匿名访问</label>");
                if (eBase.parseBool(ModelInfo["AnonymousAccess"]))
                {
                    Response.Write("&nbsp;&nbsp;&nbsp;匿名访问地址：<a style=\"color:#0099FF;\" href=\"" + eBase.getAbsoluteUrl() + "AnonymousAccess/Model.aspx?modelid=" + modelid + "\" target=\"_blank\">" + eBase.getAbsoluteUrl() + "AnonymousAccess/Model.aspx?modelid=" + modelid + "</a>");
                }
                Response.Write("<br>");
            }

            if (dr["Type"].ToString() != "4" && dr["Type"].ToString() != "5" && dr["Type"].ToString() != "10" && dr["Type"].ToString() != "11")
            {
                Response.Write("&nbsp;表单列数：<input type=\"text\" value=\"" + dr["AddColumnCount"].ToString() + "\" oldvalue=\"" + dr["AddColumnCount"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'addcolumncount');\" />");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(61);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");


                Response.Write("&nbsp;列标题宽度：<input type=\"text\" value=\"" + dr["ColumnTitleWidth"].ToString() + "\" oldvalue=\"" + dr["ColumnTitleWidth"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'columntitlewidth');\" />");
                Response.Write("&nbsp;列标题宽度(M)：<input type=\"text\" value=\"" + dr["mColumnTitleWidth"].ToString() + "\" oldvalue=\"" + dr["mColumnTitleWidth"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'mcolumntitlewidth');\" />");


                Response.Write("&nbsp;&nbsp;查看状态按钮：");
                //Response.Write("<select onchange=\"setModelPropertys(this,'viewActionBotton');\">");//查看状态显示编辑、删除按钮
                //Response.Write("<option value=\"true\"" + (Propertys.getValue("viewActionBotton") == "true" ? " selected=\"true\"" : "") + ">显示</option>");
                //Response.Write("<option value=\"false\"" + (Propertys.getValue("viewActionBotton") == "false" || Propertys.getValue("viewActionBotton").Length == 0 ? " selected=\"true\"" : "") + ">不显示</option>");
                //Response.Write("</select>");

                Response.Write("<select onchange=\"setModel(this,'viewActionBotton');\">");//查看状态显示编辑、删除按钮
                Response.Write("<option value=\"1\"" + (eBase.parseBool(dr["viewActionBotton"]) ? " selected=\"true\"" : "") + ">显示</option>");
                Response.Write("<option value=\"0\"" + (!eBase.parseBool(dr["viewActionBotton"]) ? " selected=\"true\"" : "") + ">不显示</option>");
                Response.Write("</select>");

            }
            if (dr["Type"].ToString() != "3" && dr["Type"].ToString() != "4" && dr["Type"].ToString() != "5" && dr["Type"].ToString() != "10" && dr["Type"].ToString() != "11")
            {
                Response.Write("&nbsp;添加按钮：<input type=\"text\" value=\"" + dr["AddText"].ToString() + "\" oldvalue=\"" + dr["AddText"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'addtext');\" />");
                Response.Write("&nbsp;编辑按钮：<input type=\"text\" value=\"" + dr["EditText"].ToString() + "\" oldvalue=\"" + dr["EditText"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'edittext');\" />");
                Response.Write("&nbsp;删除按钮：<input type=\"text\" value=\"" + dr["DeleteText"].ToString() + "\" oldvalue=\"" + dr["DeleteText"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'deletetext');\" />");
                Response.Write("&nbsp;添加保存：<input type=\"text\" value=\"" + dr["AddSaveText"].ToString() + "\" oldvalue=\"" + dr["AddSaveText"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'addsavetext');\" />");
                Response.Write("&nbsp;编辑保存：<input type=\"text\" value=\"" + dr["EditSaveText"].ToString() + "\" oldvalue=\"" + dr["EditSaveText"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'editsavetext');\" />");
            }
            if (!eBase.parseBool(dr["subModel"]) && eBase.parseBool(dr["auto"]) && dr["Type"].ToString() != "3" && dr["Type"].ToString() != "4" && dr["Type"].ToString() != "5" && dr["Type"].ToString() != "10" && dr["Type"].ToString() != "11")
            {
                Response.Write("&nbsp;签收类型：");
                Response.Write("<select onchange=\"setModel(this,'SignType');\">");
                Response.Write("<option value=\"0\"" + (dr["SignType"].ToString() == "0" ? " selected=\"true\"" : "") + ">无</option>");
                Response.Write("<option value=\"1\"" + (dr["SignType"].ToString() == "1" ? " selected=\"true\"" : "") + ">查看签收</option>");
                Response.Write("<option value=\"2\"" + (dr["SignType"].ToString() == "2" ? " selected=\"true\"" : "") + ">签名签收</option>");
                Response.Write("</select>");

                Response.Write("&nbsp;表单按钮对齐：");
                Response.Write("<select onchange=\"setModel(this,'ButtonAlign');\">");
                Response.Write("<option value=\"left\"" + (dr["ButtonAlign"].ToString() == "left" ? " selected=\"true\"" : "") + ">左对齐</option>");
                Response.Write("<option value=\"center\"" + (dr["ButtonAlign"].ToString() == "center" ? " selected=\"true\"" : "") + ">居中对齐</option>");
                Response.Write("<option value=\"right\"" + (dr["ButtonAlign"].ToString() == "right" ? " selected=\"true\"" : "") + ">右对齐</option>");
                Response.Write("</select>");
                Response.Write("<br>");
            }
            if ((!eBase.parseBool(dr["subModel"]) && !eBase.parseBool(dr["auto"])) || dr["Type"].ToString() == "11")
            {
                Response.Write("&nbsp;自动布局：");
                Response.Write("<select onchange=\"setModel(this,'AutoLayout');\">");
                Response.Write("<option value=\"1\"" + ( eBase.parseBool( dr["AutoLayout"]) ? " selected=\"true\"" : "") + ">是</option>");
                Response.Write("<option value=\"0\"" + (!eBase.parseBool(dr["AutoLayout"]) ? " selected=\"true\"" : "") + ">否</option>");
                Response.Write("</select>");
                Response.Write("&nbsp;M自动布局：");
                Response.Write("<select onchange=\"setModel(this,'mAutoLayout');\">");
                Response.Write("<option value=\"1\"" + (eBase.parseBool(dr["mAutoLayout"]) ? " selected=\"true\"" : "") + ">是</option>");
                Response.Write("<option value=\"0\"" + (!eBase.parseBool(dr["mAutoLayout"]) ? " selected=\"true\"" : "") + ">否</option>");
                Response.Write("</select><br>");
            }

            //Response.Write("&nbsp;保存文本：<input type=\"text\" value=\"" + HttpUtility.HtmlEncode(dr["TextFields"].ToString()) + "\" oldvalue=\"" + HttpUtility.HtmlEncode(dr["TextFields"].ToString()) + "\"  class=\"edit\" style=\"width:540px;\" onBlur=\"setModel(this,'textfields');\" />");
            //Response.Write("&nbsp;模块权限：<input type=\"text\" value=\"" + HttpUtility.HtmlEncode(dr["Power"].ToString()) + "\" oldvalue=\"" + HttpUtility.HtmlEncode(dr["Power"].ToString()) + "\"  class=\"edit\" style=\"width:540px;\" onBlur=\"setModel(this,'power');\" />"); 

            if (dr["Type"].ToString() != "3" && eBase.parseBool(dr["subModel"]) && eBase.parseBool(dr["JoinMore"]))
            {
                Response.Write("&nbsp;添加方式：");
                Response.Write("<select onchange=\"setModel(this,'AddMode');\">");
                Response.Write("<option value=\"0\"" + (dr["AddMode"].ToString() == "0" ? " selected=\"true\"" : "") + ">逐个添加</option>");
                string ct = eBase.DataBase.getValue("select count(1) from a_eke_sysModels where type=3 and deltag=0 and ParentID='" + dr["ModelID"].ToString() + "'");//数据子模块的数量
                if(ct!="0") Response.Write("<option value=\"1\"" + (dr["AddMode"].ToString() == "1" ? " selected=\"true\"" : "") + ">选择添加</option>");
                Response.Write("<option value=\"2\"" + (dr["AddMode"].ToString() == "2" ? " selected=\"true\"" : "") + ">Excel式添加</option>");
                Response.Write("<option value=\"3\"" + (dr["AddMode"].ToString() == "3" ? " selected=\"true\"" : "") + ">叠加添加</option>");
                Response.Write("</select>");
            }
            if (dr["Type"].ToString()!="3" && eBase.parseBool(dr["subModel"]) )
            {
                Response.Write("&nbsp;冗余上级数据：");
                Response.Write("<select onchange=\"setModel(this,'Redundant');\">");
                Response.Write("<option value=\"0\"" + (dr["Redundant"].ToString() == "0" ? " selected=\"true\"" : "") + ">不冗余</option>");
                Response.Write("<option value=\"1\"" + (dr["Redundant"].ToString() == "1" ? " selected=\"true\"" : "") + ">冗余</option>");
                Response.Write("</select><br>");
            }

            if (dr["Type"].ToString() == "1")
            {
                Response.Write("<div>");
                Response.Write("&nbsp;保存文本：");
                Response.Write("<textarea reload=\"true\" id=\"model_textfields\" jsonformat=\"[{&quot;text&quot;:&quot;表单Name&quot;,&quot;value&quot;:&quot;frmName&quot;},{&quot;text&quot;:&quot;字段编码&quot;,&quot;value&quot;:&quot;Field&quot;}]\" name=\"textarea\" style=\"width:80%;display:none;\" rows=\"2\"  onBlur=\"setModel(this,'textfields');\" oldvalue=\"" + HttpUtility.HtmlEncode(dr["TextFields"].ToString()) + "\">" + HttpUtility.HtmlEncode(dr["TextFields"].ToString()) + "</textarea>");
                Response.Write("<img src=\"images/jsonedit.png\" style=\"cursor:pointer;margin-right:5px;\" align=\"absmiddle\" onclick=\"Json_Edit('model_textfields','保存文本');\">");
                Response.Write(getJsonText(dr["TextFields"].ToString(), "Field"));
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(62);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("</div>");
            }
            #region 步骤条件
            if (dr["Type"].ToString() == "8")
            {
                Response.Write("<div>&nbsp;步骤条件:");

                Response.Write("<textarea reload=\"true\" id=\"stepcondition\" jsonformat=\"[{&quot;text&quot;:&quot;变量&quot;,&quot;value&quot;:&quot;variable&quot;},{&quot;text&quot;:&quot;运算符&quot;,&quot;value&quot;:&quot;operator&quot;},{&quot;text&quot;:&quot;值&quot;,&quot;value&quot;:&quot;value&quot;}]\" name=\"textarea\" style=\"width:90%;display:none;\" rows=\"2\"  onBlur=\"setModel(this,'stepcondition');\" oldvalue=\"" + ModelInfo["StepCondition"].ToString().HtmlEncode() + "\">" + ModelInfo["StepCondition"].ToString().HtmlEncode() + "</textarea>");
                Response.Write("<img src=\"images/jsonedit.png\" style=\"cursor:pointer;margin-right:5px;\" align=\"absmiddle\" onclick=\"Json_StepConditionEdit('stepcondition','步骤条件');\">");
                Response.Write("</div>");
            }
            #endregion



            if (!eBase.parseBool(dr["subModel"]) && dr["Type"].ToString() != "3")
            {
                Response.Write("<div>");
                Response.Write("&nbsp;模块权限：");
                string customaction = "[";
                DataTable tb = eBase.DataBase.getDataTable("select MC,Action from a_eke_sysActions where ModelID='" + modelid + "' and len(mc)>0 and len(Action)>0 and deltag=0");
                for (int i = 0; i < tb.Rows.Count; i++)
                {
                    if(i>0)customaction += ",";
                    customaction += "{\"text\":\"" + tb.Rows[i]["mc"].ToString() + "\",\"value\":\"" + tb.Rows[i]["Action"].ToString() + "\"}";
                }
                customaction += "]";
                customaction = "[]";
                //eBase.Writeln(customaction);
                Response.Write("<textarea reload=\"true\" id=\"model_power\" customaction=\"" + customaction.HtmlEncode() + "\" jsonformat=\"[{&quot;text&quot;:&quot;文本&quot;,&quot;value&quot;:&quot;text&quot;},{&quot;text&quot;:&quot;值&quot;,&quot;value&quot;:&quot;value&quot;}]\" name=\"textarea\" style=\"width:80%;display:none;\" rows=\"2\"  onBlur=\"setModel(this,'power');\" oldvalue=\"" + dr["Power"].ToString().HtmlEncode() + "\">" + dr["Power"].ToString().HtmlEncode() + "</textarea>");
                Response.Write("<img src=\"images/jsonedit.png\" style=\"cursor:pointer;margin-right:5px;\" align=\"absmiddle\" onclick=\"Json_modelPowerEdit('model_power');\">");
                Response.Write(getJsonText(dr["Power"].ToString(), "text"));
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(63);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("</div>");
            }


            if (dr["Type"].ToString() == "8" || 1 == 1)
            {
                if (dr["Type"].ToString() != "4" && dr["Type"].ToString() != "10" && dr["Type"].ToString() != "11" && dr["BaseModelID"].ToString().Length > 0)
                {
                    Response.Write("<div style=\"padding:10px 0px 10px 0px;\">");
                    //Response.Write("<input type=\"button\" name=\"Submit\" value=\"同步编码\" onclick=\"syncCode();\" style=\"padding:3px 10px 3px 10px;\" />");
                    //if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(64);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                    //Response.Write("<input type=\"button\" name=\"Submit\" value=\"还原编码\" onclick=\"restoreCode();\" style=\"margin-left:20px;padding:3px 10px 3px 10px;\" />");
                    //if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(65);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");

                    if (dr["BaseModelID"].ToString().Length > 0) Response.Write("<input type=\"button\" name=\"Submit\" value=\"同步基础模块\" onclick=\"syncModel();\" style=\"margin-left:20px;\" />");
                    Response.Write("</div>");
                }


                Response.Write("模块顶部HTML：<br>");
                Response.Write("<textarea htmltag=\"true\" name=\"textarea\" style=\"width:90%;\" rows=\"2\"  onBlur=\"setModel(this,'starthtml');\" oldvalue=\"" + ModelInfo["StartHTML"].ToString().HtmlEncode() + "\">" + ModelInfo["StartHTML"].ToString().HtmlEncode() + "</textarea>");
                Response.Write("<br>\r\n");


                Response.Write("模块提示：");
                // if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp();\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("<br>\r\n");
                Response.Write("<textarea htmltag=\"true\" name=\"textarea\" style=\"width:90%;\" rows=\"2\"  onBlur=\"setModel(this,'tip');\" oldvalue=\"" + ModelInfo["tip"].ToString().HtmlEncode() + "\">" + ModelInfo["tip"].ToString().HtmlEncode() + "</textarea>");
                Response.Write("<br>\r\n");

                if (dr["Type"].ToString() == "11")
                {
                    Response.Write("内容：");
                    Response.Write("<br>\r\n");
                    Response.Write("<textarea htmltag=\"true\" name=\"textarea\" style=\"width:90%;\" rows=\"6\"  onBlur=\"setModel(this,'HtmlBody');\" oldvalue=\"" + ModelInfo["HtmlBody"].ToString().HtmlEncode() + "\">" + ModelInfo["HtmlBody"].ToString().HtmlEncode() + "</textarea>");
                    Response.Write("<br>\r\n");

                    Response.Write("移动端内容：");
                    Response.Write("<br>\r\n");
                    Response.Write("<textarea htmltag=\"true\" name=\"textarea\" style=\"width:90%;\" rows=\"5\"  onBlur=\"setModel(this,'mHtmlBody');\" oldvalue=\"" + ModelInfo["mHtmlBody"].ToString().HtmlEncode() + "\">" + ModelInfo["mHtmlBody"].ToString().HtmlEncode() + "</textarea>");
                    Response.Write("<br>\r\n");
                }
            }

            Response.Write("模块备注：");
            Response.Write("<br>\r\n");
            Response.Write("<textarea htmltag=\"true\" name=\"textarea\" style=\"width:90%;\" rows=\"2\"  onBlur=\"setModel(this,'remark');\" oldvalue=\"" + ModelInfo["Remark"].ToString().HtmlEncode() + "\">" + ModelInfo["Remark"].ToString().HtmlEncode() + "</textarea>");
            Response.Write("<br>\r\n");

            /*
            eList datalist = new eList("a_eke_sysModelItems");
            datalist.Where.Add("ModelID='" + modelid + "' and delTag=0 and (Custom=0 or Equal=1 or len(ProgrameFile)>0 or len(cast(BindModelID as varchar(50)))>0 or (select count(1) from a_eke_sysModelItems b where b.delTag=0 and b.PackID=a_eke_sysModelItems.ModelItemID)>0 )"); //
            datalist.OrderBy.Add("showadd desc,addorder, PX, addTime");
           // eBase.WriteDebug(datalist.SQL);
            datalist.Bind(Rep);
            */
            if (dr["Type"].ToString() != "4" ) list();


            if (dr["Type"].ToString() != "4")
            {
                eList elist = new eList("a_eke_sysModelItems");
                elist.Where.Add("ModelID='" + modelid + "' and Custom=1");
                elist.OrderBy.Add("px,addTime");
                elist.Bind(RepCustom);
            }
                System.IO.StringWriter sw = new System.IO.StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Rep.RenderControl(htw);
                Rep.Visible = false;//不输出，要在获取后设，不然取不到内容。
                sw.Close();
                if ( dr["Type"].ToString() != "11") Response.Write(sw.ToString());

                if (dr["Type"].ToString() != "4" && dr["Type"].ToString() != "11")
           {
                    Response.Write("<strong>自定义列：</strong><br />");
                    sw = new System.IO.StringWriter();
                    htw = new HtmlTextWriter(sw);
                    RepCustom.RenderControl(htw);
                    RepCustom.Visible = false;
                    Response.Write(sw.ToString());
                    sw.Close();


                    if (dr["Type"].ToString() != "100" && dr["Type"].ToString() != "11")
                    {
                        Response.Write("自定义按钮方式：<select onchange=\"setModel(this,'ActionButtonMode');\">");
                        Response.Write("<option value=\"0\"" + (dr["ActionButtonMode"].ToString() == "0" ? " selected=\"true\"" : "") + ">系统默认</option>");
                        Response.Write("<option value=\"1\"" + (dr["ActionButtonMode"].ToString() == "1" ? " selected=\"true\"" : "") + ">追加</option>");
                        Response.Write("<option value=\"2\"" + (dr["ActionButtonMode"].ToString() == "2" ? " selected=\"true\"" : "") + ">替换</option>");
                        Response.Write("<option value=\"3\"" + (dr["ActionButtonMode"].ToString() == "3" ? " selected=\"true\"" : "") + ">自定义</option>");
                        Response.Write("</select><br>");

                        Response.Write("列表按钮：");
                        Response.Write("<textarea htmltag=\"true\" name=\"textarea\" style=\"width:90%;\" rows=\"2\" onblur=\"setModel(this,'actionbuttons');\" oldvalue=\"" + ModelInfo["ActionButtons"].ToString().HtmlEncode() + "\">" + ModelInfo["ActionButtons"].ToString().HtmlEncode() + "</textarea>");
                        Response.Write("<br>\r\n");
                        Response.Write("编辑按钮：");
                        Response.Write("<textarea htmltag=\"true\" name=\"textarea\" style=\"width:90%;\" rows=\"2\" onblur=\"setModel(this,'EditButtons');\" oldvalue=\"" + ModelInfo["EditButtons"].ToString().HtmlEncode() + "\">" + ModelInfo["EditButtons"].ToString().HtmlEncode() + "</textarea>");
                        Response.Write("<br>\r\n");
                    }
            }
            Response.End();
        }
        private void list()
        {
            DataTable tb = getItems(modelid);
            appendItems(tb, modelid);
            string pid = eParameters.QueryString("modelid");
            for (int i = tb.Rows.Count - 1; i > -1; i--)
            {
                if (tb.Rows[i]["modelid"].ToString() != pid)
                {
                    if (",addTime,addUser,editTime,editUser,delTime,delUser,delTag,CheckupCode,CheckupText,".ToLower().Contains("," + tb.Rows[i]["Code"].ToString().ToLower() + ","))
                    {
                        tb.Rows.Remove(tb.Rows[i]);
                    }
                }
            }
            //eBase.PrintDataTable(tb.Select("modelpx,ModelName,modelid,mc,code", "", ""));
            tb = tb.Select("", "showadd desc,showedit desc,showview desc,addorder,modelpx, PX, addTime").toDataTable();
            //eBase.PrintDataTable(tb);
            Rep.DataSource = tb;
            Rep.DataBind();
        }
        private DataTable getItems(string modelid)
        {
            modelpx++;
            //return eBase.DataBase.getDataTable("select b.mc as ModelName,a.* from a_eke_sysModelItems a inner join a_eke_sysModels b on a.modelid=b.modelid where a.ModelID='" + modelid + "' and a.delTag=0 and (a.Custom=0 or a.Equal=1 or len(a.ProgrameFile)>0 or len(a.BindModelID)>0 or (select count(1) from a_eke_sysModelItems where delTag=0 and PackID=a.ModelItemID)>0 )");//合并字段不显示出来
            return eBase.DataBase.getDataTable("select " + modelpx.ToString() + " as modelpx,b.mc as ModelName,a.* from a_eke_sysModelItems a inner join a_eke_sysModels b on a.modelid=b.modelid where a.ModelID='" + modelid + "' and a.delTag=0");//合并字段不显示出来
        }
        private void appendItems(DataTable tb,string modelid)
        {
            DataTable dt = eBase.DataBase.getDataTable("select modelid,mc from a_eke_sysModels where ParentID='" + modelid + "' and JoinMore=0 and show=1 and deltag=0");
            foreach (DataRow dr in dt.Rows)
            {
                DataTable tb2 = getItems(dr["modelid"].ToString());
                foreach (DataRow _dr in tb2.Rows)
                {
                    tb.Rows.Add(_dr.ItemArray);
                }
                appendItems(tb, dr["modelid"].ToString());
            }
        }
       
    }
}