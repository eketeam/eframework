﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;

namespace eFrameWork.Manage
{
    public partial class ModelItems_Js : System.Web.UI.Page
    {
        public string modelid = eParameters.QueryString("modelid");
        private eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            Response.Write("<a href=\"http://frame.eketeam.com\" style=\"float:right;\" target=\"_blank\" title=\"eFrameWork开发框架\"><img src=\"images/help.gif\"></a>");
            
            if (eConfig.showHelp())
            {
                Response.Write("<div class=\"tips\" style=\"margin-bottom:8px;\">");
                Response.Write("<b>JS脚本</b><br>");
                Response.Write("模块扩展功能需要用到的JavaScript脚本(公共为全局脚本)。");
                Response.Write("</div> ");
            }

            DataTable tb = eBase.DataBase.getDataTable("select Type,BaseJs,Addjs,EditJs,ViewJs from a_eke_sysModels where ModelID='" + modelid + "'");
            if (tb.Rows.Count > 0)
            {

                Response.Write("<dl class=\"ePanel\">\r\n");
                Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>公共</h1></dt>\r\n");
                Response.Write("<dd style=\"display:none;\">");
                if (eConfig.showHelp()) Response.Write("<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(130);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">");
                Response.Write(" <textarea htmltag=\"true\" id=\"txtbasejs\" name=\"txtbasejs\" style=\"width:95%;\" cols=\"100\" rows=\"10\"  on_Blur=\"setModel(this,'basejs');\" oldvalue=\"" + HttpUtility.HtmlEncode(tb.Rows[0]["BaseJs"].ToString()) + "\">" + HttpUtility.HtmlEncode(tb.Rows[0]["BaseJs"].ToString()) + "</textarea><br>\r\n");
                Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtbasejs,'basejs');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" />\r\n");
                Response.Write("</dd>\r\n");
                Response.Write("</dl>\r\n");



                if (tb.Rows[0]["Type"].ToString() != "10" && tb.Rows[0]["Type"].ToString() != "11")
                {
                    Response.Write("<dl class=\"ePanel\">\r\n");
                    Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>添加</h1></dt>\r\n");
                    Response.Write("<dd style=\"display:none;\">");
                    if (eConfig.showHelp()) Response.Write("<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(131);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">");
                    Response.Write(" <textarea htmltag=\"true\" id=\"txtaddjs\" name=\"txtaddjs\" style=\"width:95%;\" cols=\"100\" rows=\"10\"  on_Blur=\"setModel(this,'addjs');\" oldvalue=\"" + HttpUtility.HtmlEncode(tb.Rows[0]["AddJs"].ToString()) + "\">" + HttpUtility.HtmlEncode(tb.Rows[0]["AddJs"].ToString()) + "</textarea><br>\r\n");
                    Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtaddjs,'addjs');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" />\r\n");
                    Response.Write("</dd>\r\n");
                    Response.Write("</dl>\r\n");


                    Response.Write("<dl class=\"ePanel\">\r\n");
                    Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>修改</h1></dt>\r\n");
                    Response.Write("<dd style=\"display:none;\">");
                    if (eConfig.showHelp()) Response.Write("<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(132);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">");
                    Response.Write(" <textarea htmltag=\"true\" id=\"txteditjs\" name=\"txteditjs\" style=\"width:95%;\" cols=\"100\" rows=\"10\"  on_Blur=\"setModel(this,'editjs');\" oldvalue=\"" + HttpUtility.HtmlEncode(tb.Rows[0]["EditJs"].ToString()) + "\">" + HttpUtility.HtmlEncode(tb.Rows[0]["EditJs"].ToString()) + "</textarea><br>\r\n");
                    Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txteditjs,'editjs');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" />\r\n");
                    Response.Write("</dd>\r\n");
                    Response.Write("</dl>\r\n");


                    Response.Write("<dl class=\"ePanel\">\r\n");
                    Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>查看</h1></dt>\r\n");
                    Response.Write("<dd style=\"display:none;\">");
                    if (eConfig.showHelp()) Response.Write("<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(133);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">");
                    Response.Write(" <textarea htmltag=\"true\" id=\"txtviewjs\" name=\"txtviewjs\" style=\"width:95%;\" cols=\"100\" rows=\"10\"  on_Blur=\"setModel(this,'viewjs');\" oldvalue=\"" + HttpUtility.HtmlEncode(tb.Rows[0]["ViewJs"].ToString()) + "\">" + HttpUtility.HtmlEncode(tb.Rows[0]["ViewJs"].ToString()) + "</textarea><br>\r\n");
                    Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtviewjs,'viewjs');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" />\r\n");
                    Response.Write("</dd>\r\n");
                    Response.Write("</dl>\r\n");
                }
            }
            //string value = eBase.DataBase.getValue("select ListFields from a_eke_sysModels where ModelID='" + modelid + "'");
            //Response.Write("自定义列：");
            //Response.Write("<input type=\"text\" value=\"" + HttpUtility.HtmlEncode(value) + "\" oldvalue=\"" + HttpUtility.HtmlEncode(value) + "\"  class=\"edit\" style=\"width:90%;\" onBlur=\"setModel(this,'listfields');\" />");


            Response.End();
        }
    }
}