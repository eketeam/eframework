﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;
using System.Xml;
using System.Xml.Serialization;

namespace eFrameWork.Manage
{
    public partial class ModelItems_CheckUp : System.Web.UI.Page
    {
        public string modelid = eParameters.QueryString("modelid");
        public string getJsonText(string jsonstr, string name)
        {
            StringBuilder sb = new StringBuilder();
            if (jsonstr.Length > 0)
            {
                /*
                eJson json = new eJson(jsonstr);
                foreach (eJson m in json.GetCollection())
                {
                    sb.Append("<span style=\"display:inline-block;margin-right:6px;border:1px solid #ccc;padding:3px 12px 3px 12px;\">" + HttpUtility.HtmlDecode(m.GetValue(name)) + "</span>");
                }
                */
                JsonData json = jsonstr.ToJsonData();
                foreach (JsonData m in json)
                {
                    sb.Append("<span style=\"display:inline-block;margin-right:6px;border:1px solid #ccc;padding:3px 12px 3px 12px;\">" + HttpUtility.HtmlDecode(m.getValue(name)) + "</span>");
                }
            }           
            return sb.ToString();
        }
        private DataRow _modelinfo;
        public DataRow ModelInfo
        {
            get
            {
                if (_modelinfo == null)
                {
                    DataTable dt = eBase.DataBase.getDataTable("select * from a_eke_sysModels where ModelID='" + modelid + "'");
                    if (dt.Rows.Count > 0) _modelinfo = dt.Rows[0];
                }
                return _modelinfo;
            }
        }

        /// <summary>
        /// 主库或扩展库
        /// </summary>
        private eDataBase _exdatabase;
        private eDataBase ExDataBase
        {
            get
            {
                if (_exdatabase == null)
                {
                    if (ModelInfo["DataSourceID"].ToString().Length > 0)
                    {
                        _exdatabase = new eDataBase(ModelInfo);
                    }
                    else
                    {
                        _exdatabase = eConfig.DefaultDataBase;
                    }
                }
                return _exdatabase;
            }
        }
        private eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            if (Request.Form["xml"] != null)
            {
                string xml=Request.Form["xml"].ToString();
                //eBase.AppendLog(xml);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                XmlNode node = doc.SelectSingleNode("/root/data");
                if (node != null)
                {
                    foreach (XmlNode _node in node.ChildNodes)
                    {
                        DataTable dt = _node.ChildNodes.toDataTable();
                        if (dt.TableName == "a_eke_sysCheckUps")
                        {
                            dt.Columns.Add("CheckupID", typeof(string));
                            dt.Columns.Add("ModelID", typeof(string));
                            foreach (DataRow _dr in dt.Rows)
                            {
                                _dr["CheckupID"] = Guid.NewGuid().ToString();
                                _dr["modelid"] = modelid;
                            }
                            eBase.DataBase.SchemaImport(dt);
                        }
                    }
                }
                eBase.clearDataCache("a_eke_sysCheckUps");
                eResult.Success("导入成功!");
            }

            Response.Write("<a href=\"http://frame.eketeam.com\" style=\"float:right;\" target=\"_blank\" title=\"eFrameWork开发框架\"><img src=\"images/help.gif\"></a>");
            
            if (eConfig.showHelp())
            {
                Response.Write("<div class=\"tips\" style=\"margin-bottom:6px;\">");
                Response.Write("<b>审批</b><br>");
                Response.Write("设置本模块的审批流程。<br>需要做审批的模块，数据结构必须包含CheckupCode(当前审批状态编码)和CheckupText(当前审批状态名称)两个列且设置数据库默认值为未审批状态。<br>");
                Response.Write("</div> ");
            }
            if (ModelInfo["code"].ToString().Length == 0) return;
            string AllCols = ExDataBase.getTableColumnNames(ModelInfo["code"].ToString(), "").ToLower();

           

            DataRow dr = eBase.DataBase.getDataTable("select * from a_eke_sysModels where ModelID='" + modelid + "'").Select()[0];
            //Response.Write("&nbsp;动作权限：<input type=\"text\" value=\"" + HttpUtility.HtmlEncode(dr["actionPower"].ToString()) + "\" oldvalue=\"" + HttpUtility.HtmlEncode(dr["actionPower"].ToString()) + "\"  class=\"edit\" style=\"width:650px;\" onBlur=\"setModel(this,'actionPower');\" />");
            //Response.Write("&nbsp;权限控制：<br>");
            //Response.Write("<textarea reload=\"true\" id=\"actionpower\" jsonformat=\"[{&quot;text&quot;:&quot;动作编码&quot;,&quot;value&quot;:&quot;action&quot;},{&quot;text&quot;:&quot;字段编码&quot;,&quot;value&quot;:&quot;field&quot;},{&quot;text&quot;:&quot;值&quot;,&quot;value&quot;:&quot;value&quot;}]\" name=\"textarea\" style=\"width:90%;display:none;\" rows=\"2\"  onBlur=\"setModel(this,'actionpower');\" oldvalue=\"" + HttpUtility.HtmlEncode(dr["actionPower"].ToString()) + "\">" + HttpUtility.HtmlEncode(dr["actionPower"].ToString()) + "</textarea>");
            //Response.Write("<img src=\"images/jsonedit.png\" style=\"cursor:pointer;margin-right:5px;\" align=\"absmiddle\" onclick=\"Json_Edit('actionpower');\">");
            //Response.Write(getJsonText(dr["actionPower"].ToString(), "action"));
            //if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(165);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");


            //if (AllCols.ToLower().IndexOf("checkupcode") == -1 || AllCols.ToLower().IndexOf("checkuptext") == -1) return;
            //Response.Write("<br>\r\n");
            //Response.Write("<br>\r\n");            
            eList elist = new eList("a_eke_sysCheckUps");
            elist.Where.Add("ModelID='" + modelid + "' ");
            elist.Where.Add("delTag=0");
            elist.OrderBy.Add("px,addTime");
            elist.Bind(Rep);

            //eBase.WriteHTML(elist.getDataTable().toXML());
            Response.Write("<div style=\"text-align:right;margin-bottom:6px;\">");
            if (elist.RecordsCount > 0)
            {
                Response.Write("<a class=\"button\" href=\"javascript:;\" onclick=\"switch_checkupstate(this);\"><span style=\"letter-spacing:1px;\"><i class=\"set\">导出XML</i></span></a>");
                DataTable dt = elist.getDataTable().Copy();
                dt.PrimaryKey = null;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["px"] = i + 1;
                }
                dt.Columns.Remove("CheckupID");
                dt.Columns.Remove("ModelID");
                dt.Columns.Remove("Propertys");
                dt.Columns.Remove("addUser");
                dt.Columns.Remove("editTime");
                dt.Columns.Remove("editUser");
                dt.Columns.Remove("delTime");
                dt.Columns.Remove("delUser");
                dt.Columns.Remove("delTag");

                XmlDocument doc = new XmlDocument();
                dt.ExtendedProperties.Add("name", "a_eke_sysCheckUps");
                doc.appendData(dt);

                string xml = doc.InnerXml;// dt.toXML();

                Response.Write("<textarea id=\"xmlcode\" name=\"xmlcode\" style=\"display:none;margin-top:6px;width:98%;height:80px;border:1px solid #ccc;resize:none;\">" +  HttpUtility.HtmlEncode(xml) + "</textarea>");
            }
            else
            {

                Response.Write("<textarea id=\"xmlcode\" name=\"xmlcode\" style=\"width:98%;height:80px;border:1px solid #ccc;resize:none;\"></textarea>");
                Response.Write(" <a class=\"button\" href=\"javascript:;\" onclick=\"import_checkupdata();\"><span><i class=\"set\">导入</i></span></a>");
            }
            Response.Write("</div>");



            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Rep.RenderControl(htw);
            Rep.Visible = false;//不输出，要在获取后设，不然取不到内容。
            Response.Write(sw.ToString());
            sw.Close();
            Response.End();

        }
    }
}