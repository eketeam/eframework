﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelItems_List.aspx.cs" Inherits="eFrameWork.Manage.ModelItems_List" %>
<!DOCTYPE html>
<html>
<head">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title><%=eConfig.manageName() %></title>
</head>
<body>
<asp:Repeater id="Rep" runat="server" >
<headertemplate>
<%#
"<table id=\"eDataTable_List\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" width=\"99%\" style=\"margin-top:6px;min-width:1620px;\">" +
"<thead>" +
"<tr>" +
"<td width=\"110\">模块</td>" +
"<td width=\"110\">显示" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(98);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"130\">编码" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(97);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"70\">显示(M)" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(99);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
(ModelInfo["type"].ToString()=="1" || ModelInfo["type"].ToString()=="5" || ModelInfo["type"].ToString()=="10" ? "<td width=\"60\">计算</td>" : "") +

"<td width=\"55\">状态(M)</td>" +
"<td width=\"70\">表单Name</td>" +
"<td width=\"70\">表单ID</td>" +
"<td width=\"80\">宽度(PX)" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(100);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"70\">宽度(M)" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(101);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">开启排序" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(102);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">拖动位置" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(103);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">拖动宽度" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(104);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"110\">标题</td>" +
"<td width=\"110\">内容</td>" +
"<td wid4th=\"180\">自定义查看" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(105);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
//"<td width=\"150\">默认排序</td>" +
//"<td width=\"150\">条件</td>" +
"<td width=\"50\">顺序" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(106);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">" : "") + "</td>" +
"</tr>" +
"</thead>"+
"<tbody eSize=\"false\" eMove=\"true\">"
%>
</headertemplate>
<itemtemplate>
<%#
"<tr" + ((Container.ItemIndex + 1) % 2 == 0 ? " class=\"alternating\" eclass=\"alternating\"" : " eclass=\"\"") + " erowid=\"" + Eval("ModelItemID") + "\" onmouseover=\"this.className='cur';\" onmouseout=\"this.className=this.getAttribute('eclass');\" >" +
"<td height=\"32\" title=\"" + Eval("ModelName") + "\" style=\"overflow:visible;white-space:normal;\">" + Eval("ModelName") + "</td>"+
"<td class=\"tdshowlist\"><label><input reloadbak=\"true\" id=\"showlist_" + Eval("ModelItemID") + "\" type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','showlist');\"" + (eBase.parseBool(Eval("showlist").ToString()) ? " checked" : "") + " />"+ Eval("MC") + "</label></td>" +
"<td>" +(eBase.parseBool( Eval("Custom").ToString()) && eBase.parseBool(Eval("showlist").ToString()) ? "<input reload=\"true\" class=\"text\" type=\"text\" value=\""+ Eval("CustomCode").ToString() + "\" oldvalue=\""+ Eval("CustomCode").ToString() + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','customcode');\">" : Eval("Code") )+ "</td>" +
"<td><input type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','mshowlist');\"" + (eBase.parseBool(Eval("mshowlist").ToString()) ? " checked" : "") + " /></td>" +
//(ModelInfo["type"].ToString()=="1" || ModelInfo["type"].ToString()=="5" || ModelInfo["type"].ToString()=="10" ? (",int,bigint,decimal,smallint,tinyint,float,numeric,money,smallmoney,".IndexOf("," + Eval("type").ToString().ToLower() + ",") > -1 || Eval("CustomCode").ToString().Length > 0 ? "<td><input type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','sum');\"" + (eBase.parseBool(Eval("sum").ToString()) ? " checked" : "") + " /></td>" : "<td>&nbsp;</td>") : "") +
(ModelInfo["type"].ToString()=="1" || ModelInfo["type"].ToString()=="5" || ModelInfo["type"].ToString()=="10" ? 
(",int,bigint,decimal,smallint,tinyint,float,numeric,money,smallmoney,".IndexOf("," + Eval("type").ToString().ToLower() + ",") > -1
|| Eval("CustomCode").ToString().Length > 0 ?
"<td><select onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','sum');\" style=\"width:50px;\">"+
"<option value=\"0\"" + (Eval("sum").ToString()=="0" ? " selected=\"true\"" : "") + ">无</option>" +
"<option value=\"1\"" + (Eval("sum").ToString()=="1" ? " selected=\"true\"" : "") + ">求和</option>" +
"<option value=\"2\"" + (Eval("sum").ToString()=="2" ? " selected=\"true\"" : "") + ">平均数</option>" +
"<option value=\"3\"" + (Eval("sum").ToString()=="3" ? " selected=\"true\"" : "") + ">最大值</option>" +
"<option value=\"4\"" + (Eval("sum").ToString()=="4" ? " selected=\"true\"" : "") + ">最小值</option>" +
//sum,avg,max,min
"<select>"+
"</td>"
: "<td>&nbsp;</td>") 
: "") +
//"<td><input type=\"checkbox\" name=\"mliststate\" onclick=\"setModelItemPropertys(this,'" + Eval("ModelItemID") + "','mliststate');\"" + ( getPropertys(Container.DataItem,"mliststate").ToLower()=="true" ||  getPropertys(Container.DataItem,"mliststate")=="1"  ? " checked" : "") + " /></td>" + //要更新其他字段
"<td><input type=\"checkbox\" name=\"mliststate\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','mliststate');\"" + ( eBase.parseBool(Eval("mliststate").ToString().ToLower())  ? " checked" : "") + " /></td>" +

"<td>" + (eBase.parseBool(Eval("Custom").ToString()) && eBase.parseBool(Eval("showlist").ToString()) ? "<input class=\"text\" type=\"text\" value=\""+ Eval("frmname").ToString() + "\" oldvalue=\""+ Eval("frmname").ToString() + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','frmname');\">" : Eval("frmName") ) + "</td>" +
"<td>" + (eBase.parseBool(Eval("Custom").ToString()) && eBase.parseBool(Eval("showlist").ToString()) ? "<input class=\"text\" type=\"text\" value=\""+ Eval("frmid").ToString() + "\" oldvalue=\""+ Eval("frmid").ToString() + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','frmid');\">" : Eval("frmID") ) + "</td>" +
"<td><input class=\"text\" type=\"text\" value=\""+ (Eval("listwidth").ToString()=="0" ? "" : Eval("listwidth").ToString()) + "\" oldvalue=\""+ (Eval("listwidth").ToString()=="0" ? "" : Eval("listwidth").ToString()) + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','listwidth');\"></td>" +
"<td><input class=\"text\" type=\"text\" value=\""+ (Eval("mlistwidth").ToString()=="0" ? "" : Eval("mlistwidth").ToString()) + "\" oldvalue=\""+ (Eval("mlistwidth").ToString()=="0" ? "" : Eval("mlistwidth").ToString()) + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','mlistwidth');\"></td>" +

"<td><input type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','orderby');\"" + (eBase.parseBool(Eval("orderby").ToString()) ? " checked" : "") + " style=\"" + (Eval("Custom").ToString()=="True" && Eval("CustomCode").ToString().Length==0 ? "display:none;" : "") + "\" /></td>" +
"<td><input type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','move');\"" + (eBase.parseBool(Eval("move").ToString()) ? " checked" : "") + " /></td>" +
"<td><input type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','size');\"" + (eBase.parseBool(Eval("size").ToString()) ? " checked" : "") + " /></td>" +
"<td>"+
"<select onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','headeralign');\" style=\"width:50px;\">" +
"<option value=\"left\"" + (Eval("headeralign").ToString()=="left" ? " selected=\"true\"" : "") + ">左对齐</option>" +
"<option value=\"center\"" + (Eval("headeralign").ToString()=="center" ? " selected=\"true\"" : "") + ">居中对齐</option>" +
"<option value=\"right\"" + (Eval("headeralign").ToString()=="right" ? " selected=\"true\"" : "") + ">右对齐</option>" +
"<select>"+
"<label style=\"margin-left:5px;\"><input type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','HeaderAutoNewLine');\"" + (eBase.parseBool(Eval("HeaderAutoNewLine").ToString()) ? " checked" : "") + " />换行</label>"+
"</td>" +
"<td>"+
"<select onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','bodyalign');\" style=\"width:50px;\">" +
"<option value=\"left\"" + (Eval("bodyalign").ToString()=="left" ? " selected=\"true\"" : "") + ">左对齐</option>" +
"<option value=\"center\"" + (Eval("bodyalign").ToString()=="center" ? " selected=\"true\"" : "") + ">居中对齐</option>" +
"<option value=\"right\"" + (Eval("bodyalign").ToString()=="right" ? " selected=\"true\"" : "") + ">右对齐</option>" +
"<select>"+
"<label style=\"margin-left:5px;\"><input type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','BodyAutoNewLine');\"" + (eBase.parseBool(Eval("BodyAutoNewLine").ToString()) ? " checked" : "") + " />换行</label>"+
"</td>" +
"<td><input class=\"text\" type=\"text\" value=\""+ HttpUtility.HtmlEncode(Eval("listhtml").ToString()) + "\" oldvalue=\""+ HttpUtility.HtmlEncode(Eval("listhtml").ToString()) + "\" title=\"格式如：{data:id}\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-自定义查看');\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','listhtml');\"></td>" +

//"<td><input type=\"checkbox\" name=\"defaultorder\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','defaultorder');\"" + (Eval("defaultorder").ToString()=="0" ? "" : " checked") + " style=\"" + (Eval("Custom").ToString()=="True" || Eval("Type").ToString()=="text" ? "display:none;" : "") + "\" />"+
//"<select onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','defaultorder');\" style=\"width:60px;" + (Eval("defaultorder").ToString()=="0" ? "display:none;" : "") + "\">" +
//"<option value=\"1\"" + (Eval("defaultorder").ToString()=="1" ? " selected=\"true\"" : "") + ">升序</option>" +
//"<option value=\"2\"" + (Eval("defaultorder").ToString()=="2" ? " selected=\"true\"" : "") + ">降序</option>" +
//"</select>" +
//"<input type=\"text\" value=\""+ Eval("defaultOrderPX") + "\" oldvalue=\""+ Eval("defaultOrderPX") + "\" style=\"width:60px;" + (Eval("defaultorder").ToString()=="0" ? "display:none;":"") + "\" class=\"edit\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','defaultorderpx');\">" + 
//"</td>" +
//"<td>"+
//"<select onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','condition');\" style=\"width:100px;" + (Eval("Custom").ToString()=="True" ? "display:none;" : "")+ "\">" +     eBase.getOperator(Eval("condition").ToString()) +
//"</select>" +
//"<input type=\"text\" value=\""+ Eval("conditionvalue") + "\" oldvalue=\""+ Eval("conditionvalue") + "\" style=\"width:60px;" + (Eval("condition").ToString().Length==0 ? "display:none;":"") + "\" class=\"edit\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','conditionvalue');\">" + 

"</td>" +
//"<td><input class=\"text\" reload=\"true\" type=\"text\" value=\""+ (Eval("listorder").ToString()=="999999" ? "" : Eval("listorder").ToString()) + "\" oldvalue=\""+ (Eval("listorder").ToString()=="999999" ? "" : Eval("listorder").ToString()) + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','listorder');\"></td>"+
"<td style=\"cursor:move;\">" + (Container.ItemIndex + 1) + "</td>"+
"</tr>"
%>
</itemtemplate>
<footertemplate><%#"</tbody></table>"%></footertemplate>
</asp:Repeater>

</body>
</html>
