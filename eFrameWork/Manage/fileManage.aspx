﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="fileManage.aspx.cs" Inherits="eFrameWork.Manage.fileManage" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a>&nbsp;->&nbsp;<asp:Literal id="litNav" runat="server" /></div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
 <style>
  html, body {
    margin:0px;padding:0px;font-size:13px;
    }
</style>
<script>
var path = "<%=path%>";
</script>
<script>
var menu_a=null;
var menu_type=0;
var menu_name = "";
function _down()
{
    //var url = "?" + (typeof (ModelID) == "string" && ModelID.length > 0 ? "ModelID=" + ModelID + "&" : "") + "act=download" + (path.length > 0 ? "&path=" + path : "") + "&name=" + encodeURIComponent(menu_name);
    var url = "?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) {
        url += "AppItem=" + AppItem + "&";
    }
    else {
        if (typeof (ModelID) == "string" && ModelID.length > 0) {
            url += "ModelID=" + ModelID + "&";
        }
    }
    url += "act=download" + (path.length > 0 ? "&path=" + path : "") + "&name=" + encodeURIComponent(menu_name);
    //alert(url);
    window.open(url);
};
function contextmenu(evt,obj,type,name)
{
    //type 1文件夹，2文件
    if (evt.button != 2) { return; }
    var ext = $(obj).attr("ext");
    if (type == 2) {
        $(".etreeMenu a:contains('查看')").show();
    }
    else {
        $(".etreeMenu a:contains('查看')").hide();
    }
    if (".bak".indexOf(ext) == -1 || ext.length == 0) {
        $(".etreeMenu a:contains('备份')").show();
    }
    else {
        $(".etreeMenu a:contains('备份')").hide();
    }
    if (".html.htm.css.js.txt".indexOf(ext) > -1 && ext.length>2)
    {
        $(".etreeMenu a:contains('编辑')").show();
    }
    else
    {
        $(".etreeMenu a:contains('编辑')").hide();
    }
    if (type == 2)
    {
        //$(".etreeMenu a:contains('下载')").attr("href","?modelid=" + ModelID + "&act=download" + (path.length>0 ? "&path=" + path : "") + "&name=" + name);
        var url = "?";
        if (typeof (AppItem) == "string" && AppItem.length > 0) url += "AppItem=" + AppItem + "&";
        if (typeof (ModelID) == "string" && ModelID.length > 0) url += "ModelID=" + ModelID + "&";
        url += "act=download" + (path.length > 0 ? "&path=" + path : "") + "&name=" + name;
        $(".etreeMenu a:contains('下载')").attr("href", url);
        $(".etreeMenu a:contains('下载')").show();
    }
    else {
        $(".etreeMenu a:contains('下载')").hide();
    }
    if (".zip".indexOf(ext) == -1 || ext.length==0) {
        $(".etreeMenu a:contains('压缩')").show();
    }
    else {
        $(".etreeMenu a:contains('压缩')").hide();
    }
    if (".zip".indexOf(ext) > -1 && ext.length > 2) {
        $(".etreeMenu a:contains('解压')").show();
    }
    else {
        $(".etreeMenu a:contains('解压')").hide();
    }
	menu_type=type;
	menu_a=obj;
	menu_name=name;
	var oRect = obj.getBoundingClientRect();	
	var top = eScroll().top + oRect.top + obj.offsetHeight;
	var left = eScroll().left + oRect.right - obj.offsetWidth;	
	top= eScroll().top + evt.clientY;
	left= eScroll().left + evt.clientX;
	var menu=getobj("etreeMenu");
	menu.style.top=top + "px";
	menu.style.left=left + "px";
	show_etreeMenu();	
};
function bodyck(e)
{
	hide_etreeMenu();
};
function bodykd(e)
{
	e=window.event||e; 
	if(e.keyCode==27){hide_etreeMenu();}
};
function show_etreeMenu()
{
	var menu=getobj("etreeMenu");
	menu.style.display="";
	if(document.body.addEventListener){document.body.addEventListener("keydown",bodykd, false);}else{document.body.attachEvent("onkeydown",bodykd);}
	if(document.body.addEventListener){document.body.addEventListener("click",bodyck, false);}else{document.body.attachEvent("onclick",bodyck);}
};
function hide_etreeMenu()
{
	var menu=getobj("etreeMenu");
	menu.style.display="none";
	if (document.body.addEventListener){document.body.removeEventListener('keydown', bodykd, false);}else{document.body.detachEvent("onkeydown",bodykd);}
	if (document.body.addEventListener){document.body.removeEventListener('click', bodyck, false);}else{document.body.detachEvent("onclick",bodyck);}
};
$(document).ready(function()
{
    //document.body.oncontextmenu = function (e) { alert(e.target); return false; };
    //document.body.oncontextmenu = function () { return false; };	
    document.body.oncontextmenu = function () {
        var src = getEventObject();
        //document.title = src.tagName;
        if (src.tagName == "DT" || src.tagName == "DD" || src.tagName == "DIV" || src.tagName == "IMG") {
            return false;
        }
        return true;
    };
});
//查看文件
function file_view() {
    var path = $(menu_a).attr("data-clipboard-text");
    viewFile(path);
};
//重命名文件、文件夹
function file_rename() {
    //ReName(menu_name,menu_type);
   
    var name = menu_name;
    if (menu_type == 2) {
        var idx = menu_name.lastIndexOf(".");
        var name = menu_name.substring(0, idx);
    }
    layer.prompt({ title: '输入文件' + (menu_type == 1 ? '夹' : '') + '名称', formType: 3, value: name }, function (value, index) {
        var url = "?";
        if (typeof (AppItem) == "string" && AppItem.length > 0) url += "AppItem=" + AppItem + "&";
        if (typeof (ModelID) == "string" && ModelID.length > 0) url += "ModelID=" + ModelID + "&";
        url += "act=rename" + (path.length > 0 ? "&path=" + path : "") + "&type=" + menu_type + "&oldname=" + menu_name + "&newname=" + value;
        url += "&ajax=true&t=" + now();
        layer.close(index);
        //document.location.href = url;
        $.ajax({
            type: "get",
            async: false,
            url: url,
            dataType: "json",
            success: function (data) {
                if (data.success == "1") {
                    if (data.errcode == "0") {
                        file_reload();
                    }
                    layer.msg(data.message);
                }
            }
        });

    });
};
//备份文件
function file_back() {
    var url = "?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) url += "AppItem=" + AppItem + "&";
    if (typeof (ModelID) == "string" && ModelID.length > 0) url += "ModelID=" + ModelID + "&";
    url += "act=bak&type=" + menu_type + (path.length > 0 ? "&path=" + path : "") + "&name=" + encodeURIComponent(menu_name);
    url += "&ajax=true&t=" + now();
    //document.location.assign(url);
    $.ajax({
        type: "get",
        async: false,
        url: url,
        dataType: "json",
        success: function (data) {
            if (data.success == "1") {
                file_reload();
                layer.msg(data.message);
            }
        }
    });
};
//删除文件、文件夹
function file_del() {
    //var _back=confirm('您确定要删除该文件' + (menu_type==1 ? '夹' : '') + '吗？'); if(!_back){return;};
    var url = "?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) url += "AppItem=" + AppItem + "&";
    if (typeof (ModelID) == "string" && ModelID.length > 0) url += "ModelID=" + ModelID + "&";
    url += "act=del&type=" + menu_type + (path.length > 0 ? "&path=" + path : "") + "&name=" + encodeURIComponent(menu_name);
    url += "&ajax=true&t=" + now();
    //document.location.assign(url);
    layer.confirm('您确定要删除该文件' + (menu_type == 1 ? '夹' : '') + '吗？', {
        title: "文件" + (menu_type == 1 ? "夹" : "") + "删除确认?"
        , btn: ['删除', '取消'] //按钮
        , shadeClose: true //点击遮罩关闭层
    }, function (index) {
        //document.location.assign(url);
        //
        $.ajax({
            type: "get",
            async: false,
            url: url,
            dataType: "json",
            success: function (data) {
                layer.close(index);
                if (data.success == "1") {
                    file_reload();
                    layer.msg(data.message);
                }
            }
        });

    });
};
var eaceditor;
//编辑文件
function file_edit()
{
    var name = menu_name;
    //edit(name);  //eControls.js
    //return;
    var url = "?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) url += "AppItem=" + AppItem + "&";
    if (typeof (ModelID) == "string" && ModelID.length > 0) url += "ModelID=" + ModelID + "&";
    url += "act=edit" + (path.length > 0 ? "&path=" + path : "") + "&name=" + name;
    $.ajax({
        type: "get",
        async: false,
        url: url,
        dataType: "json",
        success: function (json) {
            if (json.success == "1") {
                //alert(json.value);
                var html = '';
                if (name.toLowerCase().indexOf(".html") > -1) {
                    html += '<div style="padding:8px;background-color:#f2f2f2;"><a href="javascript:;" onclick="showHtmlTag();" style="color:#0026ff;">插入数据</a>&nbsp;&nbsp;<a href="javascript:;" onclick="showPathSelect();" style="color:#0026ff;">插入库文件</a></div>';
                }
                //html+='<textarea id="content" name="content" class="autoNumber" style="width:98%;height:540px;">' + json.value + '</textarea>';

                html += '<pre id="eaceditor" style="">';
                html += json.value;
                html += '</pre>';


                ///
                layer.open({
                    type: 1 //此处以iframe举例
                , title: name
                    //,skin: 'layui-layer-molv' //加上边框 layui-layer-rim  layui-layer-lan layui-layer-molv layer-ext-moon
                , shadeClose: true //点击遮罩关闭层
                , area: ['90%', '90%']
                , shade: 0.2 //透明度
                , maxmin: false
                , resize: false
                , btnAlign: 'l' //lcr
                , moveType: 0 //拖拽模式，0或者1
                , anim: 0 //0-6的动画形式，-1不开启
                , content: html
                , btn: ['保存修改', '关闭'] //只是为了演示
                , yes: function (index, layero) {

                    //var value=$("#content").val().replace(/\r/ig,"").replace(/\n/ig,"0x\\r\\n");
                    //var value=$("#content").val().encode();  
                    var value = eaceditor.getValue();
                    url = "?";
                    if (typeof (AppItem) == "string" && AppItem.length > 0) {
                        url += "AppItem=" + AppItem + "&";
                    }
                    else {
                        if (typeof (ModelID) == "string" && ModelID.length > 0) {
                            url += "ModelID=" + ModelID + "&";
                        }
                    }
                    url += "act=save" + (path.length > 0 ? "&path=" + path : "") + "&name=" + name;
                    $.ajax({
                        type: "post",
                        async: true,
                        data: { value: value },
                        url: url,
                        dataType: "json",
                        success: function (data) {
                            layer.msg(data.message);
                            //layer.close(index); 不关闭、继续修改
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                        }
                    });

                }
                , cancel: function (index, layero) {
                    //layer.msg("X关闭!");
                }
                , success: function (layero, index) {
                    var h = $('#layui-layer' + index).height() - 140;
                    var w = $('#layui-layer' + index).width() - 6;

                    //alert($('#layui-layer'+ index).height());
                    /*
                 
                    $("#content").height(h).width(w);
                    $("#content").setTextareaCount({width: "40px",bgColor: "#f2f2f2",color: "#000",display: "inline-block"});
                    */
                    eaceditor = ace.edit("eaceditor");
                    eaceditor.setTheme("ace/theme/tomorrow");//tomorrow,twilight
                    eaceditor.session.setMode("ace/mode/html");
                    $("#eaceditor").height(h).width(w);
                }
                });

                ///
            }
        }
    });
};
//压缩文件
function file_zip() {
    var url = "?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) url += "AppItem=" + AppItem + "&";
    if (typeof (ModelID) == "string" && ModelID.length > 0) url += "ModelID=" + ModelID + "&";
    url += "act=zip&type=" + menu_type + (path.length > 0 ? "&path=" + path : "") + "&name=" + encodeURIComponent(menu_name);
    url += "&ajax=true&t=" + now();
    //document.location.assign(url);
    $.ajax({
        type: "get",
        async: false,
        url: url,
        dataType: "json",
        success: function (data) {
            if (data.success == "1") {
                file_reload();
                layer.msg(data.message);
            }
        }
    });
};
//解压文件
function file_unzip() {
    //var _back = confirm('解压会覆盖现有文件,确定要执行吗?');
    //if(!_back){return;};
    var url = "?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) url += "AppItem=" + AppItem + "&";
    if (typeof (ModelID) == "string" && ModelID.length > 0) url += "ModelID=" + ModelID + "&";
    url += "act=unzip&type=" + menu_type + (path.length > 0 ? "&path=" + path : "") + "&name=" + encodeURIComponent(menu_name);
    url += "&ajax=true&t=" + now();
    //document.location.assign(url);
    layer.confirm('解压会覆盖现有文件,确定要执行吗?', {
        title: "文件解压确认?"
        , btn: ['解压', '取消'] //按钮
        , shadeClose: true //点击遮罩关闭层
    }, function (index) {
        //document.location.assign(url);
        $.ajax({
            type: "get",
            async: false,
            url: url,
            dataType: "json",
            success: function (data) {
                layer.close(index);
                if (data.success == "1") {
                    file_reload();
                    layer.msg(data.message);
                }
            }
        });
    });
};
//重新加载
function file_reload() {
    var url = "?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) url += "AppItem=" + AppItem + "&";
    if (typeof (ModelID) == "string" && ModelID.length > 0) url += "ModelID=" + ModelID + "&";
    url += "act=reload&ajax=true" + (path.length > 0 ? "&path=" + path : "");
    url += "&t=" + now();
    $.ajax({
        type: "get",
        async: false,
        url: url,
        dataType: "json",
        success: function (data) {
            if (data.success == "1") {
                $("#filemanage_files").html(data.body);
            }
        }
    });
};
//文件上传回调
function autoupload_callback(files) {
    file_reload();
};

//新建文件
function file_newFile() {
    layer.prompt({ title: '输入文件名称', formType: 3, value: '' }, function (value, index) {
        //var url ="?" + (typeof(AppItem)=="string" && AppItem.length > 0 ? "AppItem=" + AppItem  : "ModelID=" + ModelID ) + "&act=addfile" + (path.length>0 ? "&path=" + path : "") + "&name=" + value;
        var url = "?";
        if (typeof (AppItem) == "string" && AppItem.length > 0) url += "AppItem=" + AppItem + "&";
        if (typeof (ModelID) == "string" && ModelID.length > 0) url += "ModelID=" + ModelID + "&";
        url += "act=newfile" + (path.length > 0 ? "&path=" + path : "") + "&name=" + value;
        url += "&ajax=true&t=" + now();
        layer.close(index);
        //document.location.href = url;
        $.ajax({
            type: "get",
            async: false,
            url: url,
            dataType: "json",
            success: function (data) {
                if (data.success == "1") {
                    if (data.errcode == "0") {
                        file_reload();
                    }
                    layer.msg(data.message);
                }
            }
        });
    });
};
//新建文件夹
function file_newFolder() {
    layer.prompt({ title: '输入文件夹名称', formType: 3, value: name }, function (value, index) {
        //var url = "?" +  (typeof(AppItem)=="string" && AppItem.length > 0 ? "AppItem=" + AppItem  : "ModelID=" + ModelID ) + "&act=addfolder" + (path.length>0 ? "&path=" + path : "") + "&name=" + value;
        var url = "?";
        if (typeof (AppItem) == "string" && AppItem.length > 0) url += "AppItem=" + AppItem + "&";
        if (typeof (ModelID) == "string" && ModelID.length > 0) url += "ModelID=" + ModelID + "&";
        url += "act=newfolder" + (path.length > 0 ? "&path=" + path : "") + "&name=" + value;
        url += "&ajax=true&t=" + now();
        layer.close(index);
        //document.location.href = url;
        $.ajax({
            type: "get",
            async: false,
            url: url,
            dataType: "json",
            success: function (data) {
                if (data.success == "1") {
                    file_reload();
                    layer.msg(data.message);
                }
            }
        });
    });
};
</script>
<div id="etreeMenu" class="etreeMenu" style="display:none;z-index:1000;">
<a href="javascript:;" onclick="file_view();">查看</a>
<%if (Power["edit"])
  { %><a href="javascript:;" onclick="file_edit();">编辑</a><%} %>
<%if (Power["rename"])
  { %><a href="javascript:;" onclick="file_rename();">重命名</a><%} %>
<%if (Power["backup"]) 
  { %><a href="javascript:;" onclick="file_back();">备份</a><%} %>
<%if (Power["zip"]) 
  { %><a href="javascript:;" onclick="file_zip();">压缩</a><%} %>
<%if (Power["unzip"]) 
  { %><a href="javascript:;" onclick="file_unzip();">解压</a><%} %>
<%if (Power["download"])
  { %><a href="javascript:;" onclickbak="_down();" target="_blank">下载</a><%} %>
<%if (Power["del"])  
  { %><a href="javascript:;" onclick="file_del();">删除</a><%} %>
</div>
<div style="margin:10px;">
<asp:Literal id="litBody" runat="server" />
</div>

<script type="text/javascript">
    var clip = new Clipboard(".copypath");
    clip.on('success', function (e) {
        //layer.open({ content: "复制成功!请打开要赠送好友的聊天窗口粘贴.", skin: "msg", time: 2 });
        layer.msg("地址复制成功!");
    });

    clip.on('error', function (e) {
        //alert("复制失败")
    });
</script>
</asp:Content>