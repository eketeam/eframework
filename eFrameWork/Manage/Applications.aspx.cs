﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;


namespace eFrameWork.Manage
{
    public partial class Applications : System.Web.UI.Page
    {
        public string act = eParameters.Request("act");
        public eForm edt;
        public eAction Action;
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");           

            edt = new eForm("a_eke_sysApplications", user);
            edt.AddControl(eFormControlGroup);
            Action = new eAction();
            Action.Actioning += new eActionHandler(Action_Actioning);
            Action.Listen();
        }
        protected void Action_Actioning(string Actioning)
        {
            switch (Actioning)
            {
                case "":
                    eList elist = new eList("a_eke_sysApplications");
                    elist.Fields.Add("ApplicationID,MC,SM,addTime");
                    elist.Where.Add("delTag=0");
                    elist.Where.Add("ServiceID" + (user["ServiceID"].Length == 0 ? " is null" : "='" + user["ServiceID"] + "'"));
                    elist.OrderBy.Default = "isnull(px,999999),addTime";//默认排序
                    elist.Bind(eDataTable, ePageControl1);

                    break;
                default:
                    edt.onChange += new eFormTableEventHandler(edt_onChange);
                    edt.Handle();
                    break;
            }
            //Response.Write(Actioning + "A");
        }
        public void edt_onChange(object sender, eFormTableEventArgs e)
        {
            if (e.eventType == eFormTableEventType.Inserting)
            {
                if (user["ServiceID"].Length > 0) edt.Fields.Add("ServiceID", user["ServiceID"]);
            }
            if (e.eventType == eFormTableEventType.Deleted)
            {
                string sql = "update a_eke_sysApplicationItems set delTag=1 where ApplicationID='" + e.ID + "'";
                eBase.DataBase.Execute(sql);
            }
            if (e.eventType == eFormTableEventType.Updated || e.eventType == eFormTableEventType.Inserted || e.eventType == eFormTableEventType.Deleted)
            {
                //清除相关缓存
                eBase.clearDataCache("a_eke_sysApplications");
                eBase.clearDataCache("a_eke_sysSites");
                eBase.clearDataCache("a_eke_sysSiteItems");
                runtimeCache.Remove();
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {
                lit.Text = "应用管理 - " + eConfig.manageName(); 
            }
        }
    }
}