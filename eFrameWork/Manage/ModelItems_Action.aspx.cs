﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;

namespace eFrameWork.Manage
{
    public partial class ModelItems_Action : System.Web.UI.Page
    {
        public string modelid = eParameters.QueryString("modelid");
        private eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            Response.Write("<a href=\"http://frame.eketeam.com\" style=\"float:right;\" target=\"_blank\" title=\"eFrameWork开发框架\"><img src=\"images/help.gif\"></a>");
            
            if (eConfig.showHelp())
            {
                Response.Write("<div class=\"tips\" style=\"margin-bottom:8px;\">");
                Response.Write("<b>动作</b><br>");
                Response.Write("模块扩展功能需要执行的SQL语句。<br>");
                Response.Write("1.多个执行语句之间用;换行隔开，/*注释内容*/，以--或//开始注释。<br>");
                Response.Write("2.select().each( update table set deletag=0 where id={data:id})<br>");
                Response.Write("3.(select * from table where id='{data:id}').each( update tableb set body='{data:body}' where id='{data:id}')<br>");
                Response.Write("4.系统编码：insert(添加),update(编辑),view(查看),del(删除),checkup(审核),all(所有动作)<br>");
                Response.Write("5.调用函数：{interface:命名空间.类名.函数名(参数1，参数2)}<br>");
                Response.Write("</div> ");
            }

            eList elist = new eList("a_eke_sysActions");
            elist.Where.Add("ModelID='" + modelid + "' ");
            elist.OrderBy.Add("addTime");
            elist.Bind(Rep);

            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Rep.RenderControl(htw);
            Rep.Visible = false;//不输出，要在获取后设，不然取不到内容。
            Response.Write(sw.ToString());

            //DataTable tb = eBase.DataBase.getDataTable("select ViewSQL,AddSQL,EditSQL,DeleteSQL from a_eke_sysModels where ModelID='" + modelid + "'");
            DataTable tb = eBase.DataBase.getDataTable("select * from a_eke_sysModels where ModelID='" + modelid + "'");
             if (tb.Rows.Count > 0)
             {
                 Response.Write("<dl class=\"ePanel\">\r\n");
                 Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>查看完成</h1></dt>\r\n");
                 Response.Write("<dd style=\"display:none;\">");
                 Response.Write("执行SQL：<br>");
                 //if (eConfig.showHelp()) Response.Write("<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(127);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">");
                 Response.Write(" <textarea htmltag=\"true\" id=\"txtviewsql\" name=\"txtviewsql\" style=\"width:95%;\" cols=\"100\" rows=\"10\" on_Blur=\"setModel(this,'viewsql');\" oldvalue=\"" + HttpUtility.HtmlEncode(tb.Rows[0]["ViewSQL"].ToString()) + "\">" + HttpUtility.HtmlEncode(tb.Rows[0]["ViewSQL"].ToString()) + "</textarea><br>\r\n");
                 Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtviewsql,'viewsql');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" />\r\n");
                 Response.Write("</dd>\r\n");
                 Response.Write("</dl>\r\n");

                 Response.Write("<dl class=\"ePanel\">\r\n");
                 Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>添加完成</h1></dt>\r\n");
                 Response.Write("<dd style=\"display:none;\">");
                 Response.Write("执行SQL：<br>");
                 if (eConfig.showHelp()) Response.Write("<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(127);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">");
                 Response.Write(" <textarea htmltag=\"true\" id=\"txtaddsql\" name=\"txtaddsql\" style=\"width:95%;\" cols=\"100\" rows=\"10\"  on_Blur=\"setModel(this,'addsql');\" oldvalue=\"" + HttpUtility.HtmlEncode(tb.Rows[0]["AddSQL"].ToString()) + "\">" + HttpUtility.HtmlEncode(tb.Rows[0]["AddSQL"].ToString()) + "</textarea><br>\r\n");
                 Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtaddsql,'addsql');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" /><br>\r\n");
                 Response.Write("跳转URL：<input type=\"text\" value=\"" + tb.Rows[0]["addFinshURL"].ToString() + "\" oldvalue=\"" + tb.Rows[0]["addFinshURL"].ToString() + "\"  class=\"edit\" style=\"width:350px;\" onBlur=\"setModel(this,'addFinshURL');\" /><br>");
                 Response.Write("</dd>\r\n");
                 Response.Write("</dl>\r\n");

                 Response.Write("<dl class=\"ePanel\">\r\n");
                 Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>修改完成</h1></dt>\r\n");
                 Response.Write("<dd style=\"display:none;\">");
                 Response.Write("执行SQL：<br>");
                 if (eConfig.showHelp()) Response.Write("<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(128);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">");
                 Response.Write(" <textarea htmltag=\"true\" id=\"txteditsql\" name=\"txteditsql\" style=\"width:95%;\" cols=\"100\" rows=\"10\"  on_Blur=\"setModel(this,'editsql');\" oldvalue=\"" + HttpUtility.HtmlEncode(tb.Rows[0]["EditSQL"].ToString()) + "\">" + HttpUtility.HtmlEncode(tb.Rows[0]["EditSQL"].ToString()) + "</textarea><br>\r\n");
                 Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txteditsql,'editsql');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" /><br>\r\n");
                 Response.Write("跳转URL：<input type=\"text\" value=\"" + tb.Rows[0]["editFinshURL"].ToString() + "\" oldvalue=\"" + tb.Rows[0]["editFinshURL"].ToString() + "\"  class=\"edit\" style=\"width:350px;\" onBlur=\"setModel(this,'editFinshURL');\" /><br>");
                 Response.Write("</dd>\r\n");
                 Response.Write("</dl>\r\n");

                 Response.Write("<dl class=\"ePanel\">\r\n");
                 Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>删除完成</h1></dt>\r\n");
                 Response.Write("<dd style=\"display:none;\">");
                 Response.Write("执行SQL：<br>");
                 if (eConfig.showHelp()) Response.Write("<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(129);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">");
                 Response.Write(" <textarea htmltag=\"true\" id=\"txtdeletesql\" name=\"txtdeletesql\" style=\"width:95%;\" cols=\"100\" rows=\"10\"  on_Blur=\"setModel(this,'deletesql');\" oldvalue=\"" + HttpUtility.HtmlEncode(tb.Rows[0]["DeleteSQL"].ToString()) + "\">" + HttpUtility.HtmlEncode(tb.Rows[0]["DeleteSQL"].ToString()) + "</textarea><br>\r\n");
                 Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtdeletesql,'deletesql');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" /><br>\r\n");
                 Response.Write("跳转URL：<input type=\"text\" value=\"" + tb.Rows[0]["delFinshURL"].ToString() + "\" oldvalue=\"" + tb.Rows[0]["delFinshURL"].ToString() + "\"  class=\"edit\" style=\"width:350px;\" onBlur=\"setModel(this,'delFinshURL');\" /><br>");
                 Response.Write("</dd>\r\n");
                 Response.Write("</dl>\r\n");
             }
            Response.End();
        }
    }
}