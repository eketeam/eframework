﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace eFrameWork.Manage
{
    public partial class Reports : System.Web.UI.Page
    {
        public string id = eParameters.Request("id");
        public string act = eParameters.Request("act");
        public string sql = "";
        public eForm edt;
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            edt = new eForm("a_eke_sysReports", user);

            if (act.Length == 0)
            {
                List();
                return;
            }
            if (act == "export")
            {
                string xml = ExportReports();

                //eBase.WriteHTML(xml);
                byte[] buffer = Encoding.UTF8.GetBytes(xml);
                byte[] outBuffer = new byte[buffer.Length + 3];
                outBuffer[0] = (byte)0xEF;
                outBuffer[1] = (byte)0xBB;
                outBuffer[2] = (byte)0xBF;
                Array.Copy(buffer, 0, outBuffer, 3, buffer.Length);

                string modelName = "报表";
                string fileName = modelName + ".efw";
                if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToLower().IndexOf("msie") > -1) fileName = HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8);  //IE需要编码
                //Response.ContentType = "text/xml";
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Accept-Ranges", "bytes");
                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
                Response.Write(Encoding.UTF8.GetString(outBuffer));
                Response.End();

                Response.End();
            }
            if (act == "del")
            {
                string id = eParameters.QueryString("id");
                eBase.DataBase.Execute("delete from a_eke_sysReports where ReportID='" + id + "'");
                eBase.DataBase.Execute("delete from a_eke_sysReportItems where ReportID='" + id + "'");

                eBase.clearDataCache(); //清除所有缓存
                if (Request.ServerVariables["HTTP_REFERER"] == null)
                {
                    Response.Redirect("Reports.aspx", true);
                }
                else
                {
                    Response.Redirect(Request.ServerVariables["HTTP_REFERER"].ToString(), true);
                }
            }
            #region 信息添加、编辑
            f2.Options = eBase.ReportControlType.ToJson();
            edt.AddControl(eFormControlGroup);
            edt.onChange += new eFormTableEventHandler(edt_onChange);
            edt.Handle();
            #endregion
        }
        private string ExportReports()
        {
            XmlDocument doc = new XmlDocument();
            string ids = eParameters.QueryString("reportids");
            ids = "'" + ids.Replace(",", "','") + "'";

            eList a_eke_sysReports = new eList("a_eke_sysReports");
            a_eke_sysReports.Where.Add("deltag=0");
            a_eke_sysReports.Where.Add("ReportID in (" + ids + ")");


            eList a_eke_sysReportItems = new eList("a_eke_sysReportItems");
            a_eke_sysReportItems.Where.Add("deltag=0");
            a_eke_sysReports.Add(a_eke_sysReportItems);


            DataTable Reports = a_eke_sysReports.getDataTable();

            if (Reports.Columns.Contains("addTime")) Reports.Columns.Remove("addTime");
            if (Reports.Columns.Contains("DataSourceID")) Reports.Columns.Remove("DataSourceID");
            if (Reports.Columns.Contains("LabelIDS")) Reports.Columns.Remove("LabelIDS");
            if (Reports.Columns.Contains("ServiceID")) Reports.Columns.Remove("ServiceID");

            //eBase.PrintDataTable(Reports);

            DataTable ReportItems = a_eke_sysReportItems.getDataTable();

            //eBase.PrintDataTable(ReportItems);
            //Response.Write(ids);

            #region 生成XML
            Reports.ExtendedProperties.Add("name", "a_eke_sysReports");
            doc.appendData(Reports);
            ReportItems.ExtendedProperties.Add("name", "a_eke_sysReportItems");
            doc.appendData(ReportItems);
            #endregion

            return doc.InnerXml;
        }
        public void edt_onChange(object sender, eFormTableEventArgs e)
        {
            if (e.eventType == eFormTableEventType.Inserting)
            {
                if (user["ServiceID"].Length > 0) edt.Fields.Add("ServiceID", user["ServiceID"]);
            }
        }
        private void List()
        {
            eList datalist = new eList("a_eke_sysReports");
            datalist.Where.Add("delTag=0 and ModelID is null");
            datalist.Where.Add("ServiceID" + (user["ServiceID"].Length == 0 ? " is null" : "='" + user["ServiceID"] + "'"));
            datalist.OrderBy.Add("addTime desc");
            datalist.Bind(Rep, ePageControl1);
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {
                lit.Text = "报表 - " + eConfig.manageName();
            }
        }
    }
}