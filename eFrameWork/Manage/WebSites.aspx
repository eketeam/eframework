﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebSites.aspx.cs" Inherits="Manage_WebSites" %>
<%@ Import Namespace="System.Data" %>
<style>
input.text {
    width: 210px;padding-left:6px;
    height: 22px;
line-height: 22px;
color: #444;
border: 1px solid #ccc;
margin-right:20px;
}
    a.del {display:inline-block;width:20px;height:20px;margin-right:10px;font-size:0px;

    background:url(../images/del.png) no-repeat center center;
    }
    .webitem {
    margin-bottom:10px;
    }
    .webitem li {
        border: 1px dashed transparent;
        height:35px;line-height:35px;  
        padding-left:6px;  
    }
    .webitem li:hover {
        border: 1px dashed #ccc;
        background-color: #f2f2f2;
    }
    .webitem li * {
     vertical-align:middle;
    }
</style>
<script>
    function getitemhtml()
    {
        var html = '<li><a href="javascript:;" class="del" onclick="delsite(this);"></a>网站名称：<input type="text" name="name" id="name" onblur="upsite(this);" class="text" />绑定域名：<input type="text" id="wdomain" name="wdomain" onblur="upsite(this);" class="text" /></li>';
        return html;
    };
    function addsite(btn)
    {
        var td = $(btn).parent();
        var ul = td.find("ul:eq(0)");
        var control = $("#f11");
      
        var model = control.val().toJson();
        //alert(model.toJson());
        //model.Convert = true;
        var item = new Object();
        item["id"] = "";
        item["name"] = "";
        item["wdomain"] = "";
        item["delete"] = "false";
        model.append(item);
        control.val(model.toJson());


        var itemhtml = getitemhtml();
        ul.append(itemhtml);

        //alert(btn);
    };
    function upsite(input)
    {
        var li = $(input).parent();      
        var index = li.index();
        //document.title = index;
        var control = $("#f11");

        var model = control.val().toJson();
        //alert(control.val());
        //model.Convert = true;
        //alert("AX" + model.tostring());
       // return;
        var item = model.get(index);
       // alert(JSON.stringify(item));
        //item.Convert = undefined;
        //alert(input.name + "::" + input.value);
        item[input.name] = input.value;
        //alert(JSON.stringify(item));
        //alert(model.tostring());
        control.val(model.toJson());
    };
    function delsite(a)
    {
        var li = $(a).parent();
        var index = li.index();
        var control = $("#f11");
        layer.confirm('', {btn: ['确定', '取消'], title: "确认要删除吗?",shadeClose: true}, function (idx, layero) {
            var model = control.val().toJson();
            //model.Convert = true;
            var item = model.get(index);
            if (item.id.toString().length > 0)
            {
                item["delete"] = "true";
                li.hide();
            }
            else
            {
                model.remove(index);
                li.remove();
            }
            control.val(model.toJson());
            layer.close(idx);
        });
    };
</script>
<textarea name="f11a" id="f11a" style="width:600px;height:90px;display:none;"><%=HttpUtility.HtmlEncode(Webs.toJSON()) %></textarea>
<input type="hidden" name="f11" id="f11" value="<%=HttpUtility.HtmlEncode(Webs.toJSON()) %>" />
<ul class="webitem">
<%foreach(DataRow dr in Webs.Rows) {%>
<li><a href="javascript:;" class="del" onclick="delsite(this);"></a>网站名称：<input type="text" name="name" id="name" onblur="upsite(this);" class="text" value="<%=dr["name"].ToString() %>" />绑定域名：<input type="text" id="wdomain" name="wdomain" onblur="upsite(this);" class="text" value="<%=dr["wdomain"].ToString() %>" /></li>
<%} %>
</ul>
<input type="button" name="Submit" value="添加" onclick="addsite(this);" style="padding:3px 15px 3px 15px;" />