﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;

namespace eFrameWork.Manage
{
    public partial class ModelItems_Layout : System.Web.UI.Page
    {
        public string modelid = eParameters.QueryString("modelid");
        private DataTable _modeltabs;
        public DataTable ModelTabs
        {
            get
            {
                if (_modeltabs == null)
                {
                    _modeltabs = eBase.DataBase.getDataTable("select * from a_eke_sysModelTabs where delTag=0 and ModelID='" + modelid + "' order by px,addTime");
                }
                return _modeltabs;
            }
        }
        private DataTable _modelpanels;
        public DataTable ModelPanels
        {
            get
            {
                if (_modelpanels == null)
                {
                    _modelpanels = eBase.DataBase.getDataTable("select * from a_eke_sysModelPanels where delTag=0 and ModelID='" + modelid + "' order by px,addTime");
                }
                return _modelpanels;
            }
        }
        private eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            Response.Write("<a href=\"http://frame.eketeam.com\" style=\"float:right;\" target=\"_blank\" title=\"eFrameWork开发框架\"><img src=\"images/help.gif\"></a>");
            
            if (eConfig.showHelp())
            {
                Response.Write("<div class=\"tips\" style=\"margin-bottom:6px;\">");
                Response.Write("<b>布局</b><br>");
                Response.Write("为表单添加选项卡、面板、列显示顺序、跨行、跨列等。<br>");
                Response.Write("建议完成表单所有功能再为表单布局。");
                Response.Write("</div> ");
            }


            RepTabs.DataSource = ModelTabs;
            RepTabs.DataBind();



            RepGroups.DataSource = ModelPanels;
            RepGroups.ItemDataBound += new RepeaterItemEventHandler(RepGroups_ItemDataBound);
            RepGroups.DataBind();


            #region 所有列


            /*
            
            string sql = "SELECT d.mc as modelName,a.MC, a.ModelItemID,a.ModelTabID,a.ModelPanelID,a.addrowspan,a.addcolspan,a.addorder FROM a_eke_sysModelItems a ";
            sql += " inner join a_eke_sysModels d on d.ModelID=a.ModelID ";
            sql += " left join a_eke_sysModelTabs b on a.ModelTabID=b.ModelTabID ";
            sql += " left join a_eke_sysModelPanels c on a.ModelPanelID=c.ModelPanelID ";
            sql += " where a.ModelID='" + modelid + "' and a.showAdd=1 and a.controlType<>'hidden' ";
            sql += " order by ISNULL(b.px,999999),ISNULL(c.px,999999),a.AddOrder, a.PX ";


            sql="select * from ";
            sql += " (";
            sql += " SELECT d.mc as modelName,a.MC, a.ModelItemID,a.ModelTabID,a.ModelPanelID,a.addrowspan,a.addcolspan,a.addorder,ISNULL(b.px,999999)as bpx,ISNULL(c.px,999999) as cpx,a.PX,a.addTime  FROM a_eke_sysModelItems a ";
            sql += " inner join a_eke_sysModels d on d.ModelID=a.ModelID";
            sql += " left join a_eke_sysModelTabs b on a.ModelTabID=b.ModelTabID ";
            sql += " left join a_eke_sysModelPanels c on a.ModelPanelID=c.ModelPanelID ";
            sql += " where a.ModelID='" + modelid + "' and a.delTag=0 and a.PackID is null and a.showAdd=1 and a.controlType<>'hidden' ";
            sql += " union ";
            sql += " SELECT d.mc as modelName,a.MC, a.ModelItemID,a.ModelTabID,a.ModelPanelID,a.addrowspan,a.addcolspan,a.addorder,ISNULL(b.px,999999)as bpx,ISNULL(c.px,999999) as cpx,a.PX,a.addTime  FROM a_eke_sysModelItems a ";
            sql += " inner join a_eke_sysModels d on d.ModelID=a.ModelID";
            sql += " left join a_eke_sysModelTabs b on a.ModelTabID=b.ModelTabID ";
            sql += " left join a_eke_sysModelPanels c on a.ModelPanelID=c.ModelPanelID ";
            sql += " where d.delTag=0 and d.ParentID ='" + modelid + "' and d.JoinMore=0 and a.showAdd=1 and a.controlType<>'hidden' ";
            sql += " ) as t";
            sql += " order by t.bpx,t.cpx,t.AddOrder,t.px,t.addTime";
             */

            string sql = "select * from ";
            sql += " (";
            //主模块
            sql += " SELECT d.mc as modelName,a.MC, a.ModelItemID,a.ModelTabID,a.ModelPanelID,a.addrowspan,a.addcolspan,a.addorder as itemOrder,ISNULL(b.px,999999) as tabpx,";
            sql += " b.addTime as tabTime,ISNULL(c.px,999999) as panelPX ,case when a.ModelTabID is null then 2 else 1 end as hasTab,";
            sql += " case when a.ModelPanelID is null then 2 else 1 end as hasPanel,";
            sql += " c.addTime as panelTime,a.PX as itemPX,a.addTime as itemTime ";
            sql += " FROM a_eke_sysModelItems a ";
            sql += " inner join a_eke_sysModels d on d.ModelID=a.ModelID";
            sql += " left join a_eke_sysModelTabs b on a.ModelTabID=b.ModelTabID ";
            sql += " left join a_eke_sysModelPanels c on a.ModelPanelID=c.ModelPanelID ";
            sql += " where a.ModelID='" + modelid + "' and a.delTag=0 and a.PackID is null and a.showAdd=1 and a.controlType<>'hidden' ";
            sql += " union ";
            //1v1子模块
            sql += " SELECT d.mc as modelName,a.MC, a.ModelItemID,a.ModelTabID,a.ModelPanelID,a.addrowspan,a.addcolspan,a.addorder as itemOrder,ISNULL(b.px,999999) as tabpx,";
            sql += " b.addTime as tabTime,ISNULL(c.px,999999) as panelPX ,case when a.ModelTabID is null then 2 else 1 end as hasTab,";
            sql += " case when a.ModelPanelID is null then 2 else 1 end as hasPanel,";
            sql += " c.addTime as panelTime,a.PX as itemPX,a.addTime as itemTime ";
            sql += " FROM a_eke_sysModelItems a ";
            sql += " inner join a_eke_sysModels d on d.ModelID=a.ModelID";
            sql += " left join a_eke_sysModelTabs b on a.ModelTabID=b.ModelTabID ";
            sql += " left join a_eke_sysModelPanels c on a.ModelPanelID=c.ModelPanelID ";
            sql += " where d.delTag=0 and d.ParentID ='" + modelid + "' and d.JoinMore=0 and a.showAdd=1 and a.controlType<>'hidden' ";
            sql += " ) as t";
            sql += " order by t.hasTab,t.tabpx,t.tabTime,t.hasPanel,t.panelPX,t.panelTime,t.itemOrder,t.itemPX,t.itemTime";

            //eBase.Writeln(sql);
            /*
            DataTable dt = eBase.DataBase.getDataTable(sql);
            RepColumns.ItemDataBound += new RepeaterItemEventHandler(RepColumns_ItemDataBound);
            RepColumns.DataSource = dt;
            RepColumns.DataBind();
            */
            list();
            #endregion

            #region 子模块
            sql = "SELECT ModelID,ModelTabID,ModelPanelID,MC FROM  a_eke_sysModels";
            sql += " where ParentID='" + modelid + "' and JoinMore=1 and Type in (1,7) and delTag=0";
            sql += " order by addTime";
            DataTable tb = eBase.DataBase.getDataTable(sql);
            RepModels.ItemDataBound += new RepeaterItemEventHandler(RepModels_ItemDataBound);
            RepModels.DataSource = tb;
            RepModels.DataBind();
            #endregion
        }
        private void list()
        {
            DataTable tb = getItems(modelid);
            appendItems(tb, modelid);
            //eBase.PrintDataTable(tb);
            tb = tb.Select("", "hasTab,tabpx,tabTime,hasPanel,panelPX,panelTime,itemOrder,itemPX,itemTime").toDataTable();
            RepColumns.ItemDataBound += new RepeaterItemEventHandler(RepColumns_ItemDataBound);
            RepColumns.DataSource = tb;
            RepColumns.DataBind();
        }
        private DataTable getItems(string modelid)
        {
            string sql = "SELECT d.mc as modelName,a.MC, a.ModelItemID,a.ModelTabID,a.ModelPanelID,a.addrowspan,a.addcolspan,a.addorder as itemOrder,ISNULL(b.px,999999) as tabpx,";
            sql += " b.addTime as tabTime,ISNULL(c.px,999999) as panelPX ,case when a.ModelTabID is null then 2 else 1 end as hasTab,";
            sql += " case when a.ModelPanelID is null then 2 else 1 end as hasPanel,";
            sql += " c.addTime as panelTime,a.PX as itemPX,a.addTime as itemTime ";
            sql += " FROM a_eke_sysModelItems a ";
            sql += " inner join a_eke_sysModels d on d.ModelID=a.ModelID";
            sql += " left join a_eke_sysModelTabs b on a.ModelTabID=b.ModelTabID ";
            sql += " left join a_eke_sysModelPanels c on a.ModelPanelID=c.ModelPanelID ";
            sql += " where a.ModelID='" + modelid + "' and a.delTag=0 and a.PackID is null and (a.showEdit=1 or a.showAdd=1 or a.showView=1) and a.controlType<>'hidden' ";
           // eBase.Writeln(sql);
            return eBase.DataBase.getDataTable(sql);
        }
        private void appendItems(DataTable tb, string modelid)
        {
            DataTable dt = eBase.DataBase.getDataTable("select modelid,mc from a_eke_sysModels where ParentID='" + modelid + "' and JoinMore=0 and show=1 and deltag=0");
            foreach (DataRow dr in dt.Rows)
            {
                DataTable tb2 = getItems(dr["modelid"].ToString());
                foreach (DataRow _dr in tb2.Rows)
                {
                    tb.Rows.Add(_dr.ItemArray);
                }
                appendItems(tb, dr["modelid"].ToString());
            }
        }

        //面板 绑定 选项卡 - OK
        protected void RepGroups_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Control ctrl = e.Item.Controls[0];
                Literal lit = (Literal)ctrl.FindControl("LitTabs");
                if (lit != null)
                {
                    //string sql = "select ModelTabID,MC from a_eke_sysModelTabs where ModelID='" + modelid + "' order by px,addTime";
                    //lit.Text = eBase.DataBase.getOptions(sql, "MC", "ModelTabID", DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString());
                    lit.Text = ModelTabs.toOptions("ModelTabID", "MC", DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString());
                }
            }
        }
        //所有列 - OK
        protected void RepColumns_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string sql = "";
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Control ctrl = e.Item.Controls[0];
                Literal lit = (Literal)ctrl.FindControl("LitTabs");
                if (lit != null)
                {
                    //sql = "select ModelTabID,MC from a_eke_sysModelTabs where ModelID='" + modelid + "' order by px,addTime";
                    //lit.Text = eBase.DataBase.getOptions(sql, "MC", "ModelTabID", DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString());
                    lit.Text = ModelTabs.toOptions("ModelTabID", "MC", DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString());
                }

                lit = (Literal)ctrl.FindControl("LitGroups");
                if (lit != null)
                {
                    DataTable tb;
                    if (DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString().Length > 0)
                    {
                        sql = "select ModelPanelID,MC from a_eke_sysModelPanels where delTag=0 and ModelID='" + modelid + "' and ModelTabID='" + DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString() + "'  order by px,addTime";
                        tb = ModelPanels.Select("", "ModelTabID='" + DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString() + "'", "px,addTime");
                    }
                    else
                    {
                        sql = "select ModelPanelID,MC from a_eke_sysModelPanels where delTag=0 and ModelID='" + modelid + "' and  ModelTabID is Null order by px,addTime";
                        tb = ModelPanels.Select("", "ModelTabID  is null", "px,addTime");
                    }
                    //lit.Text = eBase.DataBase.getOptions(sql, "MC", "ModelPanelID", DataBinder.Eval(e.Item.DataItem, "ModelPanelID").ToString());
                    lit.Text = tb.toOptions("ModelPanelID", "MC", DataBinder.Eval(e.Item.DataItem, "ModelPanelID").ToString());

                }
            }
        }
        // 子模块 - OK
        protected void RepModels_ItemDataBound(object sender, RepeaterItemEventArgs e)
        { 
             string sql = "";
             if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
             {
                 Control ctrl = e.Item.Controls[0];
                 Literal lit = (Literal)ctrl.FindControl("LitTabs");
                 if (lit != null)
                 {
                     //sql = "select ModelTabID,MC from a_eke_sysModelTabs where ModelID='" + modelid + "' order by px,addTime";
                     //lit.Text = eBase.DataBase.getOptions(sql, "MC", "ModelTabID", DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString());
                     lit.Text = ModelTabs.toOptions("ModelTabID", "MC", DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString());

                 }
                 lit = (Literal)ctrl.FindControl("LitGroups");
                 if (lit != null)
                 {
                     DataTable tb;                     
                     if (DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString().Length > 0)
                     {
                         sql = "select ModelPanelID,MC from a_eke_sysModelPanels where delTag=0 and ModelID='" + modelid + "' and ModelTabID='" + DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString() + "'  order by px,addTime";
                         tb = ModelPanels.Select("", "ModelTabID='" + DataBinder.Eval(e.Item.DataItem, "ModelTabID").ToString() + "'", "px,addTime");
                     }
                     else
                     {
                         sql = "select ModelPanelID,MC from a_eke_sysModelPanels where delTag=0 and ModelID='" + modelid + "' and  ModelTabID is Null order by px,addTime";
                         tb = ModelPanels.Select("", "ModelTabID is null", "px,addTime");
                     }
                     //lit.Text = eBase.DataBase.getOptions(sql, "MC", "ModelPanelID", DataBinder.Eval(e.Item.DataItem, "ModelPanelID").ToString());
                     lit.Text = tb.toOptions("ModelPanelID", "MC", DataBinder.Eval(e.Item.DataItem, "ModelPanelID").ToString());
                 }
             }
        }
        
       
    }
}