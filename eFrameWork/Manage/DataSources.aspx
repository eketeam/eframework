﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="DataSources.aspx.cs" Inherits="eFrameWork.Manage.DataSources" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 数据源<a id="btn_add" style="<%=(act == "" ? "" : "display:none;" )%>" class="button" href="<%=edt.getAddURL()%>"><span><i class="add">添加</i></span></a></div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
  
   </div>
 <%} %>
      <div class="tips" style="margin:6px;"><b>提示</b><br>设置系统所需要数据源，目前已支持SqlServer，MYSQL，Oracle，SqlServerCe，SQLite，Access，Excel。<br />角色用户库是指存放角色表(a_eke_sysRoles)、用户表(a_eke_sysUsers)的数据库。</div>
<%
    if (act == "add" || act == "edit" || act == "copy")
{
%>
<script>
    function tryconn(obj)
    {
        var type = $("#f2").val();
        if (type.length == 0) {
            layer.msg("请选择连接类型!");
            return;
        }

        //var connstr = $("#f3").val();
        var connstr = $(obj).parent().parent().find("input:eq(0)").val();
        if (connstr.length == 0)
        {
            layer.msg("请输入连接字符串!");
            return;
        }
        var url = "?act=tryconn";
        //layer.msg("连接成功!");
        $.ajax({
            type: "POST", async: false,
            data: { type: type, value: connstr },
            url: url,
            dataType: "json",
            success: function (data) {
                layer.msg(data.message);
            }
        });
    };
    function changetype(obj)
    {
        var type = obj.value;//.toLowerCase();

        switch (type)
        {
            case "System.Data.SqlClient":
                $("#f3").val('Data Source=127.0.0.1,1433;Initial Catalog=;User Id=sa;Password=123456;');
                break;
            case "System.Data.OleDb":
                $("#f3").val('Provider=Microsoft.jet.OLEDB.4.0;Data Source=|DataDirectory|\db.mdb;');
                break;
            case "MySql.Data.MySqlClient":
                $("#f3").val('Server=localhost;PORT=3306;Database=TestDB;UID=root;Pwd=123456;charset=utf8;UseAffectedRows=True;Allow Zero Datetime=True;');
                break;
            case "System.Data.SqlServerCe.4.0":
                $("#f3").val('Data Source=|DataDirectory|\DataBase.sdf');
                break;
            case "Oracle.ManagedDataAccess.Client":
                $("#f3").val('Data Source=127.0.0.1/ORCL;User ID=system;Password=;');
                break;
            case "System.Data.SQLite":
                $("#f3").val('Data Source=|DataDirectory|\Sqlite.db;Version=3;');
                break;
            default:
                $("#f3").val('');
                break;
        }
        //Data Source=;Initial Catalog=;User Id=;Password=;
    };
</script>
<div style="margin:6px;">

    <asp:PlaceHolder ID="eFormControlGroup" runat="server">
	<form name="frmaddoredit" id="frmaddoredit" method="post" action="<%=edt.getSaveURL()%>">
	<input name="id" type="hidden" id="id" value="<%=edt.ID%>">
    <input name="act" type="hidden" id="act" value="save">  
	<input name="fromurl" type="hidden" id="fromurl" value="<%=edt.FromURL%>">  
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="eDataView">

      <tr>
        <td width="126" class="title"><ins>*</ins>名称：</td>
        <td class="content"><span class="eform">
		 <ev:eFormControl ID="f1" Field="MC" width="600" notnull="true" fieldname="名称" runat="server" />
		</span></td>
      </tr>
	 
        <tr>
          <td class="title"><ins>*</ins>连接类型：</td>
          <td class="content"><span class="eform">
		  <ev:eFormControl ID="f2" ControlType="select" Field="ProviderName" notnull="true" Attributes="onchange=&quot;changetype(this);&quot;" Options="[{text:SqlServer,value:System.Data.SqlClient},{text:OleDb,value:System.Data.OleDb},{text:MySql,value:MySql.Data.MySqlClient},{text:SqlServerCe,value:System.Data.SqlServerCe.4.0},{text:Oracle,value:Oracle.ManagedDataAccess.Client},{text:SQLite,value:System.Data.SQLite}]" runat="server" /> 
		  </span></td>
        </tr>
       
         <tr>
          <td class="title"><ins>*</ins>生产库-连接字符串：</td>
          <td class="content"><span class="eform" style="display:inline-block;">
		  <ev:eFormControl ID="f3" ControlType="text" width="600" notnull="true" Field="ConnectionString" DefaultValue="Data Source=;Initial Catalog=;User Id=;Password=;" runat="server" /></span>
              <a class="button" href="javascript:;" onclick="tryconn(this);"><span><i>测试</i></span></a>
          </td>
        </tr>
     <tr>
          <td class="title">测试库-连接字符串：</td>
          <td class="content"><span class="eform" style="display:inline-block;">
		  <ev:eFormControl ID="f3a" ControlType="text" width="600" notnull="false" Field="TestConnectionString" DefaultValue="" runat="server" /></span>
              <a class="button" href="javascript:;" onclick="tryconn(this);"><span><i>测试</i></span></a>
          </td>
        </tr>
  		<tr>
          <td class="title">说明：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f5" ControlType="textarea" Field="BZ" width="600" height="60" runat="server" />
		   </span></td>
        </tr>
    
        <tr>
       <td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">		
		<a class="button" href="javascript:;" onclick="if(frmaddoredit.onsubmit()!=false){frmaddoredit.submit();}" style="display:none;"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" onclick="ajaxSubmit(frmaddoredit);"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" style="margin-left:30px;" onclick="history.back();"><span><i class="back">返回</i></span></a>
		</td>
	   </tr>
	 
    </table>
	 </form>
</asp:PlaceHolder>
	 </div>
<%}%>
<div style="margin:6px;overflow-x:auto;overflow-y:hidden;">

<asp:Repeater id="Rep" runat="server">
<headertemplate>
<table id="eDataTable" class="eDataTable" border="0" cellpadding="0" cellspacing="1" width="100%">
<thead>
<tr bgcolor="#f2f2f2">
<td width="80">序号</td>
<td width="260">编号</td>
<td>名称</td>
<td>说明</td>
<td width="100">添加时间</td>
<td width="150">操作</td>
</tr>
</thead>
</headertemplate>
<itemtemplate>
<tr<%# ((Container.ItemIndex+1) % 2 == 0 ? " class=\"alternating\" eclass=\"alternating\"" : " eclass=\"\"") %>>
<td height="32"><%# (Container.ItemIndex + 1)%></td>
<td><a class="copy" href="javascript:;" data-clipboard-action="copy" data-clipboard-text="<%# Eval("DataSourceID")%>"></a><%# Eval("DataSourceID")%></td>
<td><%# Eval("MC")%></td>
<td><%# Eval("BZ").ToString()%></td>
<td><%# Eval("addTime","{0:yyyy-MM-dd}") %></td>
<td>
<a href="?act=edit&id=<%#  Eval("DataSourceID").ToString()%>">修改</a>
<a href="?act=del&id=<%#  Eval("DataSourceID").ToString()%>" onclick="javascript:return confirm('确认要删除吗？');">删除</a>
</td>
</tr>
</itemtemplate>
<footertemplate></table></footertemplate>
</asp:Repeater>
</div>
<div style="margin:6px;"><ev:ePageControl ID="ePageControl1" PageSize="20" PageNum="9" runat="server" /></div>
</asp:Content>
