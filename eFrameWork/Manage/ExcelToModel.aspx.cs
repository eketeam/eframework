﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Extension.ComObject;
using System.Data.OleDb;
using System.IO;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using System.Text;
using EKETEAM.ePinyin;

public partial class Manage_ExcelToModel : System.Web.UI.Page
{
    public string file = eParameters.QueryString("file");
    private eUser user;
    private string tempPath = "";
    public eExtensionsList allowExts = new eExtensionsList(".xls.xlsx.csv");
    protected void Page_Load(object sender, EventArgs e)
    {
        user = new eUser("Manage");
        user.Check();
        //eBase.Writeln(ePinyin.getFirstLetter("中国人民邮电大学"));
        //eBase.Writeln(ePinyin.getPinYin("中国人民邮电大学"));
        //eBase.Writeln(ePinyin.getPinYin("中国人民邮电大学",true));
        tempPath = eRunTime.tempPath;
        if (file.Length == 0)
        {
            #region 保存文件
            if (Request.Files.Count == 1)
            {
                HttpPostedFile postfile = Request.Files[0];
                if (postfile.ContentLength > 0)
                {
                    string ext = postfile.FileName.fileExtension();
                    string filename = eBase.GetFileName() + ext;
                    string pathname = tempPath + filename;
                    if (!Directory.Exists(tempPath)) Directory.CreateDirectory(tempPath);
                    postfile.safeSaveAs(pathname,allowExts);
                    Response.Redirect(eBase.getAspxFileName() + "?file=" + filename, true);

                }
            }
            #endregion
        }
        else
        {
            if (Request.Form.Count > 3)
            {
                Save();
            }
            else
            {
                List();
            }
        }
    }
    private void Save()
    {
        string pathname = tempPath + file;
        string excelPath = pathname;// Server.MapPath("教材订单.xls");
        OleDbConnection Oleconn = new OleDbConnection("provider=microsoft.jet.oledb.4.0;Excel 8.0;ReadOnly=True;HDR=YES;IMEX=1;database=" + excelPath + ";");
        Oleconn.Open();
        DataTable dt = Oleconn.GetSchema("Tables");
        int midx = 1;
        foreach (DataRow dr in dt.Rows)
        {
            string tablename = dr["TABLE_NAME"].ToString().Replace("'", "");
            if (tablename.IndexOf("$_") > -1) continue;
            string name = tablename.Replace("$", "");
            if (Request.Form["model" + midx.ToString()] != null)
            {
                DataTable tb = new DataTable();//取前一行数据
                OleDbDataAdapter sda = new OleDbDataAdapter("Select * From [" + tablename + "]", Oleconn);
                sda.Fill(tb);
                if (tb.Rows.Count == 1 && tb.Columns.Count == 1 && tb.Columns[0].ColumnName.ToLower() == "f1") continue;//空表格


                string mc = Request.Form["model" + midx.ToString() + "_name"].ToString();//模块名称
                string code = Request.Form["model" + midx.ToString() + "_code"].ToString();//模块编码

                //eBase.Writeln(mc + "::" + code);
                #region 生成字段表
                DataTable col = new DataTable();
                col.Columns.Add("TABLE_NAME", typeof(string));
                col.Columns.Add("COLUMN_NAME", typeof(string));
                col.Columns.Add("ORDINAL_POSITION", typeof(int));
                col.Columns.Add("COLUMN_DEFAULT", typeof(string));
                col.Columns.Add("IS_NULLABLE", typeof(string));
                col.Columns.Add("DATA_TYPE", typeof(string));//
                col.Columns.Add("CHARACTER_MAXIMUM_LENGTH", typeof(int));
                col.Columns.Add("NUMERIC_PRECISION", typeof(int));
                col.Columns.Add("NUMERIC_SCALE", typeof(int));
                col.Columns.Add("DESCRIPTION", typeof(string));
                col.Columns.Add("PrimaryKey", typeof(string));

                int rowindex = 1;
                DataRow row = col.Rows.Add();
                row["TABLE_NAME"] = code;
                row["COLUMN_NAME"] = "ID";
                row["ORDINAL_POSITION"] = rowindex;
                row["IS_NULLABLE"] = "False";
                row["DATA_TYPE"] = "int";
                row["CHARACTER_MAXIMUM_LENGTH"] = 8;
                row["NUMERIC_PRECISION"] = 0;
                row["NUMERIC_SCALE"] = 0;
                row["DESCRIPTION"] = "主键";
                row["PrimaryKey"] = "True";

                for (int i = 0; i < tb.Columns.Count; i++)
                {
                    rowindex++;
                    string basename = "model" + midx.ToString() + "_row" + (1 + i).ToString() + "_";
                    string _name = Request.Form[basename + "name"].ToString();
                    string _code = Request.Form[basename + "code"].ToString();
                    string _type = Request.Form[basename + "type"].ToString();
                    string _len = Request.Form[basename + "len"].ToString();
                    string _scale = Request.Form[basename + "scale"].ToString();
                    //eBase.Writeln(_name + "::" + _code + "::" + _type + "::" + _len + "::" + _scale);

                    row = col.Rows.Add();
                    row["TABLE_NAME"] = code;
                    row["COLUMN_NAME"] = _code;
                    row["ORDINAL_POSITION"] = rowindex;
                    row["IS_NULLABLE"] = "True";
                    row["DATA_TYPE"] = _type;
                    row["CHARACTER_MAXIMUM_LENGTH"] = _type.IndexOf("char") > -1 ? _len : "0";
                    row["NUMERIC_PRECISION"] = _type.IndexOf("decimal") > -1 ? _len : "0";
                    row["NUMERIC_SCALE"] = _scale.Length == 0 ? "0" : _scale;
                    row["DESCRIPTION"] = _name;
                    row["PrimaryKey"] = "False";
                    //string colname = tb.Columns[i].ColumnName;
                    //if (tb.Columns.Contains(colname))
                    tb.Columns[i].ColumnName = _code;
                    //eBase.Writeln(colname);
                }
                #region 系统字段
                #region 添加时间
                rowindex++;
                row = col.Rows.Add();
                row["TABLE_NAME"] = code;
                row["COLUMN_NAME"] = "addTime";
                row["ORDINAL_POSITION"] = rowindex;
                row["COLUMN_DEFAULT"] = "getdate()";
                row["IS_NULLABLE"] = "True";
                row["DATA_TYPE"] = "datetime";
                row["CHARACTER_MAXIMUM_LENGTH"] = 0;
                row["NUMERIC_PRECISION"] = 0;
                row["NUMERIC_SCALE"] = 0;
                row["DESCRIPTION"] = "添加时间";
                row["PrimaryKey"] = "False";
                #endregion
                #region 添加人员
                rowindex++;
                row = col.Rows.Add();
                row["TABLE_NAME"] = code;
                row["COLUMN_NAME"] = "addUser";
                row["ORDINAL_POSITION"] = rowindex;
                row["IS_NULLABLE"] = "True";
                row["DATA_TYPE"] = "nvarchar";
                row["CHARACTER_MAXIMUM_LENGTH"] = 36;
                row["NUMERIC_PRECISION"] = 0;
                row["NUMERIC_SCALE"] = 0;
                row["DESCRIPTION"] = "添加人员";
                row["PrimaryKey"] = "False";
                #endregion

                #region 修改时间
                rowindex++;
                row = col.Rows.Add();
                row["TABLE_NAME"] = code;
                row["COLUMN_NAME"] = "editTime";
                row["ORDINAL_POSITION"] = rowindex;
                row["IS_NULLABLE"] = "True";
                row["DATA_TYPE"] = "datetime";
                row["CHARACTER_MAXIMUM_LENGTH"] = 0;
                row["NUMERIC_PRECISION"] = 0;
                row["NUMERIC_SCALE"] = 0;
                row["DESCRIPTION"] = "修改时间";
                row["PrimaryKey"] = "False";
                #endregion
                #region 修改人员
                rowindex++;
                row = col.Rows.Add();
                row["TABLE_NAME"] = code;
                row["COLUMN_NAME"] = "editUser";
                row["ORDINAL_POSITION"] = rowindex;
                row["IS_NULLABLE"] = "True";
                row["DATA_TYPE"] = "nvarchar";
                row["CHARACTER_MAXIMUM_LENGTH"] = 36;
                row["NUMERIC_PRECISION"] = 0;
                row["NUMERIC_SCALE"] = 0;
                row["DESCRIPTION"] = "修改人员";
                row["PrimaryKey"] = "False";
                #endregion

                #region 删除时间
                rowindex++;
                row = col.Rows.Add();
                row["TABLE_NAME"] = code;
                row["COLUMN_NAME"] = "delTime";
                row["ORDINAL_POSITION"] = rowindex;
                row["IS_NULLABLE"] = "True";
                row["DATA_TYPE"] = "datetime";
                row["CHARACTER_MAXIMUM_LENGTH"] = 0;
                row["NUMERIC_PRECISION"] = 0;
                row["NUMERIC_SCALE"] = 0;
                row["DESCRIPTION"] = "删除时间";
                row["PrimaryKey"] = "False";
                #endregion
                #region 删除人员
                rowindex++;
                row = col.Rows.Add();
                row["TABLE_NAME"] = code;
                row["COLUMN_NAME"] = "delUser";
                row["ORDINAL_POSITION"] = rowindex;
                row["IS_NULLABLE"] = "True";
                row["DATA_TYPE"] = "nvarchar";
                row["CHARACTER_MAXIMUM_LENGTH"] = 36;
                row["NUMERIC_PRECISION"] = 0;
                row["NUMERIC_SCALE"] = 0;
                row["DESCRIPTION"] = "删除人员";
                row["PrimaryKey"] = "False";
                #endregion


                #region 删除标记
                rowindex++;
                row = col.Rows.Add();
                row["TABLE_NAME"] = code;
                row["COLUMN_NAME"] = "delTag";
                row["ORDINAL_POSITION"] = rowindex;
                row["COLUMN_DEFAULT"] = "0";
                row["IS_NULLABLE"] = "True";
                row["DATA_TYPE"] = "int";
                row["CHARACTER_MAXIMUM_LENGTH"] = 0;
                row["NUMERIC_PRECISION"] = 0;
                row["NUMERIC_SCALE"] = 0;
                row["DESCRIPTION"] = "删除标记";
                row["PrimaryKey"] = "False";
                #endregion

                #endregion
                #endregion
                #region 生成结构
                //eBase.PrintDataTable(col);
                col.TableName = code;
                col.ExtendedProperties.Add("name", code);
                //string createsql = eBase.DataBase.getTableSQL(col);
                //eBase.Writeln(createsql);
                eBase.DataBase.SchemaCreate(col);
                #endregion
                #region 导入数据
                //eBase.PrintDataTable(tb);
                tb.TableName = code;
                tb.ExtendedProperties.Add("name", code);
                eBase.DataBase.SchemaImport(tb);
                #endregion
                #region 生成模块
                string modelid = Guid.NewGuid().ToString(); //"b45c24d7-ccc2-4564-90cd-eb345a56f841";
                eTable etb = new eTable("a_eke_sysModels");
                //etb.Fields.Add("ModelID", modelid);
                etb.Fields.Add("Type", "1");
                etb.Fields.Add("auto", "1");
                etb.Fields.Add("MC", mc);
                etb.Fields.Add("Code", code);
                etb.Fields.Add("Power", "[{\"text\":\"列表\",\"value\":\"list\"},{\"text\":\"详细\",\"value\":\"view\"},{\"text\":\"添加\",\"value\":\"add\"},{\"text\":\"编辑\",\"value\":\"edit\"},{\"text\":\"删除\",\"value\":\"del\"},{\"text\":\"导入\",\"value\":\"import\"},{\"text\":\"导出\",\"value\":\"export\"}]");
                etb.Fields.Add("DefaultCondition", "delTag=0");
                etb.Fields.Add("DefaultOrderby", "addTime desc");
                etb.Add();
                //etb.Where.Add("ModelID='" + modelid + "'");
                //etb.Update();

                // string modelid = "b45c24d7-ccc2-4564-90cd-eb345a56f841"; //etb.ID
                //eBase.Writeln(etb.ID);
                modelid = etb.ID;

                DataTable items = eBase.DataBase.getDataTable("select ModelItemID,ModelID,Custom,MC,Code,Type,Length,PX,SYS,primaryKey,Num,frmName,frmID,ShowList,showView,showAdd,showEdit,ControlType,addControlType,editControlType,OrderBy,Move,Size,ListHTML,FormatString, DateFormat,ListOrder, ListWidth from a_eke_sysModelItems where 1=0");
                items.TableName = "a_eke_sysModelItems";
                items.ExtendedProperties.Add("name", "a_eke_sysModelItems");
                string syscols = ",id,addtime,adduser,edittime,edituser,deltime,deluser,deltag,";
                int num = 1;
                string frmname = "M" + modelid.Substring(0, 2) + "_F";
                #region 序号
                DataRow _row = items.Rows.Add();
                _row["ModelItemID"] = Guid.NewGuid().ToString();
                _row["ModelID"] = modelid;
                _row["Custom"] = true;
                _row["MC"] = "序号";
                _row["SYS"] = false;
                _row["primaryKey"] = false;
                _row["PX"] = num;
                _row["num"] = num;
                _row["frmName"] = frmname + num.ToString();
                _row["frmID"] = frmname + num.ToString();
                _row["ShowList"] = true;
                _row["showView"] = false;
                _row["showAdd"] = false;
                _row["showEdit"] = false;
                _row["ControlType"] = "text";
                _row["addControlType"] = "text";
                _row["editControlType"] = "text";
                _row["OrderBy"] = false;
                _row["Move"] = true;
                _row["Size"] = true;
                _row["ListHTML"] = "{row:index}";
                _row["FormatString"] = DBNull.Value;
                _row["DateFormat"] = DBNull.Value;
                _row["ListOrder"] = num;
                _row["ListWidth"] = "60";

                #endregion

                foreach (DataRow _dr in col.Rows)
                {
                    #region 其他列
                    num++;
                    _row = items.Rows.Add();
                    _row["ModelItemID"] = Guid.NewGuid().ToString();
                    _row["ModelID"] = modelid;
                    _row["Custom"] = false;
                    _row["MC"] = _dr["DESCRIPTION"].ToString();
                    _row["Code"] = _dr["COLUMN_NAME"].ToString();
                    _row["Type"] = _dr["DATA_TYPE"].ToString();
                    _row["Length"] = _dr["CHARACTER_MAXIMUM_LENGTH"].ToString() == "0" ? _dr["NUMERIC_PRECISION"].ToString() : _dr["CHARACTER_MAXIMUM_LENGTH"].ToString();

                    _row["SYS"] = syscols.IndexOf("," + _dr["COLUMN_NAME"].ToString().ToLower() + ",") == -1 ? false : true;
                    _row["primaryKey"] = _dr["COLUMN_NAME"].ToString().ToLower() == "id" ? true : false;
                    _row["PX"] = num;
                    _row["num"] = num;
                    _row["frmName"] = frmname + num.ToString();
                    _row["frmID"] = frmname + num.ToString();
                    _row["ShowList"] = syscols.IndexOf("," + _dr["COLUMN_NAME"].ToString().ToLower() + ",") == -1 || _dr["COLUMN_NAME"].ToString().ToLower() == "addtime" ? true : false;
                    _row["showView"] = syscols.IndexOf("," + _dr["COLUMN_NAME"].ToString().ToLower() + ",") == -1 ? true : false;
                    _row["showAdd"] = syscols.IndexOf("," + _dr["COLUMN_NAME"].ToString().ToLower() + ",") == -1 ? true : false;
                    _row["showEdit"] = syscols.IndexOf("," + _dr["COLUMN_NAME"].ToString().ToLower() + ",") == -1 ? true : false;
                    _row["ControlType"] = _dr["DATA_TYPE"].ToString().ToLower().IndexOf("date") > -1 ? "date" : "text";
                    _row["addControlType"] = _dr["DATA_TYPE"].ToString().ToLower().IndexOf("date") > -1 ? "date" : "text";
                    _row["editControlType"] = _dr["DATA_TYPE"].ToString().ToLower().IndexOf("date") > -1 ? "date" : "text";
                    _row["OrderBy"] = true;
                    _row["Move"] = true;
                    _row["Size"] = true;
                    _row["ListHTML"] = DBNull.Value;
                    _row["FormatString"] = _dr["DATA_TYPE"].ToString().ToLower().IndexOf("date") > -1 ? "{0:yyyy-MM-dd}" : "";
                    _row["DateFormat"] = _dr["DATA_TYPE"].ToString().ToLower().IndexOf("date") > -1 ? "yyyy-MM-dd" : "";

                    _row["ListWidth"] = _dr["DATA_TYPE"].ToString().ToLower().IndexOf("date") > -1 ? 90 : 0;
                    _row["ListOrder"] = num;
                    #endregion
                }
                #region 操作列
                num++;
                _row = items.Rows.Add();
                _row["ModelItemID"] = Guid.NewGuid().ToString();
                _row["ModelID"] = modelid;
                _row["Custom"] = true;
                _row["MC"] = "操作";
                _row["SYS"] = false;
                _row["primaryKey"] = false;
                _row["PX"] = num;
                _row["num"] = num;
                _row["frmName"] = frmname + num.ToString();
                _row["frmID"] = frmname + num.ToString();
                _row["ShowList"] = true;
                _row["showView"] = false;
                _row["showAdd"] = false;
                _row["showEdit"] = false;
                _row["ControlType"] = "text";
                _row["addControlType"] = "text";
                _row["editControlType"] = "text";
                _row["OrderBy"] = false;
                _row["Move"] = true;
                _row["Size"] = true;
                _row["ListHTML"] = "<a href=\"{base:url}act=view&id={data:ID}\" class=\"view\">查看</a> <a href=\"{base:url}act=edit&id={data:ID}\" class=\"edit\">修改</a> <a href=\"{base:url}act=del&id={data:ID}\" onclick=\"javascript:return confirm('确认要删除吗？');\" class=\"del\">删除</a>";
                _row["FormatString"] = DBNull.Value;
                _row["DateFormat"] = DBNull.Value;
                _row["ListOrder"] = num;
                _row["ListWidth"] = "130";
                #endregion

                eBase.DataBase.Execute("update a_eke_sysModels set MaxItems=" + num.ToString() + " where ModelID='" + modelid + "'");
                eBase.DataBase.SchemaImport(items);
                //eBase.PrintDataTable(items);
                eBase.clearDataCache("a_eke_sysModels");
                eBase.clearDataCache("a_eke_sysModelItems");
                #endregion
            }
            midx++;

        }
        Oleconn.Close();
        try { System.IO.File.Delete(pathname); }
        catch { }           
        litBody.Text = "生成成功!";

    }
    private void List()
    {
        string pathname = tempPath + file;
        string excelPath = pathname;// Server.MapPath("教材订单.xls");
        StringBuilder sb = new StringBuilder();
        sb.Append("<form id=\"form1\" name=\"form1\" method=\"post\" action=\"\">\r\n");


        OleDbConnection Oleconn = new OleDbConnection("provider=microsoft.jet.oledb.4.0;Excel 8.0;ReadOnly=True;HDR=YES;IMEX=1;database=" + excelPath + ";");
        Oleconn.Open();
        DataTable dt = Oleconn.GetSchema("Tables");
        int midx = 1;
        foreach (DataRow dr in dt.Rows)
        {
            string tablename = dr["TABLE_NAME"].ToString().Replace("'", "");
            if (tablename.IndexOf("$_") > -1) continue;
            DataTable col = Oleconn.GetSchema("Columns", new string[] { null, null, tablename }); // Oleconn.GetSchema("Columns"); //"Tables" "Columns"
            if (col.Columns.Contains("TABLE_CATALOG")) col.Columns.Remove("TABLE_CATALOG");
            if (col.Columns.Contains("TABLE_SCHEMA")) col.Columns.Remove("TABLE_SCHEMA");
            if (col.Columns.Contains("COLUMN_GUID")) col.Columns.Remove("COLUMN_GUID");
            if (col.Columns.Contains("COLUMN_PROPID")) col.Columns.Remove("COLUMN_PROPID");
            if (col.Columns.Contains("TYPE_GUID")) col.Columns.Remove("TYPE_GUID");

            if (col.Columns.Contains("DOMAIN_CATALOG")) col.Columns.Remove("DOMAIN_CATALOG");
            if (col.Columns.Contains("DOMAIN_SCHEMA")) col.Columns.Remove("DOMAIN_SCHEMA");
            if (col.Columns.Contains("DESCRIPTION")) col.Columns.Remove("DESCRIPTION");
            if (col.Columns.Contains("DOMAIN_NAME")) col.Columns.Remove("DOMAIN_NAME");

            if (col.Columns.Contains("COLLATION_CATALOG")) col.Columns.Remove("COLLATION_CATALOG");
            if (col.Columns.Contains("COLLATION_SCHEMA")) col.Columns.Remove("COLLATION_SCHEMA");
            if (col.Columns.Contains("COLLATION_NAME")) col.Columns.Remove("COLLATION_NAME");

            if (col.Columns.Contains("CHARACTER_SET_CATALOG")) col.Columns.Remove("CHARACTER_SET_CATALOG");
            if (col.Columns.Contains("CHARACTER_SET_SCHEMA")) col.Columns.Remove("CHARACTER_SET_SCHEMA");
            if (col.Columns.Contains("CHARACTER_SET_NAME")) col.Columns.Remove("CHARACTER_SET_NAME");

            if (col.Columns.Contains("DATETIME_PRECISION")) col.Columns.Remove("DATETIME_PRECISION");
            // eBase.PrintDataTable(col);

            DataTable tb = new DataTable();//取前一行数据
            OleDbDataAdapter sda = new OleDbDataAdapter("Select top 1 * From [" + tablename + "]", Oleconn);
            sda.Fill(tb);
            //eBase.PrintDataTable(tb);
            if (tb.Rows.Count == 1 && tb.Columns.Count == 1 && tb.Columns[0].ColumnName.ToLower() == "f1") continue;//空表格

            string name = tablename.Replace("$", "");
            string py = ePinyin.getFirstLetter(name);
            #region 模块信息
            sb.Append("<div>");
            sb.Append("<input type=\"checkbox\" name=\"model" + midx.ToString() + "\"  onclick=\"show(this,'" + midx.ToString() + "');\" value=\"1\">");
            sb.Append("&nbsp;&nbsp;模块名称：<input class=\"edittext2\" name=\"model" + midx.ToString() + "_name\" type=\"text\" value=\"" + name + "\">");
            sb.Append("&nbsp;&nbsp;编码：<input class=\"edittext2\" name=\"model" + midx.ToString() + "_code\" type=\"text\" value=\"" + py + "\">");
            sb.Append("</div>");
            #endregion
            sb.Append("<div class=\"box" + midx.ToString() + "\" style=\"display:none;\">");
            #region 表头
            sb.Append("<table border=\"1\" cellpadding=\"0\" cellspacing=\"1\" bgcolor=\"#CCCCCC\" w3idth=\"99%\" class=\"eDataTable\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr bgcolor=\"#F2F2F2\">\r\n");
            sb.Append("<td width=\"30\" height=\"25\">序号</td>\r\n");
            sb.Append("<td width=\"130\">名称</td>\r\n");
            sb.Append("<td width=\"70\">编码</td>\r\n");
            sb.Append("<td width=\"130\">数据类型</td>\r\n");
            sb.Append("<td width=\"60\">长度</td>\r\n");
            sb.Append("<td width=\"50\">小数位</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>\r\n");
            #endregion
            #region 遍历字段
            List<string> pys = new List<string>();
            for (int i = 0; i < tb.Columns.Count; i++)
            {
                DataColumn dcc = tb.Columns[i];
                name = dcc.ColumnName;
                py = ePinyin.getFirstLetter(name).Replace("/", "");
                #region 编码重复处理
                if (pys.Contains(py.ToUpper()))
                {
                    int x = 2;
                    string tmp = py + x.ToString();
                    while (pys.Contains(tmp.ToUpper()))
                    {
                        tmp = py + x.ToString();
                        x++;
                    }
                    py = tmp;
                }
                #endregion
                pys.Add(py.ToUpper());
                #region 行输出
                sb.Append("<tr bgcolor=\"#FFFFFF\">\r\n");
                sb.Append("<td height=\"25\">" + (1 + i).ToString() + "</td>\r\n");
                sb.Append("<td><input class=\"edittext\" name=\"model" + midx.ToString() + "_row" + (1 + i).ToString() + "_name\" type=\"text\" value=\"" + name + "\"></td>\r\n");
                sb.Append("<td><input class=\"edittext\" name=\"model" + midx.ToString() + "_row" + (1 + i).ToString() + "_code\" type=\"text\" value=\"" + py + "\"></td>\r\n");
                sb.Append("<td>");

                sb.Append("<select name=\"model" + midx.ToString() + "_row" + (1 + i).ToString() + "_type\">\r\n");
                string coltype = dcc.DataType.ToString().ToLower().Replace("system.", "").Replace("string", "nvarchar").Replace("double", "decimal");

                string len = "0";
                if (coltype == "nvarchar") len = "255";
                if (coltype == "decimal") len = "15";

                string scale = "0";


                //eBase.Writeln(coltype);

                sb.Append("<option value=\"nvarchar\"" + (coltype == "nvarchar" ? " selected=\"true\"" : "") + ">文本(nvarchar)</option>\r\n");
                sb.Append("<option value=\"int\"" + (coltype == "int" ? " selected=\"true\"" : "") + ">整数(int)</option>\r\n");
                sb.Append("<option value=\"decimal\"" + (coltype == "decimal" ? " selected=\"true\"" : "") + ">小数(decimal)</option>\r\n");
                sb.Append("<option value=\"datetime\"" + (coltype == "datetime" ? " selected=\"true\"" : "") + ">时间(datetime)</option>\r\n");
                sb.Append("<option value=\"bit\"" + (coltype == "bit" ? " selected=\"true\"" : "") + ">是/否(bit)</option>\r\n");
                sb.Append("<option value=\"ntext\"" + (coltype == "text" ? " selected=\"true\"" : "") + ">备注(text)</option>\r\n");

                sb.Append("</select>\r\n");
                //sb.Append( dcc.DataType.ToString() );

                sb.Append("</td>\r\n");
                sb.Append("<td><input class=\"edittext\" name=\"model" + midx.ToString() + "_row" + (1 + i).ToString() + "_len\" type=\"text\" value=\"" + (len == "0" ? "" : len) + "\"></td>\r\n");
                sb.Append("<td><input class=\"edittext\" name=\"model" + midx.ToString() + "_row" + (1 + i).ToString() + "_scale\" type=\"text\" value=\"" + (coltype != "decimal" && scale == "0" ? "" : scale) + "\"></td>\r\n");
                sb.Append("</tr>\r\n");
                #endregion
            }
            #endregion
            #region 表尾
            sb.Append("</tbody>\r\n");
            sb.Append("</table>\r\n");
            #endregion
            sb.Append("</div>");
            midx++;
        }
        Oleconn.Close();
        sb.Append("<p style=\"margin-top:10px;margin-bottom:15px;\"><a class=\"button\" href=\"javascript:;\" onclick=\"submitform();\" _click=\"form1.submit();\"><span style=\"letter-spacing:1px;\">确定导入</span></a></p>");

        sb.Append("</form>\r\n");
        litBody.Text = sb.ToString();
    }
}