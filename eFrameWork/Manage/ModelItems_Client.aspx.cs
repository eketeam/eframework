﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;

namespace eFrameWork.Manage
{
    public partial class ModelItems_Client : System.Web.UI.Page
    {
        public string modelid = eParameters.QueryString("modelid");
        private DataRow _modelinfo;
        public DataRow ModelInfo
        {
            get
            {
                if (_modelinfo == null)
                {
                    DataTable dt = eBase.DataBase.getDataTable("select * from a_eke_sysModels where ModelID='" + modelid + "'");
                    if (dt.Rows.Count > 0) _modelinfo = dt.Rows[0];
                }
                return _modelinfo;
            }
        }
        private JsonData _propertys;
        public JsonData Propertys
        {
            get
            {
                if (_propertys == null)
                {
                    _propertys = JsonMapper.ToObject("{}");
                    if (ModelInfo.Table.Columns.Contains("Propertys") && ModelInfo["Propertys"].ToString().Length > 2 && ModelInfo["Propertys"].ToString().StartsWith("{"))
                    {
                        _propertys = JsonMapper.ToObject(ModelInfo["Propertys"].ToString());
                    }
                }
                return _propertys;
            }
        }
        private eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            Response.Write("<a href=\"http://frame.eketeam.com\" style=\"float:right;\" target=\"_blank\" title=\"eFrameWork开发框架\"><img src=\"images/help.gif\"></a>");
            
            if (eConfig.showHelp())
            {
                Response.Write("<div class=\"tips\" style=\"margin-bottom:6px;\">");
                Response.Write("<b>客户端验证</b><br>");
                Response.Write("设置表单提交前的数据类型、长度、大小、是否必填等。");
                Response.Write("</div> ");
            }
            Response.Write("&nbsp;唯一性验证方式：");
            //Response.Write("<select onchange=\"setModelPropertys(this,'SingleType');\">");
            //Response.Write("<option value=\"0\"" + (Propertys.getValue("SingleType") == "0" ? " selected=\"true\"" : "") + ">一起验证</option>");
            //Response.Write("<option value=\"1\"" + (Propertys.getValue("SingleType") == "1" ? " selected=\"true\"" : "") + ">逐个验证</option>");

            Response.Write("<select onchange=\"setModel(this,'SingleType');\">");
            Response.Write("<option value=\"0\"" + (ModelInfo["SingleType"].ToString() == "0" ? " selected=\"true\"" : "") + ">一起验证</option>");
            Response.Write("<option value=\"1\"" + (ModelInfo["SingleType"].ToString() == "1" ? " selected=\"true\"" : "") + ">逐个验证</option>");
            Response.Write("</select>");

            /*
            eList datalist = new eList("a_eke_sysModelItems");
            datalist.Where.Add("ModelID='" + modelid + "' and delTag=0 and showAdd=1");// and sys=0   and Custom=0
            datalist.OrderBy.Add("addorder, PX, addTime");
            //eBase.Writeln(datalist.SQL);
            datalist.Bind(Rep);
            */

            list();


            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Rep.RenderControl(htw);
            Rep.Visible = false;//不输出，要在获取后设，不然取不到内容。
            Response.Write(sw.ToString());
            sw.Close();
            Response.End();
        }
        private void list()
        {
            DataTable tb = getItems(modelid);
            appendItems(tb, modelid);
            tb = tb.Select("showadd=1 or showedit=1", "showadd desc,showedit desc,addorder, PX, addTime").toDataTable();
            Rep.DataSource = tb;
            Rep.DataBind();
        }
        private DataTable getItems(string modelid)
        {
            return eBase.DataBase.getDataTable("select b.mc as ModelName,a.* from a_eke_sysModelItems a inner join a_eke_sysModels b on a.modelid=b.modelid where a.ModelID='" + modelid + "' and a.delTag=0 and a.showAdd=1");
        }
        private void appendItems(DataTable tb, string modelid)
        {
            DataTable dt = eBase.DataBase.getDataTable("select modelid,mc from a_eke_sysModels where ParentID='" + modelid + "' and JoinMore=0 and show=1 and deltag=0");
            foreach (DataRow dr in dt.Rows)
            {
                DataTable tb2 = getItems(dr["modelid"].ToString());
                foreach (DataRow _dr in tb2.Rows)
                {
                    tb.Rows.Add(_dr.ItemArray);
                }
                appendItems(tb, dr["modelid"].ToString());
            }
        }
    }
}