﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Applications.aspx.cs" Inherits="eFrameWork.Manage.Applications" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script>
    function SelectICO() {
        var url = "../Plugins/FontIco.aspx";
        layer.open({
            type: 2
          , title: "选择图标"
          , shadeClose: true //点击遮罩关闭层
          , area: ["80%", "80%"]

          , content: url

          , success: function (layero, index) {
              arrLayerIndex.push(index);
          }
          , cancel: function (index, layero) {
              arrLayerIndex.pop();
          }

        });

    };
    function setIco(obj) {
        var span = getobj("spanico");
        span.innerHTML = obj.innerHTML.replace("<i></i>", "");
        var input = getobj("f5");
        input.value = obj.innerHTML;
        layer.close(arrLayerIndex.pop());
    };
</script>
<style>
  .eform span i {
    font-size:20px;color:#000;
    }
</style>
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 应用管理<a id="btn_add" style="<%=( Action.Value == "" ? "" : "display:none;" )%>" class="button" href="<%=edt.getAddURL()%>"><span><i class="add">添加</i></span></a></div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>

<div style="margin:6px;line-height:25px;font-size:13px;">
 <%
if(Action.Value.Length > 0 )
{
%>
    
       <asp:PlaceHolder ID="eFormControlGroup" runat="server">
    <form name="frmaddoredit" id="frmaddoredit" method="post" action="<%=edt.getSaveURL()%>">
	<input name="id" type="hidden" id="id" value="<%=edt.ID%>">
    <input name="act" type="hidden" id="act" value="save">  
	<input name="fromurl" type="hidden" id="fromurl" value="<%=edt.FromURL%>">  
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="eDataView">
      <tr>
        <td width="126" class="title"><ins>*</ins>应用名称：</td>
        <td class="content"><span class="eform">
		 <ev:eFormControl ID="f1" Field="MC" width="600" notnull="true" fieldname="名称" runat="server" />
		</span></td>
      </tr>
	    <tr>
          <td class="title"><ins>*</ins>应用类型：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f8" ControlType="select" notnull="true" fieldname="应用类型" Options="[{text:电脑端,value:1},{text:移动端,value:2},{text:电脑端+移动端,value:3}]" Field="Type" runat="server" />
		   </span></td>
        </tr>
	  <tr>
          <td class="title">应用图片：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f2" ControlType="fileselect" Width="500" Field="Pic" Options="{&quot;basepath&quot;:&quot;upload&quot;}" runat="server" />
		   </span></td>
        </tr>		
         <tr>
          <td class="title">图标：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f3" ControlType="fileselect" Width="500" Field="Icon" Options="{&quot;basepath&quot;:&quot;upload&quot;}" runat="server" />
		   </span></td>
        </tr>
         <tr>
          <td class="title">活动图标：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f4" ControlType="fileselect" Width="500" Field="IconActive" Options="{&quot;basepath&quot;:&quot;upload&quot;}" runat="server" />
		   </span></td>
        </tr>
         <tr>
          <td class="title">字体图标：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f5" ControlType="hidden" Width="500" Field="IconHTML" HtmlTag="true" runat="server" />
              <%="<span id=\"spanico\" style=\"margin-right:10px;font-size:20px;\">"+ f5.Value.ToString() + "</span>" + (Action.Value == "edit" ? "<a href=\"javascript:;\" onclick=\"SelectICO();\">选择</a>" : "") %> 
		   </span></td>
        </tr>
  		<tr>
          <td class="title">说明：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f6" ControlType="textarea" Field="SM" width="600" height="60" runat="server" />
		   </span></td>
        </tr>
        <tr>
          <td class="title">顺序：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="F7" ControlType="sort" Field="PX" width="600" DefaultValue="0" runat="server" />
		   </span></td>
        </tr>
        <tr>
       <td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">		
		<a class="button" href="javascript:;" onclick="if(frmaddoredit.onsubmit()!=false){frmaddoredit.submit();}"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" style="margin-left:30px;" onclick="history.back();"><span><i class="back">返回</i></span></a>
		</td>
	   </tr>
	 
    </table>
	 </form>
</asp:PlaceHolder>
 <%
 }else{
%>

<ev:eListControl ID="eDataTable" ShowMenu="true" LineHeight="35" CellSpacing="1" Style="min-width:1200px;" runat="server">
<ev:eListColumn ControlType="text" FieldName="序号" Width="50" runat="server">{row:index}</ev:eListColumn>
<ev:eListColumn ControlType="text" Field="ApplicationID" FieldName="应用编号" Width="260" OrderBy="true" runat="server">
<a class="copy" href="javascript:;" data-clipboard-action="copy" data-clipboard-text="{data:ApplicationID}"></a>{data:ApplicationID}
</ev:eListColumn>
<ev:eListColumn ControlType="text" Field="MC" FieldName="应用名称" OrderBy="true" runat="server" >
<a href="ApplicationItems.aspx?AppId={data:ID}">{data:MC}</a>
</ev:eListColumn>
<ev:eListColumn ControlType="select" Options="[{text:电脑端,value:1},{text:移动端,value:2},{text:电脑端+移动端,value:3}]" Field="Type" FieldName="应用类型" runat="server" />
<ev:eListColumn ControlType="text" Field="Pic" FieldName="应用图片" Width="110" runat="server" >
<img src="../{data:Pic}" width="30" style="max-width:30px;max-height:30px;" onerror="this.src='../images/none.gif';">
</ev:eListColumn>
<ev:eListColumn ControlType="text" Field="Icon" FieldName="图标" Width="80" runat="server" >
<img src="../{data:Icon}" width="25" onerror="this.src='../images/none.gif';">
</ev:eListColumn>
<ev:eListColumn ControlType="text" Field="IconActive" FieldName="活动图标" Width="80" runat="server" >
<img src="../{data:IconActive}" width="25" onerror="this.src='../images/none.gif';">
</ev:eListColumn>
<ev:eListColumn ControlType="text" Field="IconHTML" FieldName="字体图标" Width="80" runat="server" >
    <span style="font-size:20px;">{data:IconHTML}</span>
</ev:eListColumn>
<ev:eListColumn ControlType="text" Field="SM" FieldName="说明" runat="server" />
<ev:eListColumn ControlType="text" Field="addTime" FieldName="添加时间" Width="100" FormatString="{0:yyyy-MM-dd}" OrderBy="true" runat="server" />
<ev:eListColumn ControlType="text" FieldName="操作" Width="130" runat="server">
    <a href="{base:url}act=edit&id={data:ID}">修改</a>
    <a href="{base:url}act=del&id={data:ID}" onclick="javascript:return confirm('确认要删除吗？');">删除</a>
    <a href="ExportConfig.aspx?columns=true&appid={data:ID}" target="_blank">导出</a>
</ev:eListColumn>
</ev:eListControl>
<div style="margin:10px;">
<ev:ePageControl ID="ePageControl1" PageSize="10" PageNum="9" runat="server" />
</div>

 <%
 }
%>

</div>
</asp:Content>
