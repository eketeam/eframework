﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="AppRoles.aspx.cs" Inherits="eFrameWork.Manage.AppRoles" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
    <div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 角色管理<a id="btn_add" style="<%=( Action.Value == "" ? "" : "display:none;" )%>" class="button" href="<%=eform.getAddURL()%>"><span><i class="add">添加</i></span></a></div>
    <%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
<div style="margin:6px;line-height:25px;font-size:13px;">
 <%
if(Action.Value.Length > 0 )
{
%>
    
       <asp:PlaceHolder ID="eFormControlGroup" runat="server">
    <form name="frmaddoredit" id="frmaddoredit" method="post" action="<%=eform.getSaveURL()%>">
	<input name="id" type="hidden" id="id" value="<%=eform.ID%>">
    <input name="act" type="hidden" id="act" value="save">  
	<input name="fromurl" type="hidden" id="fromurl" value="<%=eform.FromURL%>">  
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="eDataView">
      <tr>
        <td width="126" class="title"><ins>*</ins>角色名称：</td>
        <td class="content"><span class="eform">
		 <ev:eFormControl ID="f1" Field="MC" width="200" notnull="true" fieldname="名称" runat="server" />
		</span></td>
      </tr>
          <tr>
        <td class="title">所属企业：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F2" ControlType="select" Field="SiteID" width="200" FieldName="所属企业" FieldType="int" setDataBase="false" BindObject="a_eke_sysSites" BindText="MC" BindValue="SiteID" BindCondition="delTag=0" BindOrderBy="addTime" notnull="false" runat="server" />
		</span></td>
      </tr>  
        <tr valign="top">
            <td class="title">应用：</td>
              <td class="content">
                  <asp:Literal id="LitApps" runat="server" />
                   <ev:eSubForm ID="eSubForm1" FileName="../Customs/base/AppRolePower.aspx" runat="server" />
                  </td>
        </tr>
  		<tr>
          <td class="title">说明：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f3" ControlType="textarea" Field="SM" width="600" height="60" runat="server" />
		   </span></td>
        </tr>

        <tr>
       <td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">		
		<a class="button" href="javascript:;" onclick="if(frmaddoredit.onsubmit()!=false){frmaddoredit.submit();}" style="display:none;"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" onclick="ajaxSubmit(frmaddoredit);"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" style="margin-left:30px;" onclick="history.back();"><span><i class="back">返回</i></span></a>
		</td>
	   </tr>
	 
    </table>
	 </form>
</asp:PlaceHolder>
 <%
 }
 else
 {
%>

<dl id="eSearchBox" class="ePanel">
<dt><h1 onclick="showPanel(this);" class="search"><a href="javascript:;" class="cur" onfocus="this.blur();"></a>搜索</h1></dt>
<dd style="display:none;">
<asp:PlaceHolder ID="eSearchControlGroup" runat="server">
<form id="frmsearch" name="frmsearch" method="post" onsubmit="return goSearch(this);" action="<%=elist.getSearchURL()%>">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title">角色名称：</td>
<td class="content"><span class="eform"><ev:eSearchControl ID="s1" Name="s1" ControlType="text" Field="MC" Operator="like" FieldName="角色名称" DataType="string" Width="300px" runat="server" /></span></td>
</tr>
<tr>
<td colspan="4" class="title" style="text-align:left;padding-left:125px;"><a class="button" href="javascript:;" onclick="if(frmsearch.onsubmit()!=false){frmsearch.submit();}"><span><i class="search">搜索</i></span></a></td>
</tr>
</table>
</form>
</asp:PlaceHolder>
</dd>
</dl>
<ev:eListControl ID="eDataTable" ShowMenu="true" LineHeight="35" CellSpacing="1" runat="server" >
<ev:eListColumn ControlType="text" FieldName="编号" Width="260" runat="server"><a class="copy" href="javascript:;" data-clipboard-action="copy" data-clipboard-text="{data:ID}"></a>{data:ID}</ev:eListColumn>
<ev:eListColumn ControlType="text" Field="MC" FieldName="角色名称" OrderBy="true" runat="server" />
<ev:eListColumn ControlType="text" Field="SM" FieldName="说明" runat="server" />
<ev:eListColumn ControlType="text" Field="addTime" FieldName="添加时间" Width="100" FormatString="{0:yyyy-MM-dd}" OrderBy="true" runat="server" />
<ev:eListColumn ControlType="text" FieldName="操作" Width="130" runat="server">
    <a href="{base:url}act=copy&id={data:ID}" onclick="javascript:return confirm('确认要复制吗？');">复制</a>
    <a href="{base:url}act=edit&id={data:ID}">修改</a>
    <a href="{base:url}act=del&id={data:ID}" onclick="javascript:return confirm('确认要删除吗？');">删除</a>
</ev:eListColumn>
</ev:eListControl>
<div style="margin:10px;">
<ev:ePageControl ID="ePageControl1" PageSize="20" PageNum="9" runat="server" />
</div>

<%} %>

</div>
</asp:Content>
