﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using LitJson;

namespace eFrameWork.Manage
{
    public partial class SiteItems : System.Web.UI.Page
    {
        public string AppItem = "";
        public string siteid = "";
        public string act = "";
        private string parentModelID = "";
        string sql = "";
        /// <summary>
        /// 主库
        /// </summary>
        private eDataBase _database;
        private eDataBase DataBase
        {
            get
            {
                if (_database == null)
                {
                    _database = eConfig.DefaultDataBase;
                }
                return _database;
            }
        }
        private DataTable _applications;
        public DataTable Applications
        {
            get
            {
                if (_applications == null)
                {
                    sql = "select a.ApplicationID,a.MC,b.SiteItemID,b.appName,b.domain from a_eke_sysApplications a";
                    sql += " left join a_eke_sysSiteItems b on a.ApplicationID=b.ApplicationID and b.SiteID='" + siteid + "'";
                    sql += " where a.delTag=0";
                    sql += " order by a.px,a.addTime";
                    _applications = DataBase.getDataTable(sql);
                    //eBase.Writeln(sql);
                    //_applications = DataBase.getDataTable("select a.ApplicationID,a.MC from a_eke_sysApplications a where a.delTag=0 order by a.px,a.addTime");
                }
                return _applications;
            }
        }
        private DataTable _applicationitems;
        public DataTable ApplitionItems
        {
            get
            {
                if (_applicationitems == null)
                {
                    sql = "select a.ApplicationItemID,a.ApplicationID,a.ModelID,a.ParentID,a.MC,b.Power,a.PX,a.addTime";
                    sql += " from a_eke_sysApplicationItems a";
                    sql += " left join a_eke_sysModels b on a.ModelID=b.ModelID";
                    sql += " where a.delTag=0";
                    _applicationitems = DataBase.getDataTable(sql);
                }
                return _applicationitems;
            }
        }
        private eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            siteid = eParameters.QueryString("id");
            act = eParameters.Request("act").ToLower();
            AppItem = eParameters.Request("AppItem");

            if (AppItem.Length > 0)
            {
                DataRow[] appRows = eBase.a_eke_sysApplicationItems.Select("ApplicationItemID='" + AppItem + "'");
                if (appRows.Length == 0) return;
                parentModelID = appRows[0]["ModelID"].ToString();
            }
            switch (act)
            {
                case "del":
                    sql = "update a_eke_sysSiteItems set delTag=1 where SiteID='" + siteid + "'";
                    eBase.DataBase.Execute(sql);
                    break;
                case "save":
                    save();
                    break;
                default:
                    LitBody.Text = getApps();
                    break;
            }

        }
        private void save()
        {
            if (siteid.Length == 0)
            {
                if (HttpContext.Current.Items["ID"] != null) siteid = HttpContext.Current.Items["ID"].ToString();
            }
            JsonData json = null;
            string jsonstr = eParameters.Form("eformdata_" + parentModelID);
            if (jsonstr.Length > 0)
            {
                json = jsonstr.ToJsonData();
                json = json.GetCollection("eformdata_" + parentModelID).GetCollection()[0];
            }
            foreach (DataRow dr in Applications.Rows)
            {
                string name = "model_" + dr["ApplicationID"].ToString().Replace("-", "");
                string value = json == null ? eParameters.Form(name) : json.GetValue(name);
                if (value.Length > 0) //有权限
                {
                    string appname = "";                   
                    name = "appname_" + dr["ApplicationID"].ToString().Replace("-", "");
                    appname = json == null ? eParameters.Form(name) : json.GetValue(name);
                    appname = appname.Replace("'", "''");

                    string domain = "";
                    name = "domain_" + dr["ApplicationID"].ToString().Replace("-", "");
                    domain = json == null ? eParameters.Form(name) : json.GetValue(name);
                    domain = domain.Replace("'", "''");

                    /*
                    sql = "if exists (select * from a_eke_sysSiteItems where ApplicationID='" + dr["ApplicationID"].ToString() + "' and SiteID='" + siteid + "')";
                    sql += " update a_eke_sysSiteItems set delTag=0,AppName='" + appname + "',domain='" + domain + "' where ApplicationID='" + dr["ApplicationID"].ToString() + "' and SiteID='" + siteid + "'";
                    sql += " else ";
                    sql += "insert into a_eke_sysSiteItems (SiteItemID,ApplicationID,SiteID,AppName,domain) ";
                    sql += " values ('" + Guid.NewGuid().ToString() + "','" + dr["ApplicationID"].ToString() + "','" + siteid + "','" + appname + "','" + domain + "')";
                    */
                    sql = "";
                    string ct = DataBase.getValue("select count(1) from a_eke_sysSiteItems where ApplicationID='" + dr["ApplicationID"].ToString() + "' and SiteID='" + siteid + "'");
                    if (ct == "0")
                    {
                        sql = "insert into a_eke_sysSiteItems (SiteItemID,ApplicationID,SiteID,AppName,domain) ";
                        sql += " values ('" + Guid.NewGuid().ToString() + "','" + dr["ApplicationID"].ToString() + "','" + siteid + "','" + appname + "','" + domain + "')";
                    }
                    else
                    {
                        sql = "update a_eke_sysSiteItems set delTag=0,AppName='" + appname + "',domain='" + domain + "' where ApplicationID='" + dr["ApplicationID"].ToString() + "' and SiteID='" + siteid + "'";
                    }
                    //eBase.AppendLog(sql);
                    DataBase.Execute(sql);
                }
                else
                {
                    sql = "delete from a_eke_sysSiteItems where ApplicationID='" + dr["ApplicationID"].ToString() + "' and SiteID='" + siteid + "'";
                    DataBase.Execute(sql);
                }
            }

            eBase.clearDataCache("a_eke_sysSiteItems");
        }


        private string getApps()
        {
            StringBuilder sb = new StringBuilder();
            foreach (DataRow dr in Applications.Rows)
            {
                DataRow[] rows = ApplitionItems.Select("ApplicationID='" + dr["ApplicationID"].ToString() + "' and ParentID is null", "px,addTime");
                if (rows.Length == 0) continue;

                sb.Append("<div class=\"powerModel\">");
                sb.Append("<span class=\"modelname\" style=\"width: 250px;\">");
                sb.Append("<input type=\"checkbox\" name=\"model_" + dr["ApplicationID"].ToString().Replace("-", "") + "\" id=\"model_" + dr["ApplicationID"].ToString().Replace("-", "") + "\" value=\"true\"" + (dr["SiteItemID"].ToString().Length>0  ? " checked" : "") + (act == "view" ? " disabled" : "") + " />");
                sb.Append("<label for=\"model_" + dr["ApplicationID"].ToString().Replace("-", "") + "\">" + dr["mc"].ToString() + "</label>");
                sb.Append("</span>");

                sb.Append("<span class=\"cond\" style=\"width: 320px;\">");
                sb.Append("应用名称：<input type=\"text\" class=\"text\" style=\"width: 210px;padding-left:6px;\" name=\"appname_" + dr["ApplicationID"].ToString().Replace("-", "") + "\" value=\"" + (dr["appName"].ToString().Length > 0 ? dr["appName"].ToString() : dr["MC"].ToString()) + "\" />");
                sb.Append("</span>");


                sb.Append("<span class=\"cond\" style=\"width: 320px;\">");
                sb.Append("绑定域名：<input type=\"text\" class=\"text\" style=\"width: 210px;padding-left:6px;\" name=\"domain_" + dr["ApplicationID"].ToString().Replace("-", "") + "\" value=\"" + dr["domain"].ToString() + "\" />");
                sb.Append("</span>");

                sb.Append("</div>");


            }
            return sb.ToString();
        }
    }
}