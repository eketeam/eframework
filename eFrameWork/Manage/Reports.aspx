﻿<%@ Page Title="" Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Reports.aspx.cs" Inherits="eFrameWork.Manage.Reports" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<script>
    function select_callback() {
        if ($("input[name='selitem']:checked").length == 0) {
            $("#btn_export").hide();
        }
        else {
            $("#btn_export").show();
        }
    };
    function Report_Import() {
        var url = "ModelImport.aspx";


        layer.open({
            type: 2,
            skin: 'layui-layer-rim', //加上边框
            title: "报表导入",
            maxmin: false,
            shadeClose: true, //点击遮罩关闭层
            area: ['600px', '300px'],
            content: url,
            success: function (layero, index) { arrLayerIndex.push(index); },
            cancel: function (index, layero) { arrLayerIndex.pop(); },
            end: function (index) { arrLayerIndex.pop(); }
        });
    };
    function Report_ExportConfig() {
        var sels = $("input[name='selitem']:checked");
        if (sels.length == 0) {
            return;
        }
        var ids = "";
        sels.each(function (i, elem) {
            if (ids.length > 0) { ids += ","; }
            ids += elem.value;
        });       
        var url = "Reports.aspx?act=export&ajax=true&reportids=" + ids;
        window.open(url);
    };
</script>
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 报表<a id="btn_add" style="<%=(act == "" ? "" : "display:none;" )%>" class="button" href="<%=edt.getAddURL()%>"><span><i class="add">添加</i></span></a>
     <a id="btn_import" class="button" href="javascript:;" onclick="Report_Import();"><span><i class="set">导入</i></span></a>
    <a id="btn_export" class="button" href="javascript:;" onclick="Report_ExportConfig();" style="display:none;"><span><i class="save">导出</i></span></a>
</div>

     <div style="margin:6px;line-height:25px;font-size:13px;">
    <div class="tips" style="margin-bottom:6px;"><b>提示</b><br>在此进行定义,通过eReportControl控件进行调用。示例地址：Examples/Report.aspx</div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>   
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
  
 <%} %>
     </div>

<%
    if (act == "add" || act == "edit" || act == "copy")
{
%>
<div style="margin:6px;">
    <asp:PlaceHolder ID="eFormControlGroup" runat="server">
	<form name="frmaddoredit" id="frmaddoredit" method="post" action="<%=edt.getSaveURL()%>">
	<input name="id" type="hidden" id="id" value="<%=edt.ID%>">
    <input name="act" type="hidden" id="act" value="save">  
	<input name="fromurl" type="hidden" id="fromurl" value="<%=edt.FromURL%>">  
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="eDataView">

      <tr>
        <td width="126" class="title"><ins>*</ins>名称：</td>
        <td class="content"><span class="eform">
		 <ev:eFormControl ID="f1" Field="MC" width="600" notnull="true" fieldname="名称" runat="server" />
		</span></td>
      </tr>
	 
        <tr>
          <td class="title">报表类型：</td>
          <td class="content"><span class="eform">
		  <ev:eFormControl ID="f2" ControlType="select" Field="ControlType" DefaultValue="table" runat="server" /> 
		  </span></td>
        </tr>
    <tr>
          <td class="title">数据源：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f3" ControlType="select" Field="DataSourceID" FieldType="uniqueidentifier" BindObject="a_eke_sysDataSources" BindValue="DataSourceID" BindText="MC" BindCondition="delTag=0" Options="[{text:主库,value:NULL}]" DefaultValue="NULL" BindOrderBy="addTime desc" runat="server" />
		   </span></td>
        </tr>

  		<tr>
          <td class="title">说明：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f4" ControlType="textarea" Field="SM" width="600" height="60" htmlTag="true" runat="server" />
		   </span></td>
        </tr>
        <tr>
       <td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">		
		<a class="button" href="javascript:;" onclick="if(frmaddoredit.onsubmit()!=false){frmaddoredit.submit();}" style="di3splay:none;"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" onclick="ajaxSubmit(frmaddoredit);" style="display:none;"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" style="margin-left:30px;" onclick="history.back();"><span><i class="back">返回</i></span></a>
		</td>
	   </tr>
	 
    </table>
	 </form>
</asp:PlaceHolder>
	 </div>
<%}%>
<div style="margin:6px;overflow-x:auto;overflow-y:hidden;">
<asp:Repeater id="Rep" runat="server">
<headertemplate>
<table id="eDataTable" class="eDataTable" border="0" cellpadding="0" cellspacing="1" width="100%">
<thead>
<tr bgcolor="#f2f2f2">
<td width="35"><input type="checkbox" name="checkbox" value="0" onclick="selectAllItems(this);"></td>
<td width="260">编号</td>
<td>名称</td>
<td>报表类型</td>
<td>说明</td>
<td width="100">添加时间</td>
<td width="150">操作</td>
</tr>
</thead>
</headertemplate>
<itemtemplate>
<tr<%# ((Container.ItemIndex+1) % 2 == 0 ? " class=\"alternating\" eclass=\"alternating\"" : " eclass=\"\"") %>>
 <td height="32"><input type="checkbox" name="selitem" value="<%# Eval("ReportID").ToString()%>"></td>
<td><a class="copy" href="javascript:;" data-clipboard-action="copy" data-clipboard-text="<%# Eval("ReportID")%>"></a><%# Eval("ReportID")%></td>
<td><a href="ReportItems.aspx?ReportID=<%# Eval("ReportID").ToString()%>"><%# Eval("MC")%></a></td>
<td><%# eBase.getReportControlTypeText(Eval("ControlType").ToString())%></td>
<td><%# Eval("SM").ToString()%></td>
<td><%# Eval("addTime","{0:yyyy-MM-dd}") %></td>
<td>
<a href="?act=copy&id=<%#  Eval("ReportID").ToString()%>">复制</a>
<a href="?act=edit&id=<%#  Eval("ReportID").ToString()%>">修改</a>
<a href="?act=del&id=<%#  Eval("ReportID").ToString()%>" onclick="javascript:return confirm('确认要删除吗？');">删除</a>
</td>
</tr>
</itemtemplate>
<footertemplate></table></footertemplate>
</asp:Repeater>
</div>
<div style="margin:6px;"><ev:ePageControl ID="ePageControl1" PageSize="20" PageNum="9" runat="server" /></div>
</asp:Content>