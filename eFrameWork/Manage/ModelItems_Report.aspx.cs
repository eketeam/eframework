﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;

namespace eFrameWork.Manage
{
    public partial class ModelItems_Report : System.Web.UI.Page
    {
        public string modelid = eParameters.QueryString("modelid");
        private eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            Response.Write("<a href=\"http://frame.eketeam.com\" style=\"float:right;\" target=\"_blank\" title=\"eFrameWork开发框架\"><img src=\"images/help.gif\"></a>");
            
            if (eConfig.showHelp())
            {
                Response.Write("<div class=\"tips\" style=\"margin-bottom:6px;\">");
                Response.Write("<b>报表</b><br>");
                Response.Write("设置表格、图表。");
                Response.Write("</div> ");
            }


            eList datalist = new eList("a_eke_sysReports");
            datalist.Where.Add("ModelID='" + modelid + "' and delTag=0");
            datalist.OrderBy.Add("PX, addTime");
            //datalist.Bind(RepX);
            datalist.Bind(Rep);




            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            ControlGroup.RenderControl(htw);
            ControlGroup.Visible = false;//不输出，要在获取后设，不然取不到内容。
            Response.Write(sw.ToString());
            sw.Close();
            Response.End();
        }
    }
}