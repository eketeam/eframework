﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.FrameWork;
using EKETEAM.Data;

namespace eFrameWork.Manage
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            eUser user = new eUser("Manage");
            string act = eParameters.QueryString("act");
            if (act == "setfixed")
            {
                string name = eParameters.QueryString("name");
                string value = eParameters.QueryString("value");
                string type = name == "manage_left_fixed" ? "左侧固定" : "顶部固定";
                if (name == "manage_left_fixed")
                {
                    Session.Remove("Manage_LeftFixed");
                }
                else
                {
                    Session.Remove("Manage_TopFixed");
                }
                /*
                string sql = "if exists (select * from a_eke_sysUserCustoms Where parName='" + name + "' and UserID='" + user.ID + "')";
                sql += "update a_eke_sysUserCustoms set parValue='" + value + "' where parName='" + name + "' and UserID='" + user.ID + "'";
                sql += " else ";
                sql += "insert into a_eke_sysUserCustoms (UserCustomID,UserID,parName,MC,parValue) ";
                sql += " values ('" + Guid.NewGuid().ToString() + "','" + user.ID + "','" + name + "','" + type + "','" + value + "')";
                */
                string sql = "";
                string ct = eBase.UserInfoDB.getValue("select count(1) from a_eke_sysUserCustoms Where parName='" + name + "' and UserID='" + user.ID + "'");
                if (ct == "0")
                {
                    sql = "insert into a_eke_sysUserCustoms (UserCustomID,UserID,parName,MC,parValue) ";
                    sql += " values ('" + Guid.NewGuid().ToString() + "','" + user.ID + "','" + name + "','" + type + "','" + value + "')";
                }
                else
                {
                    sql += "update a_eke_sysUserCustoms set parValue='" + value + "' where parName='" + name + "' and UserID='" + user.ID + "'";
                }
                eBase.UserInfoDB.Execute(sql);
                Response.End();
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("欢迎登录" + eConfig.manageName() + "!<br>");
            sb.Append("用户名：" + user["Name"] + "<br>");
            sb.Append("用户ID：" + user.ID + "<br>");
            sb.Append("用户SiteID：" + user["SiteID"] + "<br>");
            sb.Append("用户OrgCode：" + user["OrgCode"] + "<br>");
            sb.Append("用户UserCode：" + user["UserCode"] + "<br>");

            DataTable tb = eBase.DataBase.getDataTable("select LoginCount,LastLoginTime from a_eke_sysUsers where UserID='" + user["ID"].ToString() + "'");
            if (tb.Rows.Count > 0)
            {
                string logincount = tb.Rows[0]["LoginCount"].ToString();
                string lastlt = string.Format("{0:yyyy-MM-dd HH:mm:ss}", tb.Rows[0]["LastLoginTime"]);

                sb.Append("登录次数：" + logincount + "<br>");
                sb.Append("上次登录时间：" + lastlt + "<br>");
            }
            LitBody.Text = sb.ToString();

            //DataTable dt = eOleDB.getColumns("Demo_BidProjects");
            //eBase.PrintDataTable(dt);

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {
                lit.Text = "首页 - " + eConfig.manageName(); 
            }
        }
    }
}