﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Cache.aspx.cs" Inherits="eFrameWork.Manage.Cache" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 缓存管理</div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
<div style="margin:6px;line-height:25px;font-size:13px;">
    <asp:Literal ID="LitBody" runat="server" />
</div>
</asp:Content>