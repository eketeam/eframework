﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="AppUsers.aspx.cs" Inherits="eFrameWork.Manage.AppUsers" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 用户管理<a id="btn_add" style="<%=(act == "" ? "" : "display:none;" )%>" class="button" href="<%=eform.getAddURL()%>"><span><i class="add">添加</i></span></a></div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
<script>
function checkUser(frm)
{
	//frm.elements["id"]
	//if(frm.id.value.length>0){return true;}
	if("<%=act%>" != "add"){return true;}
	var url="?act=getuser&value=" + frm.f1.value + "&t=" + now();
	var html=PostURL(url);
	if(html=="false")
	{
		alert("该用户名已存在!");
		frm.f1.focus();
		return false;
	}
	return true;
};
</script>
<%
    if (act == "edit" || act == "add" || act == "view")
{
%>

<div style="margin:6px;">
 <asp:PlaceHolder ID="eFormControlGroup" runat="server">
<form name="frmaddoredit" id="frmaddoredit" method="post" action="<%=eform.getSaveURL()%>">
	<input name="id" type="hidden" id="id" value="<%=id%>">
    <input name="act" type="hidden" id="act" value="save">  
	<input name="fromurl" type="hidden" id="fromurl" value="<%=eform.FromURL%>">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="eDataView">
      <tr>
        <td width="126" class="title"><font color="#FF0000">*</font> 用户名：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="f1" Field="yhm" width="200" FieldName="用户名"  notnull="true" runat="server" />
		</span></td>
      </tr>
      <tr>
        <td class="title"><font color="#FF0000">*</font> 密码：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="f2" ControlType="password" Field="MM" width="200" FieldName="密码" notnull="true" runat="server" />
		</span></td>
      </tr>
      <tr>
        <td class="title"><font color="#FF0000">*</font> 姓名：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="f3" Field="xm" width="200" FieldName="姓名"  notnull="true" runat="server" />
		</span></td>
      </tr>
     <tr>
        <td class="title">昵称：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="f8" Field="nick" width="200" FieldName="昵称" notnull="false" runat="server" />
		</span></td>
      </tr>
     <tr>
        <td class="title"><font color="#FF0000">*</font> 用户类型：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F4" ControlType="select" Field="UserType" width="200" FieldName="用户类型" Options="[{text:开发人员,value:1},{text:系统用户,value:2}]" notnull="true" runat="server" />
		</span></td>
      </tr>  
    <tr>
        <td class="title">所属企业：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F6" ControlType="select" Field="SiteID" width="200" FieldName="所属企业" FieldType="int" setDataBase="false" BindObject="a_eke_sysSites" BindText="MC" BindValue="SiteID" BindCondition="delTag=0 and (ParentID=0 or ParentID is null)" BindOrderBy="addTime" Attributes="onchange=&quot;autoFill(this,this.value,'F7');&quot;" notnull="false" runat="server" />
		</span></td>
      </tr>  
    <tr>
        <td class="title">管理站点：</td>
         <td class="content"><span class="eform" etype="select" id="F7_box">
             <ev:eFormControl ID="F7" ControlType="select" Field="webCode" width="200" FieldName="管理站点" FieldType="string" setDataBase="false" BindObject="a_eke_sysWebSites" BindText="SiteName" BindValue="WebCode" BindCondition="delTag=0 and (ParentID=0 or ParentID is null) and (SiteID='{querystring:pid}')" BindOrderBy="addTime" BindAuto="false" notnull="false" runat="server" />
            </span></td>
    </tr>
	   <tr valign="top">
        <td class="title">角色权限：</td>
        <td class="content"><asp:Literal id="LitRoles" runat="server" />
             <ev:eSubForm ID="eSubForm1" FileName="../Customs/base/AppUserPower.aspx" runat="server" />
        </td>
      </tr> 
     <tr>
        <td class="title">用户状态：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F5" ControlType="radio" Field="Active" FieldName="用户状态" Options="[{text:启用,value:1},{text:停用,value:0}]" DefaultValue="1" notnull="true" runat="server" />
		</span></td>
      </tr>  
	 <tr>
       <td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">		
		 <a class="button" href="javascript:;" onclick="if(frmaddoredit.onsubmit()!=false){frmaddoredit.submit();}" style="display:none;"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" onclick="ajaxSubmit(frmaddoredit);"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" style="margin-left:30px;" onclick="history.back();"><span><i class="back">返回</i></span></a>
		</td>
	   </tr>	
    </table>
	</form>
     <ev:eSubForm ID="eSubForm2" Visible="false" FileName="../Customs/base/UserCode.aspx" runat="server" />
	</asp:PlaceHolder>
</div>
    <script>
        $(document).ready(function () {
            var f6v = "<%=F6.Value.ToString()%>";
           
            if (f6v.length > 0) {
                var f6 = $("#F6").get(0);
                autoFill(f6, f6.value, 'F7');
            }
            var f7v = "<%=F7.Value.ToString()%>";
            if (f7v.length > 0)
            {
                $("#F7").val(f7v);
            }
        });
    </script>
	<%}else{%>	
<div style="margin:6px;overflow-x:auto;overflow-y:hidden;">
<dl id="eSearchBox" class="ePanel">
<dt><h1 onclick="showPanel(this);" class="search"><a href="javascript:;" class="cur" onfocus="this.blur();"></a>搜索</h1></dt>
<dd style="display:none;">
<asp:PlaceHolder ID="eSearchControlGroup" runat="server">
<form id="frmsearch" name="frmsearch" method="post" onsubmit="return goSearch(this);" action="<%=elist.getSearchURL()%>">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title">用户名：</td>
<td class="content"><span class="eform"><ev:eSearchControl ID="s1" Name="s1" ControlType="text" Field="YHM" Operator="like" FieldName="用户名" DataType="string" Width="300px" runat="server" /></span></td>
<td class="title">姓名：</td>
<td class="content"><span class="eform"><ev:eSearchControl ID="s2" Name="s2" ControlType="text" Field="XM" Operator="like" FieldName="姓名" DataType="string" Width="300px" runat="server" /></span></td>
</tr>
<tr>
<td class="title">部门：</td>
<td class="content"><span class="eform"><ev:eSearchControl ID="s4" Name="s4" ControlType="text" Field="Department" Operator="like" FieldName="部门" Width="300px" DataType="string" runat="server" /></span></td>
<td class="title">用户类型：</td>
<td class="content"><span class="eform"><ev:eSearchControl ID="s3" Name="s3" ControlType="select" Field="UserType" Operator="=" Options="[{text:开发人员,value:1},{text:系统用户,value:2}]" FieldName="用户类型"  DataType="string" runat="server" /></span></td>
</tr>
<tr>
<td colspan="4" class="title" style="text-align:left;padding-left:125px;"><a class="button" href="javascript:;" onclick="if(frmsearch.onsubmit()!=false){frmsearch.submit();}"><span><i class="search">搜索</i></span></a></td>
</tr>
</table>
</form>
</asp:PlaceHolder>
</dd>
</dl>
<ev:eListControl ID="eDataTable" Class="eDataTable" CellSpacing="1" LineHeight="32" runat="server" >
    <ev:eListColumn ControlType="text" FieldName="编号" Width="260" runat="server"><a class="copy" href="javascript:;" data-clipboard-action="copy" data-clipboard-text="{data:ID}"></a>{data:ID}</ev:eListColumn>
    <ev:eListColumn ControlType="text" Field="YHM" FieldName="用户名" runat="server" />
    <ev:eListColumn ControlType="text" Field="XM" FieldName="姓名" runat="server" />
    <ev:eListColumn ControlType="text" Field="Nick" FieldName="昵称" runat="server" />
    <ev:eListColumn ControlType="text" Field="Department" FieldName="部门" runat="server" />
    <ev:eListColumn ControlType="radio" Field="UserType" FieldName="用户类型" Options="[{text:开发人员,value:1},{text:系统用户,value:2}]" runat="server" />
    <ev:eListColumn ControlType="text" Field="Active" FieldName="用户状态" Options="[{text:启用,value:True},{text:停用,value:False}]" runat="server">
        <a href="?act=active&id={data:id}&value={data:showvalue}"><img src="{base:VirtualPath}{data:ShowPIC}" border="0"></a>
    </ev:eListColumn>
    <ev:eListColumn ControlType="text" Field="addTime" FieldName="添加时间" Width="100" FormatString="{0:yyyy-MM-dd}" runat="server" />
    <ev:eListColumn ControlType="text" FieldName="操作" Width="130" runat="server">
    <a href="{base:url}act=edit&id={data:ID}">修改</a>
    <a href="{base:url}act=del&id={data:ID}" onclick="javascript:return confirm('确认要删除吗？');">删除</a>
</ev:eListColumn>
</ev:eListControl>
<asp:Repeater id="Rep" runat="server">
<headertemplate>
<%#
"<table id=\"eDataTable\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" width=\"100%\">\r\n"+
"<thead>\r\n"+
  "<tr bgcolor=\"#f2f2f2\">\r\n"+
  	"<td width=\"250\">编号</td>\r\n"+
	"<td>用户名</td>\r\n"+
	"<td>姓名</td>\r\n"+
	"<td width=\"80\">添加时间</td>\r\n"+
	"<td width=\"120\">操作</td>\r\n"+
  "</tr>\r\n"+
"</thead>\r\n"
%>
</headertemplate>
<itemtemplate>
<%#
"<tr" + ((Container.ItemIndex+1) % 2 == 0 ? " class=\"alternating\" eclass=\"alternating\"" : " eclass=\"\"") + ">\r\n"+
    "<td height=\"32\"><a class=\"copy\" href=\"javascript:;\" data-clipboard-action=\"copy\" data-clipboard-text=\"" + Eval("UserID") + "\"></a>"+ Eval("UserID") + "</td>\r\n"+
	"<td>"+ Eval("YHM") + "</td>\r\n"+
	"<td>"+ Eval("XM") + "</td>\r\n"+
	"<td>"+ Eval("addTime","{0:yyyy-MM-dd}") + "</td>\r\n"+
	"<td>"+
	"<a href=\"" + eform.getActionURL("edit",Eval("UserID").ToString())  + "\">修改</a>"+
	"<a href=\""+ eform.getActionURL("del",Eval("UserID").ToString()) +"\" onclick=\"javascript:return confirm('确认要删除吗？');\">删除</a>"+
	"</td>\r\n"+
"</tr>\r\n"
%>
</itemtemplate>
<footertemplate>
<%#"</table>\r\n"%>
</footertemplate>
</asp:Repeater>

</div>
<div style="margin:6px;"><ev:ePageControl ID="ePageControl1" PageSize="20" PageNum="9" runat="server" /></div>
<%}%>	
</asp:Content>
