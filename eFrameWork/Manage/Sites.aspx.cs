﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;


namespace eFrameWork.Manage
{
    public partial class Sites : System.Web.UI.Page
    {
        public string id = eParameters.Request("id");
        public string act = eParameters.Request("act");
        public string sql = "";
        public eForm edt;
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            edt = new eForm("a_eke_sysSites", user);
            //edt.AutoRedirect = false;
            if (act.Length == 0)
            {

                List();
                return;
            }
            edt.AddControl(eFormControlGroup);
            edt.onChange += new eFormTableEventHandler(eform_onChange);
            edt.Handle();         
        }
        public void eform_onChange(object sender, eFormTableEventArgs e)
        {
            if (e.eventType == eFormTableEventType.Updated || e.eventType == eFormTableEventType.Inserted || e.eventType == eFormTableEventType.Deleted)
            {
                //清除相关缓存
                eBase.clearDataCache("a_eke_sysSites");
                eBase.clearDataCache("a_eke_sysSiteItems");
                runtimeCache.Remove();
            }
        }
        private void List()
        {
            eDataTable.CanEdit = true;
            eDataTable.CanDelete = true;
            eList datalist = new eList("a_eke_sysSites");
            datalist.Where.Add("delTag=0 and (ParentID=0 or ParentID is null)");
            datalist.OrderBy.Add("addTime desc");
            //datalist.Bind(Rep, ePageControl1);
            datalist.Bind(eDataTable, ePageControl1);
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {

                lit.Text = "企业管理 - " + eConfig.manageName(); 
            }
        }
    }
}