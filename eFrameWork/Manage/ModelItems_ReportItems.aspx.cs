﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;

public partial class Manage_ModelItems_ReportItems : System.Web.UI.Page
{
    public string ReportID = eParameters.QueryString("ReportID");
    public DataRow row;
    private eUser user;
    protected void Page_Load(object sender, EventArgs e)
    {
        user = new eUser("Manage");
        DataTable tb = eBase.DataBase.getDataTable("select * from a_eke_sysReports where ReportID='" + ReportID + "'");
        if (tb.Rows.Count == 0) Response.End();
        row = tb.Rows[0];

        string act = eParameters.QueryString("act").ToLower();
        if (act.Length == 0)
        {
            List();
        }
        else
        {
            string ReportItemID = eParameters.QueryString("ReportItemID");
            string value = eParameters.Request("value");
            string item = eParameters.QueryString("item").ToLower();
            string sql = "";
            #region 获取数据
            if (act == "getdata")
            {
                Response.Clear();
                List();
                System.IO.StringWriter sw = new System.IO.StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                ControlGroup.RenderControl(htw);
                ControlGroup.Visible = false;

                Response.Write(sw.ToString());
                Response.End();
            }
            #endregion
            #region 添加
            if (act == "additem")
            {
                string type = eParameters.QueryString("type");
                string ModelID = eBase.DataBase.getValue("Select ModelID from a_eke_sysReports where ReportID='" + ReportID + "'");
                if (ModelID.Length > 0)
                {
                    //sql = "insert into a_eke_sysReportItems (ReportItemID,ModelID,ReportID,Type) values ('" + Guid.NewGuid().ToString() + "','" + ModelID + "','" + ReportID + "','" + type + "')";
                }
                else
                {
                    //sql = "insert into a_eke_sysReportItems (ReportItemID,ReportID,Type) values ('" + Guid.NewGuid().ToString() + "','" + ReportID + "','" + type + "')";
                }
                //eBase.DataBase.Execute(sql);
                eBase.DataBase.eTable("a_eke_sysReportItems")
                      .Fields.Add("ReportItemID", Guid.NewGuid().ToString())
                      .Fields.Add("ModelID", ModelID.Length > 0 ? ModelID : null)
                      .Fields.Add("ReportID", ReportID)
                      .Fields.Add("Type", type)
                      .Add();
                eResult.Success("添加成功!");
            }
            #endregion
            #region 修改动作
            if (act == "setreport")
            {
                //sql = "update a_eke_sysReports set " + item + "='" + value + "' where ReportID='" + ReportID + "'";
                //if (value == "NULL") sql = "update a_eke_sysReports set " + item + "=" + value + " where ReportID='" + ReportID + "'";
                //eBase.DataBase.Execute(sql);
                eBase.DataBase.eTable("a_eke_sysReports")
                       .Fields.Add(item, value == "NULL" ? null : value)
                       .Where.Add("ReportID='" + ReportID + "'")
                       .Update();
                eBase.clearDataCache("a_eke_sysReports");
                eResult.Success("修改成功!");
            }
            if (act == "setitem")
            {
                if (item == "px" && value == "0") value = "999999";
                //sql = "update a_eke_sysReportItems set " + item + "='" + value + "' where ReportItemID='" + ReportItemID + "'";
                //if (value == "NULL") sql = "update a_eke_sysReportItems set " + item + "=" + value + " where ReportItemID='" + ReportItemID + "'";
                //eBase.DataBase.Execute(sql);
                eBase.DataBase.eTable("a_eke_sysReportItems")
                      .Fields.Add(item, value == "NULL" ? null : value)
                      .Where.Add("ReportItemID='" + ReportItemID + "'")
                      .Update();
                eBase.clearDataCache("a_eke_sysReportItems");
                eResult.Success("修改成功!");
            }
            #endregion
            #region  删除
            if (act == "delitem")
            {
                //eBase.DataBase.Execute("delete from a_eke_sysReportItems where ReportItemID='" + ReportItemID + "'");//ReportID='" + ReportID + "' and 
                eBase.DataBase.eTable("a_eke_sysReportItems").Where.Add("ReportItemID='" + ReportItemID + "'").DeleteTrue();
                eBase.clearDataCache("a_eke_sysReportItems");
                eResult.Success("删除成功!");
            }
            #endregion

            #region  拖动排序
            if (act == "setorders")
            {
                string ids = eParameters.Form("ids");
                string[] arr = ids.Split(",".ToCharArray());
                for (int i = 0; i < arr.Length; i++)
                {
                    value = (i + 1).ToString();
                    eBase.DataBase.Execute("update a_eke_sysReportItems set PX='" + value + "' where ReportItemID='" + arr[i] + "'");//ReportID='" + ReportID + "' and 
                }
                //eOleDB.Execute("update a_eke_sysReportItems set PX='999999' where ReportID='" + ReportID + "' and ReportItemID not in ('" + ids.Replace(",", "','") + "')");
                eBase.clearDataCache("a_eke_sysReportItems");
                eResult.Success("排序成功!");
            }
            #endregion
        }
    }
    private void List()
    {
        #region 列表
        eList elist = new eList("a_eke_sysReportItems");
        elist.Where.Add("ReportID='" + ReportID + "' and Type='Y'");
        elist.Where.Add("deltag=0");
        elist.OrderBy.Add("px,addTime");
        elist.Bind(RepY);


        elist = new eList("a_eke_sysReportItems");
        elist.Where.Add("ReportID='" + ReportID + "' and Type='X'");
        elist.Where.Add("deltag=0");
        elist.OrderBy.Add("px,addTime");
        elist.Bind(RepX);
        #endregion
    }
}