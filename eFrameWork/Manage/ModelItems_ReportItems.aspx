﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelItems_ReportItems.aspx.cs" Inherits="Manage_ModelItems_ReportItems" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>报表数据选项</title>
    <link href="../Plugins/layui226/css/layui.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />     
    <link href="../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
    <link href="../Plugins/Theme/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
	<link href="../Plugins/Theme/manage/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
	<script src="../Scripts/Init.js?ver=<%=Common.Version %>"></script>
</head>
<style>
h1 {font-size:16px;}
.text{text-indent:5px;display:inline-block;width:100%;border:1px solid #ccc;font-size:12px;height:23px;line-height:23px;color:#333;}
.divloading{filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity: 0.5; opacity: 0.5; position:fixed;width:100%;height:100%;top:0px;left:0px;background:#cccccc url(images/loading.gif) no-repeat center center;}
.btnaddb {display:inline-block;width:30px;height:30px;font-size:1px;background:url(images/btn_add.png) center center no-repeat;background-size:100% 100%;}
textarea {text-indent:5px;border:1px solid #ccc;font-size:12px;line-height:23px;color:#666;}
</style>
<script>
    /* 和ReportItems.aspx 功能相同，应同步修改 */
    var ReportID = "<%=ReportID%>";
    function showloading()
    {
        $("#divloading").show();
    };
    function hideloading()
    {
        $("#divloading").hide();
    };
    function getValue(obj)
    {
        if (obj.type.toLowerCase() == "radio" || obj.type.toLowerCase() == "checkbox") 
        {
            return (obj.checked ? "1" : "0");
        }
        else
        {
            return obj.value.encode();
        }
    };
    function addReportItem(obj,type)
    {
        var _reload=parseBool(Attribute(obj,"reload"));
        var url = "?act=additem&ReportID=" + ReportID;
        url += "&type=" + type; 
        url += "&t=" + now();
        showloading();
        $.ajax({
            type: "GET", async: true,
            url: url,
            dataType: "json",
            success: function (data)
            {
                hideloading();
                //if(!_reload){return;}
                loadData();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                hideloading();
            }	
        });
    };
    function setReport(obj, Item) {
        var _reload = parseBool(Attribute(obj, "reload"));
        //if (obj.getAttribute("oldvalue") == obj.value) { return; }
        var value = getValue(obj);
        if (value == "error") { return; }
        showloading();
        var url = "?act=setreport&ReportID=" + ReportID + "&item=" + Item + "&t=" + now();
        $.ajax({
            type: "post",
            async: true,
            data:{value:value},
            url: url,
            dataType: "html",
            success: function (data) {
                hideloading();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                hideloading();
            }
        });
    };
    function setReportItem(obj, ReportItemID, Item)
    {
        var _reload=parseBool(Attribute(obj,"reload"));
        //if (obj.getAttribute("oldvalue") == obj.value) { return; }
        var value = getValue(obj);
        if (value == "error") { return; }
        showloading();
        var url = "?act=setitem&ReportID=" + ReportID + "&ReportItemID=" + ReportItemID + "&item=" + Item + "&t=" + now();
        $.ajax({
            type: "post", 
            async: true,
            data:{value:value},
            url: url,
            dataType: "html",
            success: function (data)
            {
                if (Item == "finsh")
                {
                    obj.style.color = obj.value == 1 ? "#33cc00" : "#FF0000";
                }
                hideloading();
                if(!_reload){return;}
                loadData();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                hideloading();
            }		
        });
    };
    function delReportItem(obj, ReportItemID)
    {
    
        var _reload=parseBool(Attribute(obj,"reload"));
        if (!confirm('确认要删除吗？')) { return; }
        showloading();
        var url = "?act=delitem&ReportID=" + ReportID + "&ReportItemID=" + ReportItemID + "&t=" + now();
        $.ajax({
            type: "GET", async: true,
            url: url,
            dataType: "html",
            success: function (data) {
                hideloading();
            
                if (obj.parentNode.tagName == "TD") {
                    $(getParent(obj, "tr")).remove();
                }
                else {
                    $(getParent(obj, "dl")).remove();
                }

                //if(!_reload){return;}
                //loadData();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                hideloading();
            }
        });
    };
    function loadData()
    {
        showloading();
        var url = "?ReportID=" + ReportID + "&act=getdata&t=" + now();
        $.ajax({
            type: "GET", async: true,
            url: url,
            dataType: "html",
            success: function (data) {
                hideloading();
                $("#content").html(data);
                bindEvent();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                hideloading();
            }
        });
    };
    function bindEvent()
    {
        $(".eDataTable").each(function (index, note) {
            var id=$(note).attr("id");
            var tb = new eDataTable(id, 1);
            tb.moveRow = function (index, nindex) {
                $("#" + id + " tbody tr td:last-child").each(function (index1, obj) {
                    $(obj).html(1 + index1);
                });
                var ids = "";
                $("#" + id + " tbody tr td:nth-child(1)").each(function (index1, obj) {
                    if (index1 > 0) { ids += ","; }
                    ids += $(obj).parent().attr("erowid");
                });
                if (ids.length == 0) { return; }
                showloading();
                var url = "?act=setorders&ReportID=" + ReportID + "&t=" + now();
                $.ajax({
                    type: "POST", async: true,
                    data: { ids: ids },
                    url: url,
                    dataType: "html",
                    success: function (data) {
                        hideloading();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        hideloading();
                    }
                });
            };

        });

    };
    $(document).ready(function () {
        bindEvent();
    });
</script>
<body>
<div id="divloading" class="divloading" style="display:none;">&nbsp;</div>
<div class="tips" style="margin:8px;">
<b>变量说明</b><br>
{year}&nbsp;引用当前数据的年,&nbsp;&nbsp;
{month}&nbsp;引用当前数据的月,&nbsp;&nbsp;
{data:value}&nbsp;引用Y轴的值,&nbsp;&nbsp;
{data:sum}&nbsp;汇总列。
</div>
<div id="content" style="margin:8px;">
<asp:PlaceHolder ID="ControlGroup" runat="server">
<%if(row["controltype"].ToString()=="table"){ %>
<h1>表格属性</h1>
<div style="margin:6px 0px 6px 0px;">
名称：<input reload="false" class="text" type="text" value="<%= row["YName"].ToString() %>" style="width:100px;" oldvalue="<%= row["YName"].ToString() %>" onBlur="setReport(this,'yname');">
&nbsp;&nbsp;宽度：<input reload="false" class="text" type="text" value="<%= row["ywidth"].ToString() %>" style="width:80px;" oldvalue="<%= row["ywidth"].ToString() %>" onBlur="setReport(this,'ywidth');">
&nbsp;&nbsp;标题对齐：<select style="width:60px;" onchange="setReport(this,'ytitlealign');">
        <option value="left"<%= row["ytitlealign"].ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%= row["ytitlealign"].ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%= row["ytitlealign"].ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
&nbsp;&nbsp;内容对齐：<select style="width:60px;" onchange="setReport(this,'ybodyalign');">
        <option value="left"<%= row["ybodyalign"].ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%= row["ybodyalign"].ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%= row["ybodyalign"].ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
</div>
<%} %>
报表类型：<select style="width:110px;" onchange="setReport(this,'ControlType');">
      <%= eBase.getReportControlType(row["ControlType"].ToString())%>
    </select>
<input id="chkswap" style="vertical-align:middle;" type="checkbox" onclick="setReport(this,'swap');"<%= eBase.parseBool( row["swap"]) ? " checked=\"true\"" : "" %> /><label for="chkswap" style="vertical-align:middle;display:inline-block;margin-left:5px;margin-right:5px;">数据反转</label>

<h1>一维</h1>
<asp:Repeater id="RepY" runat="server" >
<headertemplate>
<table id="eDataTable_Itemsy" class="eDataTable" border="0" cellpadding="0" cellspacing="1" widt4h="100%" style="margin-top:6px;margin-bottom:6px;min-width:<%=(row["controltype"].ToString()=="table" ? "1220px" : "1020px")%>;">
<thead>
<tr bgcolor="#f2f2f2">
<td height="25" width="30" align="center"><a title="添加列" href="javascript:;" onclick="addReportItem(this,'Y');"><img width="16" height="16" src="images/add.png" border="0"></a></td>
<td width="40">显示</td>
<td width="120">名称</td>
<%if(row["controltype"].ToString()=="table"){ %>
<td width="60">宽度</td>
<td width="70">标题对齐</td>
<td width="70">内容对齐</td>
<%} %>
<td width="80">固定值</td>
<td width="300">取值</td>
<td width="300">取名称</td>
<td width="100">时间轴</td>
<td width="50">顺序</td>
</tr>
</thead>
<tbody eSize="false" eMove="true">
</headertemplate>
<itemtemplate>
<tr erowid="<%#Eval("ReportItemID")%>">
<td height="26" align="center">
<a title="删除列" href="javascript:;" onclick="delReportItem(this,'<%# Eval("ReportItemID") %>');"><img width="16" height="16" src="images/del.png" border="0"></a></td>
<td><input reload="false" type="checkbox" onclick="setReportItem(this,'<%# Eval("ReportItemID") %>','show');"<%# (eBase.parseBool(Eval("show"))? " checked" : "") %> /></td>
<td><input style="width:100px;" reload="false" class="text" type="text" value="<%# Eval("MC").ToString() %>" oldvalue="<%# Eval("MC").ToString() %>" onBlur="setReportItem(this,'<%#Eval("ReportItemID") %>','mc');"></td>
<%if(row["controltype"].ToString()=="table"){ %>
<td><input style="width:40px;" class="text" type="text" value="<%# Eval("width").ToString()%>" oldvalue="<%# Eval("width").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','width');"></td>
<td>
    <select style="width:60px;" onchange="setReportItem(this,'<%# Eval("ReportItemID") %>','titlealign');">
        <option value="left"<%# Eval("titlealign").ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%# Eval("titlealign").ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%# Eval("titlealign").ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
</td>
<td>
    <select style="width:60px;" onchange="setReportItem(this,'<%# Eval("ReportItemID") %>','bodyalign');">
        <option value="left"<%# Eval("bodyalign").ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%# Eval("bodyalign").ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%# Eval("bodyalign").ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
</td>
<%} %>
<td><input style="width:60px;" class="text" type="text" value="<%# Eval("value").ToString()%>" oldvalue="<%# Eval("value").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','value');"></td>
<td><input style="width:280px;" class="text" type="text" value="<%# Eval("valuesql").ToString()%>" oldvalue="<%# Eval("valuesql").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','valuesql');"></td>
<td><input style="width:280px;" class="text" type="text" value="<%# Eval("bindsql").ToString()%>" oldvalue="<%# Eval("bindsql").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','bindsql');"></td>
<td>
    <select reload="true" style="width:60px;" onchange="setReportItem(this,'<%# Eval("ReportItemID") %>','timetype');">
        <option value="">无</option>
        <option value="year"<%# Eval("timetype").ToString() == "year" ? " selected=\"true\"" : ""%>>年</option>
        <option value="quarter"<%# Eval("timetype").ToString() == "quarter" ? " selected=\"true\"" : ""%>>季度</option>
        <option value="month"<%# Eval("timetype").ToString() == "month" ? " selected=\"true\"" : ""%>>月</option>
        <option value="day"<%# Eval("timetype").ToString() == "day" ? " selected=\"true\"" : ""%>>日</option>
    </select><br />
    <span style="display:<%# Eval("timetype").ToString().Length == 0 ? "none" : "" %>;">
    起始年：<input class="text" type="text" style="width:35px;" value="<%# Eval("minyear").ToString()%>" oldvalue="<%# Eval("minyear").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','minyear');"><br />
    结束年：<input class="text" type="text" style="width:35px;" value="<%# Eval("maxyear").ToString()%>" oldvalue="<%# Eval("maxyear").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','maxyear');"><br />
    默认年：<input class="text" type="text" style="width:35px;" value="<%# Eval("defaultyear").ToString()%>" oldvalue="<%# Eval("defaultyear").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','defaultyear');"><br />
    默认月：<input class="text" type="text" style="width:35px;" value="<%# Eval("defaultmonth").ToString()%>" oldvalue="<%# Eval("defaultmonth").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','defaultmonth');"><br />
    </span>
</td>

<td style="cursor:move;"><%# (Container.ItemIndex + 1)%></td>
</tr>    
</itemtemplate>
<footertemplate></tbody></table></footertemplate>
</asp:Repeater>

<h1>二维</h1>
<asp:Repeater id="RepX" runat="server" >
<headertemplate>
<table id="eDataTable_Itemsx" class="eDataTable" border="0" cellpadding="0" cellspacing="1" wi4dth="100%" style="margin-top:6px;min-width:<%=(row["controltype"].ToString()=="table" ? "970px" : "770px")%>;">
<thead>
<tr bgcolor="#f2f2f2">
<td height="25" width="30" align="center"><a title="添加列" href="javascript:;" onclick="addReportItem(this,'X');"><img width="16" height="16" src="images/add.png" border="0"></a></td>
<td width="40">显示</td>
<td width="120">名称</td>
<%if(row["controltype"].ToString()=="table"){ %>
<td width="60">宽度</td>
<td width="70">标题对齐</td>
<td width="70">内容对齐</td>
<%} %>
<td width="80">固定值</td>
<td width="450">取值</td>
<td width="50">顺序</td>
</tr>
</thead>
<tbody eSize="false" eMove="true">
</headertemplate>
<itemtemplate>
<tr erowid="<%#Eval("ReportItemID")%>">
<td height="26" align="center">
<a title="删除列" href="javascript:;" onclick="delReportItem(this,'<%# Eval("ReportItemID") %>');"><img width="16" height="16" src="images/del.png" border="0"></a></td>
<td><input reload="false" type="checkbox" onclick="setReportItem(this,'<%# Eval("ReportItemID") %>    ','show');"<%# (eBase.parseBool( Eval("show")) ? " checked" : "") %> /></td>
<td><input style="width:100px;" reload="false" class="text" type="text" value="<%# Eval("MC").ToString() %>" oldvalue="<%# Eval("MC").ToString() %>" onBlur="setReportItem(this,'<%#Eval("ReportItemID") %>','mc');"></td>
<%if(row["controltype"].ToString()=="table"){ %>
<td><input style="width:40px;" class="text" type="text" value="<%# Eval("width").ToString()%>" oldvalue="<%# Eval("width").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','width');"></td>
<td>
    <select style="width:60px;" onchange="setReportItem(this,'<%# Eval("ReportItemID") %>','titlealign');">
        <option value="left"<%# Eval("titlealign").ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%# Eval("titlealign").ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%# Eval("titlealign").ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
</td>
<td>
    <select style="width:60px;" onchange="setReportItem(this,'<%# Eval("ReportItemID") %>','bodyalign');">
        <option value="left"<%# Eval("bodyalign").ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%# Eval("bodyalign").ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%# Eval("bodyalign").ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
</td>
<%} %>
<td><input style="width:60px;" class="text" type="text" value="<%# Eval("value").ToString()%>" oldvalue="<%# Eval("value").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','value');"></td>
<td><input style="width:430px;" class="text" type="text" value="<%# Eval("valuesql").ToString()%>" oldvalue="<%# Eval("valuesql").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','valuesql');"></td>
<td style="cursor:move;"><%# (Container.ItemIndex + 1)%></td>
</tr>    
</itemtemplate>
<footertemplate></tbody></table></footertemplate>
</asp:Repeater>
<h1>自定义输出：</h1>
<textarea name="textarea" onBlur="setReport(this,'CustomOut');" style="width:600px;"><%= HttpUtility.HtmlEncode(row["CustomOut"].ToString()) %></textarea> <br />
默认输出： <br /><%=eBase.getReportOut(row["ReportID"].ToString()).HtmlEncode().Replace("\r\n","<br>") %>
 <br />
<script>
    function viewReport()
    {
        var url="ReportItems.aspx?ReportID=" + ReportID;
        url= url.addquerystring("act","viewreport");
        $.ajax({
            type: 'get',
            url: url,
            dataType: "html",
            success: function(data)
            {
                $("#reportbody").html(data);
            }
        });
    };
</script>
<a class="button" href="javascript:;" onclick="viewReport();" style="margin-top:10px;"><span><i class="search">预览</i></span></a>
<div id="reportbody" style="box-shadow: 0 0 6px 0 #ccc;border-radius: 5px;padding:10px;margin-top:15px;max-width:600px;min-height:360px;"></div>
</asp:PlaceHolder>
</div>
</body>
</html>
