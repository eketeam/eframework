﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using LitJson;

namespace eFrameWork.Manage
{
    public partial class Config : System.Web.UI.Page
    {
        public eForm eform;
        private JsonData _jsondata;
        private JsonData JsonData
        {
            get
            {
                if (_jsondata == null)
                {
                    _jsondata = new JsonData();
                    string jsonstr = eBase.DataBase.getValue("select value from a_eke_sysConfigs where name='setup'");
                    _jsondata = jsonstr.ToJsonData();
                }
                return _jsondata;
            }
        }
        protected void eform_onChange(object sender, eFormTableEventArgs e)
        {
            if (e.eventType == eFormTableEventType.Updated)
            {
                eConfig.Clear();
                eRegisterInfo.Loaded = false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            eUser user = new eUser("Manage");
            eform = new eForm("a_eke_sysConfigs", user);
            eform.AddControl(eFormControlGroup);
            eform.onChange += eform_onChange;
            string act = eParameters.Form("act");
            if (act == "save")
            {
                #region 安全性判断
                string allowExtensions = F4.Value.ToString();
                foreach (string ext in allowExtensions.Split(".".ToCharArray()))
                {
                    if (ext.Length == 0) continue;
                    if (eConfig.PreventExtensions.Contains("." + ext))
                    {
                        eResult.Error("扩展名." + ext + "被禁止上传,修改失败!");
                    }
                }
                #endregion
                eform.LoadAction("save", "5de6ac55-fe63-42c0-8096-e12db9750e66");
            }
            else
            {
                eform.LoadAction("edit", "5de6ac55-fe63-42c0-8096-e12db9750e66");
            }

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {
                lit.Text = "系统配置 - " + eConfig.manageName();
            }
        }
    }
}