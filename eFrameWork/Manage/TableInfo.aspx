﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="TableInfo.aspx.cs" Inherits="eFrameWork.Manage.TableInfo" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
input.edit{font-size:12px;border:1px solid #ccc;height:18px;line-height:18px;width:300px;}
body,td{font-size:12px;}
div.item {border: 1px dashed #fff;padding:3px 0px 3px 10px;
}
div.item:hover {
    background-color:#f2f2f2;border:1px dashed #ccc;
}
span.close,span.open{line-height:20px;display:inline-block;padding-left:20px; cursor:pointer;border:0px solid #000;}
span.close{background:url(../images/close.gif) no-repeat scroll 3px 6px transparent;}
span.open{background:url(../images/open.gif) no-repeat scroll 3px 6px transparent;}
</style>
<script>
function show(id,obj)
{
        if(obj.className.toLowerCase()=="open")
        {
            obj.className="close";
            getobj("div_" + id).style.display="none";
        }
        else
        {
            obj.className="open";
            getobj("div_" + id).style.display="";
        }
};
function opensub(obj)
{
    var name = $(obj).attr("dataname");
    if (name.length == 0)
    {
        if (obj.className.toLowerCase() == "open")
        {
            obj.className = "close";
            $(obj).parent().next().hide();
        }
        else {
            obj.className = "open";
            $(obj).parent().next().show();
        }
    }
    else
    {
        var url = "?name=" + name + "&t=" + now();
        $.ajax({
            type: "GET", async: true,
            url: url,
            dataType: "html",
            success: function (html) {
                
                $(obj).parent().after(html);
                $(obj).attr("dataname", "");
                obj.className = "open";
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }
 
};
function setTableDescription(obj,tbname)
{
        var url = "?act=settabledescription&tbname=" + tbname + "&value=" + obj.value.toCode() + "&t=" + now();
        var html=PostURL(url);
        obj.previousSibling.style.color=(obj.value.length>0 ? "#000000" : "#ff0000");
};
function setColumnDescription(obj,tbname,column)
{
        var url = "?act=setcolumndescription&tbname=" + tbname + "&column=" + column + "&value=" + obj.value.toCode() + "&t=" + now();
        var html=PostURL(url);
        obj.previousSibling.style.color=(obj.value.length>0 ? "#000000" : "#ff0000");
};
function showAll(obj, show)
{
        var divs = obj.parentNode.getElementsByTagName("div");
        for (i = 0; i < divs.length; i++) {
            divs[i].style.display = show;
        }
};
function createModel(name)
{	
	layer.open({
          type: 2
          , title: "生成实体"
          , shadeClose: false //点击遮罩关闭层
          , area: [ "50%", "70%"]   
		  ,scrollbar: false    
          //, content: ["createModel.aspx","no"]
		  , content: "createModel.aspx" + (name.length>0 ? "?name=" + name : "")
		  , btn: ['关闭'] //只是为了演示
          , success: function (layero, index) {
		 
             // arrLayerIndex.push(index);
          }
		  ,yes: function(index,layero)
		  { 
			//arrLayerIndex.pop();
			layer.close(index);
		  }	
          ,cancel: function (index, layero) 
		  {
             // arrLayerIndex.pop();
			 //alert(4);
          }

        });
};
function createTable(name) {
    layer.open({
        type: 2
          , title: "生成脚本"
          , shadeClose: false //点击遮罩关闭层
          , area: ["50%", "70%"]
		  , scrollbar: false
        //, content: ["createModel.aspx","no"]
		  , content: "createTable.aspx" + (name.length > 0 ? "?name=" + name : "")
		  , btn: ['关闭'] //只是为了演示
          , success: function (layero, index) {

              // arrLayerIndex.push(index);
          }
		  , yes: function (index, layero) {
		      //arrLayerIndex.pop();
		      layer.close(index);
		  }
          , cancel: function (index, layero) {
              // arrLayerIndex.pop();
              //alert(4);
          }

    });
};
function selectRow(obj)
{
    var src = getEventObject();
    if (src.tagName == "DIV" || (src.tagName == "INPUT" && src.name.toLowerCase() == "selitem"))
    {
        if (src.tagName != "INPUT")
        {
            var chk = $(obj).find("input[name='selitem']")[0];
            chk.checked = !chk.checked;
        }
        select_callback();
    }
};
function select_callback()
{
    var ids = getSelectIDS();
    if (ids.length < 5) {
        $("#expdata").hide();
    }
    else {
        $("#expdata").show();
    }
};
function expTables()
{
    var ids = getSelectIDS();    
    var $form = $('<form id="downfileform" target="_blank" method="post" action="ExportConfig.aspx" />');
    $form.append('<input type="hidden" name="act" value="exptable" />');
    $form.append('<input type="hidden" name="tables" value="' + ids + '" />');
    $(document.body).append($form);
    $form[0].submit();
    $form.remove();
};
</script>
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 数据结构
    <a id="expdata" style="display:none;" class="button" href="javascript:;" onclick="expTables();"><span><i class="export">导出数据</i></span></a>
    <a class="button" href="TableInfo_Export.aspx" target="_blank"><span><i class="export">导出结构信息</i></span></a>
    <a class="button" href="javascript:;" onclick="createModel('');"><span style="letter-spacing:0px;">生成实体</span></a>
    <a class="button" href="javascript:;" onclick="createTable('');"><span style="letter-spacing:0px;">生成脚本</span></a>
    <a class="button" href="?act=createschema" title="upload/temp/database_schema.xml"><span style="letter-spacing:0px;">生成框架结构</span></a>
    <!-- <a class="button" href="?act=delalltable" onclick="return confirm('删除所有表且不可恢复，确认要删除吗？');"><span style="letter-spacing:0px;">删除所有表</span></a> -->

</div>
 <%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
       <div style="margin:6px;">
<div class="tips" style="margin-bottom:6px;"><b>提示</b><br>备份和还原数据库，当前仅支持SQLServer数据库。</div>
<asp:Literal ID="LitBody" runat="server" />
        </div>
</asp:Content>