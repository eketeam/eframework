﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Xml;
using System.Xml.Serialization;

namespace eFrameWork.Manage
{
    public partial class setModel : System.Web.UI.Page
    {
        public eList elist;
        public eUser user;
        private DataTable _modelids;
        public DataTable ModelIDS
        {
            get
            {
                if (_modelids == null)
                {
                    _modelids = eBase.DataBase.getDataTable("select ModelID,ParentID from a_eke_sysModels where delTag=0");
                }
                return _modelids;
            }
        }
        public string getLabels(string ids)
        {
            if (ids.Length == 0) return "";
            string sql = "select mc from a_eke_sysLabels where LableID in ('" + ids.Replace(",","','") + "')";
            DataTable tb = eBase.DataBase.getDataTable(sql);
            string text = "";
            for (int i = 0; i < tb.Rows.Count; i++)
            {
                if(i>0) text+="、";
                text += tb.Rows[i]["mc"].ToString();
            }
            return text;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            List();
        }

        private void List()
        {
            elist = new eList("a_eke_sysModels");
            //elist.Fields.Add("*");
            elist.Fields.Add("ModelID,MC,Type,Auto,AspxFile,LabelIDS,SM");
            elist.Fields.Add("CASE WHEN Finsh=1 THEN 'images/sw_true.gif' ELSE 'images/sw_false.gif' END as ShowPIC,CASE WHEN Finsh=1 THEN '0' ELSE '1' END as ShowValue");
            elist.Where.Add("delTag=0 and type in (1,4,5,10,11) and subModel=0");
            elist.Where.Add("ServiceID" + (user["ServiceID"].Length == 0 ? " is null" : "='" + user["ServiceID"] + "'"));

            elist.Where.Add(eSearchControlGroup);
            elist.OrderBy.Add("addTime desc");
            elist.Bind(Rep, ePageControl1);
        }
        public string getids(string modelid)
        {
            DataRow[] rows = ModelIDS.Select("Convert(ParentID, 'System.String')='" + modelid + "'");
            if (rows.Length == 0)
            {
                return modelid;
            }
            else
            {
                foreach (DataRow dr in rows)
                {
                    modelid += "," + getids(dr["modelid"].ToString());
                }
            }
            return modelid;
        }
    }
}