﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelItems_FillData.aspx.cs" Inherits="eFrameWork.Manage.ModelItems_FillData" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title><%=eConfig.manageName() %></title>
       <link href="../Plugins/eControls/default/style.css?ver=1.0.4" rel="stylesheet" type="text/css" />   
    <link href="../Plugins/Theme/default/style.css?ver=1.0.4" rel="stylesheet" type="text/css" />   
</head>
<body>
<div style="margin:10px;">
<asp:Repeater id="Rep" runat="server" >
<headertemplate>
<%#
"<table id=\"eDataTable\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" width=\"540\">" +
"<thead>" +
"<tr>" +
"<td height=\"25\" width=\"120\">模块</td>"+
"<td width=\"180\">字段" +  (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(154);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"280\">回填字段" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(155);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">唯一性</td>"+
"</tr>" +
"</thead>"
%>
</headertemplate>
<itemtemplate>
<%#
"<tr>" +
"<td height=\"26\">" + Eval("ModelName").ToString() + "</td>" +
"<td>" + Eval("code").ToString() + " (" + Eval("mc").ToString() + ")</td>" +
"<td>"
%>
<select onChange="parent.setModelItem(this,'<%# Eval("ModelItemID")%>','FillModelItemID');" oldvalue="<%#Eval("FillModelItemID") %>" style="width:270px;">
	<option value="NULL">无</option>
	<asp:Literal id="LitColumns" runat="server" />
</select>
<%#
"</td>" +
"<td>" + 
(Eval("ModelID").ToString()==parentid ? "<input type=\"checkbox\" onclick=\"parent.setModelItem(this,'" + Eval("ModelItemID") + "','FillSingle');\"" + ( Eval("FillSingle").ToString()=="1" ? " checked" : "") + ">" : "&nbsp;")+
"</td>" +
"</tr>"
%>
</itemtemplate>
<footertemplate><%#"</table>"%></footertemplate>
</asp:Repeater>
</div>
</body>
</html>