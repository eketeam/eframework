﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using EKETEAM.UserControl;


namespace eFrameWork.Manage
{
    public partial class Cache : System.Web.UI.Page
    {
        public string act = eParameters.QueryString("act");
        public string name = eParameters.QueryString("name");
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Web.Caching.Cache cache = HttpRuntime.Cache;            
            IDictionaryEnumerator CacheEnum = cache.GetEnumerator();
            
            #region 删除
            if (act == "del")
            {
                if (name == "all")
                {
                    while (CacheEnum.MoveNext())
                    {
                        if (CacheEnum.Key.ToString().ToLower().IndexOf("system.web.webpages.deployment_") > -1) continue;
                        if (CacheEnum.Key.ToString().ToLower().IndexOf("token") > -1) continue;
                        if (",ticket,message_accesstoken,rewriterconfig,".IndexOf("," + CacheEnum.Key.ToString().ToLower() + ",") > -1) continue;
                        cache.Remove(CacheEnum.Key.ToString());
                    }

                    HttpRuntime.Cache.Remove("PageCache");
                    eBase.DataBase.removePrimaryKeys();

                    eBase.clearDataCache();
                   
                }
                else
                {
                    HttpRuntime.Cache.Remove(name);
                }
                runtimeCache.Remove();
                Response.Redirect("Cache.aspx", true);
            }
            #endregion
            StringBuilder sb = new StringBuilder();           
            while (CacheEnum.MoveNext())
            {
                if (CacheEnum.Key.ToString().ToLower().IndexOf("system.web.webpages.deployment_") > -1) continue;
                if (",ticket,message_accesstoken,rewriterconfig,".IndexOf("," + CacheEnum.Key.ToString().ToLower() + ",") > -1) continue;

                sb.Append("<span style=\"display:inline-block;width:350px;\">" + CacheEnum.Key.ToString() + "</span><a href=\"?act=del&name=" + CacheEnum.Key.ToString() + "\">移除</a>&nbsp;&nbsp;<a href=\"?act=view&name=" + CacheEnum.Key.ToString() + "\" target=\"_blank\">查看</a><br>");
                if (act == "view" && CacheEnum.Key.ToString().ToLower()==name.ToLower())
                {
                    object obj = HttpRuntime.Cache[name];

                    eBase.Write("<div>" + name + "：<br>");


                    if (obj.GetType() == typeof(DataSet))
                    {
                        DataSet ds=obj as DataSet;

                       // eBase.Writeln(ds.Tables.Count.ToString());
                        string tbname = eParameters.QueryString("tbname");
                        if (tbname.Length == 0)
                        {
                            foreach (DataTable dt in ds.Tables)
                            {
                                eBase.Writeln("<a href=\"?act=view&name=" + name + "&tbname=" + dt.TableName  + "\">" + dt.TableName + "</a>:::" + dt.Rows.Count.ToString());
                            }
                        }
                        else
                        {
                            DataTable dt=ds.Tables[tbname];
                            eBase.Writeln(dt.TableName + ":::" + dt.Rows.Count.ToString());
                             eBase.PrintDataTable(dt);
                            /*
                            foreach (DataTable dt in ds.Tables)
                            {
                                eBase.Writeln(dt.TableName + ":::" + dt.Rows.Count.ToString());
                                eBase.PrintDataTable(dt);
                            }
                            */
                        }
                    }
                    if (obj.GetType().ToString() == "System.String") eBase.Write(obj.ToString());
                    if (obj.GetType().ToString() == "System.Data.DataTable")
                    {

                        //eBase.PrintDataTable((DataTable)obj);
                        DataTable dt = obj as DataTable;
                        eBase.Write("<link href=\"../Plugins/eControls/default/style.css?ver=1.0.53\" rel=\"stylesheet\" type=\"text/css\" />\r\n");

                      

                        ePageControl ePage = new ePageControl();
                        ePage.PageSize = 25;
                        //ePage.showPageSize = true;
                        ePage.RecordsCount = dt.Rows.Count;

                        DataTable tb = dt.Clone();

                        int start = ePage.PageSize * (ePage.Page - 1);
                        int end = start + ePage.PageSize;
                        for (int i = start; i < end && i < dt.Rows.Count; i++)
                        {
                            tb.Rows.Add(dt.Rows[i].ItemArray);
                        }
                        eBase.PrintDataTable(tb);
                        eBase.Writeln("<div style=\"margin:10px 0px 10px 0px;\">" + ePage.getControlHTML() + "</div>");                        
                    }
                    if (obj.GetType().ToString() == "System.Collections.Generic.Dictionary`2[System.String,System.String]")
                    {
                        Dictionary<string, string> dirs = (Dictionary<string, string>)obj;//new Dictionary<string,string>(StringComparer.OrdinalIgnoreCase);
                        foreach (string key in dirs.Keys)
                        {
                            eBase.Write("<hr>");
                            eBase.Write(key + "<br>");
                            eBase.WriteHTML(dirs[key].ToString());
                        }
                    }
                    if (obj.GetType() == typeof(eDictionary))
                    {
                        eDictionary dirs = obj as eDictionary;
                        foreach (string key in dirs.Keys)
                        {
                            eBase.Write("<hr>");
                            eBase.Write(key + "<br>");
                            eBase.WriteHTML(dirs[key].ToString());
                        }
                    }
                    //eBase.Write(obj.GetType().ToString());
                    eBase.Write("</div>");
                    eBase.End();
                }
            }
            if (HttpRuntime.Cache.Count > 1)
            {
                sb.Append("<a href=\"?act=del&name=all\">移除所有</a><br>");
            }
            if (HttpRuntime.Cache.Count == 0)
            {
                sb.Append("<a>暂无缓存</a>");
            }
            LitBody.Text = sb.ToString();
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {
                lit.Text = "缓存管理 - " + eConfig.manageName();
            }
        }
    }
}