﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelItems_Report.aspx.cs" Inherits="eFrameWork.Manage.ModelItems_Report" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title><%=eConfig.manageName() %></title>
</head>
<body>
<asp:PlaceHolder ID="ControlGroup" runat="server">
<style>span.titleitem {display:inline-block;width:100px;text-align:right;line-height:28px; }
</style>
<asp:Repeater id="Rep" runat="server" >
<headertemplate>
<table id="eDataTable_Report" class="eDataTable" border="0" cellpadding="0" cellspacing="1" width="99%" style="margin-bottom:8px;">
<thead>
<tr>
<td height="25" width="35" align="center"><a title="添加报表" href="javascript:;" onclick="addReport(this);"><img width="16" height="16" src="images/add.png" border="0"></a></td>
<td width="40">显示</td>
<td width="100">名称</td>
<td width="90">报表类型</td>
<td width="90">数据源</td>
<td width="70">客户端ID</td>
<td width="60">报表宽</td>
<td width="60">报表高</td>
<td width="80">报表高(M)</td>
<td width="100">报表标题</td>
<td width="180">简介</td>
<td width="60">数据反转</td>
<td width="60">定时刷新</td>
<td width="60">数据</td>
<td>备注</td>
</tr>
</thead>
<tbody esize="false" emove="true">
</headertemplate>
<itemtemplate>
<tr erowid="<%# Eval("ReportID")%>">
<td height="26" align="center"><a title="删除报表" href="javascript:;" onclick="delReport(this,'<%# Eval("ReportID")%>');"><img width="16" height="16" src="images/del.png" border="0"></a></td>
    <td><input reload="false" type="checkbox" onclick="setReport(this,'<%# Eval("ReportID") %>','show');"<%# (eBase.parseBool(Eval("show"))? " checked" : "") %> /></td>
<td><input class="text" type="text" value="<%# Eval("MC")%>" oldvalue="<%# Eval("MC")%>" onBlur="setReport(this,'<%# Eval("ReportID")%>','mc');"></td>
<td>
<select onchange="setReport(this,'<%#Eval("ReportID") %>','controltype');">
<%# eBase.getReportControlType(Eval("controltype").ToString()) %>
</select>
</td>
<td>
<select onChange="setReport(this,'<%#Eval("ReportID") %>','DataSourceID');" style="width:80px;">
 <option value="NULL">主库</option>
<%# 
eBase.DataBase.getOptions("SELECT DataSourceID as value,MC as text FROM a_eke_sysDataSources where delTag=0 order by addTime", "text", "value", Eval("DataSourceID").ToString())
%>
<select>
</td>
<td><input class="text" type="text" value="<%# eBase.getReportClientID( Eval("ReportID").ToString())%>" oldvalue="<%# Eval("ClientID")%>" onBlur="setReport(this,'<%# Eval("ReportID")%>','ClientID');"></td>
<td><input class="text" type="text" value="<%# Eval("width")%>" oldvalue="<%# Eval("width")%>" onBlur="setReport(this,'<%# Eval("ReportID")%>','width');"></td>
<td><input class="text" type="text" value="<%# Eval("height")%>" oldvalue="<%# Eval("height")%>" onBlur="setReport(this,'<%# Eval("ReportID")%>','height');"></td>
    <td><input class="text" type="text" value="<%# Eval("mheight")%>" oldvalue="<%# Eval("mheight")%>" onBlur="setReport(this,'<%# Eval("ReportID")%>','mheight');"></td>
<td><input class="text" type="text" value="<%# Eval("title")%>" oldvalue="<%# Eval("title")%>" onBlur="setReport(this,'<%# Eval("ReportID")%>','title');"></td>
<td><textarea oldvalue="<%# Eval("introduce")%>" style="width:90%;" rows="1" onBlur="setReport(this,'<%# Eval("ReportID")%>','introduce');"><%# Eval("introduce")%></textarea></td>
<td><input type="checkbox" onclick="setReport(this,'<%# Eval("ReportID")%>','swap');"<%#eBase.parseBool(Eval("Swap").ToString()) ? " checked" : "" %> /></td>
<td><input class="text" type="text" value="<%# Eval("interval")%>" oldvalue="<%# Eval("interval")%>" onBlur="setReport(this,'<%# Eval("ReportID")%>','interval');" style="width:30px;"> s</td>
<td><a href="javascript:;" onclick="reportdlg(this,'<%=modelid%>','<%# Eval("ReportID")%>');">数据</a></td>
<td><textarea oldvalue="<%# Eval("SM")%>" style="width:90%;" rows="1" onBlur="setReport(this,'<%# Eval("ReportID")%>','sm');"><%# Eval("SM")%></textarea></td>
</tr>
</itemtemplate>
<footertemplate></tbody></table></footertemplate>
</asp:Repeater>




<a href="javascript:;" title="添加报表" onclick="addReport(this);" style="_display:inline-block;display:none;margin-bottom:10px;width:30px;height:30px;font-size:1px;background:url(images/btn_add.png) center center no-repeat;background-size:100% 100%;">&nbsp;</a>
<div style="position:relative;display:none;">
<asp:Repeater id="RepX" runat="server" >
<itemtemplate>
<dl class="ePanel" erowid="<%#Eval("ReportID")%>">
<dt><h1 oncl3ick="showPanel(this);"><a href="javascript:;" class="cur" onfocus="this.blur();"></a><span><%# Eval("MC").ToString() %></span></h1></dt>
<dd style="displ3ay:none;">
<div>
<a title="删除报表" href="javascript:;" onclick="delReport(this,'<%# Eval("ReportID") %>');"><img width="16" height="16" src="images/del.png" border="0"></a>
</div>
<span class="titleitem">名称：</span><input reload="false" class="text" type="text" value="<%# Eval("MC").ToString() %>" oldvalue="<%# Eval("MC").ToString() %>" onkeyup="setReportMC(this);" style="width:160px;" onBlur="setReport(this,'<%#Eval("ReportID") %>','mc');"><br />
<span class="titleitem">数据源：</span><select onchange="setReport(this,'<%#Eval("ReportID") %>','datasourceid');">
    <option value="NULL">主库</option>
    <%# eBase.DataBase.getOptions("select DataSourceID,MC from a_eke_sysDataSources where delTag=0 order by addTime desc","mc","DataSourceID",Eval("DataSourceID").ToString()) %>
    </select><br />
<span class="titleitem">控件类型：</span><select onchange="setReport(this,'<%#Eval("ReportID") %>','controltype');">
<%# eBase.getReportControlType(Eval("controltype").ToString()) %>
</select><br />
<span class="titleitem">说明：</span><textarea  oldvalue="<%# Eval("SM").ToString() %>" onBlur="setReport(this,'<%#Eval("ReportID") %>','sm');"><%# Eval("sm").ToString() %></textarea><br />
<a href="ReportItems.aspx?ReportID=<%#Eval("ReportID") %>" target="_blank">详细设置</a>
</dd>
</dl>
</itemtemplate>
</asp:Repeater>
</div>
<script type="text/javascript">
    function end(obj) {
        var ids = "";
        $(".ePanel").each(function (index, node) {

            if (index > 0) { ids += ","; }
            ids += $(node).attr("erowid");
        });      
        if (ids.length == 0) { return; }
        showloading();
        var url = "?act=setreportorders&ModelID=" + ModelID + "&t=" + now();
        $.ajax({
            type: "POST", async: true,
            data: { ids: ids },
            url: url,
            dataType: "html",
            success: function (data) {
                hideloading();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                hideloading();
            }
        });

    };
    $(function () {
        $('.ePanel').arrangeable({dragSelector: 'h1', dragEnd: end }); 
    });
</script>

</asp:PlaceHolder>
</body>
</html>
