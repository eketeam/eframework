﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="ModelItems.aspx.cs" Inherits="eFrameWork.Manage.ModelItems" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
  <div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 模块管理&nbsp;&nbsp;
      <label><input name="chkdisabled" type="checkbox" value="0"<%=(ReadOnly ? " checked=\"checked\"" : "") %> onclick="chkdisabled_click(this);">只读</label>
  </div>
  <script>var ModelID = "<%=ModelID%>";</script>
  <script src="javascript/columns.js?ver=<%=Common.Version %>"></script>
  <script>
 function createStep(modelid,title)
 {
 //alert(modelid);
 
 var html='<div style="margin:15px;line-height:30px;">';
	html+='<input name="copyname" type="text" id="copyname" style="border:1px solid #ccc;height:20px;line-height:20px;font-size:13px;color:#333;width:230px;" value="' + title +'" /><br>';

	html+='</div>';
	layer.open({
		type: 1, //此处以iframe举例
		title: '添加编辑步骤', // + "窗口"
		//skin: 'layui-layer-molv' //加上边框 layui-layer-rim  layui-layer-lan layui-layer-molv layer-ext-moon
		shadeClose: true, //点击遮罩关闭层
		area: ["300px","180px"],
		btnAlign: 'l', //lcr
		moveType: 0, //拖拽模式，0或者1
		content: html,
		btn: ['确认', '取消'], 
		yes: function(index,layero)
		{
			var name=$("#copyname").val();
			var url="Models.aspx?ID=" + modelid + "&act=createstep";
			if(name.length>0){url+="&name=" + name.encode();}			
			layer.close(index);
			//alert(url);
			document.location.href=url;
		}
	});
 
 
 };
 function openFillwin(parentid,modelid)
 {
     var url = "ModelItems_FillData.aspx?parentid=" + parentid + "&modelid=" + modelid;
     layer.open({
         type: 2,
         title: "对应关系",
         maxmin: false,
         shadeClose: true, //点击遮罩关闭层
         area: ["750px", "450px"],
         // content: [url,'no'], 
         content: url,
         success: function (layero, index) { },
         cancel: function (index, layero) { },
         end: function (index) { }
     });
 };
 function Model_Copy(modelid) {
     var url = "ModelCopy.aspx?single=true&modelid=" + modelid;
     layer.open({
         type: 2,
         title: "复制模块",
         maxmin: false,
         shadeClose: true, //点击遮罩关闭层
         area: ["650px", "300px"],
         // content: [url,'no'], 
         content: url,
         success: function (layero, index) { },
         cancel: function (index, layero) { },
         end: function (index) { document.location.assign(document.location.href); }
     });
 };
  </script>
  <style>
t3d{-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;}
input.edit{ text-indent:5px;border:1px solid #ccc;font-size:12px;height:23px;line-height:23px;}
textarea {border:1px solid #ccc;font-size:12px;line-height:20px;padding-left:5px;}
.text{text-indent:5px;display:inline-block;width:100%;border:1px solid #ccc;font-size:12px;height:23px;line-height:23px;}

#eDataTable .edit{text-indent:5px;background-color:transparent;border:0px solid #ccc;height:26px;width:100%;font-size:12px;padding:0px; over4flow:hidden; background-color:#f2f2f2;}
.divloading{filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity: 0.5; opacity: 0.5; position:fixed;width:100%;height:100%;top:0px;left:0px;background:#cccccc url(images/loading.gif) no-repeat center center;}
</style>
  <div id="divloading" class="divloading" style="display:none;">&nbsp;</div>
  <div style="margin:6px;margin-top:0px;border:0px solid #ff0000;">
    <div style="margin-bottom:10px;">
      <asp:Literal ID="LitMenu" runat="server" />
    </div>
    <dl class="eTab" style="min-width:1100px;">
      <dt><%=titles %></dt>
      <dd><%=bodys %></dd>
    </dl>
  </div>
</asp:Content>
