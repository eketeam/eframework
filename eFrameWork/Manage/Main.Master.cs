﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;

namespace eFrameWork.Manage
{
    public partial class Main : System.Web.UI.MasterPage
    {
        public eUser user;
        private string _top_fixed;
        public bool TopFixed
        {
            get
            {
                if (_top_fixed == null)
                {
                    string sname = "Manage_TopFixed";
                    if (Session[sname] == null)
                    {
                        string tmp = eBase.DataBase.getValue("select parValue from a_eke_sysUserCustoms where ParName='manage_top_fixed' and UserID='" + user.ID + "'");
                        if (tmp.Length == 0) tmp = "true";
                        Session[sname] = tmp;
                        _top_fixed = tmp;
                    }
                    else
                    {
                        _top_fixed = Session[sname].ToString();
                    }
                }
                return eBase.parseBool(_top_fixed);
            }
        }
        private string _left_fixed;
        public bool LeftFixed
        {
            get
            {
                if (_left_fixed == null)
                {
                    string sname = "Manage_LeftFixed";
                    if (Session[sname] == null)
                    {
                        string tmp = eBase.DataBase.getValue("select parValue from a_eke_sysUserCustoms where ParName='manage_left_fixed' and UserID='" + user.ID + "'");
                        if (tmp.Length == 0) tmp = "true";
                        Session[sname] = tmp;
                        _left_fixed = tmp;
                    }
                    else
                    {
                        _left_fixed = Session[sname].ToString();
                    }
                }
                return eBase.parseBool(_left_fixed);
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            user = new eUser("Manage");//Manage为设定的登录区域
            user.Check();//检测用户是否登录,未登录则跳转到登录页
        }
        protected void Page_Load(object sender, EventArgs e)
        {          
         
        }
    }
}