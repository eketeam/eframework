﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelItems_Action.aspx.cs" Inherits="eFrameWork.Manage.ModelItems_Action" %>
<asp:Repeater id="Rep" runat="server" >
<headertemplate>
<%#
"<table id=\"eDataTable\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" width=\"99%\" style=\"margin-bottom:8px;\">" +
"<thead>" +
"<tr>" +
"<td height=\"25\" width=\"35\" align=\"center\"><a title=\"添加动作\" href=\"javascript:;\" onclick=\"addAction(this);\"><img width=\"16\" height=\"16\" src=\"images/add.png\" border=\"0\"></a></td>" +
"<td width=\"50\">启用</td>" +
"<td width=\"105\">名称" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(124);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"105\">编码" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(125);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"105\">步骤</td>" +
"<td width=\"105\">数据源</td>" +
"<td>执行SQL" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(126);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"</tr>" +
"</thead>"
%>
</headertemplate>
<itemtemplate>
<%#
"<tr>" +
"<td height=\"26\" align=\"center\"><a title=\"删除动作\" href=\"javascript:;\" onclick=\"delAction(this,'" + Eval("ActionID") + "');\"><img width=\"16\" height=\"16\" src=\"images/del.png\" border=\"0\"></a></td>" +
"<td  class=\"tdshowexport\"><input reloadbak=\"true\" type=\"checkbox\" onclick=\"setAction(this,'" + Eval("ActionID") + "','isOpen');\"" + (eBase.parseBool(Eval("isOpen")) ? " checked" : "") + " /></td>" +
"<td><input class=\"text\" type=\"text\" value=\"" + Eval("MC").ToString() + "\" oldvalue=\"" + Eval("MC").ToString() + "\" onBlur=\"setAction(this,'" + Eval("ActionID") + "','mc');\"></td>" +
"<td><input class=\"text\" type=\"text\" value=\"" + Eval("Action").ToString() + "\" oldvalue=\"" + Eval("Action").ToString() + "\" onBlur=\"setAction(this,'" + Eval("ActionID") + "','action');\"></td>" +
"<td>" +
"<select onChange=\"setAction(this,'" + Eval("ActionID") + "','stepmodelid');\" style=\"width:90px;\">"+
"<option value=\"NULL\">无</option>" +
 eBase.DataBase.getOptions("select MC,ModelID from a_eke_sysModels where delTag=0 and Type=8 and ParentID='" + modelid + "'", "MC", "ModelID", Eval("StepModelID").ToString()) + 
"<select>"+
"</td>"+
"<td>"+
"<select onChange=\"setAction(this,'" + Eval("ActionID") + "','DataSourceID');\" style=\"width:90px;\">"+
"<option value=\"\">无</option>"+
"<option value=\"maindb\"" + (Eval("DataSourceID").ToString()=="maindb" ? " selected=\"true\"" : "") + ">主库</option>"+
eBase.DataBase.getOptions("SELECT DataSourceID as value,MC as text FROM a_eke_sysDataSources where delTag=0 order by addTime", "text", "value", Eval("DataSourceID").ToString())+
"<select>"+
"</td>"+
"<td><textarea oldvalue=\""+ Eval("SQL").ToString().HtmlEncode() + "\" style=\"width:90%;\" rows=\"2\" onBlur=\"setAction(this,'" + Eval("ActionID") + "','sql');\">" + Eval("SQL").ToString().HtmlEncode() + "</textarea></td>" +
"</tr>"
%>
</itemtemplate>
<footertemplate><%#"</table>"%></footertemplate>
</asp:Repeater>