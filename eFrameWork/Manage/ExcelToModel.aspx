﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExcelToModel.aspx.cs" Inherits="Manage_ExcelToModel" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Excel生成模块</title>
    <script src="../Scripts/init.js?ver=<%=Common.Version %>"></script>
    <script src="../Plugins/layui226/layui.all.js"></script>
    <link href="../Plugins/eControls/default/style.css" rel="stylesheet" type="text/css" />   
<link href="../Plugins/Theme/manage/style.css" rel="stylesheet" type="text/css" />   
    <style>
        input.edittext {
        border:none; background-col3or:#f1f1f1;display:inline-block;min-width:30px;width:98%;
        }
        select {width:98%;
        }
    </style>
</head>
<script>
    function submitform() {
        if ($("input:checked").length == 0) {
            layer.msg("请选择数据!");
            return;
        }
        form1.submit();
    };
    function show(obj, idx) {
        if (obj.checked) {
            $(".box" + idx).show();
        }
        else {
            $(".box" + idx).hide();
        }
    };
    function checkfrm(frm) {
        if (frm.imgFile.value.length == 0) {
            top.layer.msg("请选择要上传的文件!");
            frm.imgFile.focus();
            return false;
        }
        return true;
    }
</script>
<body>
<asp:Literal id="litBody" runat="server" />
 <%if(file.Length ==0){ %>
<form method="post" enctype="multipart/form-data" id="Form1" onSubmit="return checkfrm(this);">
    <p style="margin-top:8px;">
    <INPUT type="file" id="imgFile" name="imgFile" style="width:160px;overflow:hidden;vertical-align:middle;" onchange="autoupload_check(this);" allowexts="<%=allowExts.ToString()%>" preventexts="<%=eConfig.PreventExtensions.ToString()%>" accept="<%=allowExts.ToString(",")%>" />&nbsp;<INPUT name="button" type="submit" id="button" value=" 导 入 ">
	<input name="act" type="hidden" id="act" value="save">
    </p>
</form>
<%}else{ %>

<%} %>
</body>
</html>