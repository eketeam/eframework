﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;

namespace eFrameWork.Manage
{
    public partial class ModelItems_List : System.Web.UI.Page
    {
        public string modelid = eParameters.QueryString("modelid");
        private DataRow _modelinfo;
        public DataRow ModelInfo
        {
            get
            {
                if (_modelinfo == null)
                {
                    //DataTable dt = eBase.DataBase.getDataTable("select * from a_eke_sysModels where ModelID='" + modelid + "'");
                    DataTable dt = eBase.DataBase.getDataTable("select b.Type as bType, a.* from a_eke_sysModels a left join a_eke_sysModels b on a.ParentID=b.ModelID where a.ModelID='" + modelid + "'");
                    if (dt.Rows.Count > 0) _modelinfo = dt.Rows[0];
                }
                return _modelinfo;
            }
        }
        private JsonData _propertys;
        public JsonData Propertys
        {
            get
            {
                if (_propertys == null)
                {
                    _propertys = JsonMapper.ToObject("{}");
                    if (ModelInfo.Table.Columns.Contains("Propertys") && ModelInfo["Propertys"].ToString().Length > 2 && ModelInfo["Propertys"].ToString().StartsWith("{"))
                    {
                        _propertys = JsonMapper.ToObject(ModelInfo["Propertys"].ToString());
                    }
                }
                return _propertys;
            }
        }
        public string getJsonText(string jsonstr, string name)
        {
            StringBuilder sb = new StringBuilder();
            if (jsonstr.Length > 0 && jsonstr.StartsWith("["))
            {

                JsonData data = JsonMapper.ToObject(jsonstr);
                foreach (JsonData jd in data)
                {
                    //eBase.Writeln(jd.getValue("value"));
                    sb.Append("<span style=\"display:inline-block;margin-right:6px;border:1px solid #ccc;padding:3px 12px 3px 12px;\">" + HttpUtility.HtmlDecode(jd.getValue(name)) + "</span>");
                }
                /*
                eJson json = new eJson(jsonstr);
                foreach (eJson m in json.GetCollection())
                {
                    sb.Append("<span style=\"display:inline-block;margin-right:6px;border:1px solid #ccc;padding:3px 12px 3px 12px;\">" + HttpUtility.HtmlDecode(m.GetValue(name)) + "</span>");
                }
                */
            }
            return sb.ToString();
        }
        public string getPropertys(object item, string name)
        {

            DataRowView row = (DataRowView)item;
            if (row.DataView.Table.Columns.Contains("Propertys"))
            {
                if (row["Propertys"].ToString().Length > 0 && row["Propertys"].ToString().StartsWith("{"))
                {
                    JsonData jd = JsonMapper.ToObject(row["Propertys"].ToString());
                    if (jd.Contains(name))
                    {
                        return jd.getValue(name);
                    }
                }
            }
            return "";
        }
        private eUser user;
        private int modelpx = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            if (ModelInfo == null)
            {
                Response.Write("模块不存在!");
                Response.End();
            }
            Response.Write("<a href=\"http://frame.eketeam.com\" style=\"float:right;\" target=\"_blank\" title=\"eFrameWork开发框架\"><img src=\"images/help.gif\"></a>");
            
            if (eConfig.showHelp())
            {
                Response.Write("<div class=\"tips\" style=\"margin-bottom:6px;\">");
                Response.Write("<b>列表</b><br>");
                Response.Write("设置列表相关参数；显示列是否显示、显示顺序、显示宽度、高度、点击排序、自定义显示等相关参数。");
                Response.Write("</div> ");
            }

            if (ModelInfo["Type"].ToString() != "10_" && ModelInfo["Type"].ToString() != "11")
            {
                Response.Write("窗口宽度：");
                Response.Write("<input type=\"text\" value=\"" + ModelInfo["width"].ToString() + "\" oldvalue=\"" + ModelInfo["width"].ToString() + "\"  class=\"edit\" style=\"width:50px;\" onBlur=\"setModel(this,'width');\" />");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(84);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("&nbsp;窗口高度：");
                Response.Write("<input type=\"text\" value=\"" + ModelInfo["height"].ToString() + "\" oldvalue=\"" + ModelInfo["height"].ToString() + "\"  class=\"edit\" style=\"width:50px;\" onBlur=\"setModel(this,'height');\" />");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(85);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
            }

            if (ModelInfo["bType"].ToString() == "1" && eBase.parseBool(ModelInfo["JoinMore"]) && ModelInfo["Type"].ToString() != "11")
            {
                //Response.Write("外键：");
                //Response.Write("<input type=\"text\" value=\"" + ModelInfo["Foreignkey"].ToString() + "\" oldvalue=\"" + ModelInfo["Foreignkey"].ToString() + "\"  class=\"edit\" style=\"width:90px;\" onBlur=\"setModel(this,'foreignkey');\" />");


                Response.Write("&nbsp;最少行数：");
                Response.Write("<input type=\"text\" value=\"" + ModelInfo["minCount"].ToString() + "\" oldvalue=\"" + ModelInfo["minCount"].ToString() + "\"  class=\"edit\" style=\"width:90px;\" onBlur=\"setModel(this,'mincount');\" />");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(86);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("&nbsp;最多行数：");
                Response.Write("<input type=\"text\" value=\"" + ModelInfo["maxCount"].ToString() + "\" oldvalue=\"" + ModelInfo["maxCount"].ToString() + "\"  class=\"edit\" style=\"width:90px;\" onBlur=\"setModel(this,'maxcount');\" />");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(87);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("<br>");
            }


            if (ModelInfo["Type"].ToString() != "11")
            {
                Response.Write("<label><input type=\"checkbox\" onclick=\"setModel(this,'customheight');\"" + (eBase.parseBool(ModelInfo["customheight"]) ? " checked" : "") + " />拖动行高</label>");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(88);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");

                //Response.Write("<input type=\"checkbox\" name=\"customwidth\" id=\"customwidth\" onclick=\"setModel(this,'customwidth');\"" + (ModelInfo["customwidth"].ToString().Replace("1","True") == "True" ? " checked" : "") + " /><label for=\"customwidth\">拖动列宽</label>");
                Response.Write("&nbsp;&nbsp;<label><input type=\"checkbox\" onclick=\"setModel(this,'customshow');\"" + (eBase.parseBool(ModelInfo["customshow"]) ? " checked" : "") + " />列显示隐藏菜单</label>"); //
                //Response.Write("<input type=\"checkbox\" name=\"custommove\" id=\"custommove\" onclick=\"setModel(this,'custommove');\"" + (ModelInfo["custommove"].ToString().Replace("1","True") == "True" ? " checked" : "") + " /><label for=\"custommove\">拖动行位置</label>");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(89);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");

                Response.Write("&nbsp;默认分页大小：<input type=\"text\" value=\"" + ModelInfo["PageSize"].ToString() + "\" oldvalue=\"" + ModelInfo["PageSize"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'pagesize');\" />");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(90);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("&nbsp;默认分页大小(M)：<input type=\"text\" value=\"" + ModelInfo["mPageSize"].ToString() + "\" oldvalue=\"" + ModelInfo["mPageSize"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'mpagesize');\" />");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(91);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("&nbsp;默认行高：<input type=\"text\" value=\"" + ModelInfo["LineHeight"].ToString() + "\" oldvalue=\"" + ModelInfo["LineHeight"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'lineheight');\" />");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(92);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("&nbsp;默认行高(M)：<input type=\"text\" value=\"" + ModelInfo["mLineHeight"].ToString() + "\" oldvalue=\"" + ModelInfo["mLineHeight"].ToString() + "\"  class=\"edit\" style=\"width:40px;\" onBlur=\"setModel(this,'mlineheight');\" />");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(93);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
            }
            //if (ModelInfo.Table.Columns.Contains("mListMode"))
            if (ModelInfo["Type"].ToString() != "11")
            {
                
                Response.Write("&nbsp;列表样式(M)：");
                /*
                Response.Write("<select onchange=\"setModelPropertys(this,'mListMode');\">");//移动端-列表样式
                Response.Write("<option value=\"table\"" + (Propertys.getValue("mListMode").ToLower() == "table" ? " selected=\"true\"" : "") + ">表格</option>");
                Response.Write("<option value=\"panel\"" + (Propertys.getValue("mListMode").ToLower() == "panel" ? " selected=\"true\"" : "") + ">面板</option>");
                //Response.Write("<option value=\"card\"" + (Propertys.getValue("mListMode").ToLower() == "card" ? " selected=\"true\"" : "") + ">卡片</option>");
                Response.Write("</select>");
                */
                Response.Write("<select onchange=\"setModel(this,'mListMode');\">");//移动端-列表样式
                Response.Write("<option value=\"table\"" + (ModelInfo["mListMode"].ToString().ToLower() == "table" ? " selected=\"true\"" : "") + ">表格</option>");
                Response.Write("<option value=\"panel\"" + (ModelInfo["mListMode"].ToString().ToLower() == "panel" ? " selected=\"true\"" : "") + ">面板</option>");
                Response.Write("</select>");

                Response.Write("&nbsp;默认(M)：");
                //Response.Write("<select onchange=\"setModelPropertys(this,'mListOpen');\">");//移动端-默认打开
                //Response.Write("<option value=\"true\"" + (Propertys.getValue("mListOpen") == "true" ? " selected=\"true\"" : "") + ">打开</option>");
                //Response.Write("<option value=\"false\"" + (Propertys.getValue("mListOpen") == "false" ? " selected=\"true\"" : "") + ">关闭</option>");
                Response.Write("<select onchange=\"setModel(this,'mListOpen');\">");//移动端-默认打开
                Response.Write("<option value=\"true\"" + ( ModelInfo["mListOpen"].ToString().ToLower()  == "true" ? " selected=\"true\"" : "") + ">打开</option>");
                Response.Write("<option value=\"false\"" + ( ModelInfo["mListOpen"].ToString().ToLower()  == "false" ? " selected=\"true\"" : "") + ">关闭</option>");
                Response.Write("</select>");
            }

            if (ModelInfo["Type"].ToString() != "11")
            {
                Response.Write("&nbsp;<input type=\"button\" name=\"Submit\" value=\"清除用户定义\" onclick=\"clearCustoms();\" style=\"padding:3px 10px 3px 10px;\" />");
            }
            if (ModelInfo.Table.Columns.Contains("selectMore") && (ModelInfo["type"].ToString() == "1" || ModelInfo["type"].ToString() == "5"))
            {
                Response.Write("&nbsp;多选：<select onchange=\"setModel(this,'selectMore');\">");//合计类型
                Response.Write("<option value=\"0\"" + (ModelInfo["selectMore"].ToString() == "0" ? " selected=\"true\"" : "") + ">关闭</option>");
                Response.Write("<option value=\"1\"" + (ModelInfo["selectMore"].ToString() == "1" ? " selected=\"true\"" : "") + ">打开</option>");
                Response.Write("</select>");
            }

            if (ModelInfo["Type"].ToString() != "10" && ModelInfo["Type"].ToString() != "11" && ModelInfo.Table.Columns.Contains("MergeMenu"))
            {
                Response.Write("&nbsp;<label><input type=\"checkbox\" name=\"mergemenu\" onclick=\"setModel(this,'mergemenu');\"" + (eBase.parseBool( ModelInfo["MergeMenu"]) ? " checked" : "") + " />合并操作菜单</label>");
                Response.Write("&nbsp;<label><input type=\"checkbox\" name=\"mmergemenu\" onclick=\"setModel(this,'mmergemenu');\"" + (eBase.parseBool( ModelInfo["mMergeMenu"]) ? " checked" : "") + " />合并操作菜单(M)</label>");
            }
            if (ModelInfo["type"].ToString() == "1" || ModelInfo["type"].ToString() == "5" || ModelInfo["type"].ToString() == "10")
            {
                Response.Write("<input type=\"checkbox\" name=\"pagesum\" id=\"pagesum\" onclick=\"setModel(this,'pagesum');\"" + (ModelInfo["pagesum"].ToString().Replace("1", "True") == "True" ? " checked" : "") + " /><label for=\"pagesum\">本页计算</label>");
                Response.Write("<select onchange=\"setModel(this,'sumtype');\">");//合计类型
                Response.Write("<option value=\"1\"" + (ModelInfo["SumType"].ToString() == "1" ? " selected=\"true\"" : "") + ">始终计算</option>");
                Response.Write("<option value=\"2\"" + (ModelInfo["SumType"].ToString() == "2" ? " selected=\"true\"" : "") + ">作为条件</option>");
                Response.Write("</select>");
            }
            if (ModelInfo["Type"].ToString() != "11")
            {
                Response.Write("&nbsp;<label><input type=\"checkbox\" name=\"mshowrecordsinfo\" onclick=\"setModel(this,'mshowrecordsinfo');\"" + (eBase.parseBool(ModelInfo["mShowRecordsInfo"]) ? " checked" : "") + " />显示记录数(M)</label>");
                Response.Write("&nbsp;<label><input type=\"checkbox\" name=\"Contrast\" onclick=\"setModel(this,'Contrast');\"" + (eBase.parseBool(ModelInfo["Contrast"]) ? " checked" : "") + " />启用对比</label>");
            }
            if (ModelInfo["Type"].ToString() != "10" && ModelInfo["Type"].ToString() != "11")
            {
                Response.Write("<br>补充自定义列：");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(94);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("<br>\r\n");
                Response.Write("<textarea htmltag=\"true\" name=\"textarea\" style=\"width:90%;\" rows=\"2\"  onBlur=\"setModel(this,'listfields');\" oldvalue=\"" + ModelInfo["ListFields"].ToString().HtmlEncode() + "\">" + ModelInfo["ListFields"].ToString().HtmlEncode() + "</textarea>");
                Response.Write("<br>\r\n");


                Response.Write("联合表：");
                Response.Write("<br>\r\n");
                Response.Write("<textarea htmltag=\"true\" name=\"textarea\" style=\"width:90%;\" rows=\"2\"  onBlur=\"setModel(this,'jointable');\" oldvalue=\"" + HttpUtility.HtmlEncode(ModelInfo["JoinTable"].ToString()) + "\">" + HttpUtility.HtmlEncode(ModelInfo["JoinTable"].ToString()) + "</textarea>");
                Response.Write("<br>\r\n");

                //string value = eBase.DataBase.getValue("select CondValue from a_eke_sysConditions where ModelID='" + modelid + "' and  RoleID is null and UserID is null and delTag=0");           
                //Response.Write("模块条件：<br>");
                //Response.Write("<input type=\"text\" value=\"" + HttpUtility.HtmlEncode(value) + "\" oldvalue=\"" + HttpUtility.HtmlEncode(value) + "\"  class=\"edit\" style=\"width:90%;\" onBlur=\"setModel(this,'modelcondition');\" /><br>");





                Response.Write("基础条件：");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(95);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("<br>\r\n");
                Response.Write("<input type=\"text\" value=\"" + HttpUtility.HtmlEncode(ModelInfo["defaultcondition"].ToString()) + "\" oldvalue=\"" + HttpUtility.HtmlEncode(ModelInfo["defaultcondition"].ToString()) + "\"  class=\"edit\" style=\"width:90%;\" onBlur=\"setModel(this,'defaultcondition');\" />");
                Response.Write("<br>\r\n");
                Response.Write("自定义条件文件路径：");
                Response.Write("<br>\r\n");
                Response.Write("<input type=\"text\" value=\"" + ModelInfo["ConditionFile"].ToString() + "\" oldvalue=\"" + ModelInfo["ConditionFile"].ToString() + "\"  class=\"edit\" style=\"width:300px;\" onBlur=\"setModel(this,'ConditionFile');\" />");
                Response.Write("<br>\r\n");
                Response.Write("默认排序：");
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(96);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("<br>\r\n");
                Response.Write("<input type=\"text\" value=\"" + HttpUtility.HtmlEncode(ModelInfo["defaultorderby"].ToString()) + "\" oldvalue=\"" + HttpUtility.HtmlEncode(ModelInfo["defaultorderby"].ToString()) + "\"  class=\"edit\" style=\"width:90%;\" onBlur=\"setModel(this,'defaultorderby');\" />");
                Response.Write("<br>\r\n");
                Response.Write("<div>");
                Response.Write("&nbsp;操作权限：<br>");
                Response.Write("<textarea reload=\"true\" id=\"actionpower\" jsonformat=\"[{&quot;text&quot;:&quot;动作编码&quot;,&quot;value&quot;:&quot;action&quot;},{&quot;text&quot;:&quot;字段编码&quot;,&quot;value&quot;:&quot;field&quot;},{&quot;text&quot;:&quot;运算符&quot;,&quot;value&quot;:&quot;operator&quot;},{&quot;text&quot;:&quot;值&quot;,&quot;value&quot;:&quot;value&quot;}]\" name=\"textarea\" style=\"width:90%;display:none;\" rows=\"2\"  onBlur=\"setModel(this,'actionpower');\" oldvalue=\"" + HttpUtility.HtmlEncode(ModelInfo["actionPower"].ToString()) + "\">" + HttpUtility.HtmlEncode(ModelInfo["actionPower"].ToString()) + "</textarea>");
                Response.Write("<img src=\"images/jsonedit.png\" style=\"cursor:pointer;margin-right:5px;\" align=\"absmiddle\" onclick=\"Json_OperationPowerEdit('actionpower','操作权限');\">");
                Response.Write(getJsonText(ModelInfo["actionPower"].ToString(), "action"));
                if (eConfig.showHelp()) Response.Write(" <img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(165);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">");
                Response.Write("</div>");
            }
            else
            {
                if (ModelInfo["Type"].ToString() != "11")
                {
                    Response.Write("<br>完整查询SQL语句：<br>");
                    Response.Write("<textarea title=\"别名必须带as关键词\" name=\"textarea\" style=\"width:90%;\" rows=\"4\"  onBlur=\"setModel(this,'listsql');\" oldvalue=\"" + HttpUtility.HtmlEncode(ModelInfo["ListSQL"].ToString()) + "\">" + HttpUtility.HtmlEncode(ModelInfo["ListSQL"].ToString()) + "</textarea><br>\r\n");

                    //Response.Write("&nbsp;主键：<input id=\"inputpk\" type=\"text\" value=\"" + ModelInfo["PrimaryKey"].ToString() + "\" oldvalue=\"" + ModelInfo["PrimaryKey"].ToString() + "\"  class=\"edit\" style=\"width:120px;\" onBlur=\"setModel(this,'primarykey');\" />");
                }
            }
           // eBase.Writeln(ModelInfo["actionPower"].ToString());

            /*
            eList datalist = new eList("a_eke_sysModelItems");
            datalist.Where.Add("ModelID='" + modelid + "' and delTag=0");
            datalist.OrderBy.Add("showlist desc,listorder,px");
            datalist.Bind(Rep);
            */
            list();


            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Rep.RenderControl(htw);
            Rep.Visible = false;//不输出，要在获取后设，不然取不到内容。
            if (ModelInfo["Type"].ToString() != "11") Response.Write(sw.ToString());
            sw.Close();


            Response.Write("<dl class=\"ePanel\" style=\"margin-top:10px;\">\r\n");
            Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>自定义样式</h1></dt>\r\n");
            Response.Write("<dd style=\"display:none;\">");
            Response.Write("<strong>PC端：</strong><br />");
            //Response.Write("<textarea name=\"textarea\" style=\"width:90%;\" rows=\"4\"  onBlur=\"setModelPropertys(this,'CssText');\" oldvalue=\"" + HttpUtility.HtmlEncode(Propertys.getValue("CssText")) + "\">" + HttpUtility.HtmlEncode(Propertys.getValue("CssText")) + "</textarea><br />");
            Response.Write("<textarea htmltag=\"true\" id=\"txtcsstext\" name=\"txtcsstext\" style=\"width:90%;\" rows=\"4\"  on_Blur=\"setModel(this,'CssText');\" oldvalue=\"" + ModelInfo["CssText"].ToString().HtmlEncode() + "\">" + ModelInfo["CssText"].ToString().HtmlEncode() + "</textarea><br />");
            Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtcsstext,'CssText');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" /><br>\r\n");
            Response.Write("<strong>移动端：</strong><br />");
            //Response.Write("<textarea name=\"textarea\" style=\"width:90%;\" rows=\"4\"  onBlur=\"setModelPropertys(this,'mCssText');\" oldvalue=\"" + HttpUtility.HtmlEncode(Propertys.getValue("mCssText")) + "\">" + HttpUtility.HtmlEncode(Propertys.getValue("mCssText")) + "</textarea><br />");
            Response.Write("<textarea htmltag=\"true\" id=\"txtmcsstext\" name=\"txtmcsstext\" style=\"width:90%;\" rows=\"4\"  on_Blur=\"setModel(this,'mCssText');\" oldvalue=\"" + ModelInfo["mCssText"].ToString().HtmlEncode() + "\">" + ModelInfo["mCssText"].ToString().HtmlEncode() + "</textarea><br />");
            Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtmcsstext,'mCssText');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" />\r\n");
            Response.Write("</dd>\r\n");
            Response.Write("</dl>\r\n");
            if (ModelInfo["Type"].ToString() != "11")
            {
                Response.Write("<strong>自定义列表：</strong><br />");
                Response.Write("<dl class=\"ePanel\" style=\"margin-top:10px;\">\r\n");
                Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>PC端</h1></dt>\r\n");
                Response.Write("<dd style=\"display:none;\">");
                Response.Write("<strong>开始标签：</strong><br />");
                //Response.Write("<textarea name=\"textarea\" style=\"width:90%;\" rows=\"2\"  onBlur=\"setModelPropertys(this,'HeaderTemplate');\" oldvalue=\"" + HttpUtility.HtmlEncode(Propertys.getValue("HeaderTemplate")) + "\">" + HttpUtility.HtmlEncode(Propertys.getValue("HeaderTemplate")) + "</textarea><br />");
                Response.Write("<textarea htmltag=\"true\" id=\"txtheadertemplate\" name=\"txtheadertemplate\" style=\"width:90%;\" rows=\"2\"  on_Blur=\"setModel(this,'HeaderTemplate');\" oldvalue=\"" + ModelInfo["HeaderTemplate"].ToString().HtmlEncode() + "\">" + ModelInfo["HeaderTemplate"].ToString().HtmlEncode() + "</textarea><br />");
                Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtheadertemplate,'HeaderTemplate');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" /><br>\r\n");

                Response.Write("<strong>循环标签：</strong><br />");
                //Response.Write("<textarea name=\"textarea\" style=\"width:90%;\" rows=\"4\"  onBlur=\"setModelPropertys(this,'ItemTemplate');\" oldvalue=\"" + HttpUtility.HtmlEncode(Propertys.getValue("ItemTemplate")) + "\">" + HttpUtility.HtmlEncode(Propertys.getValue("ItemTemplate")) + "</textarea><br />");
                Response.Write("<textarea htmltag=\"true\" id=\"txtitemtemplate\" name=\"txtitemtemplate\" style=\"width:90%;\" rows=\"4\"  on_Blur=\"setModel(this,'ItemTemplate');\" oldvalue=\"" + ModelInfo["ItemTemplate"].ToString().HtmlEncode() + "\">" + ModelInfo["ItemTemplate"].ToString().HtmlEncode() + "</textarea><br />");
                Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtitemtemplate,'ItemTemplate');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" /><br>\r\n");

                Response.Write("<strong>结束标签：</strong><br />");
                //Response.Write("<textarea name=\"textarea\" style=\"width:90%;\" rows=\"2\"  onBlur=\"setModelPropertys(this,'FooterTemplate');\" oldvalue=\"" + HttpUtility.HtmlEncode(Propertys.getValue("FooterTemplate")) + "\">" + HttpUtility.HtmlEncode(Propertys.getValue("FooterTemplate")) + "</textarea><br />");
                Response.Write("<textarea htmltag=\"true\" id=\"txtfootertemplate\" name=\"txtfootertemplate\" style=\"width:90%;\" rows=\"2\"  on_Blur=\"setModel(this,'FooterTemplate');\" oldvalue=\"" + ModelInfo["FooterTemplate"].ToString().HtmlEncode() + "\">" + ModelInfo["FooterTemplate"].ToString().HtmlEncode() + "</textarea><br />");
                Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtfootertemplate,'FooterTemplate');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" />\r\n");

                Response.Write("</dd>\r\n");
                Response.Write("</dl>\r\n");

                Response.Write("<dl class=\"ePanel\" style=\"margin-top:10px;\">\r\n");
                Response.Write("<dt><h1 onclick=\"showPanel(this);\"><a href=\"javascript:;\" class=\"cur\" onfocus=\"this.blur();\"></a>移动端</h1></dt>\r\n");
                Response.Write("<dd style=\"display:none;\">");
                Response.Write("<strong>开始标签：</strong><br />");
                //Response.Write("<textarea name=\"textarea\" style=\"width:90%;\" rows=\"2\"  onBlur=\"setModelPropertys(this,'mHeaderTemplate');\" oldvalue=\"" + HttpUtility.HtmlEncode(Propertys.getValue("mHeaderTemplate")) + "\">" + HttpUtility.HtmlEncode(Propertys.getValue("mHeaderTemplate")) + "</textarea><br />");
                Response.Write("<textarea htmltag=\"true\" id=\"txtmheadertemplate\" name=\"txtmheadertemplate\" style=\"width:90%;\" rows=\"2\"  on_Blur=\"setModel(this,'mHeaderTemplate');\" oldvalue=\"" + ModelInfo["mHeaderTemplate"].ToString().HtmlEncode() + "\">" + ModelInfo["mHeaderTemplate"].ToString().HtmlEncode() + "</textarea><br />");
                Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtmheadertemplate,'mHeaderTemplate');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" /><br>\r\n");
                Response.Write("<strong>循环标签：</strong><br />");
                //Response.Write("<textarea name=\"textarea\" style=\"width:90%;\" rows=\"4\"  onBlur=\"setModelPropertys(this,'mItemTemplate');\" oldvalue=\"" + HttpUtility.HtmlEncode(Propertys.getValue("mItemTemplate")) + "\">" + HttpUtility.HtmlEncode(Propertys.getValue("mItemTemplate")) + "</textarea><br />");
                Response.Write("<textarea htmltag=\"true\" id=\"txtmitemtemplate\" name=\"txtmitemtemplate\" style=\"width:90%;\" rows=\"4\"  on_Blur=\"setModel(this,'mItemTemplate');\" oldvalue=\"" + ModelInfo["mItemTemplate"].ToString().HtmlEncode() + "\">" + ModelInfo["mItemTemplate"].ToString().HtmlEncode() + "</textarea><br />");
                Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtmitemtemplate,'mItemTemplate');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" /><br>\r\n");
                Response.Write("<strong>结束标签：</strong><br />");
                //Response.Write("<textarea name=\"textarea\" style=\"width:90%;\" rows=\"2\"  onBlur=\"setModelPropertys(this,'mFooterTemplate');\" oldvalue=\"" + HttpUtility.HtmlEncode(Propertys.getValue("mFooterTemplate")) + "\">" + HttpUtility.HtmlEncode(Propertys.getValue("mFooterTemplate")) + "</textarea><br />");
                Response.Write("<textarea htmltag=\"true\" id=\"txtmfootertemplate\" name=\"txtmfootertemplate\" style=\"width:90%;\" rows=\"2\"  on_Blur=\"setModel(this,'mFooterTemplate');\" oldvalue=\"" + ModelInfo["mFooterTemplate"].ToString().HtmlEncode() + "\">" + ModelInfo["mFooterTemplate"].ToString().HtmlEncode() + "</textarea><br />");
                Response.Write("<input type=\"button\" name=\"Submit\" onclick=\"setModel(txtmfootertemplate,'mFooterTemplate');\" value=\" 保  存 \" style=\"padding:3px 10px 3px 10px;\" />\r\n");
                Response.Write("</dd>\r\n");
                Response.Write("</dl>\r\n");
            }


            Response.End();
        }
        private void list()
        {
            DataTable tb = getItems(modelid);
            appendItems(tb, modelid);
            string pid = eParameters.QueryString("modelid");
            for (int i = tb.Rows.Count - 1; i > -1; i--)
            {
                if (!eBase.parseBool(tb.Rows[i]["showlist"].ToString()))
                {
                    tb.Rows[i]["listorder"] = 999999;
                }
                if (tb.Rows[i]["modelid"].ToString() != pid)
                {
                    if (",addTime,addUser,editTime,editUser,delTime,delUser,delTag,CheckupCode,CheckupText,".ToLower().Contains("," + tb.Rows[i]["Code"].ToString().ToLower() + ","))
                    {
                        tb.Rows.Remove(tb.Rows[i]);
                    }
                }
               
            }
            //eBase.PrintDataTable(tb.Select("modelid,mc,code", "", ""));
            tb = tb.Select("", "showlist desc,listorder,modelpx,mshowlist desc, PX, addTime").toDataTable();
            Rep.DataSource = tb;
            Rep.DataBind();
        }
        private DataTable getItems(string modelid)
        {
            modelpx++;
            return eBase.DataBase.getDataTable("select " + modelpx.ToString() + " as modelpx,b.mc as ModelName,a.* from a_eke_sysModelItems a inner join a_eke_sysModels b on a.modelid=b.modelid where a.ModelID='" + modelid + "' and a.delTag=0");
        }
        private void appendItems(DataTable tb, string modelid)
        {
            DataTable dt = eBase.DataBase.getDataTable("select modelid,mc from a_eke_sysModels where ParentID='" + modelid + "' and JoinMore=0 and JoinType>0 and show=1 and deltag=0");
            foreach (DataRow dr in dt.Rows)
            {
                DataTable tb2 = getItems(dr["modelid"].ToString());
                foreach (DataRow _dr in tb2.Rows)
                {
                    tb.Rows.Add(_dr.ItemArray);
                }
                appendItems(tb, dr["modelid"].ToString());
            }
        }
        
    }
}