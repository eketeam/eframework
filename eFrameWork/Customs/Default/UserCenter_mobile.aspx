﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserCenter_mobile.aspx.cs" Inherits="Customs_Default_UserCenter_mobile" %>
<style>
html,body{ background-color:#F5F5F5;}
</style>
<div class="wapper">
<div class="title">个人中心</div>
</div>

<div class="info">
<img class="face" src="<%=user["face"].ToString().Length==0 ? "images/touch-icon.png" : user["face"].ToString()%>" onerror="this.src='images/touch-icon.png';">
<div class="name"><%=user["name"].ToString() %></div>
<span><img src="images/icon-2.png"><u>部门</u><i><%=row_user["Department"].ToString() %></i></span>
<span><img src="images/icon-1.png"><u>岗位</u><i><%=row_user["zw"].ToString() %></i></span>
</div>

<div class="listitem">
<!--<a href="Custom.aspx?AppItem=0dbdd921-d272-438b-9773-8cb5858bc061" class="user">个人信息</a>-->
<%if (row_info != null)
  { %>
<a href="<%=row_info["href"].ToString() %>" class="user"><%=row_info["ModelName"].ToString() %></a>
<%} %>
<%if(row_pass!=null){ %>
<a href="<%=row_pass["href"].ToString() %>" class="pass"><%=row_pass["ModelName"].ToString() %></a>
<%} %>
<a href="LoginOut.aspx" onClick="javascript:return confirm('确认要退出吗？');" class="out">退出登录</a>
</div>
