﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;
using LitJson;


    public partial class Customs_Default_ModifyPass_mobile : System.Web.UI.Page
    {
        public string UserArea = "Application";
        private eUser user;
        public string AppItem = eParameters.Request("AppItem");
        public eModel model;
        public string appTitle = "";
        eForm eform1;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            user.Check();
            appTitle = eConfig.ApplicationTitle(user["SiteID"].ToString());

            #region 提交表单-修改密码
            if (Request.Form["f1"] != null)
            {
                //以下为框架自带JSON处理类eJson 和 第三方JSON类(LitJson)使用方法。
                //eJson json = new eJson();
                JsonData jd = new JsonData();
                string mm = eBase.UserInfoDB.getValue("select mm from a_eke_sysUsers where  Userid='" + user.ID + "'");
                string encpass = Request.Form["f1"].ToString();
                string pass = eRSA.getPass(encpass);
                if (mm != eBase.getPassWord(pass))
                {
                    //Response.Write("<script>alert('旧密码不正确，修改失败!');document.location='ModifyPass.aspx?AppItem=" + AppItem + "';</script>");
                    //json.Add("success", "0");
                    //json.Add("message", "旧密码不正确,修改失败!");
                    //Response.Write(json.ToString());

                    jd["success"] = "0"; //第一种赋值方式
                    jd.Add("message", "旧密码不正确,修改失败!"); //第二种赋值方式
                    Response.Write(jd.ToJson());
                    Response.End();
                }

                encpass = Request.Form["f2"].ToString();
                pass = eRSA.getPass(encpass);
                string sql = "update a_eke_sysUsers set ";
                sql += "mm='" + eBase.getPassWord(pass) + "'";
                sql += " where Userid='" + user.ID + "'";
                eBase.UserInfoDB.Execute(sql);
                //Response.Write("<script>alert('新密码修改成功,请牢记!');document.location='ModifyPass.aspx?AppItem=" + AppItem + "';</script>");
                //json.Add("success", "1");
                //json.Add("message", "新密码修改成功,请牢记!");
                //Response.Write(json.ToString());

                jd["success"] = "1"; //第一种赋值方式
                jd.Add("message", "新密码修改成功,请牢记!"); //第二种赋值方式
                Response.Write(jd.ToJson());
                Response.End();
            }
            #endregion

        }
    }
