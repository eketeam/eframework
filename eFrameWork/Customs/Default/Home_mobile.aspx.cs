﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

public partial class Customs_Default_Home_mobile : System.Web.UI.Page
{
    public string UserArea = "Application";
    public eModel model;
    public eUser user;       
    protected void Page_Load(object sender, EventArgs e)
    {
        user = new eUser(eBase.getUserArea(UserArea));
        user.Check();
        model = new eModel();
        StringBuilder sb = new StringBuilder();
        sb.Append("<div style=\"margin:10px;padding:8px;line-height:35px;font-size:14px;background-color:#fff;\">");
        sb.Append("欢迎登录" + eConfig.ApplicationTitle(user["SiteID"].ToString()) + "!<br>");
        sb.Append("姓名：" + user["Name"] + "<br>");
        sb.Append("用户ID：" + user.ID + "<br>");
        sb.Append("用户SiteID：" + user["SiteID"] + "<br>");
        sb.Append("用户OrgCode：" + user["OrgCode"] + "<br>");
        sb.Append("用户UserCode：" + user["UserCode"] + "<br>");
        sb.Append("用户WebCode：" + user["WebCode"] + "<br>");

        DataTable tb = eBase.UserInfoDB.getDataTable("select LoginCount,LastLoginTime from a_eke_sysUsers where UserID='" + user["ID"].ToString() + "'");
        if (tb.Rows.Count > 0)
        {
            string logincount = tb.Rows[0]["LoginCount"].ToString();
            string lastlt = string.Format("{0:yyyy-MM-dd HH:mm:ss}", tb.Rows[0]["LastLoginTime"]);

            sb.Append("登录次数：" + logincount + "<br>");
            sb.Append("上次登录时间：" + lastlt + "<br>");
        }
        sb.Append("</div>");

        litBody.Text = sb.ToString();
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Master == null) return;
    }
}