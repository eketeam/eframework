﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.IO;
using System.Net;
using System.Text;
using LitJson;


namespace eFrameWork.Customs.Base
{
    public partial class SwitchUser : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public string act = eParameters.Request("act");
        public string AppItem = eParameters.QueryString("AppItem");
        public eUser user;
        public eModel model;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(eBase.getUserArea(UserArea));
            user.Check();
            eModelInfo customModel = new eModelInfo(user);
            model = customModel.Model;

            string sql = "select * from a_eke_sysUsers where deltag=0 and Active=1 and (UserID='" + user["userpid"].ToString() + "' or ParentID='" + user["userpid"].ToString() + "')";
            DataTable tb = eBase.UserInfoDB.getDataTable(sql);
            //eBase.PrintDataTable(tb);
            if (act == "switch")
            {
                if (Request.UrlReferrer == null) return;
                if (Request.Url.Host.ToLower() != Request.UrlReferrer.Host.ToLower()) return;
                if (!eConfig.portMapping && Request.Url.Port != Request.UrlReferrer.Port) return;

                string id = eParameters.QueryString("id");
                DataRow[] rows = tb.Select("UserID='" + id + "'");
                if (rows.Length > 0)
                {
                    eUser nuser = new eUser(eBase.getUserArea(UserArea));
                    nuser["userpid"] = user["userpid"];
                    if (user["app"].ToString().Length > 0) nuser["app"] = user["app"];
                    if (user["iphoneapp"].ToString().Length > 0) nuser["iphoneapp"] = user["iphoneapp"];
                    eFHelper.saveUserInfo(nuser, rows[0]); //保存用户登录信息
                    eBase.Write("<script>top.document.location.href='" + eRunTime.absPath + "';</script>");
                }
                eBase.End();
            }
            if (tb.Rows.Count == 0)
            {
                litBody.Text = "<div style=\"margin:50px;color:#666;text-align:center;\">您暂时还没有多帐号， 不可切换!</div>";
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" width=\"99%\" class=\"eDataTable\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr bgcolor=\"#F2F2F2\">\r\n");

                sb.Append("<td  height=\"25\">序号</td>\r\n");
                sb.Append("<td>昵称</td>\r\n");
                sb.Append("<td>部门</td>\r\n");
                sb.Append("<td>操作</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>\r\n");
            for(int i=0;i<tb.Rows.Count;i++)
            {
                DataRow dr = tb.Rows[i];
                sb.Append("<tr bgcolor=\"#FFFFFF\">\r\n");
                sb.Append("<td height=\"35\">" + (1+i).ToString() + "</td>\r\n");
                sb.Append("<td>" + dr["Nick"].ToString() + "</td>\r\n");
                sb.Append("<td>" + dr["Department"].ToString() + "</td>\r\n");
                sb.Append("<td>" + "<a href=\"?appitem="+ AppItem+"&act=switch&id=" + dr["UserID"].ToString() + "\">切换</a>" + "</td>\r\n");
                sb.Append("</tr>\r\n");
            }
            sb.Append("</tbody>\r\n");
            sb.Append("</table>\r\n");


            litBody.Text = sb.ToString();

        }
    }
}