﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckUp.aspx.cs" Inherits="eFrameWork.Customs.Base.CheckUp" %>
<div style="margin:10px;">
  <form id="frmCheckUp" name="frmCheckUp" method="post" action="<%=EKETEAM.FrameWork.eBase.getAbsolutePath() + "Customs/base/CheckUp.aspx?UserArea=" + UserArea + (AppItem.Length>0 ? "&AppItem=" + AppItem : "&modelid=" + modelid) + "&id=" + id %>">
     <input name="act" type="hidden" id="act" value="save" />
  <%if (cur["showState"].ToString() != "True" && cur["showIdea"].ToString() != "True" && backCount == 0 && cur["CheckMC"].ToString() == "提交")
    {%>
      <div style="color:#666;">确认要提交审核吗？</div>
  <%}
    else
    {%>
 <%if (cur["showState"].ToString() == "1" || cur["showIdea"].ToString() != "0")
   {%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="eDataView">
<colgroup>
<col width="120">
</colgroup>
 <%if (cur["showState"].ToString() == "1")
   {%>
  <tr>
    <td class="title"><ins>*</ins>审核状态：</td>
    <td class="content"><span class="eform">
     <input type="radio" name="f1" id="f11" value="1" notnull="true" fieldname="审核状态" onclick="Check_CheckUp();" /><label for="f11">通过</label>
	 <input type="radio" name="f1" id="f12" value="2" notnull="true" fieldname="审核状态" onclick="Check_CheckUp();" /><label for="f12">不通过</label>
     <input type="radio" name="f1" id="f13" value="3" notnull="true" fieldname="审核状态" onclick="Check_CheckUp();" /><label for="f13">作废</label>

    </span></td>
  </tr>
  <%}%>
 <%if (cur["showIdea"].ToString() == "1")
   {%>
  <tr>
    <td class="title">审核意见：</td>
    <td class="content"><span class="eform"><textarea name="f2" id="f2" style="height:80px;"></textarea></span></td>
  </tr>
<%}%>
 <%if (cur["showIdea"].ToString() == "2")
   {%>
  <tr>
    <td class="title">审核意见：</td>
    <td class="content">
        <canvas id="esignin" class="esignin">浏览器不支持签名!</canvas>
        <div style="margin-top:8px;">
          <a class="button" href="javascript:;" onclick="eSignIn.clear();"><span style="letter-spacing:1px;"><i class="edit">重签</i></span></a>
        </div>
    </td>
  </tr>
<%}%>

  <%if (backCount > 0)
    {%>
   <tr id="checkup_trback" style="display:none;">
    <td class="title">退回到：</td>
    <td class="content">
	<span class="eform">
	<select name="f3" id="f3" fieldname="退回流程" notnull="false">
	<%=options%>
    </select>
	 </span>
    </td>
  </tr>
  <%}
    }%>
</table>
<%}%>
 </form>
</div>