﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;

public partial class Customs_PartOne : System.Web.UI.Page
{
    public string UserArea = "Application";
    public eModel model;
    public eUser user;
    public eModelInfo modelInfo;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        user = new eUser(eBase.getUserArea(UserArea));
        modelInfo = new eModelInfo(user);
        modelInfo.Add(subModel1);       
        modelInfo.eForm.onChange += eForm_onChange;
        modelInfo.Handle();
    }

    private void eForm_onChange(object sender, eFormTableEventArgs e)
    {
        switch (e.eventType)
        {
            case eFormTableEventType.Updating:
                modelInfo.eForm.Fields.Add("info", modelInfo.eForm.Controls["mx_F1"].Value.ToString() + "AA");
                break;
            case eFormTableEventType.Inserting:
                modelInfo.eForm.Fields.Add("info", "XX");
                break;
        }
    }
}