﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportControl.aspx.cs" Inherits="eFrameWork.Customs.ReportControl" %>
<div class="nav">您当前位置：<a href="<%=eBase.getApplicationHomeURL() %>" target="_top">首页</a> -> <%=model.ModelInfo["MC"].ToString()%></div>
<div style="margin:10px;">
 <div class="title">一维数据报表(固定值)。同一报表数据，输出不同图表类型</div>
<ev:eReportControl ID="eReportControl1" ReportID="728fe47c-5d71-4beb-bf78-3d84b8ab3f68" ControlType="pie" Remark="" Introduce="显示为饼图" runat="server" />
<ev:eReportControl ID="eReportControl2" ReportID="728fe47c-5d71-4beb-bf78-3d84b8ab3f68" ControlType="donut" Remark=""  Introduce="显示为环图" runat="server" />
<ev:eReportControl ID="eReportControl3" ReportID="728fe47c-5d71-4beb-bf78-3d84b8ab3f68" ControlType="rose" Remark="" Introduce="显示为玫瑰图" runat="server" />
    <!--
<ev:eReportControl ID="eReportControl4" ReportID="728fe47c-5d71-4beb-bf78-3d84b8ab3f68" ControlType="table" Remark="" Introduce="显示为表格" runat="server" />
    -->
    <div class="clear"></div>

 <div class="title">二维数据报表(固定值)。同一报表数据，输出不同图表类型</div>
    <ev:eReportControl ID="eReportControl5" ReportID="ab8c90d8-be41-4922-8bda-f98483854f43" Width="33.33333333%" ControlType="line" Introduce="显示为拆线图" runat="server" />    
    <ev:eReportControl ID="eReportControl6" ReportID="ab8c90d8-be41-4922-8bda-f98483854f43" Width="33.33333333%" ControlType="column" Introduce="显示为柱状图" runat="server" />
    <ev:eReportControl ID="eReportControl7" ReportID="ab8c90d8-be41-4922-8bda-f98483854f43" Width="33.33333333%" ControlType="bar" Introduce="显示为条形图" runat="server" />
    <!--
    <ev:eReportControl ID="eReportControl8" ReportID="ab8c90d8-be41-4922-8bda-f98483854f43" Width="33.33333333%" ControlType="area" Introduce="显示为面积图" runat="server" />
    <ev:eReportControl ID="eReportControl9" ReportID="ab8c90d8-be41-4922-8bda-f98483854f43" Width="33.33333333%" ControlType="table" Introduce="显示为表格" runat="server" />
      -->
      <div class="clear"></div>
 <div class="title">动态报表</div>
<ev:eReportControl ID="eReportControl10" ReportID="4eb66919-7014-4827-9efd-69225d7b6c34" ControlType="pie" Remark="" Introduce="显示为饼图" runat="server" />
<ev:eReportControl ID="eReportControl11" ReportID="67d08ca7-9eb9-4a70-a442-b00c67d3663a" ControlType="column" Remark="" Introduce="显示为柱状图" runat="server" />

<ev:eReportControl ID="eReportControl12" ReportID="6c0aabbb-a354-4ac8-945a-b2db702f915c"  Introduce="显示为柱状图" runat="server" />

</div>