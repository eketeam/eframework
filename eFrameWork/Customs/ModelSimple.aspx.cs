﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;

namespace eFrameWork.Customs
{
    public partial class ModelSimple : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public string ModelID = eParameters.Request("modelid");
        public string AppItem = eParameters.Request("AppItem");
        public eModel model;
        public eList elist;
        public eForm eform; 
        public eAction Action;
        public eUser user;       

        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(eBase.getUserArea(UserArea));
            eModelInfo customModel = new eModelInfo(user);
            model = customModel.Model;
            ModelID = model.ModelID;

            Action = new eAction();
            //Action.ModelID = ModelID; 
            Action.Actioning += new eActionHandler(Action_Actioning);
            Action.Listen();
        }
        private void List()
        {
            eDataTable.Power = model.Power; //应用模块权限
            elist = new eList("Demo_Persons");
            elist.Where.Add("delTag=0");
            elist.Where.Add("SiteID='" + user["siteid"] + "'");
            string userCond = eParameters.Replace(model.UserCondition, null, user);//替换用户、URL等参数
            elist.Where.Add(userCond);//用户条件
            elist.Where.Add(s1); //按姓名查询
            elist.OrderBy.Default = "addTime desc";//默认排序
            elist.Bind(eDataTable, ePageControl1);
        }
        protected void Action_Actioning(string Actioning)
        {
            eform = new eForm("Demo_Persons", user);
            eform.ModelID = ModelID.Replace("-","_");
            switch (Actioning)
            {
                case "":
                    List();
                    break;
                default:
                    eform.AddControl(eFormControlGroup);
                    eform.onChange += new eFormTableEventHandler(eform_onChange);
                    eform.Handle();
                    break;
            }
        }
        public void eform_onChange(object sender, eFormTableEventArgs e)
        {
            if (e.eventType == eFormTableEventType.Inserting)
            {
                if (user["SiteID"].Length > 0) eform.Fields.Add("SiteID", user["SiteID"]);
            }
        }
    }
}