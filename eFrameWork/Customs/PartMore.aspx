﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PartMore.aspx.cs" Inherits="Customs_PartMore" %>
<ev:eCustomModel ID="subModel1" subModelID="1234" subModelName="人员信息" TableName="Demo_Customs_PartMore" Width="500" Height="400" runat="server">
<ev:eCustomForm  runat="server">
<table width="320" border="0" cellpadding="0" cellspacing="0" class="eDataView" style="margin:10px;">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title"><%=(modelInfo.Action.ToLower() == "add" ? "<ins>*</ins>" : "") %>姓名：</td>
<td class="content"><span id="span_mx_F1" class="eform"><ev:eFormControl ID="mx_F1" Name="mx_F1" ControlType="text" Field="XM" FieldName="姓名" NotNull="True" MaxLength="50" runat="server" /></span></td>
</tr>
<tr>
<td class="title">性别：</td>
<td class="content"><span id="span_mx_F2" class="eform"><ev:eFormControl ID="mx_F2" Name="mx_F2" ControlType="select" Field="XB" FieldName="性别" NotNull="False" BindAuto="True" BindObject="Dictionaries" BindText="MC" BindValue="MC" BindOrderBy="PX,AddTime" BindCondition="delTag=0 and ParentID='41cbbf18-cc6e-4f38-8375-48f2e8a7452a'" runat="server" /></span></td>
</tr>
<tr>
<td class="title">电话：</td>
<td class="content"><span id="span_mx_F3" class="eform"><ev:eFormControl ID="mx_F3" Name="mx_F3" ControlType="text" Field="DH" FieldName="电话" NotNull="False" MaxLength="50" runat="server" /></span></td>
</tr>
</table>
</ev:eCustomForm>
<ev:eCustomList runat="server">
<ev:eListControl ID="eDataTable" LineHeight="40" runat="server" >
<ev:eListColumn Name="mx_F1" FieldName="姓名" Field="XM" runat="server" />
<ev:eListColumn Name="mx_F2" FieldName="性别" Field="XB" runat="server" />
<ev:eListColumn Name="mx_F3" FieldName="电话" Field="DH" runat="server" />
</ev:eListControl>
</ev:eCustomList>
</ev:eCustomModel>