﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.Customs
{
    public partial class ReportControl : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public eUser user;
        public eModel model;
        public string ModelID = eParameters.Request("modelid");
        public string AppItem = eParameters.Request("AppItem");
        protected void Page_Load(object sender, EventArgs e)
        {

            user = new eUser(eBase.getUserArea(UserArea));
            eModelInfo customModel = new eModelInfo(user);
            model = customModel.Model;

            eReportControl10.User = user;
            eReportControl11.User = user;
      
        }
    }
}