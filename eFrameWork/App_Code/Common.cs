﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using System.Text;


public class Common
{
    public static string Version
    {
        get
        {
            return eConfig.Version();
        }
    }
    public static string Ver()
    {
        return eConfig.Version();
    }
    public static string LoadJS
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            if (eConfig.getItem("editorjs") == "1")
            {
                if (eConfig.getItem("htmlEditor") == "ueditor")
                {
                    sb.Append("<script src=\"../Plugins/ueditor1.4.3.3/ueditor.config.js\"></script>\r\n");
                    sb.Append("<script src=\"../Plugins/ueditor1.4.3.3/ueditor.all.min.js\"></script>\r\n");
                    sb.Append("<script src=\"../Plugins/ueditor1.4.3.3/lang/zh-cn/zh-cn.js\"></script>\r\n");
                }
                else
                {
                    sb.Append("<script src=\"../Plugins/kindeditor351/kindeditor.js\"></script>\r\n");
                }
            }
            if (eConfig.getItem("acejs") == "1")
            {
                sb.Append("<script src=\"../Plugins/ace/ace.js\"></script>\r\n");
                sb.Append("<script src=\"../Plugins/ace/ext-language_tools.js\"></script>\r\n");
            }
            if (eConfig.getItem("antvjs") == "1")
            {
                sb.Append("<script src=\"../Plugins/AntV/g2.min.js\"></script>\r\n");
                sb.Append("<script src=\"../Plugins/AntV/data-set.min.js\"></script>\r\n");
            }           
            return sb.ToString();
        }
    }
}